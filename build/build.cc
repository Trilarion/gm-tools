/* build.cc - build tool for GM Tools
 * 
 * C Free Software Foundation 2004, 2005
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */
 
#include <stdio.h>
#include <string.h>
#include <dirent.h> // directory operations
#include <unistd.h> 
#include <sys/stat.h> // file stats
#include <sys/types.h> // stat structure
 
#include "dandefs.h"
#include "err_log.h"
#include "danio.h"
#include "sat.h"
#include "code.h"

// types & globals

char *base_dir;

code *code_list; // a list of sets of code files.


// function declarations

void cloze();
// end the program safely





//
//
// ******************************* PROCESSING ***********************
//
//



void read_source(FILE *in, char *file_name)
{
  char line[MAX_INPUT];
  int i; // counter
  int quote; // number of quotes (") encountered
  
  // read the file
  while (fgets(line, MAX_INPUT, in) != NULL)
  {
    if (!strncmp(line, "#include", 8)) // line begins with "#include"
    {
//      dwrite("Found include:\n");
      quote = 0;
      for (i = 8; line[i] != '\0' && line[i] != '\n' && line[i] != '\r'; i++)
      {
        if (line[i] == ' ')
          continue; // skip non-filename characters
        if (line[i] == '<')
        {
          dwrite("(from library: ");
          continue;
        }
        if (line[i] == '"') 
        {  
          quote++;  // count the quotes, looking for the second
          if (quote >=2)
            break;
          else
            continue;
        }//fi: quote (")    
        if (line[i] == '>') 
        {
          dwrite(")");
          break; // done with this line: exit the for loop.
        }  
        dwrite("%c", line[i]);
      }  
      dwrite (" ");
//      dwrite("\n");
    }//fi
    
    if (!strncmp(line, "int main(", 9))
      dwrite("\n  (%s contains a main function.)", file_name);
  }//while
}//read source

//
//
// ******************************* FILE MANIPULATION ***********************
//
//


void open_data(FILE*& in, const char *file_name)
{
  // open input file
  log_event(logDEFAULT, "Opening %s for input.\n", file_name);
  
  if ((in = fopen(file_name, "rt"))
      == NULL)
  {
      log_event(logERROR, "Cannot open input file: %s\n", file_name);
      dwrite("Fatal error - cannot open input file: %s\n", file_name);
      cloze();
      exit(errINVALID);
  }//fi
  log_event("File %s opened for input.\n", file_name);
}// open data

void show_file(FILE *in)
//output the contents of the file
{
  char c; // for character at a time input
  
  while (true) // mid tested loop for displaying file
  {
    c = fgetc(in);
    if (c == EOF) continue;
    if (c != 13) dwrite("%c", c);
  }// while
}//show_file

//
//
// ******************************* Start / End *****************************
//
//

void init()
{
  init_danio();
  start_log(logDEFAULT);
}//init

void setup()
{
  // find the base directory
  base_dir = new_str(MAX_INPUT);
  getcwd(base_dir, MAX_INPUT);
  log_event(logDEFAULT, "Base dir: [%s]\n", base_dir);
  
  // setup the dummy header for the code_list
  code_list = new code(DUMMY_HEADER_NAME);
  
}//setup

void intro()
{
  dwrite("Build tool for GM Tools C 2005.\n");
}//intro

/*
void check_useage(int argc)
{
  if (argc != REQUIRED_ARGS) 
  {
    dwrite("Useage: build <filename>\n");
    dwrite("Helps determine build dependencies.");
    log_event(logERROR, "Useage error - invalid number of arguments\n");
    cloze();
    exit(errUSEAGE);
  }//fi
}// check_useage
*/

void cloze()
// end the program
{
  dwrite("Press any key to end.\n");
  dgetch();
  end_log();
  close_danio();
}//cloze

//
//
// ******************************* MAIN *****************************
//
//



int main(int argc, char **argv)
{
  FILE *source;

  init(); // generic initializations
  setup(); // specific initializations
  intro(); // introductory message

  /* print files in current directory in reverse order */
  struct dirent **namelist;
  int n;
  struct stat buf;

  n = scandir(".", &namelist, 0, alphasort);
  if (n < 0)
    perror("scandir");
  else 
  {
      while(n--) 
      {
        stat(namelist[n]->d_name, &buf); // get file information
        if (S_ISDIR(buf.st_mode))        // test to see if it is a directory
          dwrite("%s is a directory.", namelist[n]->d_name);
        else
        {  
          dwrite("%s : ", namelist[n]->d_name);
          if (iscode(namelist[n]->d_name))
          {
            code_list->add_file(namelist[n]->d_name);
            open_data(source, namelist[n]->d_name);
            read_source(source, namelist[n]->d_name); // read source code and output included file names and if it contains a main function.
            fclose(source);
            log_event("File [%s] closed.\n", namelist[n]->d_name);
          } else
            dwrite("is not code.");
        }//if/else
        free(namelist[n]);
        dwrite("\n");
      }//while
      free(namelist);
  }//if/else

  dwrite("Code list:\n");
  code_list->show();
    
  cloze();
  return errNO_ERROR;
}//main

/* code.h - code block object for build tool for GM Tools
 * 
 * C Free Software Foundation 2004, 2005
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */
 
#include <stdio.h>
#include <string.h>
#include <dirent.h> // directory operations
#include <unistd.h> 
#include <sys/stat.h> // file stats
#include <sys/types.h> // stat structure
 
#include "dandefs.h"
#include "err_log.h"
#include "danio.h"
#include "sat.h"
#include "code.h"



//
//
// ************************************** NON OOP FUNCTIONS ******************************
//
//

bool iscode(char *file_name)
// determine if a file contains code from the file suffix
{
  char *p = file_name; // pointer into the file name
  int i; // counter
  
  log_event(logDEFAULT, "iscode() - file_name [%s] ", file_name);
  
  while (*p != '.')
  {
    if (*p == '\0') // end of file name
    {
      log_event(logDEFAULT, "has no suffix.\n");
      return false; // no code suffix found (no suffix)
    }
    p++;  
  }//while
  
  p++; // move past '.'
  log_event(logDEFAULT, "suffix is [%s]\n", p); // record the suffix for debugging.
  for (i = 0; CODE_SUFFIX[i][0] != '\0'; i++)
  {
    if (!strcmp(p, CODE_SUFFIX[i])) // match in CODE_SUFFIX, i.e. file_name is code
      return true;
  }
  return false; // matched no suffix.
}//is code


//
//
// *************************** CODE OBJECT MEMBER FUNCTIONS *****************************
//
//

code::code(const char *file_name)
// basic initialisation of a code block descriptor
{
  base_name = new_str(file_name); // stub
  header = NULL;
  source = NULL;
  dependency_list = NULL;
  is_main = false;
  next = NULL;
}// code constructor

int code::add_code(const char *file_name)
// add a new file to the code list
{
  code *c, *p; //tempory code handles c for the new code, p to look at the code list
  
  c = new code(file_name);
  
  log_event(logDEFAULT, "%s added to code list.\n", file_name);
  
  p = this;
  while (p->next != NULL)
    p = p->next;
  p->next = c;
  
  return errNO_ERROR;
}//add code

int code::add_file(const char *file_name)
// add a (possibly) new file to the code list  
{
  log_event(logDEFAULT, "add_file() - attempting to add [%s] to code list.\n", file_name);
  return errNO_ERROR;
}// add file

void code::show()
{
  code *p;
  
  p = this;
  if (!strcmp(base_name, DUMMY_HEADER_NAME)) 
    p = p->next;
  while (p != NULL)
  {
    if (base_name != NULL)
      dwrite("Base Name: [%s]\n", p->base_name);
    else  
      dwrite("Base Name: [NULL]\n");
    p = p->next;
  }//while  
}//show

/* code.h - code block object for build tool for GM Tools
 * 
 * C Free Software Foundation 2004, 2005
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */
 
#ifndef CODE_H
#define CODE_H 
 
#include <stdio.h>
#include <string.h>
#include <dirent.h> // directory operations
#include <unistd.h> 
#include <sys/stat.h> // file stats
#include <sys/types.h> // stat structure
 
#include "dandefs.h"
#include "err_log.h"
#include "danio.h"
#include "sat.h"


// constants
const char CODE_SUFFIX[][10] = {"cc\0", "cpp\0", "c\0", "h\0", "\0"};
const char SOURCE_SUFFIX[][10] = {"cc\0", "cpp\0", "c\0", "\0"};
const char HEADER_SUFFIX[][10] = {"h\0", "\0"};

const char DUMMY_HEADER_NAME[] = "dummy header";

// file types
const int NOT_CODE = 0;
const int SOURCE = 1;
const int HEADER = 2;



int iscode(char *file_name);
// determines if a file contains code from the file suffix. Returns an indication of file type, see file types above.


class dependency
{
public:
  char *file_name;
  dependency *next;
};

class code // a set of code files used to create an object file
{
public:
  code(const char *file_name);
  int add_code(const char *file_name); // add a new file to the code list
//  add_dependency(char *file_name)
  void show(); // display details of the code chunk
//  bool isknown(const char *file_name);
    // test to see if the file is allready in the code list.
  int add_file(const char *file_name);
    // add a (possibly) new file to the code list  
    
  
  char *base_name;
  char *header; // with path
  char *source; //
  dependency *dependency_list;
  bool is_main;
  code *next;
}; // code

#endif


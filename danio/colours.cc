/* colours.cc - colour matrix generator.
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */
 
/*
 * This program is a test for the pallet functions in danio and provides a means
 * to find colour values, which should be deffined in pallet.h for each danio
 * implementation.
 */ 

#include <stdlib.h>

#include "danio.h"

using namespace std;

const int MAX_ROW = 16;
const int MAX_COL = 16;

int main(int argc, char *argv[])
{
  int row, col;
  
  init_danio();
  dwrite("                        Colour List.\n\n");
  
  dwrite("    ");
  for (col = 0; col < MAX_COL; col++)
  {
    dwrite("%3x ", col);
  }//for
  dwrite("\n");
  
  for (row = 0; row < MAX_ROW; row++)
  {
    setpcolour(7);
    dwrite("%2x  ", row);
    for (col = 0; col < MAX_COL; col++)
    {
      setpcolour(row*16+col);
      dwrite("  A ");
    }//for
    dwrite("\n");
  }//for
  
  dgetch();
  close_danio();
  
  return 0;
}//main

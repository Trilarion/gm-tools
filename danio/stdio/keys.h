/* keys.h - list of mnemonic key definitions
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires see http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */
// NOTE the key definitions must change with varios io interfaces.
     
#ifndef KEYS_H

#define KEY_ESC 27 
#define KEY_F1 265
#define KEY_F2 266 
#define KEY_F3 267 
#define KEY_F4 268 
#define KEY_F5 269 
#define KEY_F6 270 
#define KEY_F7 271 
#define KEY_F8 272 
#define KEY_F9 273 
#define KEY_F10 274
#define KEY_F11 275 
#define KEY_F12 276 
	
#define KEY_ENTER 13
		    
#define KEY_CTRL_A 1
#define KEY_CTRL_B 2 
#define KEY_CTRL_C 3 
#define KEY_CTRL_D 4 
#define KEY_CTRL_E 5 
#define KEY_CTRL_F 6 
#define KEY_CTRL_G 7 
#define KEY_CTRL_H 8 
#define KEY_CTRL_I 9 
#define KEY_CTRL_J 10 
#define KEY_CTRL_K 11 
#define KEY_CTRL_L 12 
#define KEY_CTRL_M 13 
#define KEY_CTRL_N 14 
#define KEY_CTRL_O 15
#define KEY_CTRL_P 16
#define KEY_CTRL_Q 17
#define KEY_CTRL_R 18 
#define KEY_CTRL_S 19 
#define KEY_CTRL_T 20 
#define KEY_CTRL_U 21 
#define KEY_CTRL_V 22 
#define KEY_CTRL_W 23 
#define KEY_CTRL_X 24 
#define KEY_CTRL_Y 25 
#define KEY_CTRL_Z 26 
		     
		     
#define KEY_DOWN_ARROW 258
#define KEY_UP_ARROW 259
#define KEY_LEFT_ARROW 260
#define KEY_RIGHT_ARROW 261
			    
#define KEY_PGDN 338
#define KEY_PGUP 339
#define KEY_END 360
#define KEY_HOME 262
#define KEY_INS 331
#define KEY_DEL 330
	
#define KEY_BACKSPACE 263
#define KEY_MINUS 45
#define KEY_PLUS 43
	
	
#endif // keys.h

/* stub.cc - regression test program for danio rutines
 * 
 * C Free Software Foundation 2004 2005
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

#include "danio.h"
#include "dandefs.h"
#include "err_log.h"

void va_test(char *s, ...)
  // test pasing a va_list to dvwrite
{
  va_list ap;

  va_start(ap, s);
  dvwrite(s, ap);
  va_end(ap);
}//va_test

int main()
{
  // Start up stuff.
  start_log();
  init_danio();

  // tests - basic to all modes - character input and output.
  dwrite("Testing dwrite - text output.\n");
  drefresh(); // flush to screen

  dwrite("Testing dwrite with parameters. This should be 2-->%d\n", 2);
  dwrite("Testing dwrite with parameters. This should be \"Hello\"-->\"%s\"\n", "Hello");
  drefresh();

  dwrite("Testing input. Press any alphanumeric key.\n");
  drefresh();
  int testinput = dgetch();
  dwrite("Key pressed was-->%c\n", testinput);
  dwrite("With numeric value--->%d\n", testinput);
  drefresh();

  dwrite("Testing passing a va_list to dvwrite.\n");
  va_test("This should be 2-->%d\n", 2);
  drefresh();

  // tests - available in some modes - screen clearing, motion, colour.


  // tests - available in a few modes - windowing, drawing


  dwrite("Testing complete, press any key to finish.");
  drefresh();
  dgetch();

  // tidy up
  close_danio();
  end_log();

  return 0;
} // end main


// end - stub.cc


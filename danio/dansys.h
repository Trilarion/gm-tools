/*  dansys.h - danio system interface
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires see http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */
// interface layer for system specific, non IO calls (time; random numbers)

#ifndef DANSYS_H
#define DANSYS_H



#if unix
#define d_random(x) (1+(int) ((float) (x)*random()/(RAND_MAX + 1.0)))
#else  // for some reason random() is called rand() in windows
#define d_random(x) (1+(int) ((float) (x)*rand()/(RAND_MAX + 1.0)))
#endif
// generate a random number from 1 to x

void randomize();
  // initilise random number generater with seed from the clock



#endif


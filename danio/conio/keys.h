/*  keys.h - list of mnemonic key definitions
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires see http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */
// NOTE the key definitions change with varios io interfaces.
     
#ifndef danioKEYS_H

#define danioKEY_ESC 27 
#define danioKEY_F1 314
#define danioKEY_F2 315 
#define danioKEY_F3 316
#define danioKEY_F4 317
#define danioKEY_F5 318
#define danioKEY_F6 319
#define danioKEY_F7 320
#define danioKEY_F8 321
#define danioKEY_F9 322
#define danioKEY_F10 323
#define danioKEY_F11 645
#define danioKEY_F12 646
	
#define danioKEY_ENTER 13
		    
#define danioKEY_CTRL_A 1
#define danioKEY_CTRL_B 2 
#define danioKEY_CTRL_C 3 
#define danioKEY_CTRL_D 4 
#define danioKEY_CTRL_E 5 
#define danioKEY_CTRL_F 6 
#define danioKEY_CTRL_G 7 
#define danioKEY_CTRL_H 8 
#define danioKEY_CTRL_I 9 
#define danioKEY_CTRL_J 10 
#define danioKEY_CTRL_K 11 
#define danioKEY_CTRL_L 12 
#define danioKEY_CTRL_M 13 
#define danioKEY_CTRL_N 14 
#define danioKEY_CTRL_O 15
#define danioKEY_CTRL_P 16
#define danioKEY_CTRL_Q 17
#define danioKEY_CTRL_R 18 
#define danioKEY_CTRL_S 19 
#define danioKEY_CTRL_T 20 
#define danioKEY_CTRL_U 21 
#define danioKEY_CTRL_V 22 
#define danioKEY_CTRL_W 23 
#define danioKEY_CTRL_X 24 
#define danioKEY_CTRL_Y 25 
#define danioKEY_CTRL_Z 26 
		     
		     
#define danioKEY_DOWN_ARROW 592
#define danioKEY_UP_ARROW 584
#define danioKEY_LEFT_ARROW 587
#define danioKEY_RIGHT_ARROW 589
			    
#define danioKEY_PGDN 593
#define danioKEY_PGUP 585
#define danioKEY_END 591
#define danioKEY_HOME 583
#define danioKEY_INS 594
#define danioKEY_DEL 595
	
#define danioKEY_BACKSPACE 8
#define danioKEY_MINUS 45
#define danioKEY_PLUS 43
#define danioKEY_TAB 9	
	
#endif // keys.h

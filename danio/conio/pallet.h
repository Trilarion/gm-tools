/*  pallet.h - colour mnemonics for danio/conio
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires see http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

#ifndef PALLET_H
#define PALLET_H

// pcol: pallet colour
// bkg: background
// fg: forground

const int BACKGROUND =   0;   

// colours with black background
const int pcolBLACK = 0;
const int pcolBLUE = 1;
const int pcolGREEN = 2;
const int pcolCYAN = 3;
const int pcolRED = 4;
const int pcolMAGENTA = 5;
const int pcolBROWN = 6;
const int pcolLIGHTGRAY = 7;
const int pcolDARKGRAY = 8;
const int pcolLIGHTBLUE = 9;
const int pcolLIGHTGREEN = 10;
const int pcolLIGHTCYAN = 11;
const int pcolLIGHTRED = 12;
const int pcolLIGHTMAGENTA = 13;
const int pcolYELLOW = 14;
const int pcolWHITE = 15;

// and (&) to a colour to make lighter / brighter    
// (only works with dark colours)
const int BOLD =         8;  


// const int X_ON_Y = pcolX & (pcolY * 16);
const int pcolWHITE_ON_BLUE = pcolWHITE & (pcolBLUE * 16); // = 0x1f
const int pcolLIGHTGRAY_ON_BLUE = 0x17;


#endif // pallet.h


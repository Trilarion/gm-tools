/*  dansys.cc - non io system specific calls
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires see http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */


#include <time.h>
#include <stdlib.h>
#include <errno.h>   
#include <string.h>  

#include "err_log.h"

void randomize()                    
// This time stuff needs to be in a danio layer. FIX ME
{            
/*                       
#if unix
  timeval *t;       
              
  t = new timeval;

  if ( !gettimeofday(t, NULL) )                    
  {               
    log_fatal_event(strerror(errno), "get time of day err: %s\n", strerror(errno));       
  }                 
  srandom((unsigned int) t->tv_sec);    

  delete t;              

#else 
*/
  // assuming windows
  time_t t;
  
  localtime(&t);
  if ( errno ) // set by localtime()
  {               
    log_fatal_event(errno, "get time of day err: %s\n", strerror(errno));        
  }//fi
  srand((unsigned int) t);  

//#endif

}// randomize                                      

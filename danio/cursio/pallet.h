/*  pallet.h - colour mnemonics for danio/cursio
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires see http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

#ifndef PALLET_H
#define PALLET_H 
 
const int BACKGROUND =    0;
const int pcolBLACK  =    0;
const int pcolGREEN  =    2;
const int pcolRED    =    1; 
const int pcolLIGHTGRAY = 7;
const int pcolDARKGRAY =  8;
const int pcolWHITE  = 0x40;     
const int pcolBLUE =     32;

// pcolX_ON_Y
const int pcolWHITE_ON_BLUE = 0x67; // FIXME check this
const int pcolLIGHTGRAY_ON_BLUE = 0x27; // FIXME check this

#endif // end pallet.h

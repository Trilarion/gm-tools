/*  dankbd.cc - keyboard wrapper code for danio project
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires see http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */
      
// vga/vgagl.h version - implements dankbd with vga/vgagl.h
   	       	       	       	       	       	       	   
	       						   
#include <stdarg.h> 
#include <vga.h>
#include <vgagl.h> 

#include "keys.h"  // key mnemononics
#include "/home/dan/source/danio/dankbd.h"
#include "dan_keyboard_buffer.h"  
// Source file needs to be included (here or in danio.cc) as application 
// programs don't know about dan_keyboard_buffer (only danio).
#include "dan_keyboard_buffer.cc" 
       	       	       	    
dan_keyboard_buffer *key_buffer;
int dgetch_wait_ms = -1;
  
int init_dankbd() 		
{  	       			
  if ((key_buffer = new dan_keyboard_buffer()) == NULL) // init my key buffer
  {  	     	       		   
    log_event("Out of memory\n");
    return (EXIT_FAILURE);
  }
  log_event("key buffer init\n");
  
  return SUCCESS;	 
} // init danio	 
   	   	 		     
int dgetch()			     			
// implement full buffered raw mode input!
// tk need to time the wait for input off dgetch_wait_ms
{ 				     	  
  int ans;	      		     

//  log_event("dgetch\n");
  do  	       	       	      	     	       			  
  {  	      	      		     
    key_buffer->update();
//    log_event("dgetch - key buffer updated.\n");
    ans = key_buffer->pull();	     		
//    log_event("dgetch - ans pulled.\n");
  } while (ans == -1 && dgetch_wait_ms == -1);
  				     
//  log_event("dgetch - ans: %d", ans);
  return ans;	      		     
}//dgetch  	      		     
       	     	      		     
void close_dankbd()   		     
{    	   	      
  keyboard_close();    	       
  log_event("keyboard closed\n");
}    	   	      
		      
		      
void dgetch_wait(int ms)
  // set the wait period for dgetch() in milli seconds
  // -1 for unlimited wait, 0 for no wait
{ 		      
  dgetch_wait_ms = ms;
} // dgetch_wait      
     

/*  dankeys.cc - buffered key input MY WAY
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires see http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

 
// use raw keyboard mode so that CTRL and ALT combinations can be recognised
// but use a software keybuffer to so we can deal in single key strokes.
 
#include <stdio.h>				     
#include <stdlib.h>				     
#include <vgakeyboard.h>     			     
#include <vga.h>
#include <vgagl.h>

#include "dan_keyboard_buffer.h"
#include "/home/dan/source/include/dandefs.h"     
#define SCREEN_MODE G640x480x256
     
     
void init()	      		
{    		      		
  (void) vga_init();  	     	
  vga_setmode(SCREEN_MODE);  	
  gl_setcontextvga(SCREEN_MODE);
  gl_setfont(8, 8, gl_font8x8);	
  gl_setwritemode(FONT_COMPRESSED + WRITEMODE_OVERWRITE);
  gl_setfontcolors(0, vga_white());
  gl_enableclipping();     	     	 	     	
  // Show the background     	
  gl_clearscreen(0);  	     	
} // init		      		
     	     	       						  
void main()    	       			       			  
{    	       	       			       			  
  int ans = -1;
  dan_keyboard_buffer *key_buffer;
     		
  start_log();  // init dandefs.h - err.log
  		
  init(); // vga
  log_event("nowhere yet\n");
  if ((key_buffer = new dan_keyboard_buffer()) == NULL) // init my key buffer
  {  	     	       		   
    log_event("Out of memory\n");
    exit(EXIT_FAILURE);		   
  }  	     	       		   
  log_event("key buffer init\n");
      	     	       		    	       			  
  do  	       	       	      	    	       			  
  {  	     	      		    
    key_buffer->update();	    
//    log_event("update()\n");
     	     	       	     	    
    ans = key_buffer->pull();
//    log_event("pull()\n");
    if (ans != -1)     		    			   
    { 	      	       		    		   
      gl_printf(8,8,"key pressed: %d    ASCII printed as: \"%c\"    ",
     	   	ans, (char) ans);
    } 	      	   		    		   
      	      	   		    		   
  } while (ans != 'w');
  
  log_event("quiting\n");
  keyboard_close();    	       
  log_event("keyboard closed\n");
  vga_setmode(TEXT);   		
  end_log();
}//main	      
      	      
	     

/*  danscr.h - screen output encapsulation for danio project
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires see http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

// see DANIO
#ifndef DANSCR_H
#define DANSCR_H

#include <stdlib.h>
#include <stdarg.h>

#include "/home/dan/source/include/dandefs.h" 
#include "/home/dan/source/include/sat.h"

#define CURSOR_INVIS 0
#define CURSOR_VIS 1

// screen dimentions, assigned values at runtime by init_danscr()
extern int dcur_maxx, dcur_maxy;

// window stuff
class dwindow 
// to provide cross platform windowing, must encapsulate all window 
// attributes in a cross platform class			  
// NOTE: the top left corner of the screen is 1,1
{            				    	  
public:					    	  
  dwindow(int new_x1, int new_y1, int new_x2, int new_y2, 
	  char *new_name = NULL);
    // window constructor		       
  void *get_win_ptr() { return win_ptr; }      	    
    // return the pointer to a third party window   
  void setscroll(int scrl);		       
    // equivelent to dset_scroll(scrl, this);  
  void *win_ptr; // pointer to a third party window
  char *name; // used to give a window a name, usefull for debugging
private: // NEVER try to use this data from application code, the
         //  values depends on the io implementation and so are undefined
	 //  for particular applications. Access should be made through public
	 //  member functions or danio functions that manipulate windows
  int x1, y1, x2, y2;	   		       
  int wcurx; 
  int wcury;
}; 		     
  
// ***************** INITIALISATION + OPTIONS

int init_danscr();  	       
  // initialise a danio screen session, returns 0 on success
void close_danscr(); 	       	       
  // end a danio screen session	       
void dcur(int new_mode);
  // change the cursor appearance
void dset_scroll(int scrl, dwindow *win = NULL);
  // turn window (or screen) scrolling on or off 
  // defaults to full screen
       	       

// ***************** OUTPUT

void dwrite(char *s, ...);
  // formated output
void dwrite(dwindow *win, char *s, ...);
  // formated output
void dvwrite(char *s, va_list ap);	  
  // formated output, using a va_list previously extracted
void dvwrite(dwindow *win, char *s, va_list ap);	  
  // formated output, using a va_list previously extracted

void dclear();
  // clear the screen
void dclear(dwindow *win);
  // clear a window
void dcleareol();
  // clear from the cursor to the end of the line

void drefresh(dwindow *win = NULL);
  // redraw/update the window



// ***************** CURSOR POSITION

void dcur_move(int x, int y, dwindow *win = NULL);
  // move the writing cursor to column x, row y
void dgraph_move(int x, int y, dwindow *win = NULL);
  // move the graphics cursor
int get_dcur_x();
  // get the current writing column
int get_dcur_y();    	     
  // get the current writing row
int get_graph_x();   	    
int get_graph_y();   
  // get the graphics cursor position


// ******************* COLOUR

int setpcolour(short new_colour);
// pick a colour from the pallet. 	       
// Colours from COLOUR_PAIRS to 2*COLOUR_PAIRS-1 are bold
// return 0 on success

int wsetpcolour(dwindow *win, short new_colour);    
// Pick a colour from the pallet, for a window.
// Colours from COLOUR_PAIRS to 2*COLOUR_PAIRS-1 are bold
// return 0 on success
		      
#endif		      
// end - danscr.h     
		      

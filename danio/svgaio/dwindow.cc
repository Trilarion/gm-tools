/*  dwindow.h - windowing encapsulation for danio project
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires see http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

// NOTE: application programs should not access dwindow members directly
//   but do so through other danio functions   	       	   
// svga/gl implementation
extern GraphicsContext *physical_screen;
    	     	  
dwindow::dwindow(int new_x1, int new_y1, int new_x2, int new_y2,
    	     	 char *new_name)
// dwindow constructor	   	  	       
{   	      	    		  
  x1 = new_x1; y1 = new_y1; x2 = new_x2; y2 = new_y2;
  wwidth = x2 - x1 + 1;		
  wheight = y2 - y1 + 1;	
  gl_setcontextvgavirtual(SCREEN_MODE); // create a new virtual screen
  win_ptr = gl_allocatecontext(); // create a new grapghics context   
  gl_getcontext((GraphicsContext *)win_ptr); // link the virtual screen to the new graphics context
  gl_setcontextheight(ycurtoscrn(wheight));  // set height of window
  gl_setcontextwidth(xcurtoscrn(wwidth)); // set width of window	  
    		    	     	       	      	  
  /* do a test
//  gl_setcontextvga(SCREEN_MODE);
  gl_line(1, 1, xcurtoscrn(wwidth), ycurtoscrn(wheight), WHITE);
  log_event("dwindow constructor - gl_line(1,1, %d, %d, WHITE)\n",
    	    xcurtoscrn(wwidth), ycurtoscrn(wheight));
  gl_copyscreen(physical_screen);
  dwait(500); 
  end test */
    
  setscroll(ON);  // setup window scrolling  	      
    	    	     	       	     	     	      
  if (new_name != NULL)	       	  // name the window  
    name = new_str(new_name);	     	     	      
  else	    	     	     	     	     	      
    name = new_str("unamed window"); 	     	      
    	    	     	     	     	     	      
  log_event("dwindow constructor - window and graphics context \"%s\" created.\n",
    	    name);   	     	     	     	      		     
  log_event("  window cursor co-ordinates (x1: %d, y1: %d, x2: %d, y2: %d)\n",
    	    x1, y1, x2, y2); 	     	     
  log_event("  window global graphics co-ordinates (x1: %d, y1: %d, x2: %d, y2: %d)\n",
       	    xcurtoscrn(x1), ycurtoscrn(y1), xcurtoscrn(x2), ycurtoscrn(y2));
  log_event("  window local graphics co-ordinates (x1: 1, y1: 1, x2: %d, y2: %d)\n",
       	    xcurtoscrn(wwidth), ycurtoscrn(wheight));
  log_event("  window pixel size: width: %d, height: %d\n",
       	    wwidth * TEXT_HEIGHT, wheight * TEXT_HEIGHT);
}// dwindow constructor	       		   
     	     	      	       		   
     	     	      	       	
void dwindow::setscroll(int new_scrl)
{    		  	       	
  scrl = new_scrl;	       
} // set scr	  	       
    		  	       
void dwindow::dcur_wmove(int new_x, int new_y)
// move the window's cursor	     
{     	 	  		     
  cur_x = new_x; cur_y = new_y;
}   		  
    		  
// end - dwindow svga/gl implementation     
    		  
    

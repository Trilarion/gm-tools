/*  danscr.cc - screen code for danio project
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires see http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

// vga.h version - implements danscr with vga.h/vgagl.h

// NOTE: It is essentail to call drefresh() to get _any_ output to screen!

#include <stdarg.h>					   
#include <string.h>
#include <time.h>

#include "../danscr.h"
#include "../dwindow.h"
#include "../dankbd.h"
#include "dan_keyboard_buffer.h"
#include "/home/dan/source/include/dandefs.h"  
#include "/home/dan/source/include/sat.h"

#define MAX_VGA_LINE 256			      
#define TEXT_HEIGHT 8
#define TEXT_WIDTH 8
#define BACKGROUND 0     	
#define WHITE  	      15     
#define BLACK	      0	     		
#define LIGHTGREY     7	     		
#define DARKGRAY      8	     		
#define BLUE   	      32
#define GREEN         2

extern dan_keyboard_buffer *key_buffer; // so wait can update the buffer

// Screen Properties 
GraphicsContext *physical_screen;
dwindow *dmain_screen;
double ratio; 

//#define SCREEN_MODE G1024x768x256 // Only one SCREEN_MODE to be un-commented
#define SCREEN_MODE G640x480x256
//#define SCREEN_MODE G320x400x256
	      					      
// screen dimentions, assigned values at runtime by init_danscr()
int dcur_maxx, dcur_maxy;
int ddraw_maxx, ddraw_maxy;

int cur_x = 1, cur_y = 1;  // vgagl doesn't track these, so we will
int draw_x = 1, draw_y = 1; // the drawing cursor, different from the text cursor
	      
void set_pallet(); // setup the colour pallet

void dwait(int ms)
// wait for ms micro seconds
{	     	    	    
  clock_t start;
     	       	    
  start = clock();  
  while (((clock() - start) / CLOCKS_PER_SEC * 100) < ms)
    key_buffer->update(); // busy wait: maintain input  ???	       
}// dwait      	       	      


// ********************** FOR SVGA ONLY ********************
// 	      
int xcurtoscrn(int x)
{  	      	    	     
  int scrnx;   		     
   			     
  scrnx = x * TEXT_WIDTH - 7;
   
  return scrnx;
}  	      	    
   
int ycurtoscrn(int y)
{  	       	    
  int scrny;   		     
   	   		     
  scrny = y * TEXT_HEIGHT - 7;
   		
  return scrny;	
}  	      

// ***************** INITIALISATION + OPTIONS
     
int init_danscr()			     
// initialise vidieo output		     
// the standard screen is a window too for vgagl
// this enables me to controll all the things that svga/gl doensn't give me.
{     
  int errorcode;
  log_event("init_danscr\n");
      	 		     
  // initialize vga	     	
  errorcode = vga_init();    
  /* read result of initialization */
  if (errorcode)  /* an error occurred */
  {   	      	      	       					       
    log_event("init_danscr - Vga graphics error: %d\n", errorcode);
    exit(errorcode);  	       				      	       
  }// vga_init fail   	       				      
  vga_setmode(SCREEN_MODE);    				      	       
  log_event("init_danscr - vga_setmode ok\n");		      
       	 	      					      
  // Don't need to set the context to the physical screen as we will use a???
  // virtual screen.
  // Seem to need it to generate the sudo constants  (see below)
  // initialise vga_gl 	      				      
  errorcode = gl_setcontextvga(SCREEN_MODE);		      
  if (errorcode)      
  {    	      	      	       				      	       
    log_event("danscr - Vga_gl graphics error: %d\n", errorcode);
    exit(errorcode);   	       		  		      	       
  }// gl_setcontextvga failed  	       	       	   	      
  log_event("init_danscr - gl_setcontextvga ok\n");	      
   // 	 	      	      				      
      	 	      	      				      
  // save a pointer to the physical screen		      
  physical_screen = gl_allocatecontext(); 		      
  gl_getcontext(physical_screen);  			      
  log_event("init_danscr - Context of pysical screen stored.\n");
      	 	      	      			     
  // initialise the virtual screen we will use for page flipping
#ifndef TOGGLEOFF     	      			     	 
  gl_setcontextvgavirtual(SCREEN_MODE);  // dmain_screen is used instead
#endif        	      	      			     	 
      	 	      
  // text     	      	      			     	 
  gl_setfont(TEXT_WIDTH, TEXT_HEIGHT, gl_font8x8);   	   
//  gl_setwritemode(FONT_COMPRESSED + WRITEMODE_MASKED);  // might be handy for writing on pictures	 
  gl_setwritemode(FONT_COMPRESSED + WRITEMODE_OVERWRITE); // default
  gl_setfontcolors(0, vga_white());		     
  log_event("init_danscr - Text output enabled.\n");
  	 	      
  gl_enableclipping();	      			     	 
  log_event("init_danscr - gl_enableclipping ok.\n");
	 	      
  // initialise runtime detirmined sudo-constats     	 
  ratio = (double) HEIGHT / (double) WIDTH; // ratio used for circle correction
  dcur_maxx = WIDTH / TEXT_WIDTH; 		     	 
  dcur_maxy = HEIGHT / TEXT_HEIGHT;
  ddraw_maxx = WIDTH; 
  ddraw_maxy = HEIGHT;
  log_event("init_danscr - Screen constants recorded.\n");
  log_event("  circle correction ratio: %.4f\n", ratio);
  log_event("  dcur_maxx: %d, dcur_maxy: %d\n", dcur_maxx, dcur_maxy);
  log_event("  ddraw_maxx: %d, ddraw_maxy: %d\n", ddraw_maxx, ddraw_maxy);
		      
  // create the virtual screen				      
  dmain_screen = new dwindow(1, 1, dcur_maxx, dcur_maxy, "main screen");
  if (dmain_screen == NULL)  				      
  {	    	      	     				      
    log_event("init_danscr - Error allocating virtual window dmain_screen.\n");
    exit(errorcode);   	       		  		      	       
  }		      					      
  log_event("init_danscr - dmain_screen created\n");	      
     	      	  	      			     	 
      	 		      			     	 
  /* prove successful setup: for dubbuging use only
  gl_setcontext((GraphicsContext *) dmain_screen->win_ptr); // use virtual screen
//  gl_setcontextvga(SCREEN_MODE);			      // use physical screen
  gl_line(1, 1, xcurtoscrn(dmain_screen->x2), 
          ycurtoscrn(dmain_screen->y2), WHITE);   
  drefresh(dmain_screen);	      // copy virtual screen to physical
  vga_getch();
  log_event("init_danscr - proof of init\n");
  end of proof */

//  drefresh(dmain_screen);	      // copy virtual screen to physical
//  vga_getch();
  
  return SUCCESS;	     	       	  	     	     	 
} // init danio	     	       	  	     		 
     		 		  			 
void close_danscr() 		  
{
  vga_setmode(TEXT);
  log_event("close_danscr - vga mode set to text.\n");
}    	   	    	    	  
    			       	  	
void dset_scroll(int scrl, dwindow *win)
// Turn window (or screen) scrolling on or off.
// Default is turn full screen scrolling on.
// There is no hardware scrolling for vga, i will have to create a software 
// implementation. tk        	    
{ 		       	    
  log_event("dset_scroll\n");
  if (win == NULL)     	     
    win = dmain_screen;	     
  win->setscroll(scrl);	     
}// dset scroll        	       	  	    	       
     		       	     	  
// ***************** OUTPUT  	  
     	      		     	  	 
void dwrite(char *s, ...)		 
// text output				 
// get the argument pointer, then pass on to dvwrite()
{    	      	   	     	    	 
  va_list ap;          	       	     	    	       
     				    	 
//  log_event("dwrite(char*, ...)\n");	 
  va_start(ap, s);	      	    	 
  dvwrite(dmain_screen, s, ap);      	       	       	 
  va_end(ap);  	     	      	    	 	     
}// dwrite standard		    	      	    
     		   	      	    	 
void dwrite(dwindow *win, char *s, ...)	 
// text output to a window 	    	 
// get the argument pointer, then pass on to dvwrite()
{      	       	   	      	    	 	      
  va_list ap;          	       	     	    	       
     	       	   	      	    	  	     
//  log_event("dwrite(dwindow*, char*, ...)\n");
  va_start(ap, s); 	   	    	 
  dvwrite(win, s, ap);	   	    	 
  va_end(ap);  	   	   	    	 
}// dwrite with window	       	    	 
  	       	   	       	    	 
void dvwrite(char *s, va_list ap)   	 
// text output using a variable argument
// pass the main screen to dvwrite window version 
{ 	 		       	    	 	 
//  log_event("dvwrite(char*, va_list)\n");
  dvwrite(dmain_screen, s, ap);	     	 	 
}// dvwrite    	    	       	     	 	 
     	       	    	     	     	 	 
void dvwrite(dwindow *win, char *s, va_list ap)	 
// formated output, using a va_list previously extracted
// vgagl has no vwrite, so we use vsnprintf instead
// gl_write is not formated output, so carrage returns must be processed here.
// The easiest way to do this is character at a time output.
{    	       		     	     		 
  char out[MAX_VGA_LINE]; // the output after formatting
  char c[2];   		  // for single character output using gl_write
  int len, i;  		  // counters to control output loop
  	  			     
  c[1] = '\0'; // null terminate the character at a time output
//  log_event("dvwrite(dwindow*, char*, va_list)\n");
  
  // set the current context to the window
  
//  gl_setcontext((GraphicsContext *) win->win_ptr);
  gl_setcontext(physical_screen);
//  cur_x = win->cur_x;
//  cur_y = win->cur_y;
  
//  log_event("dvwrite - gl_setcontext ok\n");
//  log_event("dvwrite - window/context is: %s\n", win->name);
  	 
  // test the graphics context is ok
//  gl_line(1, 1, win->x2, win->y2, WHITE);
//  log_event("dvwrite - draw ok\n");
  	 		     	     		 
  // generate the string to output with vsnprintf    
  vsnprintf(out, MAX_VGA_LINE, s, ap);	   	     
//  log_event("dvwrite - vsnprintf ok\n");		     
//  log_event("dvwrite - outputing: %s\n", out);	   
     	       	       	     	     	      	 
  // Output one character at a time, keeping track of the cursor and wathcing
  // for any new line characters.    	   	 
  len = strlen(out);		     
//  log_event("dvwrite - len: %d\n", len);	   
  for (i = 0; i < len; i++)  	     	   	 
  {    	     			     
//    log_event("dvwrite - out[i]: %c\n", out[i]);	   
    if (out[i] == '\n')	     	     	      	 
    { 	       		     	     	      	 
      cur_y++; cur_x = 1;  // new line	      
    } 	       		     	      	      
    else       		     	      	      
    { 	       			     
      c[0] = out[i]; 	   // the next character to output    	      
      gl_write(xcurtoscrn(cur_x), ycurtoscrn(cur_y), c); // output
      cur_x++; 	       	   // keep track of cursor
    }// fi: check for new line     	      	  		  
  }//rof: output the string   	    	      		  		       
//  log_event("dvwrite - end\n");
//  win->cur_x = cur_x;
//  win->cur_y = cur_y;
  drefresh(win);
}// dvwrite    	    	 		      
 	       		 		      
// ***************** CURSOR POSITION	      
     	       	    	 		   
     	       		 		   
void dcur_move(int x, int y, dwindow *win) 
  // move the writing cursor to column x, row y	
{     	 	  	 		 
  cur_x = x;	  	 
  cur_y = y;	  	 
  if (win != NULL)	 
    win->dcur_wmove(x, y);
  			 
} // dan cursor move	  			
     	       		  			
int get_dcur_x()	  
{ 			  
  return cur_x;		  
}      	      		  
			  
int get_dcur_y()    	  
{    	 		  
  return cur_y;		  
}    	      		  
     	 		  
void dcur(int new_mode)	  
// change the cursor appearance
// there is no vgagl cursor - unless i make one? tk
{    	 		  			   
}    	 	    	  			   
     	 	    	  
     	 	    	  
void dclear() 		  
{ 			  
  dclear(dmain_screen);	  
}    	      	      	  	  
     	      	      	  	  
void dclear(dwindow *win)
{    		      	 /*    	  
  if (win == NULL) win = dmain_screen;
  gl_setcontext((GraphicsContext *) win->win_ptr);
  gl_clearscreen(BLACK);   */	  
} // dclear	       	     	  	    
     		       	     	  	    
void dcleareol()       	     //tk		    
// clear from the cursor to the end of the line
{    		      	     	 
  /*		      	     	 
  clrtoeol();	      	     	 
   */ 		      	     	 
}    		       	     	 	    
     		       	     	 	    
void drefresh(dwindow *win = NULL)
// draw the window to the physical screen
{/*     			     	       	 		   
  if (win == NULL || win == dmain_screen)   	      	   
  {   			     		    	      	   
    gl_setcontext((GraphicsContext *) dmain_screen->win_ptr);
    gl_copyscreen(physical_screen); // print to screen	     
  }   			     	   	    	      	   
  else			     	   	    	      	   
  {   			   		    
    log_event("drefresh - win %s\n", win->name);
//    gl_setcontext((GraphicsContext *) dmain_screen);  
    gl_setcontext(physical_screen);  
    log_event("drefresh - win %s\n", win->name);
    gl_copyboxfromcontext(   	       	    	
       (GraphicsContext *) win->win_ptr,
       1, 1, xcurtoscrn(win->wwidth), ycurtoscrn(win->wheight), // source
       xcurtoscrn(win->x1), ycurtoscrn(win->y1));  // target 	       
    log_event("drefresh - win %s\n", win->name);
//    gl_copyscreen(physical_screen); // print to screen	     
    log_event("drefresh - win %s\n", win->name);
  }*/
  dwait(50);
} // drefresh	       	     			      	     
       	      	       	     				     
       	      	       	     				     
//     	      	       	     				     
//                          COLOUR STUFF		    
//                          				    
       	      
       	      
void set_pallet()      	    
{    
  /*
  short fore, back, pair; 
  pair = 0;	       
  for (back = 0; back < COLORS; back++)
  {  	   	       	
    for (fore = 0; fore < COLORS; fore++)
    {	       	       
      if ((pair) && (pair < COLOR_PAIRS)) // 0 cannot be set
      {
        init_pair(pair, fore, back);
      }	     
      pair++;  	       
    } // for: foreground	 	       
  } // for: background
   */ 
} // set pallet     	       	       
     	     	       
int wsetpcolour(dwindow *win, short new_colour)
// Pick a colour from the pallet, for a window.
// Colours from COLOUR_PAIRS to 2*COLOUR_PAIRS-1 are bold
{    
  /*
  WINDOW *nwin = (WINDOW*) win->get_win_ptr();
  if (new_colour >= COLOR_PAIRS)
  {  	 	       	     
    wattron(nwin, A_BOLD);    	     
    wcolor_set(nwin, new_colour - COLOR_PAIRS, NULL);
  }  		       
  else		       
  {  		       
    wattroff(nwin, A_BOLD);   
    wcolor_set(nwin, new_colour, NULL);
  }  		       
    
   */ 
  return SUCCESS;	
} // set pallet colour
       	       			 
int setpcolour(short new_colour)    
// Pick a colour from the pallet.
// Colours from COLOUR_PAIRS to 2*COLOUR_PAIRS-1 are bold
{ 		
  /*
  if (new_colour >= COLOR_PAIRS)
  { 	 	       	     
    attron(A_BOLD);    	     
    color_set(new_colour - COLOR_PAIRS, NULL);
  } 	      	       
  else	      	       
  { 	      	       
    attroff(A_BOLD);   
    color_set(new_colour, NULL);
  }  		       
    
   */ 
  return SUCCESS;	
}// set pallet colour

// end - danscr.cc svga/gl version

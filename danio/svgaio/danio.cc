/*  danio.cc - wrapper code for danio project
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires see http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */
    
// vga.h version - implements danio with vga.h
// This implementation provides full graphical cpabilities

// NOTE: It is essentail to call drefresh() to get _any_ output to screen!


#include <vga.h>
#include <vgagl.h>			  
#include <stdarg.h>
   
#include "danscr.cc"
#include "dwindow.cc"
#include "dankbd.cc"
  
int init_danio()
// initialise ncurses
{ 
  int temp;
  temp += init_danscr();
  temp = init_dankbd();
  log_event("init_danio() - %d\n", temp);
  	
  return temp;
} // init danio
  	 
void close_danio()
{ 	   
  close_danscr();   
  log_event("close_danio() - closed\n");
} 	   
  	   
  	 
  	 
	

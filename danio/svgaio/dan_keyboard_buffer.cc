/*  dan_keyboard_buffer.cc - buffered key input MY WAY
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires see http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

 		      
// use raw keyboard mode so that CTRL and ALT combinations can be recognised
// but use a software keybuffer to so we can deal in single key strokes.
 		      
#include <stdio.h>    				     
#include <stdlib.h>   				     
#include <vgakeyboard.h>     			     
#include <vga.h>      
#include <vgagl.h>    		

#include "dan_keyboard_buffer.h" 
#include "/home/dan/source/include/dandefs.h"

#define FIRST_KEY 1 				     
#define LAST_KEY 111 				     
 						     
// key code offsets				     
#define OFFSET_PRINTABLE 0   			     
#define OFFSET_SCANCODE  128 			     
#define OFFSET_CTRL 	 256			     
#define OFFSET_ALT     	 384   			     
#define OFFSET_SHIFT   	 512   			     
#define OFFSET_CTRL_ALT	 640   			     
	      	 
	      	 		    
dan_keyboard_buffer::dan_keyboard_buffer()    	  
{ 	      	 		    	  
  int key;  // counter to initialise key_log 
     	      	 		    
  log_event("dan_keyboard_buffer()\n");
  if (keyboard_init()) // init raw keyboard mode, exit on failure      
  {  	       	       	  	     	       	      		  
    log_event("  keyboard init failed\n");   	      		  
    exit(EXIT_FAILURE);	  	     	       	      		  
  } // fi      	       	  	     	       	      		  
  log_event("  raw mode initiated\n");   	      		      
  	       			    
  // set limits for key buffer	    
  last_key = keys - 1; // see note in class dan_keyboard_buffer 
  end = keys + BUFF_SIZE - 1; // to mark the last position in keys 
  log_event("  key buffer limits initialised\n");
  	       			    
  // initialise the key_log with blanks. 
  for (key = FIRST_KEY; key <= LAST_KEY; key++)			  
    key_log[key] = 0;		     
  log_event("  key log initialised\n");
} // buffer init	  	    
     	      	  	   	    
int dan_keyboard_buffer::push(int key) 
{    	       	  	   	    
  int *temp;  	  	   	    
  if (last_key != end)	   	    
  {  	       	  	   	    
    last_key++;	  	   	    
    *last_key = key;   	   	    
    return 0;  	  
  }  	       	  
  else return 1;  		
}// push       	  
     	       	  
int dan_keyboard_buffer::pull()
{    	       	 
  int pulled; 	 
  int i, stored = last_key - keys + 1;
     	       	 	  	     
  if (stored > 0)	  	     
  {   	       	 	  	     
    pulled = keys[0];	  	     	   
      	       	 	  	     
    for (i = 0; i < stored; i++)
      keys[i] = keys[i+1];
    last_key--;
  }  	       			     
  else	       			 
    pulled = -1;			 
  return pulled;		 
} //pull       
     	       	
int log_key(char *key_state, char* key_log, int key)
// track key presses. returns 1 if key is pressed		   
{ 	       	       	   					   
  if (key_state[key])	       	      	  
  {	       				  
    key_log[key] += 1; // update log
// 	fprintf(stderr, "key down: %d\n", key);
  }	       	       			  
  else      	       			  
  // key clear 	       		  
  {	       	       			  
    if (key_log[key])  			  
    {      	       		
//      fprintf(stderr, "key released: %d\n", key);
      // re-map and put key in buffer
      // clear key from log 			   
      key_log[key] = KEY_NOTPRESSED;
      return KEY_PRESSED;	
    }          	       			   
  }//else      	       	
  return KEY_NOTPRESSED; 
}// log key  	       
      	     	       
      	     	       			       
int dan_keyboard_buffer::remap(int key)
// remap scancodes to ASCII
{     
  if ((ctrl) && (alt)) 
    return key + OFFSET_CTRL_ALT;
  else if (ctrl)
    return key + OFFSET_CTRL;
  else if (alt)
    return key + OFFSET_ALT;
  else
  switch(key)	       						  
  {   	    	       
    case (SCANCODE_ESCAPE): 	   
      return KEY_ESC;       	   
    case (SCANCODE_GRAVE):
      if (shift)       	
        return '~';    
      else     	       
        return '`';    	 
    case (SCANCODE_1):   	       	 
      if (shift)       
       	return '!';    
      else     	       
        return '1';    
    case (SCANCODE_2): 
      if (shift)       
        return '@';    
      else     	       
        return '2';    
    case (SCANCODE_3): 
      if (shift)       
        return '#';    
      else     	       
        return '3';    
    case (SCANCODE_4): 
      if (shift)       
        return '$';    
      else     	       
        return '4';    
    case (SCANCODE_5): 
      if (shift)       
        return '%';    
      else     	       
        return '5';    
    case (SCANCODE_6): 
      if (shift)       
        return '^';    
      else     	       
        return '6';    
    case (SCANCODE_7): 
      if (shift)       
        return '&';    
      else     	       
        return '7';    
    case (SCANCODE_8): 	  
      if (shift)       	  
        return '*';    	  
      else     	       	  
        return '8';    	  
    case (SCANCODE_9): 
      if (shift)       	  
        return '(';    	  
      else     	       	  
        return '9';      
    case (SCANCODE_0):  	 
      if (shift)       	
        return ')';    	
      else     	       	
        return '0';    	
    case (SCANCODE_MINUS):
      if (shift)       	
        return OFFSET_SHIFT + key;    	
      else     	       		
        return '-';    		
    case (SCANCODE_EQUAL):	
      if (shift)       	  	
        return '+';    	       	
      else     	       	
        return '=';    	
    case (SCANCODE_BACKSPACE):
        return 8;      	
    case (SCANCODE_TAB):
        return 23;     	
    case (SCANCODE_Q): 	       
      if (shift)       	     
        return 'Q';
      else     	       	     	 
        return 'q';    		 
    case (SCANCODE_W): 		 
      if (shift)       	       	 
        return 'W';    	       	 
      else     	       	       	 
        return 'w';    	       	 
    case (SCANCODE_E): 	       
      if (shift)       	       
        return 'E';    	       
      else     	       	       
        return 'e';    	       
    case (SCANCODE_R): 	       
      if (shift)       	       
        return 'R';    	       
      else     	       	       
        return 'r';    	
    case (SCANCODE_T): 	
      if (shift)       	
        return 'T';    	
      else     	       	
        return 't';    	
    case (SCANCODE_Y): 	
      if (shift)       	
        return 'Y';    	
      else     	       	
        return 'y';    	
    case (SCANCODE_U): 	     
      if (shift)       	     
        return 'U';    	     
      else     	       	     
        return 'u';    	     
    case (SCANCODE_I): 	     
      if (shift)       	     
        return 'I';    	     
      else     	       	     
        return 'i';    	     
    case (SCANCODE_O): 	       
      if (shift)       	     
        return 'O';    	     
      else     	       	     
        return 'o';    	
    case (SCANCODE_P): 	       
      if (shift)       	     
        return 'P';    	     
      else     	       	     
        return 'p';    	
    case (SCANCODE_BRACKET_LEFT): 	       
      if (shift)       	       	 
        return '{';    	     
      else     	       	     
        return '[';    	
    case (SCANCODE_BRACKET_RIGHT):
      if (shift)       	       	
        return '}';    	
      else 	       	
        return ']';    	
    case (SCANCODE_ENTER):
      if (shift)       	
        return 13;     	
      else 	       	
        return 13;     		       	
   // case (SCANCODE_LEFTCONTROL):     	
   //     return ;     
    case (SCANCODE_A): 
      if (shift)       
        return 'A';    
      else 	       
        return 'a';    
    case (SCANCODE_S): 
      if (shift)       
        return 'S';    
      else     	       
        return 's';    
    case (SCANCODE_D): 
      if (shift)       
        return 'D';    
      else 	       
        return 'd';    
    case (SCANCODE_F): 
      if (shift)       
        return 'F';    
      else 	       
        return 'f';    
    case (SCANCODE_G): 
      if (shift)       
        return 'G';    
      else 	       
        return 'g';    
    case (SCANCODE_H): 
      if (shift)       
        return 'H';    
      else 	       
        return 'h';    
    case (SCANCODE_J): 
      if (shift)       
        return 'J';    
      else 	       
        return 'j';    
    case (SCANCODE_K): 
      if (shift)       
        return 'K';    
      else 	       
        return 'k';    
    case (SCANCODE_L): 
      if (shift)       
        return 'L';    
      else     	       
        return 'l';    
    case (SCANCODE_SEMICOLON):
      if (shift)       	    
        return ':';    
      else 	       
        return ';';    
    case (SCANCODE_APOSTROPHE):
      if (shift)       	  
        return 34;     
      else     	       
        return 44;     
   // case (SCANCODE_LEFTSHIFT):    
   //     return ;     
    case (SCANCODE_BACKSLASH):
      if (shift)       	    
        return '|';    
      else 	       
        return '\\';    	       	   
    case (SCANCODE_Z): 
      if (shift)       
        return 'Z';    
      else 	       
        return 'z';    
    case (SCANCODE_X): 
      if (shift)       
        return 'X';    
      else 	       
        return 'x';    
    case (SCANCODE_C): 
      if (shift)       
        return 'C';    
      else 	       
        return 'c';    
    case (SCANCODE_V): 
      if (shift)       
        return 'V';    
      else 	       
        return 'v';    
    case (SCANCODE_B): 
      if (shift)       
        return 'B';    
      else 	       
        return 'b';    
    case (SCANCODE_N): 
      if (shift)       
        return 'N';    
      else     	       
        return 'n';    
    case (SCANCODE_M): 
      if (shift)       
        return 'M';    
      else 	       
        return 'm';    
    case (SCANCODE_COMMA):
      if (shift)       	
        return '<';    
      else 	       
        return ',';    
    case (SCANCODE_PERIOD):
      if (shift)       	 
        return '>';    
      else 	       
        return '.';    		    
    case (SCANCODE_SLASH):	    
      if (shift)       		    
        return '?';     	    
      else 	       		    
        return '/';    		    
    case (SCANCODE_KEYPADMULTIPLY):
        return '*';    		 
    case (SCANCODE_SPACE):
        return 32;     
    case (SCANCODE_KEYPADMINUS):
      if (shift)
        return OFFSET_SHIFT + key;
      else
       	return '-';    
    case (SCANCODE_KEYPADPERIOD):
       	if (shift)     
          return '.';  
    	else	       
          break;   // TK
    case (SCANCODE_KEYPADPLUS):
      if (shift)
        return OFFSET_SHIFT + key;
      else
        return '+';    
    case (SCANCODE_KEYPADDIVIDE):
        return '/';    	       
	  	       
	  	       
default:  	       
      return key + OFFSET_SCANCODE;
  } 	     	       		   				  
  	     	       		   				  
} // dan map 	       	 	   				  
				   
int dan_keyboard_buffer::update()  
{ 				   
  int key;			   
  				   
  keyboard_update(); 	      	   	       			  
  key_state = keyboard_getstate();	       			  
  // go through the keys		       			  
  ctrl = (key_state[SCANCODE_LEFTCONTROL] || key_state[SCANCODE_RIGHTCONTROL]);
  alt = (key_state[SCANCODE_LEFTALT] || key_state[SCANCODE_RIGHTALT]);
  shift = (key_state[SCANCODE_LEFTSHIFT] || key_state[SCANCODE_RIGHTSHIFT]);
  for (key = FIRST_KEY; key <= LAST_KEY; key++) 		  
  {  	     	       			       	  
    if (log_key(key_state, key_log, key))    
      push(remap(key));
  } // for 	       

  return -1;
}

int dan_keyboard_buffer::get_key()
{	   
  update();  
  return pull();	 
}// get key    
	       	 
	       	 
int dan_keyboard_buffer::wait_get_key()
{   	       	 
  int c;       	 
  	       	 
  do	       	 
  { 	       	 
    c = get_key();
  } while (c == -1);
  return c;    	 
}// wait get key;
	       
	       

// end - dan_keyboard_buffer.cc

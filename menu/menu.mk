#menu.mk - includeable make file for the menu project
#Daniel Vale MAR 2002

ifndef MENU_MK
MENU_MK = 1

include $(INCLUDE_DIR)/map.mk

$(MENU_DIR)/menu.o : $(MENU_DIR)/menu.h $(MENU_DIR)/menu.cc $(DANIO_H) $(INCLUDE_DIR)/dandefs.h \
         $(INCLUDE_DIR)/sat.h
	g++ $(DIR_LIST) -c $(MENU_DIR)/menu.cc -o $(MENU_DIR)/menu.o

include $(INCLUDE_DIR)/include.mk
include $(DANIO_DIR)/../danio.mk

endif


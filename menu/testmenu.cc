/*  stub.cc - test bed for menu.
 *
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */


// note : this test bed will be used to build up the menu for adv_game whilst
// developing a menu system for inclusion in future applications, compatable
// with the danio project


#include <stdlib.h>

#include "menu.h"
#include "dandefs.h"
#include "adv_cmd.h" // command list - previously from adv_game
#include "danio.h"
#include "err_log.h"

int get_user_input()
{
  return (int) dgetch();
} // get user input

void handle_event(int cmd, int& working, menu_el *main_menu)
{
  switch (cmd)
  {
    case advcmdQUIT: working = 0; break;
    case advcmdSHOW_MENU: main_menu->show(); break;
    case advcmdSAVE_CHAR: dwrite("Save the character\n"); break;
    case advcmdLOAD_CHAR: dwrite("Load a character\n"); break;
    case advcmdSHOW_CHAR: dwrite("Show Character\n"); break;
    case advcmdTOGGLE_DISPLAY: dwrite("Toggle display mode\n"); break;
    case advcmdNORTH: dwrite("Move North\n"); break;
    case advcmdSOUTH: dwrite("Move South\n"); break;
    case advcmdEAST: dwrite("Move East\n"); break;
    case advcmdWEST: dwrite("Move West\n"); break;
    case advcmdUP: dwrite("Move Up\n"); break;
    case advcmdDOWN: dwrite("Move Down\n"); break;

    case advcmdDRINK_POTION: dwrite("Drink Potion\n"); break;
    case advcmdREST: dwrite("Rest\n"); break;
    case advcmdLOOK: dwrite("Look\n"); break;
    case advcmdSTART_FIGHT: dwrite("Start fight\n"); break;
    case advcmdSPEND_XP: dwrite("Spend xp\n"); break;

    case advcmdENTER_MARKET: dwrite("Enter the market\n"); break;
    case advcmdBUY_SWORD: dwrite("Buy sword\n"); break;
    case advcmdBUY_SHIELD: dwrite("Buy shield\n"); break;
    case advcmdBUY_ARMOUR: dwrite("Buy armour\n"); break;
    case advcmdBUY_POTION: dwrite("Buy potion\n"); break;

    case NO_COMMAND: dwrite("Command not understood\n");
//    default:
  }// switch
} // handle event


int main()
{
  menu_el *main_menu;
  dwindow *new_win = NULL;
  int working = 1;
  int ans, cmd;

  start_log();
  log_event("q = %d\n\n", 'q');
  init_danio();
  main_menu = new menu_el("   Main Menu.",
                   new menu_el("~F1~ Show menu", 0, danioKEY_F1, advcmdSHOW_MENU,
            new menu_el("~F2~ Save character", 0, danioKEY_F2, advcmdSAVE_CHAR,
                   new menu_el("~F3~ Load character", 0, danioKEY_F3, advcmdLOAD_CHAR,
            new menu_el("~F4~ Toggle display mode",
                    0, danioKEY_F4, advcmdTOGGLE_DISPLAY,
                   new menu_el("~F5~ Show Character", 0, danioKEY_F5, advcmdSHOW_CHAR,
                   new menu_el("~F6~ Drink Potion", 'p', danioKEY_F6, advcmdDRINK_POTION,
            new menu_el("~F7~ Look", 'l', danioKEY_F7, advcmdLOOK,
            new menu_el("~NSEWUD~ Move",
            new menu_el("", 'n', advcmdNORTH,
              new menu_el("", 's', advcmdSOUTH,
              new menu_el("", 'e', advcmdEAST,
              new menu_el("", 'w', advcmdWEST,
              new menu_el("", 'u', advcmdUP,
                   new menu_el("", 'd', advcmdDOWN,
                   new menu_el("~R~est", 'r', advcmdREST,
                   new menu_el("~F~ight", 'f', advcmdSTART_FIGHT,
                   new menu_el("Enter the ~m~arket", 'm', 0, advcmdENTER_MARKET,
              new menu_el("   Market",
                          new menu_el("~1~ Buy Sword", '1', advcmdBUY_SWORD,
                          new menu_el("~2~ Buy Shield", '2', advcmdBUY_SHIELD,
                          new menu_el("~3~ Buy Armour", '3', advcmdBUY_ARMOUR,
                          new menu_el("~4~ Buy Potion", '4', advcmdBUY_POTION,
                     NULL ))))),
                   new menu_el("Spend ~x~p", 'x', advcmdSPEND_XP,
                   new menu_el("~Q~uit", 'q', danioKEY_ESC, advcmdQUIT,
                   NULL ))))) ))))) ))))) )))));
  new_win = new dwindow(40, 1, 80, 20);
  main_menu->set_win(new_win);

  main_menu->show();
  do
  {
    ans = get_user_input();

    log_event("ans = %d = %c\n", ans, (char) ans);
    cmd = main_menu->interpret(ans);
    log_event("cmd = %d\n", cmd);
    handle_event(cmd, working, main_menu);
    log_event("working = %d\n", working);
  }  while (working);

  close_danio();
  end_log();

  return 0;
} // main


// end - stub.cc



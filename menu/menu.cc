/*  menu.cc - menuing system		 
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */
		 
// The menuing system is intended to make adding menues to applications easy
// by providing functions which build up a menu at run time. The application
// programer should only need to ammend his function calls, his command 
// definitions and his event handler to make changes to the menu.

#define DEBUG_MENU

#include <stdlib.h>
#include <string.h>
#include <assert.h>
   
#include "menu.h"
#include "danio.h"
#include "sat.h"
#include "dandefs.h"
#include "err_log.h"
   		     
#ifndef cHIGHLIGHT   
#define cHIGHLIGHT 71
#endif	       	     
#ifndef cNORM  	     
#define cNORM 7	     
#endif	       	     
   		     
menu_el* menu_el::top = NULL;
menu_el* menu_el::pos = NULL;  
dwindow* menu_el::menu_win = NULL;

int toggle_highlight(int& highlight_on, dwindow *win)
{		     	  
  log_event("toggle_highlight() - start");

  if (highlight_on)  	  
    setpcolour(cNORM, win);
  else	   
    setpcolour(cHIGHLIGHT, win);
  highlight_on ^= 1;  

  log_event("toggle_highlight() - end");
  	   
  return highlight_on;
}// toggle highlight      	       	    
  	   	    
  	   	    
void print_highlight(dwindow *win, const char *s)
// highlight the section of a string surrounded by ~ 
{  	   	     	       	
  char *s_copy = NULL, *s_ptr = NULL; 	 
  int highlight_on = FALSE;	
  	 
  log_event("print_highlight() - start\n");  	    	      	
  log_event("print_highlight() - assert(win)\n");
  assert(win);
  s_copy = new_str(s); // make a copy of the menu line for strtok to use
  	   		      	
  // print the section preceeding the first ~ if any
  if (s_copy[0] != '~')	      	
  {	   		    
    log_event("print_highlight() - about to call setpcolour()\n");
    //    win->setpcolour(cNORM);
        setpcolour(cNORM, win);   	
    //    dwrite(win, strtok(s_copy, "~"));
    log_event("print_highlight() - write to window\n");
    /*win->*/dwrite(win, strtok(s_copy, "~"));
    log_event("print_highlight() - write to window ok\n");
    
    s_ptr = strtok(NULL, "~");	
  } else   		      	
    s_ptr = strtok(s_copy, "~");
  log_event("print_higlight() - first section printed.\n");
  	    		      
  while (s_ptr != NULL)       
  {	   	      	      
    (void) toggle_highlight(highlight_on, win);
    dwrite(win, s_ptr);	      
    s_ptr = strtok(NULL, "~");
  } 		      
    		       	     	
  delete s_copy;       	     	
  log_event("print_highlight() - end\n");  	    	      	

}// print highlight    	       	      
     	       	     	     
     	       	     	     
void menu_el::assign_all(const char *new_title, int new_key, int new_hot_key, 
     	       	       	 int new_cmd,
               	         menu_el *new_sub, menu_el *new_next)
// do all the assignments for the constructors
{  		   
  if (new_title != "")
    title = new_str(new_title);
  else		   
    title = NULL;  
  sub = new_sub;     		      	
  next = new_next; 
  key = new_key;   	
  hot_key = new_hot_key;
  cmd = new_cmd;   
  top = this;	   
  pos = top;	   
}// assign all	   
	      	   
menu_el::menu_el(const char *new_title, int new_key, int new_hot_key, 
		 int new_cmd,
	      	 menu_el *new_sub, menu_el *new_next)
// all singing	   		      	
{	   	   		      
  assign_all(new_title, new_key, new_hot_key, new_cmd, new_sub, new_next);
}//menu constructor   		      	     			 
	      	   		      	     
menu_el::menu_el(const char *new_title, int new_key, int new_hot_key, 
		 int new_cmd, menu_el *new_next)   	  
// no sub menu		     	      	  
{      	      	      	     	      	  
  assign_all(new_title, new_key, new_hot_key, new_cmd, NULL, new_next);
}//menu constructor   		      	    
				     
menu_el::menu_el(const char *new_title, int new_key, int new_cmd,
		 menu_el *new_next)   
// no sub menu	 		     	     	    
// no hot key	 		     	     	    
{      	      	      		      	     	    
  assign_all(new_title, new_key, menuNO_KEY, new_cmd, NULL, new_next);
}//menu constructor   		      		     
				     		     
				      		     
menu_el::menu_el(const char *new_title, menu_el *new_next)
// no sub menu			       		     
// no keys			       		     
{      	      	      		       		     
  assign_all(new_title, menuNO_KEY, menuNO_KEY, NO_COMMAND, NULL,
  	     new_next);
}//menu constructor   		                     
  		     		                                 
       	     	      	      	                 
void menu_el::show()          	       	       	 
{ 		     	                                   
  menu_el *el;	 // stepper                    
  el = pos;    	     	                         
  		                                         
#ifdef DEBUG_MENU                              
  log_event("menu_el::show() - start\n");      
#endif  
  dclear(menu_win);                            
#ifdef DEBUG_MENU                              
  log_event("menu_el::show() - window cleared\n");
#endif  
  while (el != NULL) 	                         
  {	       	     	                             
    if (el->title != NULL)                     
    { 	       	     	                         
      print_highlight(menu_win, el->title);  // show the menu entry
      dwrite(menu_win, "\n");    	       	     
    } 	       	     	       	                 
    el = el->next;   	       // move to the next entry
  } // while el	     	       				           
    	       	     	       				             
  if (el->pos != top)  	       	       	       	       	
    print_highlight(menu_win, "~0~ Return to previous menu.\n");
  		     	                                   
  drefresh(menu_win);	                         
#ifdef DEBUG_MENU  
  log_event("menu_el::show() - end\n"); 
#endif	                                      
}// show       	       	      	               
  		     	      
void menu_el::set_win(dwindow *new_win)
// force the menu to appear in a window
// PRECONDITION: new_win is an initialised dwindow or NULL
{      	   	     	      
  menu_win = new_win;	      
} 		     	      
  		     	      
			      	 
int menu_el::interpret(int d) 	 
// search the menu for a match to the input
{   	       	       	      	 
  int action = NO_COMMAND;     	     
  menu_el *el, *prev; 	       	 
   	       	       	      	 
  log_event("interpret %d\n", d);
   	       	       	      	 
  if (d == '0')        	      	 
  // return to previous menu  	 
  { 	       	       	      	 
    pos = top; 	       	      	 
    show();    	       	      	 
    return UP_MENU_TREE;      	 
  } 	       	       	      	 
  	     		      	 
  el = pos; // start at the current menu position
  while (el != NULL)   	     // check all items in current menu for key
  {  	       	       	      
    if (d == el->key)  	      
    {	     		      
      log_event("interpret - key match. cmd: %d\n", el->cmd);
      if (el->sub != NULL)    		 
      {	       	       	      		 
   	pos = el->sub; 	      		 
   	pos->show();   	      		 
      }	       	       	      		 
      return el->cmd;  	      	       	 
    } 	       	       	      		 
    el = el->next;     	      		 
  }// while el		      		 
  	     		      		 
  // not key match on this menu - procceed to hot key search
  action = top->interpret_hot_key(d);
	     	    	      
  if (action != NO_COMMAND) log_event("interpret - hot key match.\n");
  return action; // action is a hot key matched command or NO_COMMAND
} // interpret		      		 		 
  	     	 	      				 
int menu_el::interpret_hot_key(int d)			 
// search the menu for a hot key match			 
// done recursivly to search the entire menu tree	 
{  	     	   		      			 
  int temp = NO_COMMAND; 						 
  // check if this node matches	      			 
  if (hot_key == d)  
  {		     
    log_event("hot_key_search - key match. cmd %d\n", cmd);
    return cmd;	     
  }		     
  		     
  // decend to sub menu if it exists  			 
  if (sub != NULL) temp = sub->interpret_hot_key(d);	 
  if (temp != NO_COMMAND) return temp;	       	  	 
  // go to next menu element if it exists      	  	 
  if (next != NULL) temp = next->interpret_hot_key(d);   	 
  if (temp != NO_COMMAND) return temp;	       
  // done    	     	  	      	   
  return NO_COMMAND; 	  		   
} // hot key search  	  	      	   
  	     	     	  	      	   
int menu_el::interpret_std_key(int d)	   
// search the menu for a match to standard input only
{   	       	       	  	 
  int action = NO_COMMAND;     	     
  menu_el *el, *prev; 	       	 
   	       	       	  	 
  log_event("interpret %d\n", d);
   	       	       	  	 
  if (d == '0')        	  	 
  // return to previous menu	 
  { 	       	       	  	 
    pos = top; // ???
    show();    	       	  	 
    return UP_MENU_TREE;  	 
  } 	       	       	  	 
  	     	     	  	 
  el = pos; // start at the current menu position
  while (el != NULL)   	     // check all items in current menu for key
  {  	       	       	  
    if (d == el->key)  	  
    {	     	     	  
      log_event("interpret - key match. cmd: %d\n", el->cmd);
      if (el->sub != NULL) 		 
      {	       	       	  		 
   	pos = el->sub; 	  		 
   	pos->show();   	  		 
      }	       	       	  		 
      return el->cmd;  	  	       	 
    } 	       	       	  		 
    el = el->next;     	  		 
  }// while el	     	  		 
  	     	     	  		 
  return NO_COMMAND; 
} // interpret std key
  	     	     		      
// end - menu.cc     	   	      
  	     	     		      
	     	     
		     

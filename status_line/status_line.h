/* status_line.h - status line display
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */
// use with danio
		  
#include "danio.h"
#include "pallet.h"

#ifndef STATUS_LINE_H  
#define STATUS_LINE_H  
		       
#define STATUS_LINE 24 
//#define STATUS_COLOUR 103 // bright white on dark blue
const int STATUS_COLOUR = pcolWHITE_ON_BLUE;
#define TIME_X  55 // x ordinate for the time on the status line
// for blanking the status line, must be blanks equal to TIME_X
#define STATUS_BLANK "                                                            " 
//                    12345678901234567890123456789012345678901234567890123456789012345678901234567890
//                    00000000011111111112222222222333333333344444444445555555555666666666677777777778        	       	   
#define PROMPT_LINE 25 // y ordinate of prompt
const int PROMPT_COLOUR = pcolLIGHTGRAY_ON_BLUE;
const int COLOUR_NORM = pcolLIGHTGRAY;
const int COLOUR_HIGHLIGHT = pcolWHITE;
		      
void clear_status_line();  		       
void say_status(char *msg, ...);
void clear_prompt();   
void say_prompt(char *msg, ...);
void bad_input();     
void text_norm(dwindow *win = NULL);     
void text_highlight(dwindow *win = NULL);
		      
#endif		      
// end - status_line.h

// stub.cc - test bed for status line
/* 
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */


#include "status_line.h"
#include "danio.h"
#include "dandefs.h"
	    	
int main() 	
{ 	    
  start_log();
  init_danio();	     
  
  // test status line
  clear_status_line();
  say_status("Hello World");
  
  // normal output
  text_norm();	  
  dcur_move(1,6); 	  
  dwrite("Normal output.");  
  
  // highlighted output
  text_highlight();    
  dcur_move(1, 8);	       
  dwrite("Highlighted output.");  
  
  // prompt
  say_prompt("This is a prompt message: ");
  drefresh(); 
  dgetch(); 
  	   
  say_status("Passing a number: %d", 10);
  drefresh();
  dgetch();
  
  close_danio();
  end_log();

  return 0;
}	      

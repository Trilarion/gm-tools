/*  prompt.cc - a line editor and input management system
 *
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */


#include <string.h>

#include "prompt.h"
#include "sat.h"
#include "danio.h"
#include "dandefs.h"
#include "err_log.h"

void prompt::pmove(int x, int y)
{
//  log_event("prompt::pmove - col(x): %d row(y): %d\n", col, row);
  col = x;
  row = y;
  redraw();
} // move

void prompt::redraw()
// re-writes the prompt and positions the cursor
{
//  log_event("prompt::redraw - col(x): %d row(y): %d\n", col, row);
  dcur_move(col, row);
  setpcolour(colour);
  dwrite(s);
  dcur_move(col + p_cur, row);
  drefresh();
} // redraw

int prompt::do_ed_cmd()
{
  int i; // counter
  if ((cmd >= ' ') && (cmd < 256))
  {
    if (mode == edOVER)
    {
      s[p_cur] = cmd;   // put it in the string
      p_cur++;          // advance the cursor
    } else {
      for (i = len-1; i > p_cur; i--) // go backward to avoid copy through
      {
             s[i] = s[i-1]; // shift the remainder right, loose the last
      }
      s[p_cur] = cmd;
      p_cur++;
    }
    if (p_cur >= len) p_cur = len - 1;
  } // end - normal typing
  else switch (cmd)
  {
    case(CMD_RIGHT):
    {
      p_cur++;
      break;
    }
    case(CMD_LEFT):
    {
      p_cur--;
      break;
    }
    case(CMD_BACKSPACE):
    {
      for (i = (p_cur - 1); i < (len - 2); i++)
      {
        if (s[i] == '\0') break;
        s[i] = s[i+1];
      }
      s[len-1] = ' ';
      p_cur--;
      break;
    }
    default:
    {
      break;
    }

  }
  // keep the cursor within the boundries
  if (p_cur >= len)
    p_cur = len-1;
  else if (p_cur < 0)
    p_cur = 0;

  redraw();

  return 0;
} // do ed cmd

int prompt::do_ed_cmd(int new_cmd)
{
  cmd = new_cmd;
  return do_ed_cmd();
}


int prompt::get_ed_cmd()
// controls the key bindings to prompt functions
{
  key = cmd = dgetch();
  if (cmd == -1) return NO_COMMAND;
  log_event("key: %d\n", key);


  switch(cmd)
  {
    case(danioKEY_ENTER):
//    case(danioKEY_BREAK):
    case(danioKEY_DOWN_ARROW):
    case(danioKEY_UP_ARROW):
    case(danioKEY_PGDN):
    case(danioKEY_PGUP):
    {
      cmd = CMD_QUIT;
      return CMD_QUIT;
    }
    case(danioKEY_LEFT_ARROW):
      cmd = CMD_LEFT;
      return CMD_LEFT;
    case(danioKEY_RIGHT_ARROW):
      cmd = CMD_RIGHT;
      return CMD_RIGHT;
    case(danioKEY_BACKSPACE):
      cmd = CMD_BACKSPACE;
      return CMD_BACKSPACE;
    default: // key unrecognised
    {
      if ((cmd >= ' ') && (cmd < 256) && (cmd != danioKEY_BACKSPACE)) // normal typing 
      {
        return cmd;
      }//fi: normal typing
      log_event("unrecognised input: %d\n", cmd);
      return cmd;
    }//default
  }//switch
  return cmd; // should never reach this point
}// get edit cmd

int prompt::setplen(int max_len)
// Sets the length of the prompt 'window'
// If max_len is not set in the call it is set to DEFAULT_LEN
// The function uses getmaxyx to check that max_len is withing the bounds of
//  the window and assigns the lesser of the two values to int prompt::len
// returns 0
{
  if (max_len > (dcur_maxx+1))  // the top left corner is 0,0
    len = dcur_maxx+1;
  else
    len = max_len;
  return 0;
}//setlent

int prompt::setpstr(char * ed_str)
{
  if (s) delete s;  // may be called multiple times in a NO_AUTHORITY prompt
  p_cur = 0;
  s = new_str(len);
  if (ed_str) strncpy(ed_str, s, len);
  return 1;
}// setstr

prompt::prompt(int max_len, char *ed_str,
                    int new_colour, int set_authority)
// prompt for user input
// max_len sets the maximum length of the input string
// s is an inial string to pass to the prompt to be edited
// new_colour is a setpcolour (danio) colour pair to briten up your day
// if full_authrity == FULL_AUTHORITY the prompt executes until an
//  appropriate exit key
//   is pressed. Otherwise each edit command must be fetched and passed to
//   the object seperately
{
  // initialise the prompt
  key = -1;
  s = NULL;
  setplen(max_len);
  setpstr(ed_str);
  psetcolour(new_colour);
  authority = set_authority;
  col = get_dcur_x();
  row = get_dcur_y();
  p_cur = 0; // start at the begining of the prompt
  mode = edINS; // start in insertion mode

  // edit the prompt
  if (authority == FULL_AUTHORITY)
  {
    (void) run();
    p_cur = max_len;
    redraw();
  } else
    dgetch_wait(0);
}// prompt

char *prompt::run()
// execute as a full authority prompt
{
  redraw();  // first draw for a full authority prompt
  cmd = get_ed_cmd();
  while (cmd != CMD_QUIT)
  {
//    if (cmd) log_event("command: %d\n", cmd);
    do_ed_cmd();
//    log_event("prompt: %s\n", s);
//    log_event("p_cur: %d\n", p_cur);
    cmd = get_ed_cmd();
  }
  return s;
};

prompt::~prompt()
{
  delete s;
};


// end - prompt.cpp





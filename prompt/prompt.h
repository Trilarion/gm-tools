/*  prompt.h - an input prompt utility
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */
	       	     
		       		 
// WARNING: this code must be used within a valid danio program
//          it will not work with the stdio implementation of danio

#ifndef PROMPT_H       	  	 		      	     	   
#define PROMPT_H

#include <stdlib.h>

#include "danio.h"
#include "keys.h"

#ifndef NO_COMMAND 
#define NO_COMMAND 0				 
#endif		   
#define CMD_QUIT 1000  		 		 
#define CMD_LEFT 1001  		 
#define CMD_RIGHT 1002 		 
#define CMD_BACKSPACE 1003	       	   
	       	       	  	 
// defaults    	       		 
#define DEFAULT_LEN 20    // prompt length
#define DEFAULT_COLOUR 7  // prompt colour pair
#define FULL_AUTHORITY 1		       
#define NO_AUTHORITY 0 			       
#define edOVER 1  // overstrike mode
#define edINS 0	  // insert mode     	       	    
     		       
class prompt	       			       	   			    
{    	  	       		      	       	   			    
public:	  	       		      	       	   			    
  prompt(int max_len = DEFAULT_LEN, char *ed_str = (char*) NULL,
     	 int new_colour = DEFAULT_COLOUR,      
     	 int set_authority = FULL_AUTHORITY);  
    // for a quick full authority prompt use prompt()      	       	
  int setplen(int max_len = DEFAULT_LEN);
    // Sets the length of the prompt 'window'		
    // If max_len is not set in the call it is set to 20
    // The function uses getmaxyx to check that max_len is withing the 
    //   bounds of the window and assigns the lesser of the two values 
    //   to int prompt::len	      
    // returns the final value of prompt::len
  int psetcolour(int new_colour) {colour = new_colour; return 1;}
  int setpstr(char * ed_str = (char*) NULL);    // set the string to edit 
  void redraw();  // redisplays the input and cursor 
  void pmove(int x, int y);    	      // move the prompt - for manual control
  int get_col() { return col; }	      // returns the prompt's column
  int get_ed_cmd();    	       	      // for manual control of the prompt
  int do_ed_cmd();     	       	      //   dito	    
  int do_ed_cmd(int new_cmd);         //   dito	 and can specify a command
  char * run();                       // execute as a full authority prompt
  ~prompt();   	       	       	      // destructor 
    // deletes s - make a copy if you wnat to keep it
  char * s; // the string in the prompt (the answer / user input, the big deal)
  int key; // the last keystroke caught by get_ed_cmd() initialised to -1
private:   	      	    	      		    
  int colour; // the palet colour to use with the prompt	     
  int len; // the maximum lenght of the prompt	    	     
  int mode; // the editing mode eg. edOVER, edINS  
  int cmd;  // the last read edit command	   
  int row, col, p_cur; // start row, start column, current x offset
  int authority; // a FULL_AUTHORITY prompt cause the program to pause for 
    // input, a NO_AUTHORITY prompt can be used in a real time program
};//prompt   	      	       			   	     
  	    	      	       			   	     
#endif	 	 	  
// end - prompt.h	  
 			  
 

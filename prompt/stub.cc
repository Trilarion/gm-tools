/*  stub.cc - a testbed for the danio prompt project
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */


#include "prompt.h"    	   
#include "dandefs.h"
#include "danio.h"
   		       
int main()  	       	   
{    	    	       	   
  prompt *name, *age;  
  int cmd;	       
   		       
  // initilization     	    
  start_log();  // start the error log - dandefs.h
  init_danio();	       
  log_event("init ok\n");
  log_event("maxx: %d\n", dcur_maxx);
  	       	       
  // pose a question   
  dwrite("Hello, what is you're name: ");
  log_event("greeting ok.\n"); 
  drefresh();
  name = new prompt(); // conduct a default prompting
  log_event("prompt ok\n"); 			     
  dwrite("\n");	// NOTE: carage return is not echoed by prompt

  // report the answer 
  name->psetcolour(7); 
  dwrite("Hello %s.\n", name->s);  // NOTE use sat.h buffstr to remove white space
  delete name; 	       

  // pose a second question
  dwrite("How old are you? ");
  drefresh();
  age = new prompt(5, NULL, 7, NO_AUTHORITY); // initiate a no authority prompt
  do 	       	       
  {	       	       
    cmd = age->get_ed_cmd();
    if (cmd) // ie not NO_COMMAND which equals 0  
      log_event("%d\n", cmd); 
    age->do_ed_cmd(); 
  } while (cmd != CMD_QUIT); // several keys cause CMD_QUIT  	      
    	       	
  dwrite("\n");	
  dwrite("You are %s.", age->s);
  delete age;  		 
    	     
  drefresh();
  (void) dgetch(); 
  close_danio();
  end_log();   	  

  return errNO_ERROR;
} // main      	  
   	       	  
// end - stub.cc (prompt)    
 	     
	     

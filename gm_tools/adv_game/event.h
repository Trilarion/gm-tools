/*  event.h - event object for adv_game
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

			  
#ifndef EVENT_H		  
#define EVENT_H		  
			  
#include <time.h>	  
#include "adv_character.h"
			  
// list of action codes	  
#define actNO_ACTION 0	  
			  
			  
class event		  
{			  
public:   		  
  event(adv_character *new_who, adv_character *new_target, 
	time_t new_when, int new_when_ms, int new_what);   	   
    // setup (but not link) a new event		      
  ~event(); 		  			      
    // delete an event, without disrupting list	      
    // NB: don't delete the head of the list unless you have some other 
    // link to the list	  
  long int get_time_diff(event *some_event);
    // find (this.when - some_event.when) in milliseconds 
    // (when_ms is included too)
  void add(event *new_event);
    // add a new event, maintaining sort by time
       			  
  event *next;	// doubly linked list
  event *prev;		  
  adv_character *who; // instigator of the event
  adv_character *target; // target (for attacks) tk - multiple target ???
  time_t when; // execution time in seconds since epoch
  int when_ms; // execution time milli second extension
  int what; // action code
};     			  
       
#endif 

// end - event.h

/* adv_character.cc - character object for adv_game
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

		       
#include <stdio.h>     
#include <stdlib.h>    
#include <string.h>    
#include <assert.h>    
  	      	       
#include "hit.h"       		   
#include "adv_display.h"	   
#include "adv_character.h"

#include "dandefs.h"	
#include "err_log.h"	 
#include "danio.h"		   
#include "prompt.h"
#include "sat.h" 
#include "status_line.h"

#ifndef PAGES
// PAGES title, stats, combat, skills, perks, gear, powers, disadvantages
#define PAGES 8		       
#define TITLE_PAGE  1  	       
#define STAT_PAGE   2  	       
#define COMBAT_PAGE 3  	       
#define SKILL_PAGE  4  	       
#define PERK_PAGE   5  	       
#define GEAR_PAGE   6  	       
#define POWER_PAGE  7  	       
#define DISADVANTAGE_PAGE 8    
#endif
				   
#define top 2
#define lft 1			   
#define buf 0			   
#define cur_y_default top + buf + 1

int adv_character::earn_xp(int new_xp)
{ 	    	       
  if (new_xp < 0)      
    return -1;	       
  else 	    	       
    ch_experience += new_xp;      
  return ch_experience; 	       
} // earn xp	       
  	    	       
int adv_character::spend_xp(const int stat_id, const int xp_amnt)
// spend xp  	   
// stub: this cumbersom use of identifiers will be replaced by the more 
// streamline system inherited from character when a full conversion of 
// the interface is done     	       	       	       	  
{ 	     	       				  
  switch (stat_id)     
  {	    	   
    case sSTR:
    { 	      	       
      if (xp_amnt >= cSTR)  
      {	    	       	      
       	ch_str->change_value(1); ch_experience -= cSTR;
      }	    	       	       
      break;	       	       
    }	    	       	       
    case sDEX:       
    { 	      	       
      if (xp_amnt >= cDEX)  
      {	    	       	      
       	ch_dex->change_value(1); ch_experience -= cDEX;
      }	    	       	       
      break;	       	       
    }	    	       	       
    case sBODY:      	       
    {	      	       	       
      if (xp_amnt >= cBODY)     
      {	       	       	       
       	ch_body->change_value(1); ch_experience -= cBODY;
      }	      	       	       
      break;  	       	       
    }	  	       	       
    case sPD:      	       			    
    {	    	       	       			    
      if (xp_amnt >= cPD)     			    
      {	    	       	       			    
       	ch_pd->change_value(1); ch_experience -= cPD;
      }	     	       	       
      break; 	       	       
    }	     	       	       
    case sSTUN:         	       			    
    {	      	       	       			    
      if (xp_amnt >= cSTN)     			    
      {	     	       	       			    
       	ch_stn->change_value(1); ch_experience -= cSTN;
      }	    	       	       
      break;	       	       
    }		       	       
    case sWIELDED:         	       			    
    {	      	       	       			    
      if (xp_amnt >= cWIELDED)     			    
      {	
	assert(wielded_sk != NULL);

	wielded_sk->change_value(1); ch_experience -= cWIELDED;
      }	    	       	       
      break;	       	       
    }		       	       
    default: break;    	       
  } // switch : stat_id
  	    	       
  return ch_experience;	       		   
} // spend xp	       			   
		       
void adv_character::rest()
// resting heals wounds but (should) costs time 
// and can only be done in a safe place
{	 	       	  	     
//  printw("An inn costs $10 per night.");
//  printw("You will recover 1 hit per night.");
  dwindow *win = display->event_win;  	      
       		       	       	     
  ch_wounds = 0;   		     
  w_stun = 0;	   		     
  dwrite(win, "\nYou have rested.");
  show_body(win);    	       	     			   
  drefresh(win);       	       	     			   
} // rest 	       	       	     			   
       		    		     			   
void adv_character::end_seg_recovery()			   
// free end segment recovery	     			   
{      				     			   
  w_stun -= ch_stn->pts_to_value() / END_SEG_RECOVERY_RATIO;
  if (w_stun < 0) w_stun = 0;	       		      	   
  if (display->detail_state == detCHAR) show();
}      			     	       		      	   
       			     	       		      	   
void adv_character::end_turn_recovery()		      	   
// free end turn recovery    	       		      	   
{      			     	       		      	   
  w_stun -= ch_stn->pts_to_value() % END_SEG_RECOVERY_RATIO;
  if (w_stun < 0) w_stun = 0;	       		      	   
  if (display->detail_state == detCHAR) show();
}   		   		       		      	   
    		   		       		      	   
void adv_character::half_recovery()    		      	   
// spend segment recovering	       		      	   
{   		   		       		      	   
  w_stun -= ch_stn->pts_to_value() / FULL_SEG_RECOVERY_RATIO;
  if (w_stun < 0) w_stun = 0;	     			   
  if (display->detail_state == detCHAR) show();
}   		   		     

void adv_character::end_combat_recovery()		      	   
// end combat recovery     	       		      	   
{      	     		     	       		      	   
  w_stun = 0;			     
  if (display->detail_state == detCHAR) show();
}		       		     		  
  	       	       		       		  
int adv_character::adv_save(FILE *out)         	      	       
{ 	 	       	  	       		  
  char temp[MAX_LINE]; 				  
  		       				  
  log_event("adv_character::adv_save()\n");	  
  say_status("Saving the character."); 		  
    	 	       	  	       		  
  // put special items in the gear list		  
  sprintf(temp, "cash %d", cash);      		  
  new gear(gear_head, temp, 0);       	  	  
  log_event("adv_character::adv_save - Gear cash setup.\n");
  /* these should no longer be needed
  sprintf(temp, "healing potions %d", potions);   
  new gear(gear_head, temp, 0);	      		  
  log_event("adv_character::adv_save - Gear potions setup.\n");
    		       	    			   
  sprintf(temp, "sword %d %d", weapon_dc, weapon_ocv); 
  new gear(gear_head, temp, 0);	      		   
  log_event("adv_character::adv_save - Gear weapon setup.\n");
    		       	    			  
  sprintf(temp, "shield %d", shield); 		  
  new gear(gear_head, temp, 0);	      		  
  log_event("adv_character::adv_save - Gear shield setup.\n");
    		       	    			  
  sprintf(temp, "armour %d", armour); 		  
  new gear(gear_head, temp, 0);	      		  
  log_event("adv_character::adv_save - Gear armour setup.\n");
  */     	       	       	    	      	
  save(out); // save the character using the inherited method
  log_event("adv_character::adv_save - Inherited save completed.\n");
    		       	    	      	
  setpcolour(EVENT_COLOUR); 	      	     
  dwrite(display->event_win, "\nCharacter saved.");
  drefresh(display->event_win);	      	     
  say_status("Character saved.");
  log_event("adv_character::adv_save - end\n");
    	
  return SUCCESS;			
} //save	   	       	       	      	     
	
void adv_character::add_item(gear *new_item)
// add a new item to the gear list	    
{  
  log_event("adv_character::add_item()\n");
  if (new_item == NULL) return;
  new_item->next = gear_tail;  	   
  new_item->prev = gear_tail->prev;
  gear_tail->prev->next = new_item;
  gear_tail->prev = new_item;
  log_event("adv_character::add_item - item gained\n");
  
}// add item 	    	       
			       
gear *adv_character::drop_current_item()
// remove the current item from the gear list and return a pointer to it
{ 	      	       	       
  gear *item;	       	       
		     
  		     
  log_event("adv_character::drop_current_item()\n");
  		     	  
  // disallow imposible drops  
  if (page != GEAR_PAGE || current == gear_head || current == gear_tail)
  {		     	  
    log_event("adv_character::drop_current_item - cannot drop that item\n");
    return NULL;     	  
  }		     	  
  		     	  
  assert(current != NULL);     		  
  assert(current->prev != NULL);	  
  assert(current->next != NULL);	  
  // unready the item 
  unready_item((gear*) current);		     
  		     
  current->prev->next = current->next;	  
  current->next->prev = current->prev;	  
  item = (gear*) current;      		  
  current = item->next;	       		  
  log_event("adv_character::drop_current_item - item droped\n");
  	    	     
  return item;	       			
}// drop current item 		       			
		       
      		       		      	    
ability *adv_character::get_item(const char *item)	       	    
// get an item from the characters gear list  
{      					      
  ability *look;  	       	       	       	    
  int len = strlen(item);		    
  int not_found = TRUE;   	     	    
      	 	      	     		    
  look = gear_head->next;	    
  while ( (look != NULL) && (not_found) )   
  {   	 				    
    if ((look->name != NULL) && (!strncmp(item, look->name, len)))
      not_found = FALSE; // item found	    
    else      				    
      look = look->next;	    
  } // while: looking for item		    
  return look;	    			    
} // get item	    			    
		
//#define CONSOLIDATE 0
//#define SELECT_BEST 1
  		     
void adv_character::load_item(int& item, const char *item_name)
// load items from inventory to special storage
{	     	     	   		    
  ability *look;    	     	      		    
  int len = strlen(item_name);		    
  int val; // tempory value   		    
	     	    	      		    
  item = 0;  	    	      		    
  look = get_item(item_name); 		       
  if (look != NULL)  	      		       
  {    	     	     	      		       
    sscanf(look->name + len, "%d", &val);   	       
    item += val;     	      		       
    delete look;    	     	       
  } // fi: found item	      		       
}// load item	    	      		    
    	   	    		    	    
void adv_character::load_item(int& item1, int& item2, const char *item_name)
// load items from inventory to special storage
{   	     	     	   	    	    
  ability *look;    	     	      		    
  int len = strlen(item_name);	    	    
  int val1, val2; // tempory values   	    
    	     	    	      	    	    
  item1 = 0; item2 =0;		      	    
  look = get_item(item_name); 	    	       
  if (look != NULL)   	      	    	       
  {    	     	      	      	    	       
    sscanf(look->name + len, "%d%d", &val1, &val2);	       
    item1 += val1; item2 += val2;   	    
    delete look;    	     	       
  } // fi: found item	      	    	       
}// load item	    		    
  		    		    
  	     	    		    
int adv_character::ready_item(gear *item)
// load in details from the listed item
// all item descriptions are in the format:
// item_name [[[detail: value] detail: value] ...]
// where item_name: is the description of the sigular with underscore instead 
//                  of spaces	    	 
//       detail: is an abreviation for a detail of the item such as rDef or OCV
//       value: is a numeric value assigned to that detail
// tk: items will be based on powers and a common loading in system used based
//     on the powers
// STUB: details not crossmatched with other items: tk
{      	     	    	     	       	 
  int temp;	    	     		
  if (strstr(item->name, "sword") != NULL)  	 
  {    	   	    	     	    	 
    sscanf(item->name + 6, "%d %d", &weapon_dc , &weapon_ocv);
    if (id == ID_PLAYER) dwrite(display->event_win, "\nSword equiped - dc: %d ocv: %d", 
    	   weapon_dc, weapon_ocv);  
    item->make_ready();
    drefresh(display->event_win);
    return SUCCESS;   	     	 			      
  } else if (strstr(item->name, "shield") != NULL)
  { 	   	       	     		 	 	      
    sscanf(item->name + 7, "%d", &shield);     	 
    if (id == ID_PLAYER) dwrite(display->event_win, "\nShield equiped - dcv: %d", shield);
    item->make_ready();	 
    drefresh(display->event_win);
    return SUCCESS;   	     		 		      	     
  } else if (strstr(item->name, "armour") != NULL)		      
  { 	   	    	     		  	 	      
    sscanf(item->name + 7, "%d", &armour);     	 	      
    if (id == ID_PLAYER) dwrite(display->event_win, "\nArmour equiped - rDef: %d", armour);
    item->make_ready();	 
    drefresh(display->event_win);
    return SUCCESS;   	     		 		      
  } else if (strstr(item->name, "potion") != NULL)		      
  { 	   	    	  	  	  		      
    sscanf(item->name + 7, "%d", &temp);	  		      
    potions += temp; 	  	       	  		      
    if (id == ID_PLAYER) dwrite(display->event_win, "\nPotion equiped - total: %d", potions);
    item->make_ready();	 
    drefresh(display->event_win);
    return SUCCESS;   	  				      
  } 	   	    	  				      
  return errNO_MATCH_FOUND;   					      
}// ready item	 	 
    	   	 	 			   
int adv_character::unready_item(gear *item)	   
// unload details of the item			   
// STUB: details not crossmatched with other items: tk
{   	   	   	 			   
  int temp; 		 			   
	   		 			   
  log_event("adv_character::unready_item()\n");	   
  	   		 			   
  if (strstr(item->name, "sword") != NULL)  	   
  {    	   	 	 			   
    weapon_dc = 0; weapon_ocv = 0;		   
    if (id == ID_PLAYER) dwrite(display->event_win, "\nSword unequiped.");
    item->make_unready();			   
    log_event("adv_character::unready_item - sword unreadied\n");
    drefresh(display->event_win);
    return SUCCESS;   	     	 		   	      
  } else if (strstr(item->name, "shield") != NULL) 
  { 	   	       	     		 	   	      
    shield = 0;     	 			   
    if (id == ID_PLAYER) dwrite(display->event_win, "\nShield unequiped.");
    item->make_unready();			   
    log_event("adv_character::unready_item - shield unreadied\n");
    drefresh(display->event_win);
    return SUCCESS;   	     	 		   	      
  } else if (strstr(item->name, "armour") != NULL) 		      
  { 	   	    	     		  	   	      
    armour = 0;     	 			   
    if (id == ID_PLAYER) dwrite(display->event_win, "\nArmour unequiped.");
    item->make_unready();			   
    log_event("adv_character::unready_item - armour unreadied\n");
    drefresh(display->event_win);
    return SUCCESS;    	     		 	   	      
  } else if (strstr(item->name, "potion") != NULL) 		      
  { 	   	       	  	  	  	   	      
    sscanf(item->name + 7, "%d", &temp);	   		      
    potions -= temp;   	  	       	  	   	      
    if (id == ID_PLAYER) dwrite(display->event_win, "\nPotion(s) unequiped - total: %d", potions);
    item->make_unready();			   
    log_event("adv_character::unready_item - potion unreadied\n");
    drefresh(display->event_win);
    return SUCCESS;    	  		      		      
  }	   	       	  		      		      
  log_event("adv_character::unready_item - end\n");
  return errNO_MATCH_FOUND;   					      
}// un_ready item      	 
  	   	       	 
void adv_character::toggle_current_item_rediness()
// stub	   	       
{ 	      	       
  gear * item; 	       
  say_status("toggle rediness");
  	      	       
  if (page != GEAR_PAGE || current == gear_head || current == gear_tail)
  {	      	       	  
    log_event("adv_character::toggle_current_item_rediness - cannot toggle that item\n");
    return;       	  
  }    	      	       
  item = (gear*) current;
       	   	       
  if (item->is_ready())
  {    	   	       
    unready_item(item);
  } else { 	       	     
    ready_item(item);	       
  }//fi    	
}// toggle current item rediness 
  	   
  	     	    	 	    			      
int adv_character::adv_load(FILE *in) 	      	       	      
{      	   	       	      	      	       		      
  char temp[MAX_INPUT];       	      	       		      
  int val, val2; // tempory values	       	     	      
  ability *look; 
  gear *glook;	 // look at the gear
  weapon_rank *wlook; // look at the weapons 
  int not_found;		    
      	 			    
  say_status("Loading the saved character.");
      	      	       	      	     	       
  load(in); // use the inherited method	       
      	      	       	      	     	       
  // search weapon ranks for sword skill as it is the only weapon wielded
  not_found = TRUE;	    	    
  wlook = (weapon_rank*) combat->next;	       	     
  while ((wlook != NULL) && (not_found))      	     	       
  {    	      	       	      	     	       
    if (wlook->name != NULL && !strncmp("swords", wlook->name, 6))
    // found the sword      	      	     	       		      
    {  	       	       	    	    		       		 
      wielded_sk = wlook;   	    		       		 
      not_found = FALSE;	    
    } else	     	    	    		       		 
      wlook = (weapon_rank*) wlook->next;	       		       
  } // while: looking for weapon rank
  if (not_found) // character must have a weapon skill else 	 
                 // improvements made in the game will not be saved
  {   		 	    	    		       		 
    wielded_sk = new weapon_rank(combat, "swords", cWIELDED, 0); // tk link to combat
  }   			    	    
      			    	    
  // Equiptment: go through gear looking for special items 
      	     	   	      	    	       
  // search for cash	      	    	    
  load_item(cash, "cash");  // cash is treaded specially as we don't want it on the equiptment list ???
  /* old way of loading gear	    
  // search for potions	      	     	    
  load_item(potions, "healing potions");    
  // search for sword  (weapon_dc weapon_ocv)	   
  load_item(weapon_dc, weapon_ocv, "sword");	   
  // search for shield	      	      	    	   
  load_item(shield, "shield");         	       	   
  // search for armour	      	    
  load_item(armour, "armour");	    
  */  	   	 	    	    
  	   			    
  // loop through all items and ready those that are ready according to 
  // the save file
  look = gear_head->next;
  while (look != NULL)	 
  {	   		 
    glook = (gear*) look;
    if (glook->is_ready())
      ready_item(glook); 
    look = look->next;
  }// while
  				    
  				    
  w_stun = 0;		    	    
   			    	    
  say_status("Character loaded from %s.", DEFAULT_CHAR_FILE);
  setpcolour(EVENT_COLOUR); 	 				 
  dwrite(display->event_win, "\nCharacter loaded.");		 
  drefresh(display->event_win);	 				 
  return SUCCESS;	    
}//load	   	       	     	    				 
  		    	   
void adv_character::drink_potion()	    			 
// drink a healing potion    	    				 
{   		       
  dwindow *win = display->event_win;
  		       
  if (potions != 0)    	     		
  { // use a potion and report remianinder
    potions--; 	       	     		
    ch_wounds -= d_random(POTION_STRENGTH);
    if (ch_wounds < 0) ch_wounds = 0;
    setpcolour(EVENT_COLOUR);  
    dwrite(win, "\nYou have drunk a healing potion.");
    show_body(display->event_win);		  
    dwrite(win, "\nYou have %d potions remaining.", potions);
    drefresh(win);     
  } else { // no potions     	 	  		  
    say_status("You have no healing potions.");
  }//fi	       	       	     
}//drink potion	       	       
     	   	     	       
void adv_character::take_hit(hit_data *hit, dwindow *win)
// take a hit  	       	       	 		       		  
{ 		      
  char *say_location[2][19] =  		     
  {    	       	      					  
    NULL, NULL, NULL, "head", "head", "head", "left hand", "left arm",
    "left arm", "left shoulder", "chest", "chest", "stomach", "vitals",
    "left thigh", "left leg", "left leg", "left foot", "left foot", 
    NULL, NULL, NULL, "head", "head", "head", "right hand", "right arm",
    "right arm", "right shoulder", "chest", "chest", "stomach", "vitals",
    "right thigh", "right leg", "right leg", "right foot", "right foot" 
  };	  		  				  
     	   		       		       	 	  
  int subt_body = 0, subt_stun = 0; // damage sub_totals
  int hit_location_roll = 0;   		       	 
  int side;		       		       	 
  int stun_x;	   	       		       	 
  int i; // counter	       		       	 
  float n_stun_x;      	       		       	 
  float body_x;	       	       		       	 
    	    	       	       		       	 
  // get misses out of the way first to avoid doing unnesseary work
  if (hit->margin < 0) // ie: miss	       	 	
  { 	    	       	       		       	 	
    if (id == ID_PLAYER)       		       	 	
    { 	    	       	       		       	 	
      dwrite(win, "\nYou have been missed."); 	 	
    } else {	       	       		       	 	
      dwrite(win, "\nYou missed the beast.");         	    	
    }	     	       	       		       	 	
    	    	       	       		       	 
  } else { // a hit    	       		       	 
    // hit location    	       		       	 
    for (i = 0; i < 3; i++) hit_location_roll += d_random(6);
    side = d_random(2) - 1;    		       	 
    switch(hit_location_roll)  		       	 
    {	    	       	       		       	 
      case 3: case 4: case 5: // head	       	 
        stun_x = 5; n_stun_x = 2; body_x = 2; break;
      case 6: // hands 	       		       	 
        stun_x = 1; n_stun_x = 0.5; body_x = 0.5; break;
      case 7: case 8: // arms  		       	 
        stun_x = 2; n_stun_x = 0.5; body_x = 0.5; break;
      case 9: // shoulders      		       	 
        stun_x = 3; n_stun_x = 1; body_x = 1; break;
      case 10: case 11: // chest	       	 
        stun_x = 3; n_stun_x = 1; body_x = 1; break;
      case 12: // stomach      		       	 
        stun_x = 4; n_stun_x = 1.5; body_x = 1; break;
      case 13: // vitals       		       	 
        stun_x = 3; n_stun_x = 1; body_x = 2; break;
      case 14: // thighs       		       	 
        stun_x = 2; n_stun_x = 1; body_x = 1; break;
      case 15: case 16: // legs		       	 
        stun_x = 2; n_stun_x = 0.5; body_x = 0.5; break;
      case 17: case 18: // feet		       	 
        stun_x = 1; n_stun_x = 0.5; body_x = 0.5; break;
    }// switch: hit location   		       	 
    	    		       		       	 
    // determine stun according to location    	 
    if (hit->damage_type == KILLING)	       	 
      hit->stun_damage = hit->body_damage * stun_x ; //tk weapon stun_x bonus
    else    	      	       	   	       	  	    
      hit->stun_damage = (int) (hit->stun_damage * n_stun_x + 0.5);
    	  		       	 	       		    	 
    // apply defences	       	      	       	       	    	  
    // apply armour (resistant defences)       			 
    subt_body = hit->body_damage - armour;      	  		 
    subt_stun = hit->stun_damage - armour - ch_pd->pts_to_value();
    // apply pd (non resistant defence) for normal attacks	 
    if (hit->damage_type == NORMAL) subt_body -= ch_pd->pts_to_value();
    	  		       	 	       			 
    // apply hit location body multiple	       			 
    subt_body = (int) (subt_body * body_x + 0.5);
    	  		       		       			 
    // stun must be >= body    		       			 
    if (subt_stun < subt_body) subt_stun = subt_body;		 
    	  		       		       			 
    // ensure defences only reduce damage to minimum of zero  	 
    if (subt_body < 0) subt_body = 0;	       	       		 
    if (subt_stun < 0) subt_stun = 0;	       	       		 
      	    	      	       		       	  
    //update character 	       		       	       
    ch_wounds += subt_body;    		       	  
    w_stun += subt_stun;       		       	  
      	      	     	       		       	  
    // report result  	       		       	  
    if (id == ID_PLAYER)       		       	  
    { 	      	       	       		       	  
      dwrite(win, "\nYou have been hit in the %s \nfor %d Body %d Stun.", 
              say_location[side][hit_location_roll], hit->body_damage,
  	      hit->stun_damage);
      if (subt_body > 0) setpcolour(WARNING, win);     
      else if (subt_stun > 0) setpcolour(CAUTION, win);
      dwrite(win, "\n * You take %d Body %d Stun. *", subt_body, subt_stun);
      setpcolour(GENERAL, win);		    
    } else {         	       	 	       	      	       	     	
      // assuming monster      	 	       	      	       	     	
      dwrite(win, "\nYou hit the beast in the %s \nfor %d Body %d Stun.", 
              say_location[side][hit_location_roll], hit->body_damage,
  	      hit->stun_damage); 	       	    
      if (subt_body > 0) setpcolour(ACTION, win);	
      dwrite(win, "\nIt takes %d Body %d Stun.", subt_body, subt_stun);
      setpcolour(GENERAL, win);
    } // fi: ID	     	       		       	   
  }// fi: hit or miss	       		       	   
}// character take hit 	       	     	       			  
    	    	       	       	     	       			  
    	    	       	       	     	       			  
    	    	       	       	     	       			  
hit_data *adv_character::attack(adv_character *target)			  
// attack the target and record hit data       			  
{   	    	       	       	     	       			  
  int roll, open_roll; // a dice roll          	 		  
  int hit_chance; // chance of hitting	       			  
  int margin;	   	     	     	       
  int val; // for storing tempory values
  int i; // counter	     	     
  int damage_dice; 
  int dice_mod = 0;
  int body_done;   	     	     
  int stun_done;
  int attack_type = NORMAL;
  hit_data *hit = NULL; 	     
     	       	       	      	     
  // player attacks    	      	     
  open_roll = roll = d_random(100);      	 
  if (roll < 6)	       	      	     
    do { // open ended low    	     
      roll = d_random(100);   	     
      open_roll -= roll;      	     
    } while (roll > 95);      	     
  else if (roll > 95)  	      	     		
    do { // open ended low    	     		
      roll = d_random(100);   	     		
      open_roll += roll;      	     		
    } while (roll > 95);      	     		
  roll = open_roll;    	      	     		
    	       	 
  assert(wielded_sk != NULL);
  hit_chance = 50   // base  	     		
                   // ocv    	     	     	
               + ch_dex->pts_to_value()      	
       	       + wielded_sk->pts_to_value()  	
       	       + weapon_ocv                  	
               -(  // targets dcv       	     
               target->ch_dex->pts_to_value() 	  // targets dcv
       	       + target->shield	     	     	
               );  	       	     	     	
  margin = hit_chance - roll; 	     		
  if (margin >= 0)           	     		
  { 	       	     	     	     		
    // now determine damage  	     		
    body_done = 0;   	     	     		
    stun_done = 0; 				
    if (weapon_dc > 0)				
    {  	       	   				
      // character has a weapon			
      damage_dice = weapon_dc / 3;
      if (weapon_dc % 3 == 1)	  
        dice_mod = +1;
      else if (weapon_dc % 3 == 2)
      {	       	       
        damage_dice += 1;
        dice_mod = -1;
      } else   	       
        dice_mod = 0;
      attack_type = KILLING;			
      // tk - damage for str over minimum	
      for (i = 0; i < damage_dice; i++)	   	
      {        		     	     	   		
        body_done +=  d_random(6);    	   	   	
      }// for: roll damage
      body_done += dice_mod;
      if (body_done < 1) body_done = 1;
    } else {   		    			
      // hands: use strength			
      val = ch_str->pts_to_value();    		
      damage_dice = val / 5; if (val % 5 > 2) damage_dice++;
      for (i = 0; i < damage_dice; i++)	   	
      {        		     	     	   		
        roll = d_random(6);    	   	   	
        stun_done += roll;     	   	   	
        if (roll > 1) body_done++;   	   	
        if (roll == 6) body_done++;  	   	
      }// for: roll damage		   
    } // fi: attack type: Normal or Killing
    	   
    hit = new hit_data(body_done, stun_done, margin, attack_type);
  } // fi  	     	  	     	  			
       	   	     	     	   				
  else 	   	       	      	   				
    hit = new hit_data(0, 0, margin);
  assert(hit != NULL); 	      	   		
    	 	       	      	   		
  return hit;	       	      	   		
}// character attack   	      	   		
  	    	       	      	   		
    	     	       	     	   	     
int adv_character::dead()	     	     
{   	     	   
  int dead = (ch_wounds >= (ch_body->pts_to_value()*2)); 
  if (dead)			   			 
  {				   			 
    log_event("adv_character::dead - wounds: %d\n", ch_wounds);
    log_event("adv_character::dead - Body: %d\n", ch_body->pts_to_value());
  }				   
  
  return dead;
} // character dead    	       	 	     
    					     
int adv_character::unconcious()		     
{   					     
  return (w_stun >= ch_stn->pts_to_value());
} // character unconcious


// *************************  OLD DISPLAY RUTINES
// 
// 
// 
    	       	 
void adv_character::show_body(dwindow *win)
// display the characters body       
{    	     	    	       
  int body = ch_body->pts_to_value();
  dwrite(win, 	       	       	   
       	  "\nBody %d Wounds %d \nCan take %d more hits", 
     	 body, ch_wounds, (body * 2 - ch_wounds));
}// character show body      	   
   			       
void adv_character::show_pd(dwindow *win)
// display the characters pd       
{    	     	    	       
  dwrite(win, "\nPD %d", ch_pd->pts_to_value());
}// character show body      	   		 
   
void adv_character::show_str(dwindow *win)
// display the characters Strength       
{    	     	    	       	  
  dwrite(win, "\nStrength %d", ch_str->pts_to_value());
}// character show strength
   		     	       
void adv_character::show_stun(dwindow *win)
// display the characters stun
{    	     	     	       
  int stun = ch_stn->pts_to_value();
  dwrite(win, 	       	       	   
       	  "\nStun %d Taken %d \nCan take %d more.", 
     	 stun, w_stun, (stun - w_stun));
}// character show stun	    
     	     	       	     
void adv_character::show_dex(dwindow *win)
// display the charaers dex score   
{  	     	       	     	      
  dwrite(win, "\nDex %d", ch_dex->pts_to_value());
}//show dex	       	      	      
   	     	       	      	      
void adv_character::show_xp()     	      
// display the characters xp	      
{  	     	       	      	      
  dwrite(display->detail_win, "\nxp %d   ", ch_experience);      
}// character show xp 	      	      
   	     	      	      	      
void adv_character::show_cash(dwindow *win)
// display the charares cash  	      
{  	     	     	      	     
  dwrite(win, "\ncash $%d", cash); 
}// character show cash	      
   	     	    	      
void adv_character::show_potion() 
// display the number of healing potions
{  	     	       	      		
  dwrite(display->detail_win, "\nHealing potions: %d", potions);  
}  	     	 	      	 
void adv_character::show_wounds() 	 
// show wounds score	  	 
{  		 	      	 
  dwrite(display->detail_win, "\nWounds: %d", ch_wounds);
}// show wounds	     	       	 	    
   	       	     
void adv_character::set_name(const char *new_name) 
{    		     
  if (ch_name != NULL) delete ch_name; 
  ch_name = new_str(new_name); 
}// set name	     
     		     
char *adv_character::get_name() 
{    	    	     
  return ch_name;    
}// get name 	     
		     
void adv_character::show_name(dwindow *win)    		    
// show character's name       		    
{  		     	       		  
  if (win == NULL) win = display->detail_win;
  dwrite(win, "\nName: %s", ch_name); 
}// show name		       	  	    
   
void adv_character::show_weapon_skill(dwindow *win)
// display ranks with weapon	    		 		       
{  		      	    	
  assert(wielded_sk != NULL);
  dwrite(win, "\n+%d with %s", wielded_sk->pts_to_value(), wielded_sk->name); 
} // show weapon skill						       
   		  
void adv_character::show_combat_gear(dwindow *win)
// show important equiptment associated with combat
{  		     			     	   
  dwrite(win, "\nCombat Gear");
  // show sword	    
  dwrite(win, "\nSword %d DC +%d OCV", weapon_dc, weapon_ocv);
  // show armour    			     		    
  dwrite(win, "\nArmour %d rDEF", armour);
  // show shield  			 
  dwrite(win, "\n%d shield bonus", shield);  
} // show combat gear

//	
//   	
//         
// ******************** NEW DISPLAY RUTINES CONVERTED FROM CHARACTER.CC
// 
// 
// 
   
void adv_character::show_subtotal_stats()
// re-display the subtotal for stats
{      
  dwindow *win = display->detail_win;
  int sub_total = sum_stats();
  
  text_norm(win); 	
  dcur_move(1, stat_btm, win);
  dwrite(win, "   Total Stat Cost: %6d  ", sub_total);
}// show subtotal stats
   	       	       		  			     
void adv_character::show_stat_page()			     
{      	       	     		  
  dwindow *win = display->detail_win;
   	       	    		  
  text_highlight(win);   	       	  		   
  // heading for stats columns	  
  dcur_move(lft+buf, top+buf, win);		  
  dwrite(win, "Val Char Base Cost Max Pts");
       	       	       		   
  text_norm(win); 	      		  
  // show the stats   		   
  ability * look;     		   
  look = ch_stats->next; // first ability in list;
  int i = cur_y_default; 	      	  	   
  while (look != NULL)	    	  
  {  	       	      	    	   
    look->show(1, i, win);  	  
    look = look->next; i++; 	   
  }  	       	    	    	   
  show_subtotal_stats();    	   
       	       	    	    	   
  // show highlighted stat  	   
  text_highlight(win);
  // assert(cury > stat_top-1);	   
  current->show(1, cury, win);	  
}//show_stat_page    		   
   	       	     		   
void adv_character::show_combat_page() 
{  	       	    		  
  dwindow *win = display->detail_win;
   	       
  ability *look;     	     
  int total = 0; //total combat cost
  int i;       	 
   	       	 
  text_highlight(win);
  dcur_move(lft+buf, top+buf, win);
  dwrite(win, "Ranks Field @cost pts");
   	       	      	    
  text_norm(win); 	      
  look = combat->next;	    
  i = cur_y_default;       	    
  while (look != NULL)	    
  {    	       		    
    look->show(1, i, win);  
    total += look->get_pts();
    look = look->next;	    
    i++;       		    
  } // while   		    
  text_highlight(win);	    
  current->show(1, cury, win);
       	       	 	    
  total -= c_tail->get_pts(); // not a real node
       	      	 	    
  dcur_move(lft+buf, i+1, win);  
  text_norm(win);	 	     
  dwrite(win, "Total Combat Cost: %d", total);
  drefresh(win); 
}//show_combat_page    		
      	       	       		
      	       	       		
void adv_character::show_skill_page()
{
  dwindow *win = display->detail_win;
  int total = 0;	       		
      	       	       
  text_highlight(win);
  dcur_move(lft+buf, top+buf, win);
  dwrite(win, "Val Skill Base @cost pts");
   	       	       		
  text_norm(win);
  ability * look = skill_head->next;
  int i = cur_y_default;
  while (look != NULL) 		
  {	       			
    if (look != current)	
    {	       			
      look->show(lft+buf, i, win);
    }	       			
    else       			
    {	       			
      text_highlight(win);		
      look->show(lft+buf, i, win);  	
      text_norm(win);		
    }  	       			
    total += look->get_pts();	
    look = look->next;		
    i++;			
  }				
  dcur_move(lft+buf, i+1, win);
  text_norm(win);
  dwrite(win, "Total Skill Cost: %d", total);
}// show skill page		

void adv_character::show_perk_page()
{    	 
  dwindow *win = display->detail_win;
  int row;		  
  int total = 0;	  	
			  
  row = cur_y_default;	  
  text_highlight(win);	  
  dcur_move(lft+buf, top+buf, win);  	   		
  dwrite(win, "Perk                  Pts");
  	      	     	  
  text_norm(win);		  
  ability * look = perk_head->next;
  while (look != NULL)	  
  {	      	     	  
    if (look != current)  
    {	      	     	  
      look->show(lft+buf, row, win);
      total += look->get_pts();
    }		     	    	      
    else	     	    	      
    {	 	     	    	      
      text_highlight(win);	      
      look->show(lft+buf, row, win);  
      text_norm( );    	    	      
      total += look->get_pts();	      
    } 	 	       	    	      
    look = look->next; 	    	      
    row++;	       	    	      
  }   	 	       	    	      
      	 	       	    	      
  dcur_move(lft+buf, row+1, win);     
  text_norm(win);      		      
  dwrite(win, "Total Perks: %d", total);
}// show perk page     	    	      		  
      			    	     
void adv_character::trade_page_boundry_check()			  
// prevent current from leaving trade page
{		    	    	     
  log_event("adv_character::trade_page_boundry_check()\n");
  assert(current != NULL);	     
  assert(last_on_trade_page != NULL);
  assert(first_on_trade_page != NULL);
  			      	     
  if (current->next == first_on_trade_page) {
    if (current != gear_head) 	     
      trade_list_offset--;    	     
    else       	    	      	     
      current = current->next;	     
  }   	       	       	      	     
      	       	       	      	     
  if (current->prev == last_on_trade_page) {
      trade_list_offset++;    	     
  }   	       	    	      	     
  log_event("adv_character::trade_page_boundry_check - end\n");
}// trade page boundry check         	       	       	  
		       	      	     			  
		       	      	     			  
void adv_character::scroll_trade_page(int scrl)	  	  
// move the scroll offset     	      	     	    	  
{      			      	     	    	    	  
  int i;		      	     	    
  ability *look;		     
				     	    
  log_event("adv_character::scroll_trade_page(int scrl = %d)\n", scrl);
  log_event("adv_character::scroll_trade_page - current: %d\n", current);
       	       	       	       	       	       	      	  
  if (scrl > 0)	// scroll down       	    	      	  
  {    				     
    look = last_on_trade_page;	     
    for (i = 0; (i < scrl && current->next != NULL && look->next != NULL); i++) 
    {  		      		     	    	      
      trade_list_offset++;    	       	       	    	  
      current = current->next; 	     
      look = look->next; // prevent scolling down from clearing the page, 
                         // i.e. stop when last item is at botom of page
    }  		      	      	     	    	    	  
  } else { // scroll up		 
    look = first_on_trade_page;	 
    for (i = 0; (i > scrl && current->prev->prev != NULL && look->prev != gear_head); i--)
    {  		      	       	     	    	    	  
      trade_list_offset--;     	     	    	    	  
      current = current->prev; 	 
      look = look->prev;	 
    }  				     	    	    	  
  }//fi				     		    	  
  log_event("adv_character::scroll_trade_page - end\n", scrl);
}// scroll trade page		     			  
       	       			      			  
       	       			      
void adv_character::show_trade_page()
// show the trade page 		      
// NB: This function is called twice, once from each trader.
//     Each trader displays his own wares.	    
{      	       	       	      	      
  dwindow *win = display->detail_win; 
  ability *look;		     
  int y; // current writing line     
  int top_trade_page, btm_trade_page, mid_trade_page; // lines in trade screen
  int more_to_go, i; // flow control for display loop
       	       			     
  log_event("adv_character::show_trade_page()\n");     	       	     
  				     
  // initialise trade screen lines tk 
  top_trade_page = 3;  	       	      
  mid_trade_page = 12; 	    	      
  btm_trade_page = 22; 	    	      
       		       		      
  // show title	       		      
  text_highlight(win);        	      
  if (id == ID_PLAYER) 	    	      
  {    	       	       	    	      
    y = top_trade_page;     	      	       	
    dcur_move(lft+buf, y, win);       	  
    dwrite(win, "Own Inventory         Value");
  } else {     	       	       	      
    y = mid_trade_page+1; // first line after middle	    
    dcur_move(lft+buf, y, win);       	  	    
    dwrite(win, "Items available       Price");	    
  }    	       	       	    	      		    
  y++; 	       	       		      
       	       	       		      
  // show wares	       		      
  look = gear_head->next;   	 
  for (i = 0; (i < trade_list_offset && look != NULL); i++)
  {    	       	       
    look = look->next;  // scrolling offset				      
  }    	       	       
  first_on_trade_page = look;
       	 	       
       	       	       							      
  if (trade_list_offset > 0)	      					      
  {    	       	       		      					      
    if (look == current)
    {  		       
      text_highlight(win); 
    } else	       	   
      text_norm(win);  	   	      					      
    dcur_move(1, y, win);  	      		    	  		      
    dwrite(win, "-- more --");
    y++;	       
    if (look != NULL) look = look->next;
  }// more up          	   	      
       	       	       	   	      	   
  more_to_go = TRUE;   	   	      
  while (look != NULL && more_to_go)  	       	  		    
  {    	       	       	      	      		    
    if (look == current)
    {  		       		
      text_highlight(win); 	      		    
    } else             	       	   	      		    
      text_norm(win);  	   	      		    
    if ((y == (mid_trade_page) || y == (btm_trade_page)) &&  // last line &&
       	look != NULL && look->next != NULL) // not about to print end of list
    {  	       	       	   	      		    
      dcur_move(1, y, win); 	      		    	  
      dwrite(win, "-- more --");
      more_to_go = FALSE;     	     
    } else {   	       	       	      	  	    	  
      look->show(lft+buf, y, win, (HIDE_PTS | SHOW_PRICE));
    }  	       	       	    	
    // progress to next line	
    last_on_trade_page = look;
    look = look->next; 		
    y++;       	       	    			    
  }
  log_event("adv_character::show_trade_page - trade_list_offset: %d\n", trade_list_offset);
  log_event("adv_character::show_trade_page - first_on_trade_page: %s\n", first_on_trade_page->name);
  log_event("adv_character::show_trade_page - last_on_trade_page: %s\n", last_on_trade_page->name);     	       	     
  if (current != NULL) 
    log_event("adv_character::show_trade_page - current: %s\n", current->name);
  log_event("adv_character::show_trade_page - end\n");
}// show trade page     	      		    
      	       	       	      			    
      	       	      				    
void adv_character::show_gear_page()  		    
{    	       	      				    
  dwindow *win = display->detail_win;		    
  int total= 0;	       	 	     
	   	       		     
  text_highlight(win);    		     
  dcur_move(lft+buf, top+buf, win);
  dwrite(win, "R Item ");
	      	       	 	  
  text_norm(win);     	       
  ability * look = gear_head->next;
  int i = cur_y_default;
  while (look != NULL)		  
  {	      	      		  
    if (look != current)	  
    {	      	      		  
      look->show(lft+buf, i, win, SHOW_READY);
      total += look->get_pts();	  
    }	      	      		  
    else      	      		  
    {	      	      		  
      text_highlight(win); 	
      look->show(lft+buf, i, win, SHOW_READY);  	  
      text_norm(win); 	  	
      total += look->get_pts();	  
    }	       	      		  
    look = look->next;		  
    i++;       	      		  
  }	       	      		
	      	      		
  dcur_move(lft+buf, i+1, win);
  text_norm(win);   
  dwrite(win, "Total Gear: %d", total);
}// show gear page     	     	
	       	       	     	
void adv_character::show_power_page()
{	       	       	     	
  dwindow *win = display->detail_win;
  int total = 0;	       	     	
	 	       	     	     
  text_highlight(win);	     	     
  dcur_move(lft+buf, top+buf, win);
  dwrite(win, "Power                  Pts");
       	      	       		
  text_norm(win);
  ability * look = power_head->next;
  int i = cur_y_default;
  while (look != NULL) 	
  {    	      		
    if (look != current)
    {  	      		
      look->show(lft+buf, i, win);
      total += look->get_pts();
    }  	      		     
    else      		     
    {  	   		     
      text_highlight(win);     
      look->show(lft+buf, i, win);
      text_norm(win);	     	
      total += look->get_pts();	
    }		 	     	
    look = look->next;		
    i++;	 		
  }		 		
				
  dcur_move(lft+buf, i+1, win );
  text_norm(win);
  dwrite(win, "Total powers: %d", total);
}// show power page		
	      	 		
	      	 		
void adv_character::show_disadvantage_page()
{	      			     
  dwindow *win = display->detail_win;
  int disadvantage_total = 0;	
	      			
  text_highlight(win);		
  dcur_move(lft+buf, top+buf, win);
  dwrite(win, "Value  Disadvantage"); 
	      			
  text_norm(win);			
  ability * look = disadvantage_head->next;
  int i = cur_y_default;
  while (look != NULL)	 	       
  {	      	    	 	       
    if (look != current) // other lines  	  
    {	      	    	 	       	   
      look->show(lft+buf, i, win);
      disadvantage_total += look->get_pts();	  
    }	      	    	 	       
    else      	       	// currnet line       	       
    {	      	    	 	  
      text_highlight(win);	  	
      look->show(lft+buf, i, win);
      text_norm( );  	 	
      disadvantage_total += look->get_pts();
    }		    	 	
    look = look->next;	 	
    i++;	    	 	
  }		    	 	
				
  dcur_move(lft+buf, i+1, win);
  text_norm(win);
  dwrite(win, "Total disadvantages: %d\n(Max: %d)", 
	 disadvantage_total, ch_max_disadvantages);
}// show disadvantage page		  			  	     
 	      			
// ******************* END STUFF TO CONVERT FROM CHARACTER.CC
	      	
void adv_character::goset(int x, int y)
{   	      	     
  dwindow *win = display->detail_win;
  dcur_move(x, y-1, win);					
  if (y == cury)     	
    text_highlight(win);
  else	       	      	
    text_norm(win);	
} 	      		
	      
void adv_character::show_title_page()
{ 	      		
  dwindow *win = display->detail_win;	    
  int x, y; // my cursor
  int stat_total, combat_total, skill_total;
  int gear_total, perk_total, power_total;
  int disadvantage_total, disadvantage_contribution;
  int total_spent, balance;

  log_event("adv_cahracter::show_title_page\n");
  x = lft+buf;  
  y = cur_y_default;
  log_event("  cur_y_default: %d\n", cur_y_default);
  log_event("  cur_y: %d\n", cury);
  
  total_spent = 0;
  total_spent += stat_total = sum_stats();
  total_spent += combat_total = sum_combat();
  total_spent += skill_total = sum_skills();
  total_spent += perk_total = sum_perks();
  total_spent += gear_total = sum_gear();
  total_spent += power_total = sum_powers();
       	     
  disadvantage_total = sum_disadvantages();
  disadvantage_contribution = (disadvantage_total > ch_max_disadvantages) ? 
                              ch_max_disadvantages : disadvantage_total;
  balance = total_spent - ch_base - disadvantage_contribution - ch_experience;
       	     			    					     
  goset(x, y); dwrite(win, "Name: %s", ch_name); y++;
  goset(x, y);		   
  if (ch_age < 0)      	   
    dwrite(win, "Age: Unknown / Ageless");
  else	     
    dwrite(win, "Age: %d", ch_age);
  y++;	     	      
  goset(x, y); dwrite(win, "Race: %s", ch_race); y++;
  goset(x, y); dwrite(win, "Sex: %s", ch_sex); y++;
  goset(x, y); dwrite(win, "Living: %s", ch_living); y++;
	     
  y++;	     
  goset(x, y); dwrite(win, "Stat Cost:    %5d", (stat_total));   y++;
  goset(x, y); dwrite(win, "Combat Cost:  %5d", (combat_total)); y++;
  goset(x, y); dwrite(win, "Skill Cost:   %5d", (skill_total));  y++;
  goset(x, y); dwrite(win, "Perk Cost:    %5d", (perk_total));   y++;
  goset(x, y); dwrite(win, "Gear Cost:    %5d", (gear_total));   y++;
  goset(x, y); dwrite(win, "Power Cost:   %5d", (power_total));  y++;
  goset(x, y); dwrite(win, "-------------------" ); y++;
  goset(x, y); dwrite(win, "Total:        %5d", (total_spent));        y++;
  y++;	     
  /*  This doesn't fit in the window
  if (disadvantage_total > ch_max_disadvantages)
  { 	     
    goset(x, y); 
    dwrite(win, "Disadvatages: %5d (Total: %d)", disadvantage_contribution, disadvantage_total); 
    y++;     
  } 	     
  else	     
  { 	     
    goset(x, y); 
    dwrite(win, "Disadvatages: %5d (Max: %d)", disadvantage_total, ch_max_disadvantages); 
    y++;     
  }  	     
   */  	       	       	       	
  goset(x, y); 			
  dwrite(win, "Disadvatages: %5d", disadvantage_total, ch_max_disadvantages); 
  y++;	     
  goset(x, y); dwrite(win, "Base:         %5d", (ch_base));      y++;
  goset(x, y); dwrite(win, "Experience:   %5d", (ch_experience)); y++;
  goset(x, y); dwrite(win, "-------------------" ); y++;
  goset(x, y);			
  if (balance > 0)		
    dwrite(win, "Overspent:    %5d", balance);
  else			
    dwrite(win, "Unspent:      %5d", (-balance));
}// show title page  	
		     
		     
void adv_character::show()     	       	 	 
// display the character 	  	    
{    	       	     	    	  	    
  dwindow *win = display->detail_win;	    
     		     	 
//  display->detail_state = detCHAR;  // taken care of by adv_game.cc handle_user_selection()
  setpcolour(DETAIL_COLOUR);	   
  dclear(win); 	     	   	   
     		     	 	  
  // show a title line so we can tell what character is being displayed
  text_highlight(win);
  if (id == ID_PLAYER)       	 	  
  {  	   	      	 	  
    dwrite(win, "-* Player Character *-\n");
  }  else  { 	    	 	  
    dwrite(win, "-* Monster Details *-\n");
  } // fi : title line	 
     
  switch (page)			
  {  	     			
    case(TITLE_PAGE):		
    {	     			
      show_title_page();	
      break;			
    }	  			
    case(STAT_PAGE):		
    // stats page		
    {	  			
      show_stat_page();		
      break;			
    }// stat page		
     	  			
    case( COMBAT_PAGE ):	
    // weapon ranks		
    {	  			
      show_combat_page();	
      break;			
    }	  			
     	  			
    case ( SKILL_PAGE ):	
    {	  			
      show_skill_page();	
      break;			
     	  			
    }//skill page		
     	  			
    case ( PERK_PAGE ):		
    {	  			
      show_perk_page();		
      break;			
    }	  			
     	  			
    case ( GEAR_PAGE ):		
    {	  			
      show_gear_page();		
      break;			
    }	  			
     	  			
    case ( POWER_PAGE ):	
    {	  			
      show_power_page();	
      break;			
    }	  			
     	  			
    case ( DISADVANTAGE_PAGE ):	
    {	  			
      show_disadvantage_page();	
      break;			
    }	  			
     	  			
    default:			
    { 
      dcur_move(1, 1);
      dwrite("%d", page);   	
      break;	      		
    }	  	      		
  }  	  	      		
     
/* old one page character    		      	 	  
  // show the character	 	  
  show_name();	       	       	  
  dwrite(win, "\nStatitics");
  show_str(win);
  show_body(win);     // body 	       
  show_dex(win);      // dex
  show_pd(win);
  show_stun(win);
  // show sword skill
  if (wielded_sk != NULL) 
    show_weapon_skill(win);
  else	    
    dwrite(win, "\nNo weapon skill.");
  dwrite(win, "\n");  	       	       
  dwrite(win, "\nEquipment");      
  show_cash(win);      	      	
  show_potion();       	      	
  // show sword	       	       	
  // show armour  		
  // show shield  
  show_combat_gear(win);
  dwrite(win, "\n");  	       	       
  show_xp();
 */ 
  drefresh(win);       	    	
}// character show     		
  	       	       		
int get_monster_stat(int base, int step, int dungeon_level)
// setup monster stats from formula    
// monster abilities depend on the dungeon level, but they vary up to
// 1/5 of the level better and up to 1 level worse
// eg	       	       	     
// a level 1 monster may be between levels 0 and 2
// a level 2 monster may be between levels 1 and 3
// a level 8 monster may be between levels 7 and 9
// a level 10 monster may be between levels 9 and 12
// a level 30 monster may be between levels 29 and 36	 
{  		       	     	   	 	
  int stat, lowest, highest, variation, varience;	 	
     	   	     	     
  // find the lowest possible stat   
  lowest = base + (step * (dungeon_level - 1));
  // find the highest possible stat
  highest = base + (step * (dungeon_level + (dungeon_level / 5)));
     		     	     	 
  // find the possible variation 
  variation = highest - lowest;	 
  // randomly determine this monster's varience (individuality)
  if (variation > 0) 	     	     			     
    varience = d_random(variation + 1) - 1; // 0 to variation 
  else 			
    varience = 0;	
  // set the individual's stat	     
  stat = lowest + varience;  	   
   	     	     	     	     
  return stat;	     	     	     
}// get monset stat  	     	     
    	      	     	     	     
void adv_character::new_monster(int dungeon_level)
// generate a new monster    	     	    
{  	     	    	     	     
  log_event("new monster\n"); 	     
  ch_str->set_value(get_monster_stat(MONSTER_BASE_STRENGTH, 
   	      	       	    MONSTER_STEP_STRENGTH, 
   	      	       	    dungeon_level));	 
  assert((ch_str->pts_to_value() >= MONSTER_BASE_STRENGTH));   
  log_event("Strength %d\n", ch_str->pts_to_value()); 
  ch_body->set_value(get_monster_stat(MONSTER_BASE_BODY,  
   	      	     	    MONSTER_STEP_BODY, 		  
   	      	     	    dungeon_level)); 		  
  assert(ch_body->pts_to_value() >= MONSTER_BASE_BODY);	  
  log_event("Body %d\n", ch_body->pts_to_value()); 
  cash = get_monster_stat(MONSTER_BASE_CASH, 
       	       	     	    MONSTER_STEP_CASH, 
       	       	     	    dungeon_level);
  assert(cash >= MONSTER_BASE_CASH); 
  log_event("Cash %d\n", cash); 
  ch_dex->set_value(get_monster_stat(MONSTER_BASE_DEX,
   	      	       	    MONSTER_STEP_DEX,      
   	      	       	    dungeon_level));
  assert((ch_dex->pts_to_value() >= MONSTER_BASE_DEX));
  log_event("Dex %d\n", ch_dex->pts_to_value()); 
  if (d_random(100) < MONSTER_BASE_ARMED + MONSTER_STEP_ARMED * dungeon_level)
  {	     	       		     	    
    log_event("Monster is armed.\n");
    	     	       	       	    
    weapon_dc = get_monster_stat(MONSTER_BASE_WEAPON_DC, 
       	       	       	    MONSTER_STEP_WEAPON_DC, 
       	       	       	    dungeon_level);	 
    assert(weapon_dc >= MONSTER_BASE_WEAPON_DC);
    log_event("Weapon DC %d\n", weapon_dc); 
  } //fi: monster armed	       	   	      
   	     	    	       	   	      
  armour = get_monster_stat(MONSTER_BASE_ARMOUR, 
       	       	     	    MONSTER_STEP_ARMOUR,      
       	       	     	    dungeon_level);   	      
  assert(armour >= MONSTER_BASE_ARMOUR);      	      
  log_event("armour %d\n", armour);
  ch_pd->set_value(get_monster_stat(MONSTER_BASE_PD,  
   	      	       	    MONSTER_STEP_PD,          
   	      	       	    dungeon_level));	      
  assert((ch_pd->pts_to_value() >= MONSTER_BASE_PD));   
  log_event("PD %d\n", ch_pd->pts_to_value()); 
  ch_stn->set_value(get_monster_stat(MONSTER_BASE_STN, 
   	      	       	    MONSTER_STEP_STN,    
   	      	       	    dungeon_level));
  assert((ch_stn->pts_to_value() >= MONSTER_BASE_STN));   
  log_event("Stun %d\n", ch_stn->pts_to_value()); 
   	       	    		   		  
  assert(wielded_sk != NULL);		      
  shield = 0;         // monster doesn't get a shield or ocv bonus
  ch_wounds = 0;    	
  w_stun = 0;	    	
  id = ID_MONSTER;  	
  if (ch_name != NULL) delete ch_name;
  ch_name = new_str("Monster");   
}//new monster	     	     	   
  		    	
void adv_character::new_player()
// STUB:: create a new character - use default character
{    	   	    		   
}    		    		   
		   
int adv_character::get_alignment()
{	      	   
  return alignment;
}	      	   
	      	   
void adv_character::set_alignment(int new_alignment)
{   	     	    	   			 
  alignment = new_alignment;
}   	     	    	    
    	     	    	    
int adv_character::modify_alignment(int amnt)
{   		    			   
  alignment += amnt;
  return alignment;
}



adv_character::adv_character(adv_display *new_display) : 
  character()		
// initilise to default character    
{   	 	       	      	     
  display = new_display;      	     
  if ((ch_name = new char[MAX_INPUT]) == NULL)  
  { 	 	       		     
    dwrite("Mem err"); 		     
    exit(1); // tk 
  } 	 	       		     

  if (ch_name != NULL) delete ch_name;
  ch_name = new_str("New Character");   
/*    			
  ch_name = NULL;
  ch_living = NULL;
  ch_race = NULL;
  ch_sex = NULL;
  file_name = NULL;
*/    
  ch_body->set_value(DEFAULT_BODY);   	       	
  ch_dex->set_value(DEFAULT_DEX);     	 
  ch_experience = DEFAULT_XP;        	     	 
  cash = DEFAULT_CASH; 	     	     
  potions = DEFAULT_POTIONS;	     
  weapon_dc = DEFAULT_WEAPON_DC;     
  weapon_ocv = DEFAULT_WEAPON_OCV;   
  shield = DEFAULT_SHIELD;	     
  armour = DEFAULT_ARMOUR;	     
  ch_wounds = 0;		     
  w_stun = 0;
// bug: using "swords" instead of NULL causes seg fault at end of combat. Think this is fixed, needs testing.
//  wielded_sk = new weapon_rank(combat, NULL, cWIELDED, 0); 
  wielded_sk = new weapon_rank(combat, "swords", cWIELDED, 0); 
  id = ID_PLAYER;
  trade_list_offset = 0;
}// character constructor    	   
    	     	    		   
     		    
adv_character::~adv_character()	     
{    	       			     		    
  log_event("adv_character::~adv_character()\n");
  /* All of this is done by character::~character but why that is called for 
   * a decendant object I don't fully understand ???
  wielded_sk = NULL; // unlink from weapon skill list to 
                     // avoid double deleation	     
  // delete abilities    	     		     
  del_stats(); 	       		     		     
  del_weapon_ranks();  		     		     
  del_skills();	       		     		     
  del_perks(); 	       		     		     
  del_gear();  	       		     		     
  del_powers();	       		     		     
  del_disadvantages(); 		     		     
  if (ch_name != NULL) delete ch_name; 	       	     
  if (ch_living != NULL) delete ch_living;	     
  if (ch_race != NULL) delete ch_race;		     
  if (ch_sex != NULL) delete ch_sex; 		     
  if (file_name != NULL) delete file_name;	   */
  log_event("adv_character::~adv_character - end\n");
} // destructor  				 
    	       					 
    						 
// end - adv_character.cc			 
    		    				 
    		    				 
  		    				 
  						 
						 

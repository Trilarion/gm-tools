/*  adv_game.cc - an advaenture game             
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

                    
// NOTE for the moment character_list[0] is the player and 
// character_list[1] is the monster this is an interum measure as I convert
// from player/monster to multiple character game.
     
// File map:
//  * Declarations
//  * File Handling - load and save
//  * Activities
//  * Combat
//  * Descriptions
//  * Input Handling
//  * Startup
//  * Cleanup
//  * Main


#include <stdio.h>                             
#include <sys/time.h>       // get_time_of_day()              
#include <time.h>             // time()             
#include <stdlib.h>                            
#include <assert.h>                            
#include <string.h>                            
#include <errno.h>                             
#include <math.h>  
                
#include "adv_character.h"                      
#include "hit.h"                                  
#include "room.h"                              
#include "adv_display.h"                         
#include "adv_clock.h"
#include "adv_cmd.h"                      
#include "event.h"
#include "dandefs.h"                         
#include "danio.h"                   
#include "prompt.h"   
#include "sat.h"      
#include "status_line.h"                         
#include "keys.h"
#include "menu.h"
#include "err_log.h"

const int DEBUG_LEVEL = logALL;

                          
#define ROUND_TIME 3.0 // nominally 3.0 seconds, modify for debugging
#define NOWAIT              // turn of waiting loop used to debug main loop
#define MAX_CHARACTERS 8000 // number of characters in game
                                      
// stuff for the game prompt                   
// x ordinates for menu questions             
#define xoMAIN_MENU 23                          
#define xoPOTION 46                                
#define xoSPEND_XP 43                             
#define xoCHANGE_NAME 19                       
#define xoSHIELD 50                                 
#define xoBUY 34                              
#define xoARMOUR 50                             
#define xoSWORD 45                             

// menu states - actually a misnomer for prompt states
#define msMAIN_MENU    0                     
#define msSPEND_XP     1                      
#define msCHANGE_NAME  2                   
                                     
// global variable dungeon map                   
// This is used to set up the dungeon, but must be kept as a means to 
// save the dungeon at a later time.             
room *dungeon_map[DUNGEON_LEVELS][DUNGEON_SIZE_X][DUNGEON_SIZE_Y];
                                     

// ******************************** DECLARACTIONS *************************
// i've only declared things where i've played with the order
// NB: main() is at the bottom
                  
void restore_detail(adv_display *display, menu_el *main_menu,
                adv_character *player);
// redisplay prior detail screen
                
void do_attack(int attack_type, adv_character *attacker, 
             adv_character *defender, adv_display *display);
                                                                     
void description(int dungeon_level, room *location, 
                           adv_character **character_lsit, adv_display *display, 
             int force/* = FALSE*/);
  // give a description. if force is false and the location has not changed 
  // since the last call the the description is skipped

void take_input_control(char **question, const char *new_question, 
                                       char **old_question, int& menu_state, 
                                       const int new_menu_state, int& old_menu_state,
                                       prompt *main_prompt, const int new_x, int *old_x);
  // do a handover for functions that take control of input  
void restore_input_control(char **question, char *old_question, 
                            int& menu_state, int old_menu_state, 
                                          prompt *main_prompt, int old_x);
  // do a handover for functions that are done with control of input
                                                       
// ********* END DECLARATIONS                                  
//        
//        
//        
//        
// *********************** FILE HANDLING
            
int save_game(room **dungeon, adv_character **character_list, 
            room **location_list, int next_character)
{                                      
  FILE *out;                      
  int x, y, z; // for traversal of the dungeon
                                                
  say_status("Game saving.");          
  log_event("save_game()\n");                   
                                                
  // open game save file                   
  if ((out = fopen("save.adv", "wt")) == NULL)
  {                                              
    log_event("save_game - failed to open output file");
    say_status("Can't open output file."); 
    return errINIT_FAILED;                
  }                                        
                                             
  // save the dungeon                         
  // room *dungeon_map[DUNGEON_LEVELS][DUNGEON_SIZE_X][DUNGEON_SIZE_Y];
  for (z = 0; z < DUNGEON_LEVELS; z++)
  for (y = 0; y < DUNGEON_SIZE_Y; y++)
  for (x = 0; x < DUNGEON_SIZE_X; x++)
  {                                
    // save the room                 
    dungeon_map[z][x][y]->save(out);
  }                        
  log_event("save_game - Dungeon map saved.\n");
    
  log_event("save_game - Nubmer of characters: %d.\n", next_character);
  log_event("save_game - Cahracter[1]: %d.\n", character_list[1]);
                                            
                                            
  // save the character database                      
  fprintf(out, "%d\n", next_character); // the number of characters
  for (x = 0; x < next_character; x++)                     
  { 
    if (x != 1) // 1 is currently a special position for the monster
    // being fought, will be changed when combat goes multiple tk
    {                            
      log_event("save_game - Saving character %d.\n", x);
      assert(character_list[x] != NULL);
      fprintf(out, "%d\n", location_list[x]);  
      fprintf(out, "%d\n", character_list[x]);
      fprintf(out, "Saving character: %d\n", x);
      character_list[x]->adv_save(out);           
      log_event("save_game - Character %d saved.\n", x);
    }// fi: all but character_list[1]
  }                                    
                                        
  // save the game time                       
  // tk                                    
                                        
  // close the save file                         
  fclose(out);                                   
                                           
  say_status("Game saved.");                 
  log_event("save_game() - end\n");                    
                          
  return SUCCESS;            
}// save game                        
                                  
int load_game(room **dungeon, adv_character **character_list, 
              room **location_list, int next_character, adv_display *display)
{                                     
  FILE *in;                        
  int x, y, z; // for traversal of the dungeon
  char temp[MAX_LINE];                  
  room *r;
  
  say_status("Game loading.");      
  drefresh();                   
  log_event("load_game()\n");                
                                                    
  // open game save file                 
  if ((in = fopen("save.adv", "rt")) == NULL)
  {                                                  
    log_event("load_game - failed to open input file");
    say_status("Can't open input file."); 
    return errINIT_FAILED;                
  }           else             
  log_event("load_game - input file opened.\n");
                                                           
  // load the dungeon                               
  // room *dungeon_map[DUNGEON_LEVELS][DUNGEON_SIZE_X][DUNGEON_SIZE_Y];
  for (z = 0; z < DUNGEON_LEVELS; z++)            
  for (y = 0; y < DUNGEON_SIZE_Y; y++)            
  for (x = 0; x < DUNGEON_SIZE_X; x++)            
  {                                                      
    // load the room                                
    log_event("load_game - now loading level %d room: %d South %d East.\n", z, y, x);  
    dungeon_map[z][x][y]->load(in);
  }                                                    
  log_event("load_game - dungeon rooms loaded.\n");
                                                                  
  // load the character database
  // delete old character database first
  log_event("load_game - deleating old character data.\n");
  for (x = 0; x < next_character; x++)            
  {          
    log_event("load_game - character: %d", x);
    if (character_list[x] != NULL)   
    {                              
      log_event(" to be deleted.\n");
      delete character_list[x];           
    } else {                       
      log_event(" to be skipper.\n");
    }
    
  }                        
  fgets(temp, MAX_INPUT, in);                     
  sscanf(temp, "%d\n", &next_character); // the number of characters
  log_event("load_game - next character: %d\n", next_character);
  for (x = 0; x < next_character; x++)            
  {                                
    if (x != 1) // 1 is currently a special position for the monster
    // being fought, will be changed when combat goes multiple tk
    {                                
      fgets(temp, MAX_INPUT, in);            
      sscanf(temp, "%d\n", &location_list[x]);
      fgets(temp, MAX_INPUT, in);  // character id - not used (yet ???)
      fgets(temp, MAX_INPUT, in);  // character number in list
      log_event("Loading character: %d from -\n%s", x, temp);
      character_list[x] = new adv_character(display);
      character_list[x]->adv_load(in);                                 
    }                         
  }                         
  character_list[1] = NULL;  // monster being fought tk

  log_event("load_game - character data loaded.\n");
                                                  
  // load the game time                                   
  // tk                                                      
                                                                    
  // link the data                                            
  for (z = 0; z < DUNGEON_LEVELS; z++)                          
  for (y = 0; y < DUNGEON_SIZE_Y; y++)                          
  for (x = 0; x < DUNGEON_SIZE_X; x++)                          
  {                                         
    log_event("load game - level (y): %d east (x): %d south (y): %d\n", 
            z, x, y);                                   
    log_event("load game - Nabours:- North: %d South: %d East: %d West: %d Up: %d Down: %d\n",
                dungeon_map[z][x][y]->nabour[NORTH],  
                dungeon_map[z][x][y]->nabour[SOUTH],        
                dungeon_map[z][x][y]->nabour[EAST],         
                   dungeon_map[z][x][y]->nabour[WEST]);
                   dungeon_map[z][x][y]->way_up;
                   dungeon_map[z][x][y]->way_down;    
    // links to other rooms                             
    if (y > 0 && dungeon_map[z][x][y]->nabour[NORTH])
    {                                        
      log_event("load game - Passage made to the North\n");
      dungeon_map[z][x][y]->nabour[NORTH] = dungeon_map[z][x][y-1];
    }                                        
                                            
    if (y < DUNGEON_SIZE_Y && dungeon_map[z][x][y]->nabour[SOUTH])
    {                                        
      log_event("load game - Passage made to the South\n");
      dungeon_map[z][x][y]->nabour[SOUTH] = dungeon_map[z][x][y+1];
    }                                          
                                              
    if (x < DUNGEON_SIZE_X && dungeon_map[z][x][y]->nabour[EAST])
    {
      log_event("load game - Passage made to the East\n");
      dungeon_map[z][x][y]->nabour[EAST] = dungeon_map[z][x+1][y];
    }                                          
                                               
    if (x > 0 && dungeon_map[z][x][y]->nabour[WEST])           
    {                                          
      log_event("load game - Passage made to the West\n");
      dungeon_map[z][x][y]->nabour[WEST] = dungeon_map[z][x-1][y];
    }                                          
                                               
    if (z > 0 && dungeon_map[z][x][y]->way_up)                      
    {                                          
      log_event("load game - Passage made going Up\n");
      dungeon_map[z][x][y]->way_up = dungeon_map[z-1][x][y];
    }                                          
                                              
    if (z < DUNGEON_LEVELS && dungeon_map[z][x][y]->way_down)          
    {                                         
      log_event("load game - Passage made going Down\n");
      dungeon_map[z][x][y]->way_down = dungeon_map[z+1][x][y];
    }                                         
                                              
  }//rof: all rooms                                                  
                                                           
  // recreate the town ??? don't think i need this
  delete *dungeon;
  r = new room();                                      
  r->way_down = dungeon_map[0][0][0];
  dungeon_map[0][0][0]->way_up = r;
  location_list[0] = (*dungeon) = r; // character is in the town which is the head of the dungeon.
  
  // close the input file                          
  fclose(in);                                            
                                                    
  say_status("Game loaded.");                  
  log_event("load_game() - end\n");            
  return SUCCESS;                              
}// load game                                         
                                                    
// ******** END FILE HANDLING                  
//                                            
//                   
//                   
//                                                                             
// ************************** ACTIVITIES ****************************
                                        
void check_for_new_monster(adv_character **character_list, 
                            room **location_list, 
                                 int dungeon_level, adv_display *display)
// check for a monster in the room entered
{             
  log_event("check_for_new_monster()\n");
  if (character_list[1] == NULL)                                      
  { // no monster yet                                     
    if (location_list[0]->has_monster)                      
    {      // make the monster appear                        
      character_list[1] = new adv_character(display);            
      character_list[1]->new_monster(dungeon_level);
    }// fi: room has monster                              
  }// fi: no monster                                       
}                                                                         
                                                                   
void check_for_following_monster(adv_character **character_list,
                                       adv_display *display)
{                                                                                                    
  if (character_list[1])
  // there is a monster in the room the character is leaving              
  {                                                                                      
    // monster gets a free hit 
    dwrite(display->event_win, "\nThe foul brute swipes as you flee.");  
    do_attack(PLAYER_HIT, character_list[1], character_list[0], display);
    // monster may follow                                           
    if (d_random(2) - 1) // 50/50 chance
    {      
      delete character_list[1]; // leave the monster behind
      character_list[1] = NULL;
      log_event("monster_deleted\n");
    }                         
    else                                                        
      dwrite(display->event_win, "\nThe creature has followed you.");
  }                                                             
} // check for following monster                     
                                                                   
                                                                        
void move_player(int& dungeon_level, adv_character **character_list, 
                        room **location_list, int cmd,
                           adv_display *display)            
// move                                                         
{
  log_event("move_player()\n");
  room *destination;  
  char direction[20];
  
  check_for_following_monster(character_list, display);
  if ((!dungeon_level) && (character_list[1] != NULL))
    // prevent monster entering the town      
  {                                                        
    dwrite(display->event_win,                  
              "\nThe monster retreats at the sight of the town.");
    delete character_list[1];                                 
    character_list[1] = NULL;                                
  }                                                     
                                                      
  switch (cmd)                                        
  {                                                     
    case advcmdUP:   dungeon_level--; destination = location_list[0]->way_up; 
                     strcpy(direction, "up"); break;
    case advcmdDOWN: dungeon_level++; destination = location_list[0]->way_down;
                     strcpy(direction, "down"); break;
    case advcmdNORTH: destination = location_list[0]->nabour[NORTH]; 
                      strcpy(direction, "north"); break;
    case advcmdSOUTH: destination = location_list[0]->nabour[SOUTH]; 
                      strcpy(direction, "south"); break;
    case advcmdEAST:  destination = location_list[0]->nabour[EAST]; 
                      strcpy(direction, "east"); break;
    case advcmdWEST:  destination = location_list[0]->nabour[WEST]; 
                      strcpy(direction, "west"); break;
  } // switch             
                                                                    
                                                                      
  location_list[0] = destination;                                    
  setpcolour(ACTION, display->event_win);
  dwrite(display->event_win, "\nYou move %s.", direction);   
  setpcolour(GENERAL, display->event_win);
  drefresh(display->event_win);                                    
  check_for_new_monster(character_list, location_list, 
                      dungeon_level, display);
}// move                                                                                     

void change_name(adv_character *player, adv_display *display, menu_el *main_menu, 
                     char ** question, prompt *main_prompt, int& prompt_state)
{                                                 
  // these static variables hold information to restore control after this
  // function is done with the input
  static char *old_question;                                
  static int old_menu_state;                                
  static int old_x;                                          
                                              
  if (prompt_state != msCHANGE_NAME)
  {                                  
                                       
    say_status("Change name.");        
    take_input_control(question,                              
                                 "Enter a new name: ", 
                            &old_question, prompt_state, msCHANGE_NAME,
                            old_menu_state, main_prompt, xoCHANGE_NAME, &old_x);
    // done for now : return to caller at end of if statement          
  }                                 
  else                                 
  {                                 
    debuf(&main_prompt->s);                      
    player->set_name(main_prompt->s);           
    setpcolour(EVENT_COLOUR);           
    dwrite(display->event_win,           
                 "\nCharacters name changed to %s.", main_prompt->s);
    drefresh(display->event_win);            
    // done with input so return control
    restore_input_control(question, old_question, prompt_state,
                                 old_menu_state, main_prompt, old_x);
  }                                 
} // change name                         
                                 
                                 
void spend_xp(adv_character *player, adv_display *display, menu_el *main_menu, 
                    char ** question, prompt *main_prompt, int& prompt_state)
// spend xp                                  
{                                              
  // these static variables hold information to restore control after this
  // function is done with the input
  static char *old_question;                                
  static int old_menu_state;                                
  static int old_x;                                          
  int xp;                                
  dwindow *win = display->detail_win;
                            
  if (prompt_state != msSPEND_XP)                                     
  {                        
    log_event(logDEFAULT, "spend_xp() - Initialising.\n");
    // setup                           
    dclear(win);                                            
                                            
    say_status("Spend experience.");
    setpcolour(DETAIL_COLOUR);                                       
    player->show_xp();                                           
    player->show_dex(win);                                      
    player->show_body(win);                                    
    dwrite(win, "\n");                                                     
    dwrite(win, "Spend experience on:-\n");                   
    dwrite(win, "1. Change name. (0)\n");     
    dwrite(win, "2. Increase Strength (%d)\n", cSTR);            
    dwrite(win, "3. Increase Dexterity (%d)\n", cDEX);            
    dwrite(win, "4. Increase Body (%d)\n", cBODY);
    dwrite(win, "5. Increase PD (%d)\n", cPD);
    dwrite(win, "6. Increase STUN (%d)\n", cSTN);
    dwrite(win, "7. Increase Weapon Skill (%d)\n", cWIELDED);
    dwrite(win, "\n");                                                     
    dwrite(win, "0. Return to main menu.\n");                  
    drefresh(win);                                                       
    take_input_control(question,                                         
                        "How do you want to spend your experience? ", 
                        &old_question, prompt_state, msSPEND_XP,
                        old_menu_state, main_prompt, xoSPEND_XP, &old_x);
    // done for now : return to caller at end of if statement  
  } else {                                                                
    log_event(logDEFAULT, "spend_xp() - Handling event.\n");
    xp = player->get_xp();                                          
    // second pass                                                          
    switch(main_prompt->s[0])                                            
    {                                                                               
      case ('0'):                                                           
      {                
        log_event(logALL, "spend_xp() - Interpreted 0: exiting from spend xp loop.\n");                                                               
        // restore the detail window                             
        restore_detail(display, main_menu, player);            
        // done with input so return control                   
        restore_input_control(question, old_question, prompt_state,
                                            old_menu_state, main_prompt, old_x);
            restore_detail(display, main_menu, player);            
            return;                                                                   
      }                                                                         
                                                                                    
      case '1': { // Change name             
             change_name(player, display, main_menu, question, 
                main_prompt, prompt_state);
            break;                                        
      }      // change name                                 
      case ('2'):             // increase strength
      {                                                                          
             if (xp >= cSTR)                
             {                                                                         
              player->spend_xp(sSTR, cSTR);                             
          setpcolour(EVENT_COLOUR);                                
               dwrite(display->event_win, "\nStat increased: ");  
              player->show_str(display->event_win);            
          drefresh(display->event_win);                             
              // display new xp total                              
               dcur_move(1, 1, win);                                        
              player->show_xp();                                      
              drefresh(win);                                          
             } else {                                                                    
              say_status("Not enough experience to increase Strength.");
             } //fi                                                                       
             break;                                                                          
      }// increase dex                                                
      case ('3'):             // increase dex     
      {                                                                          
             if (xp >= cDEX)                                               
             {                                                                         
              player->spend_xp(sDEX, cDEX);                             
          setpcolour(EVENT_COLOUR);                                
               dwrite(display->event_win, "\nStat increased: ");  
              player->show_dex(display->event_win);            
          drefresh(display->event_win);                             
              // display new xp total                              
               dcur_move(1, 1, win);                                        
              player->show_xp();                                      
              drefresh(win);                                          
             } else {                                                                    
              say_status("Not enough experience to increase Dexterity.");
             } //fi                                                                       
             break;                                                                          
      }// increase dex                                                
                                                                                            
      case ('4'): { // increase body                                    
            if (xp >= cBODY)                                                     
            {                                                                               
              player->spend_xp(sBODY, cBODY);                           
               setpcolour(EVENT_COLOUR);                                       
              dwrite(display->event_win, "\nStat increased: ");  
               player->show_body(display->event_win);            
              drefresh(display->event_win);                                   
              // display new xp total                               
               dcur_move(1, 1, win);                                          
              player->show_xp();                                         
              drefresh(win);                                             
            } else {                                                            
              say_status("Insufficient experience to increase body.");
            }// fi                                                                  
             break;                                                                   
      } // increase body                                              
      case ('5'):             // increase pd
      {                                                                
             if (xp >= cPD)                   
             {                                                        
              player->spend_xp(sPD, cPD);
          setpcolour(EVENT_COLOUR);              
               dwrite(display->event_win, "\nPhysical defence increased: ");
               player->show_pd(display->event_win);   
          drefresh(display->event_win);           
              // display new xp total            
               dcur_move(1, 1, win);                      
              player->show_xp();                      
              drefresh(win);                        
             } else {                                                        
              say_status("Not enough experience to physical defence.");
             } //fi                               
             break;                                                            
      }// increase pd                   
      case ('6'): { // increase stun
            if (xp >= cSTN)                                                      
            {                                                                               
              player->spend_xp(sSTUN, cSTN);                           
               setpcolour(EVENT_COLOUR);                                       
              dwrite(display->event_win, "\nStat increased: ");  
               player->show_stun(display->event_win);            
              drefresh(display->event_win);                                   
              // display new xp total                               
               dcur_move(1, 1, win);                                          
              player->show_xp();                                         
              drefresh(win);                                             
            } else {                                                            
              say_status("Insufficient experience to increase Stun.");
            }// fi                                                                  
             break;                                                                   
      } // increase stun             
                                                                  
      case ('7'):             // increase weapon skill            
      {                                                             
             if (xp >= cWIELDED)          
             {                                                        
               player->spend_xp(sWIELDED, cWIELDED);
          setpcolour(EVENT_COLOUR);              
               dwrite(display->event_win, "\nWeapon rank increased: ");
               player->show_weapon_skill(display->event_win);
          drefresh(display->event_win);           
        // display new xp total            
               dcur_move(1, 1, win);                      
        player->show_xp();                    
        drefresh(win);                        
             } else {                                                        
          say_status("Not enough experience to increase weapon skill.");
             } //fi                                     
                                                
             break;                                                            
      }// increase weapon skill        
                                                         
      default: bad_input(); break;           
                                                              
    }//switch (ans[0])                        
  } // fi second and subsequent pass 
  // maintain control until 0 is selected
}//spend xp                                         
                          
                          
                          
// ***************************** END ACTIVITIES                  
//                                               
//                          
// ***************** TRADING        

int price_shield(int new_shield)
{
  return (int) (25 * pow(2, (float) new_shield / 5.0 ));
}       
       
int price_armour(int new_armour)
// price a armour base on rDEF and a fubinachi series pricing scheam
{                                 
  if (new_armour < 1) return 0;
  if (new_armour == 1) return 100;
  if (new_armour == 2) return 200;
  return price_armour(new_armour - 1) + price_armour(new_armour - 2);
}// price armour                      
       
int price_sword(int new_sword_dc, int new_sword_ocv)                         
// STUB - pricing should not be coupled to armour! 
{                                                        
  return price_armour(new_sword_dc-1) + new_sword_ocv * 20 - 100; // same fubinachi sequence 
}// price sword                                         
                                            
void setup_inventory(adv_character *merchant)               
// stub                                                                      
// setup some inventory for a shop                               
{                                                                           
  gear *new_item;                                      
  int i, num, rating1, rating2, cost, pts = 0;              
  char item_definition[MAX_LINE];           
                                                                
  // generate armour                                   
  num = d_random(5);                                  
  for (i = 0; i < num; i++)                         
  {                                                     
    rating1 = i + d_random(5);                      
    cost = price_armour(rating1);           
    sprintf(item_definition, "armour %d", rating1);
    merchant->add_item(new gear(NULL, item_definition, pts, cost));
  }// generate armour                                          
                                                             
  // generate shields                                        
  num = d_random(5);                                  
  for (i = 0; i < num; i++)                         
  {                                                     
    rating1 = 5+i*3 + d_random(20);                      
    cost = price_shield(rating1);              
    sprintf(item_definition, "shield %d", rating1);
    merchant->add_item(new gear(NULL, item_definition, pts, cost));
  }// generate shields                                                   
                                 
  // generate swords                                           
  num = d_random(5);                                  
  for (i = 0; i < num; i++)                         
  {                                                     
    rating1 = 1 + i + d_random(4);                      
    rating2 = d_random(10);
    cost = price_sword(rating1, rating2);
    sprintf(item_definition, "sword %d %d", rating1, rating2); // dc, ocv
    merchant->add_item(new gear(NULL, item_definition, pts, cost));
  } 
  // generate potions                                          
  merchant->add_item(new gear(NULL, "potion 1", 0, 300));
  merchant->set_id(ID_MERCHANT);                     
                                                   
}// setup inventory                  
                                             
                                              
void trade(adv_character *player, adv_display *display, menu_el *main_menu, 
                         char ** question, prompt *main_prompt, int& prompt_state)
// bring up the trading screen                                            
{                                                                            
  static adv_character *merchant;
  static int sell; // sell or buy goods      
  gear *item;
                                
  // many things are done the same whether the player or the trader is
  // selected. These variables are set up to reflect the current selection.  
  adv_character *current_character, *other_character;  
                                                    
  if (display->detail_state != detTRADE)
  {                                                                
    // first call, setup the trade window          
    sell = FALSE;  // start in the buying window    
    display->detail_state = detTRADE;
    merchant = new adv_character(display);          
    setup_inventory(merchant); // will be replaced by the actual inventory of the actual shop tk
                                                                       
    dwrite(display->event_win, "\nYou have entered the new trading market.");
    merchant->change_page_to(GEAR_PAGE);                                      
    player->change_page_to(TITLE_PAGE);                                      
    drefresh(display->event_win);                                       
    drefresh(display->detail_win);                                     
  } else {                                                           
    // second and subsequent calls                          
    log_event("trade - Trade action\n");                 
                                                                         
    if (sell)                   
    {                              
      current_character = player;
      other_character = merchant;
    } else {                    
      current_character = merchant;
      other_character = player;
    }//fi: sell               
                                                                            
    log_event("trade - main_prompt->key: %d\n", main_prompt->key);
    switch(main_prompt->key)                       
    {                                                
      case danioKEY_UP_ARROW:                   
      {                                            
        log_event("trade - cursor up\n");
        current_character->cursor_up();
        current_character->trade_page_boundry_check();
        break;                                
      }                                            
      case danioKEY_DOWN_ARROW:                 
      {                                            
             log_event("trade - cursor down\n");
        current_character->cursor_down();
          current_character->trade_page_boundry_check();
        break;                                           
      }                                                   
      case danioKEY_TAB:                             
      {                                                       
        log_event("trade - toggle buy/sell\n");
        sell ^= 1;  // toggle logic state                              
        other_character->change_page_to(GEAR_PAGE);     // show the cursor
        current_character->change_page_to(TITLE_PAGE);  // hide the cursor
        break;                                                  
      }// case: danioKEY_TAB                               
      case danioKEY_ENTER:                                 
      // purchase/sell the selected item              
      {                                                            
            log_event("Selection made\n");            
      // remove the item from the current character       
          item = current_character->drop_current_item();
          if (item == NULL) return;  
          // give the item to the other character             
          other_character->add_item(item);
          // charge the price                                
             current_character->add_cash(item->price);
          other_character->add_cash(-item->price);
          // update combat equiptment                   
             other_character->ready_item(item);
          break;
      }// case: danioKEY_ENTER                                  
      case danioKEY_PGUP:
      {            
             log_event("trade - page up\n");
          current_character->scroll_trade_page(-5);
          break;                               
      }                                               
      case danioKEY_PGDN:                         
      {                                           
             log_event("trade - page down\n");
          current_character->scroll_trade_page(+5);
          break;                   
      }                               
    }//switch: prompt->key???              
  }// fi: detail_state                                         
                               
  // redisplay                   
  dclear(display->detail_win);                                   
  // show characters cash
  text_norm();
  player->show_cash(display->detail_win);            
  // show characters gear                                     
  player->show_trade_page();                                  
  // show items for sale                                     
  merchant->show_trade_page();
  drefresh(display->detail_win);                        
  drefresh(display->event_win);                                

}// trade                                           
                                                      
                                                    
// ******************* END TRADING                
//                                                                 
//                                                            
// ************************ COMBAT ******************************



void announce_round(int round, adv_display *display) 
{                                      
  dwindow *win = display->event_win;
  setpcolour(IMPORTANT, win);          
  dwrite(win, "\nRound %d.", round);
  drefresh(win);                     
}// announce round                     
                                      
void end_fight_description(adv_character *player, int new_xp, 
                                int cash_found, adv_display *display)
{                                              
  dwindow *win = display->event_win;
                                         
  setpcolour(EVENT_COLOUR);         
  if (!player->dead())                          
  { 
    setpcolour(ACTION, win);
    dwrite(win, "\nYou won the battle!");
    setpcolour(GENERAL, win);
    player->show_body(win);           
    dwrite(win, "\nYou gained %d experience points and found $%d.",
                new_xp, cash_found);      
  } else {              
    setpcolour(WARNING, win);
    dwrite(win, "\n\nYou are dead.");
  }
  drefresh(win);          
  if (display->detail_state == detCHAR)
    player->show();          
}// end fight description                   
                                                                          
void have_fight(adv_character **character_list,
                      int dungeon_level, adv_display *display)
// initiate a battle sequence
// this is done by creating a monster in the current location
// if one doesn't already exist
// PRECONDITIONS: monster is not NULL, though *monster may be
{                                                    
  if ((character_list[1]) == NULL)   
  // no monster, so make one
  {                                              
    character_list[1] = new adv_character(display); // create a character
    character_list[1]->new_monster(dungeon_level);  // make it a monster
  }                                                        
}// have fight                                                                               
                                                            
void do_attack(int attack_type, adv_character *attacker, adv_character *defender, 
                    adv_display *display)                         
{                                                                    
  hit_data *hit;                                             
                                                                   
  hit = attacker->attack(defender);  // generate a hit
  defender->take_hit(hit, display->event_win);            // record the hit
  if ((attack_type == PLAYER_HIT)        &&   // update detail display
      (display->detail_state == detCHAR) &&                     
      (hit->body_damage > 0))                                       
    defender->show();                                                
  delete hit;              // done with the hit                                        
} // do attack                                             
                                                             
void have_round(adv_character **character_list, adv_display *display, int round)
{                                                          
  int roll; // a dice roll       
  int hit_chance; // chance of hitting                   
                                         
  announce_round(round, display);                       
  // player attacks                      tk rediness based attacks
  if (!character_list[0]->unconcious())       
    do_attack(MONSTER_HIT, character_list[0], character_list[1], display);
  // monster attacks                                   
  do_attack(PLAYER_HIT, character_list[1], character_list[0], display);
  // recoveries                   
  character_list[0]->end_seg_recovery();       
  if (character_list[1]->dead() || character_list[1]->unconcious())
  {                                                                     
    character_list[0]->earn_xp(round);
    character_list[0]->add_cash(character_list[1]->get_cash());
    end_fight_description(character_list[0], round, 
                                  character_list[1]->get_cash(), display);
    delete character_list[1];                                         
    character_list[1] = NULL;                 
    log_event("have_round() - monster deleted\n");
  } // if : monster is dead                                    
} // have round (of combat)                                       
                                                                         
void wait_sec(int sec)                            
// wait for sec seconds                           
{                                           
#ifndef NOWAIT                                 
  time_t *start, *now;                                  
                                                   
  start = new time_t;                           
  now = new time_t;                           
                                                    
  time(start);                                       
  do                                                           
    time(now);                                            
  while (difftime(*now, *start) < sec);              
  delete now;                              
  delete start;                                 
#endif                                         
}//wait sec                                         
                                                      
                                                      
void do_combat(adv_character **character_list, adv_display *display, 
                 adv_clock *time_keeper, room *location)                             
// co-ordinate time dependant game events, such as attacks, recoveries etc.
// time is tracked by the time_keeper
// upcomming events are queed 
// when this function is called all events that should occur in the time which
// has elapsed will be processed
{                                                                              
  static time_t *last_round;                                             
  static int round;                                                     
  time_t game_time;                                              
                                                                     
  game_time = time_keeper->get_game_time();                  
  if (character_list[1] != NULL) // combat will run so long as there is a 
                                 // monster. tk - test if monster present in 
                                 // room
  {                                                                                  
    if (last_round)                                                         
    { // combat has already begun                                   
      // check to see if a round has passed               
             drefresh(display->event_win);                                          
      if (difftime(game_time, *last_round) >= ROUND_TIME)
      {                                                    
      log_event("new round\n");              
             round++;                                                                 
             *last_round = game_time;  // reset the round clock
             have_round(character_list, display, round);        
             if (character_list[1] == NULL) // fight is over: monster defeated
        {                                                             
          delete last_round;                                 
          last_round = NULL;                                 
          location->has_monster = 0;               
               character_list[0]->end_combat_recovery(); 
        log_event("fight over\n");
        } //fi: end fight                                  
      }      // fi: do a round                                              
    } else { // monster has just appeared, first round not yet started
      setpcolour(WARNING, display->event_win);            
      dwrite(display->event_win, "\n * You encounter a monster. *");
      drefresh(display->event_win);                                
      setpcolour(GENERAL, display->event_win);            
      last_round = new time_t;  // start the fight clock  
      // set last round equal to current game time            
      *last_round = game_time;                                     
      round = 0;                                                      
      // go away and wait for a round                        
    } // fi: battle joined                                            
  } // fi : monster                                             
  else                                                                
  { // no monster present :- ensure clock is not ticking  
    // this is neccessary if the character flees from a monster else
    // combat is not re-set                                    
    if (last_round)                                              
    {                                                                 
      delete last_round;                                       
      last_round = NULL;                                       
    }                                                                 
  } // no monster present                                      
} // do combat                                                                         
                                                                                           
// *********************  END COMBAT                         
//                                                             
//                                                         
//                                     
// ******************************* DESCRIPTIONS *************************
                                             
void restore_detail(adv_display *display, menu_el *main_menu, adv_character *player)
// redisplay prior detail screen
{                         
  switch (display->detail_state)
  {                       
    case detMENU: main_menu->show(); break;
    case detCHAR: player->show(); break;
    default:                    
    {                                       
      dclear(display->detail_win);      
      dwrite(display->detail_win, "\nUnknown detail state.");
      drefresh(display->detail_win);                           
      break;                                            
    } // case: default                                 
  } // switch : display->detail_state                     
} // restore detail                                                   
                                                    
                                                    
void description(int dungeon_level, room *location, 
             adv_character **character_list, adv_display *display,
             int force = FALSE)
// describe the scene                                                           
{                                                                 
  dwindow *win = display->description_win;               
  static room *last_location = NULL;                     
  int i;                                        
                                                
  if ((location != last_location) || (force))               
  {                                                                
    last_location = location;                                 
    setpcolour(DESCRIPTION_COLOUR);                                 
    if (dungeon_level)                                                    
    {                                                                       
      dwrite(win,                                                                       
                    "\nYou are in the dungeon on level %d.", dungeon_level);
    } else {                                                                           
      dwrite(win, "\nYou are in the Town.");                        
    } // fi                                                           
    // give directions                                        
    dwrite(win, "\nYou can go ");                     
    if (location->nabour[NORTH]) dwrite(win, "North ");         
    if (location->nabour[SOUTH]) dwrite(win, "South ");         
    if (location->nabour[EAST]) dwrite(win, "East ");         
    if (location->nabour[WEST]) dwrite(win, "West ");         
    if (location->way_up) dwrite(win, "Up ");               
    if (location->way_down) dwrite(win, "Down ");         
    // list who is here                                 
    for (i = 0; i < location->num_occupants; i++)         
    {                                                      
      dwrite(win, "\n%s is here.", 
           character_list[location->occupant_list[i]]->get_name());
    }                                                 
                   
                   
    drefresh(win);                     
  }                             
} // description                                                            
                             
// *********************  END DESCRIPTIONS
//                             
//                             
//                              
// ********************** INPUT HANDLING **************************
                                                                                 
void take_input_control(char **question, const char *new_question, 
                                       char **old_question, int& menu_state, 
                                       const int new_menu_state, int& old_menu_state,
                                       prompt *main_prompt, const int new_x, int *old_x)
// do a handover for functions that take control of input
// This function is used both to take control and to relinquish control
// To take control:                                    
//   PRECONDITIONS:                              
//                        * QUESTIONS *                  
//     question is a pointer to the string which holds the prompt question, 
//       from the previous prompt                        
//     new_question is the new question to be possed, posibly a constant
//     old_question is a static variable of the calling function which
//       points to a string (the string may be NULL, but
//       old_question must not be NULL)                  
//                                   * MENU STATES *              
//     menu_state points to menu::state member of main_menu, which holds the 
//       menu state from the previous input controler (usually msMAIN_MENU)
//     new_menu_state holds the state identifier for the calling function
//     old_menu_state points to a static variable of the calling function to
//       store the old value of main_menu::state   
//                                        * PROMPT/X *                        
//     main_prompt is the game prompt, with prompt::col appropriate to 
//       previous question          
//     new_x is an x-ordinate suitable for the input window with the 
//       new_question                  
//     old_x points to a static variable of the calling function to hold the 
//       old value of main_prompt::col                         
//   POSTCONDITIONS:                                             
//                        * QUESTIONS *         
//     question will point to a copy of new_question
//     new_question will be unmodified                         
//     old_question will point to a string containing a copy of the previous
//       question                                      
//                                   * MENU STATES *
//     menu_state points to menu::state member of main_menu, which now contains
//       the value from new_menu_state, to identify the calling function as the
//       input controller                 
//     new_menu_state is unchanged           
//     old_menu_state points to a copy of the value previously pointed to by 
//       menu_state                 
//                                  * PROMPT/X * 
//     main_prompt::col is changed to suit new question
//     new_x is unchanged
//     old_x points to a copy of the previous value of main_prompt::col
{                                                                                              
  // change the question                               
  assert(old_question);                                     
  if (*old_question) delete *old_question;
  (*old_question) = new_str(*question);
  (*question) = new_str(new_question);   
  // change the menu state     
  old_menu_state = menu_state;
  menu_state = new_menu_state;
  // change the x ordinate of the input window
  *old_x = main_prompt->get_col();
  main_prompt->pmove(new_x, PROMPT_LINE); 
} // input control change      
      
void restore_input_control(char **question, char *old_question, 
                            int& menu_state, int old_menu_state, 
                                          prompt *main_prompt, int old_x)
// do a handover for functions that are done with control of input
{                                              
  if (*question) delete *question;           // out with the old 
  *question = new_str(old_question);           // in with the older
  menu_state = old_menu_state;
  main_prompt->pmove(old_x, PROMPT_LINE);
}// restore input control                   
                            
void cleanup_prompt(prompt *main_prompt, char *question)
      // clear any information remaining from previous entries
{                          
  setpcolour(PROMPT_COLOUR);            // make sure the colour is right
  dcur_move(1, PROMPT_LINE);
  dwrite(BLANK_LINE);                                  // clear garbage  
  main_prompt->setpstr(NULL);           // get rid of the old input 
  main_prompt->redraw();                // redraw the input window
  dcur_move(1, PROMPT_LINE);        
  dwrite(question);                     // redraw the question
} // cleanup prompt                           
                                                  
void forbid(int& ans)                           
{                                                    
  ans = advcmdNO_COMMAND;                                
  say_status("You can't do that here.");
}// forbid                             
                                                   
int filter_cmd(int cmd, room *location, int dungeon_level)
// remove impossible actions before applying the event handler
{                                                                     
  switch (cmd)                                                       
  {                                               
    case advcmdDOWN: // go down                             
      if (!location->way_down) forbid(cmd); break;    
    case advcmdUP: // go up                             
      if (!location->way_up) forbid(cmd); break;      
    case advcmdNORTH: // go north                       
      if (!location->nabour[NORTH]) forbid(cmd); break;
    case advcmdSOUTH: // go south                                    
      if (!location->nabour[SOUTH]) forbid(cmd); break;
    case advcmdEAST: // go east                                  
      if (!location->nabour[EAST]) forbid(cmd); break;
    case advcmdWEST: // go west                       
      if (!location->nabour[WEST]) forbid(cmd); break;
    case advcmdENTER_MARKET: // enter market 
    case advcmdSPEND_XP: // spend xp                                             
    case advcmdREST: // rest                                                 
      if (dungeon_level) forbid(cmd);                       
      break;                                           
    case advcmdSTART_FIGHT: // fight           
      if (!dungeon_level) forbid(cmd); break;
    case advcmdSAVE_CHAR:           
    case advcmdLOAD_CHAR:           
      if (dungeon_level) forbid(cmd);
      break;                       
  }//switch                                                                 
                                                                               
  return cmd;                                                                    
} // filter                                                                 
                                        
void handle_user_selection(int ans, room **dungeon, int& dungeon_level,     
                                          adv_character **character_list, 
                     room **location_list,                           
                                   adv_display *display, menu_el *main_menu,
                                    char **question, int& play, prompt *main_prompt, 
                     int& prompt_state, int& next_character)
{ 
  log_event("handle_user_selection()\n");
  log_event("handle_user_selection - prompt_state: %d\n", prompt_state);
  log_event("handle_user_selection - ans: %d\n", ans);
    
  switch (ans)               // carry out actions              
  {                                                                               
    case advcmdNO_COMMAND: break;      // impossible action selected   
    
    case advcmdQUIT: play = FALSE; break;         // end the game          
    
    case advcmdSAVE_GAME:                       
    {                                    
      save_game(dungeon, character_list, location_list, next_character);
      break;                                               
    }                                                      
    
    case advcmdLOAD_GAME:                                    
    {                                                         
      load_game(dungeon, character_list, location_list, next_character, display);
      break;                            
    }                                   
    
    case advcmdSHOW_CHAR:                       
    {                                   
      character_list[0]->show();                
      display->detail_state = detCHAR;          
      break; // show the character                
    }
    case advcmdTAB:
    {         
      log_event("handle_user_selection - advcmdTAB\n");
      if (display->detail_state == detTRADE)
      {         
          trade(character_list[0], display, main_menu, question, main_prompt, prompt_state);
      }         
      break;
    }// case: advcmdTAB
              
    case advcmdPGUP:                        
    {                                        
      if (display->detail_state == detCHAR)
      {          
                character_list[0]->change_page(-1);
        character_list[0]->show();
      } else                     
      if (display->detail_state == detTRADE)
      {           
          trade(character_list[0], display, main_menu,                 
                      question, main_prompt, prompt_state);
      }          
      break;                            
    }                                     
  
    case advcmdPGDN:                 
    {                                        
      if (display->detail_state == detCHAR)
      {                    
                character_list[0]->change_page(+1);
        character_list[0]->show();
              } else                    
      if (display->detail_state == detTRADE)
      {           
          trade(character_list[0], display, main_menu,                 
                      question, main_prompt, prompt_state);
      }          
      break;                           
    }                                 
  
    case advcmdCURSOR_UP:       
    {                                 
      if (display->detail_state == detCHAR) // could replace this with a switch too
      {                  
        character_list[0]->cursor_up();
        character_list[0]->show();
      } else             
      if (display->detail_state == detTRADE)
      {                   
          trade(character_list[0], display, main_menu, question, main_prompt, prompt_state);
      }                  
      break;                          
    }                   
  
    case advcmdCURSOR_DOWN:         
    {                                 
      if (display->detail_state == detCHAR)
      {        
        character_list[0]->cursor_down();
        character_list[0]->show();
      } else   
      if (display->detail_state == detTRADE)
      {           
        trade(character_list[0], display, main_menu, question, main_prompt, prompt_state);
      }          
      break;                           
    }                             
                                  
    case advcmdDRINK_POTION: // drink potion      
    {                                                                         
      character_list[0]->drink_potion();
      if (display->detail_state == detCHAR) 
        character_list[0]->show();
      break;                                                                         
    }                                                           
    
    case advcmdLOOK: // look                         
    {
      description(dungeon_level, location_list[0], character_list, display, TRUE); 
      break;      
    }
      
    case advcmdUP: // go up                               
    case advcmdDOWN: // go down                                   
    case advcmdNORTH: // go north       
    case advcmdSOUTH: // go south       
    case advcmdEAST: // go east         
    case advcmdWEST: // go west         
    {                                                                                  
      move_player(dungeon_level, character_list, location_list, ans, display);  
      break;                                                              
    }                                                            
    
    case advcmdENTER_MARKET: // enter market
    {
      trade(character_list[0], display, main_menu, question, main_prompt, prompt_state);
      break;                      
    }
    
    case advcmdSPEND_XP: // spend xp
    {
      spend_xp(character_list[0], display, main_menu, question, main_prompt, prompt_state); 
      break;
    }
    
    case advcmdREST: // rest          
    {                                                     
      character_list[0]->rest();                          
      if (display->detail_state == detCHAR) 
        character_list[0]->show();
      break;                                        
    }                                                    
    
    case advcmdSTART_FIGHT: // start fight
    {
      have_fight(character_list, dungeon_level, display); 
      break;
    }  
    
    case advcmdSAVE_CHAR:                         
    case danioKEY_F2:         // save                              
    {
      character_list[0]->adv_save(); 
      break;
    }
      
    case advcmdLOAD_CHAR:                       
    case danioKEY_F3: // load                       
    {                                                     
      character_list[0]->adv_load();                     
      if (display->detail_state == detCHAR) 
        character_list[0]->show();
      break;                                    
    }                                      
    
    case advcmdTOGGLE_DISPLAY:        
    {                                      
      if (display->is_combined())     
        display->expand();            
      else                                
        display->combine();           
      if (display->detail_state == detMENU) 
        main_menu->show();   
      break;                                                   
    }   
                                                                
    case advcmdCOMBINE_DISPLAY: // combine display                 
    {                                                                
      if (!display->is_combined()) 
        display->combine();           
      if (display->detail_state == detMENU) 
        main_menu->show();   
      break;                                                   
    }
                                                                    
    case advcmdEXPAND_DISPLAY: // expand display                  
    {                                                           
      if (display->is_combined()) display->expand();            
      if (display->detail_state == detMENU) main_menu->show();   
      break;                                                    
    }
                                                                    
    case advcmdSHOW_MENU: // show menu                       
    {                                                               
      log_event("handle user selection - show menu\n");           
      main_menu->show();                                   
      display->detail_state = detMENU;                       
      break;                                                   
    }
                                                                   
    case advcmdENTER: // Enter pressed with no command in the prompt 
    {                                        
      log_event("handle user selection - advcmdENTER\n");
      if (display->detail_state == detCHAR)
      {                                         
        character_list[0]->toggle_current_item_rediness();
        character_list[0]->show();      
      } else                               
      if (display->detail_state == detTRADE)
          trade(character_list[0], display, main_menu, question, main_prompt, prompt_state);
      break;                            
    }                                        
                                          
    case UP_MENU_TREE: break; // return from a sub menu            
                                           
    default: 
    {
      log_event("handle_user_selection() - prompt sate not handled, changed to msMAIN_MENU\n", prompt_state);
      bad_input();                                             
      break;                       
    }  
  }//switch: ans                                                     
} // user input                                                               
                                                                    
void user_input(char **question, prompt *main_prompt, room **dungeon,
                          int& dungeon_level, adv_character **character_list,             
                room **location_list,                                              
                      adv_display *display, menu_el *main_menu, int& prompt_state, 
            int& play, int& next_character)
// Track and handle user input.                                                      
// Keeps track of the prompt and looks for input in the form of a terminated
// prompt (cmd == CMD_QUIT) or a hot-key.                                      
// Input is then passed to an interpreter which converts raw input to a
// mnemonic command. Commands are then compared to the context to dissalow 
// improper commands (eg go up when there is no way up). Finally commands are
// passed to the handler.                                                        
{                                                                                                   
  int usr_cmd;    // a user selection                                                              
  int prompt_cmd; // an internal control command from the prompt, used to  
                  // detect prompt termination, usually by pressing <Enter>
                                                                                                     
  prompt_cmd = main_prompt->get_ed_cmd(); // look for input                          
  if (prompt_cmd != NO_COMMAND) // there was a keystroke                  
  {                                                                                             
    log_event(logDEFAULT, "user_input() - Prompt command: %d\n", prompt_cmd);                      
    log_event(logDEFAULT, "user_input() - Prompt key: %d\n", main_prompt->key);                      
    main_prompt->do_ed_cmd();        // make the prompt work                     
    
    clear_status_line();                           // remove old notices         
                                                                             
    usr_cmd = NO_COMMAND; // zero the user command
    if (prompt_cmd == CMD_QUIT) // i.e. the prompt has recieved a completed response (such as when <enter> has been pressed)
      usr_cmd = main_menu->interpret_std_key((int) main_prompt->s[0]);
    
    // catch hot keys;                               
    if (usr_cmd == NO_COMMAND) // skip if a command has allready been generated.
      usr_cmd = main_menu->interpret_hot_key(main_prompt->key);

    // check commands are sensible in context of game.        
    usr_cmd = filter_cmd(usr_cmd, location_list[0], dungeon_level);         
    
    if (usr_cmd != NO_COMMAND)
    {
      handle_user_selection(usr_cmd, dungeon, dungeon_level, character_list, location_list, display, 
                            main_menu, question, play, main_prompt, prompt_state, next_character);
      description(dungeon_level, location_list[0], character_list, display); 
      cleanup_prompt(main_prompt, *question);
    } // fi: proccess cmd
    
  } // input made                                                             
} // user input                                                     
                                             
// ***************** END INPUT HANLING
//                                  
//                 
//                 
// ************************** STARTUP *****************************
                          
room *new_room()              
// generate a new room          
{                                                         
  room *r;                                              
                                                         
  if ((r = new room()) == NULL)                         
  {                                                             
    dwrite("Mem err, cannot make new room().");            
    exit(1);                                                   
  }//fi                                                              
  return r;                                                    
}// new room                                                    

int find_random_nabour(int x, int y)
{
  int all_tried = FALSE;
  switch (d_random(4))      
  {                             
    start:                     
    case 1: // north      
      all_tried = TRUE;            
      if (in_bounds(x, y-1)) return NORTH;      
    case 2: // south                    
      if (in_bounds(x, y+1)) return SOUTH;                    
    case 3: // east                           
      if (in_bounds(x+1, y)) return EAST;                    
    case 4: // west                       
      if (in_bounds(x-1, y)) return WEST;                  
    if (!all_tried) goto start;                
  } // switch                 
  assert(all_tried == FALSE); // should never reach here
} // find random nabour
                   
int find_closed(       
    room *dungeon_map[DUNGEON_LEVELS][DUNGEON_SIZE_X][DUNGEON_SIZE_Y], 
                  int z, int x, int y)
// randomly chose a direction in which an unconnected room lies
{                            
  int all_tried = FALSE;
  switch (d_random(4))       
  {                                
    start:                        
    case 1: // north      
      all_tried = TRUE;            
//      log_event("Try: %d, %d :", x, y-1);
      if ((in_bounds(x, y-1)) && 
               (dungeon_map[z][x][y-1]->is_closed())
         ) return NORTH;      
    case 2: // south                   
//      log_event("Try: %d, %d :", x, y+1);
      if ((in_bounds(x, y+1)) &&       
          (dungeon_map[z][x][y+1]->is_closed())
         ) return SOUTH;                    
    case 3: // east                           
//      log_event("Try: %d, %d :", x+1, y);
      if ((in_bounds(x+1, y)) &&       
          (dungeon_map[z][x+1][y]->is_closed())
         ) return EAST;                    
    case 4: // west                       
//      log_event("Try: %d, %d :", x-1, y);
      if ((in_bounds(x-1, y)) &&       
          (dungeon_map[z][x-1][y]->is_closed())
         ) return WEST;                  
    if (!all_tried) goto start;                
  } // switch
//  log_event("found dead end");
  return BEARING_ERR;  
} // find closed       



void extend_path(      
           room *dungeon_map[DUNGEON_LEVELS][DUNGEON_SIZE_X][DUNGEON_SIZE_Y], 
                     int z, int& x, int& y, int direction)
// link to room in direction and move current location there               
{                            
  int new_x, new_y, op_direction;
                        
  // find the new room 
  switch (direction)   
  {                                             
    case NORTH: new_x = x; new_y = y - 1; op_direction = SOUTH; break;             
    case SOUTH: new_x = x; new_y = y + 1; op_direction = NORTH; break;
    case EAST:       new_x = x + 1; new_y = y; op_direction = WEST; break;
    case WEST:       new_x = x - 1; new_y = y; op_direction = EAST; break;
  } // switch: direction              
                                             
  // link the rooms in both directions!        
  dungeon_map[z][x][y]->nabour[direction] = dungeon_map[z][new_x][new_y];
  dungeon_map[z][new_x][new_y]->nabour[op_direction] = dungeon_map[z][x][y];
                                          
  // move to new room                       
  x = new_x; y = new_y;                           
}// extend path                                  
                                      
void join_one_way(                    
           room *dungeon_map[DUNGEON_LEVELS][DUNGEON_SIZE_X][DUNGEON_SIZE_Y], 
                     int z, int x, int y, int direction)
// link to room in direction and move current location there               
{                                                
  int new_x, new_y, op_direction;        
                                            
  // find the new room                     
  switch (direction)                       
  {                                             
    case NORTH: new_x = x; new_y = y - 1; break;             
    case SOUTH: new_x = x; new_y = y + 1; break;
    case EAST:       new_x = x + 1; new_y = y; break;
    case WEST:       new_x = x - 1; new_y = y; break;
  } // switch: direction              
                                             
  // link the rooms in only one direction        
  dungeon_map[z][x][y]->nabour[direction] = dungeon_map[z][new_x][new_y];
}// join one way                       
                                
void join_both_ways(                    
           room *dungeon_map[DUNGEON_LEVELS][DUNGEON_SIZE_X][DUNGEON_SIZE_Y], 
                     int z, int x, int y, int direction)
// link to room in direction and move current location there               
{                                                
  int new_x, new_y, op_direction;        
                                            
  // find the new room                     
  switch (direction)                       
  {                                             
    case NORTH: new_x = x; new_y = y - 1; op_direction = SOUTH; break;             
    case SOUTH: new_x = x; new_y = y + 1; op_direction = NORTH; break;
    case EAST:       new_x = x + 1; new_y = y; op_direction = WEST; break;
    case WEST:       new_x = x - 1; new_y = y; op_direction = EAST; break;
  } // switch: direction              
                                             
  // link the rooms in both directions!        
  dungeon_map[z][x][y]->nabour[direction] = dungeon_map[z][new_x][new_y];
  dungeon_map[z][new_x][new_y]->nabour[op_direction] = dungeon_map[z][x][y];
}// join both ways
                                                 
void add_bud(point bud_list[MAX_BUDS], int& num_buds, int x, int y)
// put a new bud into the bud list                         
{                                                                      
  if (num_buds < MAX_BUDS)                               
  {                                                              
    bud_list[num_buds].x = x;                               
    bud_list[num_buds].y = y;                               
    num_buds++;                                           
  } else                                                  
    log_event(" ******* Bud list full\n"); // this is an error, but not fatal
} // add bud                                              
                                                                
void new_branch(point bud_list[MAX_BUDS], int& num_buds, int& x, int& y)
// move to a new branch point in the maze
{     
  int chosen_bud;
  // pick a bud to branch from
  chosen_bud = d_random(num_buds) - 1; 
  // set up the new x and y
  x = bud_list[chosen_bud].x; 
  y = bud_list[chosen_bud].y;
  // remove the old bud from the bud_list
  num_buds--;                   
  for (int i = chosen_bud; i < num_buds; i++)
  {                                          
    bud_list[i].x = bud_list[i+1].x;
    bud_list[i].y = bud_list[i+1].y;
  }                                 
}// new branch
                                                                        
room *make_dungeon(adv_character **character_list, room **location_list, 
               int& next_character, adv_display *display)
// make a dungeon                                                         
// A dungeon consists of levels of rooms conected by ways up and down
// not all passages can be traversed in both directions. In general there are
// more ways down than up and more ways in than out.                 
{                                                                                     
  room *dungeon, *r, *l;  // main dungeon, room, level               
  point bud_list[MAX_BUDS]; // 0 is unused                    
  int num_buds, direction, length;           
  int i, j, x, y, z;                                            
                                                                        
  r = new_room();               
  r->has_monster = FALSE;         
  dungeon = r;          // the town                                   
 
  log_event("Suspending log for dungeon creation.\n");
  suspend_log(); // creating the dungeon writes around 40000 log lines
  // make all the rooms for the dungeon                      
  for (z = 0; z < DUNGEON_LEVELS; z++)                      
  {                                                                          
    for (y = 0; y < DUNGEON_SIZE_Y; y++)                                  
    {                                                                                 
      for (x = 0; x < DUNGEON_SIZE_X; x++)                              
      {                                                                               
        dungeon_map[z][x][y] = new_room();
             if (!(d_random(5)-1))
      {                   
        if (next_character < MAX_CHARACTERS)                   
        {                                                            
                 // make a monster present                         
            dungeon_map[z][x][y]->has_monster = next_character;       
          dungeon_map[z][x][y]->num_occupants = 1;                  
          dungeon_map[z][x][y]->occupant_list[0] = next_character;
            character_list[next_character] = new adv_character(display);
          log_event("setup - character %d created\n", next_character);
                 character_list[next_character]->new_monster(z+1);       
          location_list[next_character] = dungeon_map[z][x][y];
                 next_character++;             
        } else {
          log_event("******** to many monsters");
          exit(EXIT_FAILURE);          // tk - must be able to handle this better
        }
        
      } else {                             
        // no monster
        dungeon_map[z][x][y]->has_monster = FALSE;
               dungeon_map[z][x][y]->num_occupants = 0;           
      } //fi: monster present
      } // for all x   
    } // for all y                                                                
  } // for all z                                                               
                                                                                  
  // make the level into a maze                                   
  log_event("Start making maze\n");                          
  for (z = 0; z < DUNGEON_LEVELS; z++)                           
  {                                                                   
    log_event("Start level %d\n", z+1);             
    // make a list of rooms that can be extendead: initally empty
    num_buds = 0;                                      
    // pick a random start                                             
    x = d_random(DUNGEON_SIZE_X) - 1; y = d_random(DUNGEON_SIZE_Y) - 1;       
//    log_event("Current room %d, %d\n", x, y);
    do // build the maze                                    
    {                                                              
      length = 0;                                           
      do // build a path                                   
      {                                        
        direction = find_closed(dungeon_map, z, x, y);
        if (direction != BEARING_ERR)                        
            {                                                        
              add_bud(bud_list, num_buds, x, y); // add current location to bud_list
               extend_path(dungeon_map, z, x, y, direction);      // link to room in direction and move current location there               
              length++;                                            
//              log_event("branch extended\n");            
//          log_event("Current room %d, %d\n", x, y);
             } else {                              
              length = -1;                              
//              log_event("Branch terminated - nowhere to go\n");
            }      
      }      while ((length > 0) && (length < MAZE_MAX_PATH)); // build the path
      new_branch(bud_list, num_buds, x, y); // move to a new bud
//      if (num_buds > 0) log_event("Start new branch\n");
//      log_event("Current room %d, %d\n", x, y);
    } while(num_buds > 0); // build the maze                         
    // make random one way passages
    for (i = 0; i < ONE_WAY_PASSAGES; i++)
    { 
      x = d_random(10) - 1; y = d_random(10) - 1;    // pick random room
      direction = find_random_nabour(x, y);    // pick room in random direction
      join_one_way(dungeon_map, z, x, y, direction); // link
    } 
    // make random two way passages              
    for (i = 0; i < TWO_WAY_PASSAGES; i++)
    {                    
      x = d_random(10) - 1; y = d_random(10) - 1;    // pick random room
      direction = find_random_nabour(x, y);    // pick room in random direction
      join_both_ways(dungeon_map, z, x, y, direction); // link
    } 
  } // for: all levels                                     
      
  /* old code if you want no maze, just a big open area.
  // connect rooms throught level                              
  for (z = 0; z < DUNGEON_LEVELS; z++)                  
  {                                                                 
    for (y = 0; y < DUNGEON_SIZE_Y; y++)                          
    {                                                                            
      for (x = 0; x < DUNGEON_SIZE_X; x++)            
      {                                                
        if (y > 0)                                  
          dungeon_map[z][x][y]->nabour[NORTH] = dungeon_map[z][x][y - 1];
        if (y < (DUNGEON_SIZE_Y - 1))                  
          dungeon_map[z][x][y]->nabour[SOUTH] = dungeon_map[z][x][y + 1];
        if (x < (DUNGEON_SIZE_X - 1))                              
          dungeon_map[z][x][y]->nabour[EAST]  = dungeon_map[z][x + 1][y];
        if (x > 0)                                               
          dungeon_map[z][x][y]->nabour[WEST]  = dungeon_map[z][x - 1][y];
      } // for all x                                                                   
    } // for all y                                                                   
  } // for all z                                           
  */                                                           
  // make connections between levels                     
  for (z = 0; z < (DUNGEON_LEVELS-1); z++)                  
  {                                                          
    // make connections down                                  
    for (i = 0; i < WAYS_DOWN; i++)                          
    {                                                                   
      // make a way from this level down to the next      
      x = d_random(DUNGEON_SIZE_X) - 1;                  
      y = d_random(DUNGEON_SIZE_Y) - 1;                  
      dungeon_map[z][x][y]->way_down = dungeon_map[z+1][x][y];
    }                                                                   
                                                                       
    // make connections up                                     
    for (i = 0; i < WAYS_UP; i++)                            
    {                                                                        
      // make a way from the level below up to this
      x = d_random(DUNGEON_SIZE_X) - 1;                  
      y = d_random(DUNGEON_SIZE_Y) - 1;                  
      dungeon_map[z+1][x][y]->way_up = dungeon_map[z][x][y];
    }                                                                   
    // make conections up and down                             
    for (i = 0; i < WAYS_UP_AND_DOWN; i++)                        
    {                                                                      
      // make a way from this level down to the next and back up
      x = d_random(DUNGEON_SIZE_X) - 1;                          
      y = d_random(DUNGEON_SIZE_Y) - 1;                  
      dungeon_map[z][x][y]->way_down = dungeon_map[z+1][x][y];
      dungeon_map[z+1][x][y]->way_up = dungeon_map[z][x][y];
    } // for: bi-directional stairs  
  } // for: connect levels             
                                                               
  // conect the dungeon_map and the town (room *dungeon) 
  dungeon->way_down = dungeon_map[0][0][0];                                        
  dungeon_map[0][0][0]->way_up = dungeon;
  
  resume_log();
  return dungeon;                                                   
}// make dungeon                                                     
                                                    
void init(adv_display **display, adv_clock **time_keeper)
// call init rutines for external services       
{                                     
  int err;
  
  start_log();                                             
  set_log_level(DEBUG_LEVEL);
  log_event(logDEFAULT, "adv_game.cc:init; debug level: %d\n", DEBUG_LEVEL);            
  //initialise random number generator                   
  randomize();                                            
  log_event("adv_game.cc:init - randomize ok\n");
  // initialise danio
  err = init_danio();
  if (err)                                            
    log_event("adv_game.cc:init - danio err: %d\n", err);
  else                                                        
    log_event("adv_game.cc:init - danio ok\n");
  dset_scroll(OFF);                    
  dcur(CURSOR_INVIS); // cursor invisible
  // initilise adv_display.h                               
  *display = new adv_display();
  log_event("adv_game.cc:init - display ok\n");
  // initilises the game clock      
  *time_keeper = new adv_clock();
  log_event("adv_game.cc:init - time keeper ok\n");
  log_event("adv_game.cc:init - end\n");
}// init                                                                  
                   
void setup(adv_character *character_list[MAX_CHARACTERS],
         room **location_list,
         adv_display *display,
                menu_el **main_menu, int& dungeon_level, room **dungeon, 
                prompt ** main_prompt, char ** question,
         event **event_head,
         int& next_character)                         
// set up the game ellements                           
// initialise event list
// creates a player character                          
// creates the menu                               
// creats the character list                           
// sets the dungeon level to 0                           
// creates the main_menu and displays the menu bar     
// creates the dungeon                                       
{                                            
  event *new_event;                               
  // initialise the event list with dummy head and tail nodes
  *event_head = new event(NULL, NULL, 0, 0, 0);  // create head
  new_event = new event(NULL, NULL, 0x7FFFFFFF, 99, 0);      // create tail       
  (*event_head)->next = new_event; // link head to tail
                                             
  // initialise the character list                     
  for (int i = 0; i < MAX_CHARACTERS; i++)
    character_list[i] = NULL;       
  log_event("setup - character list nulled\n");
  // make player                                                  
  character_list[0] = new adv_character(display);   
  character_list[0]->new_player();              
  log_event("setup - player instatiated\n");
  // the menu                                          
  *main_menu = new menu_el("   ~Main Menu~",              
               new menu_el("~F1~ Show menu", 0, danioKEY_F1, advcmdSHOW_MENU,
               new menu_el("~F2~ Save Game", 0, danioKEY_F2, advcmdSAVE_GAME, 
               new menu_el("~F3~ Load Game", 0, danioKEY_F3, advcmdLOAD_GAME,   
               new menu_el("~F4~ Toggle display mode",            
                           0, danioKEY_F4, advcmdTOGGLE_DISPLAY,
               new menu_el("~F5~ Show Character", 0, danioKEY_F5, advcmdSHOW_CHAR,
               new menu_el("~F6~ Drink ~P~otion", 'p', danioKEY_F6, advcmdDRINK_POTION,
               new menu_el("~F7~ ~L~ook", 'l', danioKEY_F7, advcmdLOOK,
               new menu_el("~PgUp/PgDn~ Char Page",     
                 new menu_el("", 0, danioKEY_PGDN, advcmdPGDN, 
                 new menu_el("", 0, danioKEY_PGUP, advcmdPGUP,                      
               new menu_el("", 0, danioKEY_UP_ARROW, advcmdCURSOR_UP,
               new menu_el("", 0, danioKEY_DOWN_ARROW, advcmdCURSOR_DOWN,
               new menu_el("~NSEWUD~ Move",                
                 new menu_el("", 'n', advcmdNORTH,                             
                 new menu_el("", 's', advcmdSOUTH,                             
                 new menu_el("", 'e', advcmdEAST,             
                 new menu_el("", 'w', advcmdWEST,             
                 new menu_el("", 'u', advcmdUP,              
                 new menu_el("", 'd', advcmdDOWN,             
               new menu_el("~R~est", 'r', advcmdREST, 
               new menu_el("~F~ight", 'f', advcmdSTART_FIGHT,
               new menu_el("Enter the ~m~arket", 'm', 0, advcmdENTER_MARKET,
                 new menu_el("   Market",
//                 new menu_el("~1~ Buy Sword", '1', advcmdBUY_SWORD,
  //               new menu_el("~2~ Buy Shield", '2', advcmdBUY_SHIELD,
    //             new menu_el("~3~ Buy Armour", '3', advcmdBUY_ARMOUR,
      //           new menu_el("~4~ Buy Potion", '4', advcmdBUY_POTION,
               NULL )/*))))*/, // end of market sub menu
               new menu_el("", 0, danioKEY_TAB, advcmdTAB, // just a pure hot key, not a sub menu
               new menu_el("Spend ~x~p", 'x', advcmdSPEND_XP,
               new menu_el("~I~mport Character", 'i', 0, advcmdLOAD_CHAR,
               new menu_el("Expor~t~ Character", 't', 0, advcmdSAVE_CHAR,
               new menu_el("~Q~uit", 'q', danioKEY_ESC, advcmdQUIT,              
               new menu_el("", 0, danioKEY_ENTER, advcmdENTER,                      
               NULL ))))) ))))) ))))) ))))) ))))) ))));
  log_event("setup - menu instantiated\n");
  (*main_menu)->set_win(display->detail_win);
  (*main_menu)->show();                               
  log_event("show menu\n");                      
  setpcolour(MENU_COLOUR);                    
  dcur_move(1, MENU_LINE);                          
  dwrite("F1 for Menu.                                                                    \n");
  //           12345678901234567890123456789012345678901234567890123456789012345678901234567890
  drefresh();                                 
                                         
  // create the prompt                            
  *main_prompt = new prompt(ADV_PROMPT_LEN, NULL, 
                            PROMPT_COLOUR, NO_AUTHORITY);
  (*question) = new_str("Enter your selection: ");
  setpcolour(PROMPT_COLOUR);                
  dcur_move(1, PROMPT_LINE);
  dwrite(*question);        
  (*main_prompt)->pmove(xoMAIN_MENU, PROMPT_LINE);        // NB:  x, y order
  (*main_prompt)->redraw();                 
  // create dungeon                                          
  say_status("Making Dungeon");
  dungeon_level = 0; // set level to ground/town level
  *dungeon = make_dungeon(character_list, location_list, 
                       next_character, display);
}// setup                                                       
                                              
void intro()                                                                 
{                                             
  say_status("An adventure game: Daniel Vale.");
} // intro                                     
                                              
// ************ END STARTUP                 
//                                            
//                                            
//                                      
// ******************* CLEAN UP      **********************************
                                          
void cloze(adv_character *player, adv_display *display)
// give a finale                                         
{                                                 
  dwindow *win = display->event_win;        
                                               
  setpcolour(EVENT_COLOUR);               
  player->show_cash(win);                               
  dwrite(win, "\nWhat an adventure!");        
  drefresh(win);                                         
                                             
  say_status("           G a m e  O v e r");
  setpcolour(PROMPT_COLOUR);
  dcur_move(1, PROMPT_LINE);
  dwrite("Press any key to end.");
  dcleareol();                               
  dgetch_wait(-1);
  (void*) dgetch();                       
  close_danio();                                                 
}//cloze                                                               
                                         
// ********************* END CLEANUP        
//                                          
//                                          
//                                          
// ******************************** MAIN *********************************
                                                 
int main()                                                
{                                                                
  
  int play = TRUE;             // main loop condition
  adv_character *character_list[MAX_CHARACTERS]; // list of all the characters
  room *location_list[MAX_CHARACTERS]; // list of all the character locations
  // note: locations can have characters, and characters have locations. To
  // avoid a circular refrence we just keep two arrays and match indexes
  // therfore character_list[n] is at location_list[n]
  // n = 0 is the player                     
  int next_character = 2;      // next empty slot in character_list, 0 is for player, 1 is reserved for an attacker
  int dungeon_level;           // depth in dungeon (1 == 1st level underground)
  room *dungeon = NULL;             // the dungeon/world
  adv_display *display;             // tracks the window regions of the display
  menu_el *main_menu = NULL;      // the main menu         
  adv_clock *time_keeper = NULL;      // the game clock
  event *event_head = NULL;    // event/action tracking
                                             
  // prompt stuff                                       
  prompt *main_prompt;              // the main prompt      
  int prompt_state = msMAIN_MENU; // allows the prompt to be comandeared
  char *question;                   // the posed question   
  void *caller;                          // the function waiting for input
                                                         
  init(&display, &time_keeper);                                  
  log_event("init ok\n");                         
  setup(character_list, location_list, display,    // set up the game ellements
             &main_menu, dungeon_level, &dungeon, &main_prompt, 
      &question, &event_head, next_character);                   
  log_event("setup ok\n");                         
  intro();                          // show an introduction
  location_list[0] = dungeon;       // start player at the dungeon head
  description(dungeon_level, location_list[0], character_list, display);
  log_event("main - Number of characters: %d\n", next_character); 
  log_event("main - Character[2]: %d\n", character_list[2]);
  do  // play                                                                                
  {                                                                  
//    log_event("begin main loop\n");               
//    wait_sec(1);                                         
//    log_event("before time update\n");            
    time_keeper->update();                                       // track time
//    log_event("time\n");                                 
//    wait_sec(1);                                     
    user_input(&question, main_prompt, &dungeon,
                   dungeon_level, character_list, location_list,    
                    display, main_menu, prompt_state, play, next_character);
//    log_event("input\n");                                       
//    wait_sec(1);                                                 
    do_combat(character_list, display, time_keeper, location_list[0]);
//    log_event("combat\n");                                      
    // time_events(); // call time dependent events 
    if (character_list[0]->dead()) 
    {
      log_event("main - player died\n");
      play = FALSE; // check for end_game
    }
//    log_event("check vitals\n");
//    drefresh(); // put here to ensure regular refresh
  } while (play); //do main menu loop                    
                                                                         
  cloze(character_list[0], display);                            
  end_log();                                                            

  return 0;
}// main                                                         
                                                                      
// end - adv_game.cc                                  
                                                     
                                                


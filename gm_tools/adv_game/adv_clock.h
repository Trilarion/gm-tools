/* adv_clock.h - clock for adv_game
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

#ifndef ADV_CLOCK_H
#define ADV_CLOCK_H
       	       
#include <time.h>
#include <sys/time.h>					      
							      
class adv_clock						      
{ 	       						      
public:				 
  adv_clock();	       		 			      
  void update();       		 
  void show();	       		 
  time_t get_game_time() { return *game_time; }		       
  time_t get_real_time() { return *real_time; }		       
  time_t get_used_time() { return *used_time; }
protected:   						      
  time_t *real_time;   // the time in the real world	      
  time_t *game_time;   // the time in the game world	      
  time_t *used_time;   // the time used by actions in the game
  clock_t ticks;       // fractions of a second, ref: man clock	
  int ms;              // milliseconds, calculated from ticks 
};// adv clock	    					      
	     	    					      
	     	    					      
#endif	     						      
// end - adv_clock.h

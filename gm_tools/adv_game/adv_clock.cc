/* adv_clock.cc - time keeper for adv_game
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

		  
#include <time.h> 
#include <sys/time.h>
	 	  
#include "adv_clock.h"					   
#include "adv_display.h"
#include "status_line.h"
#include "danio.h"
#include "err_log.h"
		  
adv_clock::adv_clock()
// initilise time keeping
{	 	  	 
  real_time = new time_t;  // create storage for time    	       	  
  game_time = new time_t;
  used_time = new time_t;
  time(real_time);     	   // get the time 
  ticks = clock();
  log_event("adv_clock::constructor - ticks initialised to %ld\n", ticks);
  ms = 0; // would be nice to know the actual value, but we can sycronise during updates
  show();	  
	 	  
} // adv_clock constructor				   
	 	  					   
void adv_clock::update()				   
// check the system time and adjust time keeping acordingly
{		  
  time_t *new_time = NULL;	   
  clock_t new_ticks;		   
  		    		   
  // get the current time     	       	  
  new_time = new time_t;	   	  
  time(new_time);   		   	  
  // update ms (milli second time) 	  
  new_ticks = clock(); 		   	  
  // look for wraparound in ticks  	  
  if (new_ticks < ticks)	   	  
  { 		   		   
    ms += (new_ticks + 0x7FFFFFFF - ticks) * 100 / CLOCKS_PER_SEC;
    say_status("Ticks reset.");			
    log_event("adv_clock::update - Ticks reset. \
               ticks: %ld new_ticks: %ld ms: %d", 
	      ticks, new_ticks, ms);
  
  } 		   		       	  
  else		   		       	  
  { 		       	       	       	  
    ms += (new_ticks - ticks) * 100 / CLOCKS_PER_SEC;
  } 		   		       	  
  // zero ms each new second   	       	     
  if (*new_time > *real_time) {	     
    ms = 0; // reset the millisecond counter each new second. 
            // this will 'loose' a few ms each second, but keep clock in sync 
            // by testing it was found that once the inital sync was done
	    // ms was 99 or 100 every time the clock ticked. This could worsen
	    // if the update happens less that once per milli second
  }	    
  if (ms > 99) ms = 99; // the second will not tick over until _after_ 
                        // it has passed, therfore ms will reach 100 
                        // occasionally. This just makes it display as 99 
	       	       	// until the second ticks over at the next update
  	    
  ticks = new_ticks;
  *real_time = *new_time;
  		    
  *game_time = *real_time; // just for now: tk
  delete new_time;  
  show();	    
  		    
} // update	    
		    
void adv_clock::show()
// show the time    
{ 
  tm *time_breakdown;
  
  setpcolour(STATUS_COLOUR);
  dcur_move(TIME_X, STATUS_LINE); // ??? where is TIME_X defined
  
  time_breakdown = localtime(real_time);
  dwrite("%.2d-%.2d-%.4d %.2d:%.2d %.2d::%.2d", time_breakdown->tm_mday, 
	 time_breakdown->tm_mon, 1900 + time_breakdown->tm_year,
	 time_breakdown->tm_hour, time_breakdown->tm_min,
       	 time_breakdown->tm_sec, ms);
//  dwrite(ctime(real_time));	     
} // show		 
			 
			 
// end - adv_clock.cc	 

# adv_game.mk - include(able) makefile for adv_game and gm_tools
# C Daniel Vale MAY 2004

ifndef ADV_GAME_MK
ADV_GAME_MK := 1

$(ADV_DIR)/adv_display.o : $(ADV_DIR)/adv_display.cc $(ADV_DIR)/adv_display.h $(INCLUDE_DIR)/dandefs.h \
                $(INCLUDE_DIR)/sat.h $(DANIO_H)
	g++ -c $(DIR_LIST) $(ADV_DIR)/adv_display.cc -o $(ADV_DIR)/adv_display.o

$(ADV_DIR)/adv_character.o : $(ADV_DIR)/adv_character.h $(ADV_DIR)/adv_character.cc \
             $(ADV_DIR)/hit.h $(ADV_DIR)/adv_display.h \
             $(INCLUDE_DIR)/sat.h \
             $(PROMPT_DIR)/prompt.h \
             $(INCLUDE_DIR)/dandefs.h \
             $(DANIO_H) \
             $(CHAR_DIR)/cost.h \
             $(CHAR_DIR)/w_rank.h \
             $(CHAR_DIR)/gear.h \
             $(DANIO_DIR)/keys.h
	g++ $(DIR_LIST) -c  $(ADV_DIR)/adv_character.cc -o $(ADV_DIR)/adv_character.o

$(ADV_DIR)/hit.o : $(ADV_DIR)/hit.cc $(ADV_DIR)/hit.h $(ADV_DIR)/adv_display.h
	g++ $(DIR_LIST) -c $(ADV_DIR)/hit.cc -o $(ADV_DIR)/hit.o

$(ADV_DIR)/room.o : $(ADV_DIR)/room.cc $(ADV_DIR)/room.h $(INCLUDE_DIR)/dandefs.h $(STATUS_DIR)/status_line.h
	g++ $(DIR_LIST) -c $(ADV_DIR)/room.cc -o $(ADV_DIR)/room.o

$(ADV_DIR)/adv_clock.o : $(ADV_DIR)/adv_clock.h $(ADV_DIR)/adv_clock.cc $(ADV_DIR)/adv_display.h $(DANIO_H) \
              $(STATUS_DIR)/status_line.h  
	g++ $(DIR_LIST) -c $(ADV_DIR)/adv_clock.cc -o $(ADV_DIR)/adv_clock.o

$(ADV_DIR)/event.o : $(ADV_DIR)/event.h $(ADV_DIR)/adv_character.h $(ADV_DIR)/event.cc
	g++ $(DIR_LIST) -c $(ADV_DIR)/event.cc -o $(ADV_DIR)/event.o

endif


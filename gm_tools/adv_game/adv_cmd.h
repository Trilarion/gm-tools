/* adv_cmd.h - command mnemonics for user commands
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */


#ifndef ADV_CMD_H
#define ADV_CMD_H

// ********************** COMMANDS ***************************
// mnemonic definitions for user commands
#define advcmdNO_COMMAND 0  

// movement	   
#define advcmdDOWN  1000
#define advcmdUP    1001	
#define advcmdNORTH 1002  
#define advcmdSOUTH 1003
#define advcmdEAST  1004   
#define advcmdWEST  1005	   

// purchacing / trade
#define advcmdENTER_MARKET 2000
#define advcmdTRADE_UP     2001  
#define advcmdTRADE_DOWN   2002
#define advcmdTAB          2003	

// character	   	   	  
#define advcmdSPEND_XP 3000
#define advcmdPGDN 3001
#define advcmdPGUP 3002
#define advcmdCURSOR_UP 3003
#define advcmdCURSOR_DOWN 3004

// general activity	    
#define advcmdREST         4000	       
#define advcmdDRINK_POTION 4001	 
#define advcmdLOOK         4002

// game	 	   	    	 
#define advcmdSAVE       5000	 
#define advcmdSAVE_CHAR  5001	 
#define advcmdSAVE_GAME  5002	 
#define advcmdLOAD       5003	 
#define advcmdLOAD_CHAR  5004	 
#define advcmdLOAD_GAME  5005	 
#define advcmdSHOW_MENU  5006	 
#define advcmdSHOW_CHAR  5007	 
#define advcmdQUIT       5008	 
#define advcmdCOMBINE_DISPLAY 5009
#define advcmdEXPAND_DISPLAY  5010
#define advcmdTOGGLE_DISPLAY  5011

// combat          	    	  
#define advcmdSTART_FIGHT 6000	  

// variable context hot keys
// menu hot keys which have different meanings to be determined by handle event
#define advcmdENTER 7000
		 
// ********************** END COMMANDS
		 	    	  
#endif		 		  
// end - adv_cmd.h
		 

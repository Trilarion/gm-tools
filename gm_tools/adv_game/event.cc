/*  event.cc - event object for adv_game
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

	   
#include "event.h"

// list of action codes
#define actNO_ACTION 0
		 
		   
event::event(adv_character *new_who, adv_character *new_target, 
       	time_t new_when, int new_when_ms, int new_what)
// setup (but not link) a new event
{		   
  who = new_who;   
  target = new_target;
  when = new_when; 
  when_ms = new_when_ms;
  what = new_what;
  prev = NULL;
  next = NULL;
}// event constructor      	      	   
       	      	     
event::~event()
// delete an event, without disrupting list
{      	      	     
  if (prev != NULL)  
  {   		     
    if (next != NULL)
    { 	  	     
      prev->next = next;
      next->prev = prev;
    } else {   	     
      // deleting end node
      prev->next = NULL;  	  
    } 		     	  
  } else { 	     	     
    if (next != NULL)	  
    { 	  	     	  
      // deleting head node
      next->prev = NULL;   
      // hopefully something is still pointing to the list!
    } else {		     				   
      // last one gone	   
      ;			   
    }  		     	   
  }    		     	   
}// event::destructor      		   
       		   	   				 
long int event::get_time_diff(event *some_event)       	       
// find (this.when - some_event.when) in milliseconds (when_ms is included too)
{     		      					       
  long int diff;      					       
  		      					       
  diff = when;	      					       
  diff -= some_event->when;				       
  diff *= 100;	      					       
  diff += when_ms;    					       
  diff -= some_event->when_ms;				       
  return diff;
}		      	      				       
		      	      				       
       		      	      				       
void event::add(event *new_event)			       
// add a new event, maintaining sort by time		       
{ 		      	      				       
  event *look = this; // the caller, not the event to be added 
  // find an event that occurs after the new event	       
  while (look->next != NULL && new_event->get_time_diff(look) < 0)
    look = look->next; 	       	      		  	       
  		      			  	  	       
  // go back to event before new event			       
  while (look->prev != NULL && new_event->get_time_diff(look) > 0);
    look = look->prev; 
  
  // link the new event in. look might be the dummy head node, 
  // but can't be the dummy tail node, so no need to test for NULL
  look->next->prev = new_event;	 				  
  new_event->next = look->next;					  
  look->next = new_event;      					  
  new_event->prev = look;					  
} // add
       		      	   		  
       		      	   		  
// end - event.cc     	   		  
       			   		  

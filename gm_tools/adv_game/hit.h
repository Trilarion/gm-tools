/*  hit.h - hit data for adv_game
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

#ifndef HIT_H
#define HIT_H
      
#include "adv_display.h"
      			     
//types of hits		     		  
#define MONSTER_HIT 0
#define PLAYER_HIT 1	     	 	  
   	  	
// damage category
#define PHYSICAL 0
#define ENERGY  1
#define MENTAL 2
   	       	
// damage types	
#define NORMAL 0
#define KILLING 1
   	       
class hit_data 	      	     
{     	       		     
public:        		     
  hit_data(int new_body_damage = 0, int new_stun_damage = 0, 
      	   int new_margin = 0, int new_damage_type = NORMAL);
//  void describe(int target_type, WINDOW *win); // superseeded by adv_character::take_hit()
  int body_damage; 
  int stun_damage; 
  int margin;  	  
  int damage_type;
//  int damage_category; 
protected:     
  adv_display *display;
}; 	       	      	     			    
	       

#endif
// end - hit.h

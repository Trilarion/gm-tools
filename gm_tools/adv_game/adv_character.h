/* adv_character.h - character object for adv_game
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

#ifndef ADV_CHARACTER_H	       
#define ADV_CHARACTER_H	       
   	    	    	       
#include "hit.h"	       
#include "adv_display.h"
#include "sat.h"
#include "character.h"
#include "cost.h"
#include "w_rank.h"
#include "danio.h"

// STATS: used to identify a stat in code	    
#define sSTR 1					   
#define sDEX 2	       			    
#define sBODY 3 	       			    
#define sPD 4  	 				   
#define sSTUN 5					   
#define sWIELDED 6

#define cWIELDED 3  // STUB: need to access different types of weapon skills
   
// default starting character 	       		    
#define DEFAULT_BODY 10       		    
#define DEFAULT_DEX 10  
#define DEFAULT_XP 0  	       		  	    
#define DEFAULT_CASH 3000      			    
#define DEFAULT_POTIONS 0      			    
#define DEFAULT_WEAPON_DC 0
#define DEFAULT_WEAPON_OCV 0
#define DEFAULT_SHIELD 0   	    
#define DEFAULT_ARMOUR 0   	
#define DEFAULT_CHAR_FILE "default.chr"		    
	       	      	       			    
// Monster     	      	       			    
// monster bases are final value for level 0 monster	    	 
#define MONSTER_BASE_BODY 1  					 
#define MONSTER_BASE_CASH 0 					 
#define MONSTER_BASE_DEX 5    	  				 
#define MONSTER_BASE_WEAPON_DC 0  				 
#define MONSTER_BASE_ARMOUR 0	  				 
#define MONSTER_BASE_ARMED 20  // chance of doing killing damage
#define MONSTER_BASE_STRENGTH 5	  				 
#define MONSTER_BASE_PD 0  	  				 
#define MONSTER_BASE_STN 20	  				 
			 					 
// monster increments per level      	       	  		    
#define MONSTER_STEP_BODY 1 					 
#define MONSTER_STEP_CASH 100 					 
#define MONSTER_STEP_DEX  2 					 
#define MONSTER_STEP_WEAPON_DC 1				 
#define MONSTER_STEP_ARMOUR 1  					 
#define MONSTER_STEP_ARMED 5   	// chance of doing killing damage
#define MONSTER_STEP_STRENGTH 5	
#define MONSTER_STEP_PD 2  
#define MONSTER_STEP_STN 8 
       			 
// misc			 
#define POTION_STRENGTH 5 // maximum body recovered: tk - build potions as powers
#define	END_SEG_RECOVERY_RATIO 	20 // divisor for end of segment recoveries
#define FULL_SEG_RECOVERY_RATIO  4 // divisor for full segment recoveries 
#define ID_PLAYER 1   // used with adv_character.id to specify monster / player
#define ID_MONSTER 2 	 
#define ID_MERCHANT 3

class adv_character : public character	       	      		  
{     				  
public:  
   
           // CONSTRUCTION / DELEATION / CREATION 
  adv_character(adv_display *new_display);	    
    // constructer - initialises all fields to default values
  ~adv_character();

  void new_monster(int dungeon_level); // create a new monster
  void new_player(); // create a fresh player 	       
   		  			
                // COMBAT STUFF
  hit_data *attack(adv_character *target);	    
    // attack the target and return hit data	    
  void take_hit(hit_data *hit, dwindow *win); 
    // process damage, applying defences and reporting to screen	    
  int dead();          
  int unconcious(); 		 
   		  
                  // DISPLAY  
  void show();  // show the whole character

  void show_disadvantage_page();
  void show_power_page();
  void show_trade_page();
  void show_gear_page();
  void show_perk_page();
  void show_skill_page();
  void show_subtotal_stats();
  void show_stat_page();
  void show_combat_page();
   
  void show_title_page();
  void goset(int x, int y);
  void show_name(dwindow *win = NULL);			  
  void show_str(dwindow *win); // display the character's strength 
  void show_body(dwindow * win);  // display the character's body score
  void show_pd(dwindow * win);  // display the character's pd score
  void show_stun(dwindow * win);  // display the character's stun score
  void show_dex(dwindow *win);   // display the character's dex score 
  void show_weapon_skill(dwindow *win);    // display ranks with weapon	  
  void show_combat_gear(dwindow *win); 	  
    // show important equiptment associated with combat
  void show_cash(dwindow *win);    // display the characters total funds
  void show_potion(); // display the number of healing potions 
  void show_xp();      // display the characters xp total
  void show_wounds();  // display wounds score
//  void show_title_page(); // display the character bio page

			      // ACTIONS 				 
  void drink_potion();  // drink a healing potion
  void rest(); // recover from wounds by resting     	 
  void end_seg_recovery(); // free end segment recovery
  void end_turn_recovery(); // free end turn recovery
  void half_recovery(); // spend segment recovering
  void end_combat_recovery(); // recovery at end of combat 
  int adv_save(FILE *out = NULL); // save the character
  int adv_load(FILE *in = NULL); // load a saved character
    // an optional file may be specified, else one is asked for at runtime
  void load_item(int& item, const char *item_name);
    // load items from inventory to special storage
  void load_item(int& item1, int& item2, const char *item_name);
    // load items from inventory to special storage
  int ready_item(gear *item);		  
    // load in details from the listed item
  int unready_item(gear *item);		  
    // unload details of the item
    // NOTE an item has no game effects until it is made 'ready' otherwise it
    // is just something carried. If an item were to be removed from the 
    // gear list without being un-readied it would continue to affect combat 
    // etc.
   void toggle_current_item_rediness();
       					  
  // ACCESS CONTROL FOR PROTECTED MEMBERS 
  int get_cash() { return cash; }    	  	    
  int set_cash(int new_cash) { cash = new_cash; return cash; }
  int add_cash(int new_cash) { cash += new_cash; return cash; }
  int get_potion() { return potions; }    	    	  
  int set_potion(int new_potion) { potions = new_potion; return potions; }   
  int add_potion(int new_potion) { potions += new_potion; return potions; }
  int set_shield(int new_shield) { shield = new_shield; }
  int set_armour(int new_armour) { armour = new_armour; }
  int set_sword(int new_sword_dc = 3, int new_sword_ocv = 5) { weapon_dc = new_sword_dc; weapon_ocv = new_sword_ocv; }	       	 
  int get_xp() { return ch_experience; }  	       		    
  int earn_xp(int new_xp);     	     	  	    
    // add new_xp to xp only if new_xp >= 0, 	    
    // returns xp or -1 if new_xp is negative	    
  int spend_xp(const int stat_id, const int xp_amnt); // spend xp
  void set_name(const char *new_name);
  char *get_name();
   
  ability *get_item(const char *item);	  
    // get an item from the characters gear list  
  void add_item(gear *new_item);    	  
    // link a new item into the gear list 
  gear *drop_current_item();   	    	  
    // remove the current item from the gear list and return a pointer to it
  void set_id(int new_id) { id = new_id; }				    
  void scroll_trade_page(int scrl); 	  
    // move the scroll offset  	      	     
  void trade_page_boundry_check();  	  
    // prevent current from leaving trade page 	
  			       
  int get_alignment();	       
  void set_alignment(int new_alignment);
  int modify_alignment(int amnt);
   			      	    	  				    
protected:   		      	    	  				    
  int cash;      // in copper dollars 	       	    	     		    
  int potions;   // healing potions 	  				    
  int weapon_dc; // damage class of weapon				    
  int weapon_ocv; // ocv bonus of weapon  				    
  int armour; // resistant def worn 	  				    
  int shield; // dcv bonus for shield	  				    
   			      	    	  				    
  int w_stun;    // the amount of stun damage taken			    
   				    	  				    
  weapon_rank *wielded_sk; // shortcut to wielded weapon skill		    
// gear *wielded; // shortcut to wielded weapon   	  		    
  adv_display *display; 	    	  	    	  		    
  int id; // identity: ID_PLAYER or ID_MONSTER or ID_MERCHANT
  int alignment; // determines reaction to character for stage 2 this is 
       	       	 // < 0 == hostile; 0 == neutral; > 0 == friendly
   
  int trade_list_offset; // scroll control for trade list
  ability * last_on_trade_page;
  ability * first_on_trade_page;
};// character 	       	      	    	      	    	     		    
  		   			 	    
#endif		    			 	    
// end - adv_character.h		 	    
  

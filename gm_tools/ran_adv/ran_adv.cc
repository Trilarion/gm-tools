/* ran_adv.cc - random adventure designer
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */


#include <stdio.h>  // required for rewind ???
#include <stdlib.h>    
#include <assert.h>    
#include <string.h>    
#include <stdarg.h> // variable argument lists

#include "dandefs.h"             
#include "danio.h"        
#include "err_log.h"
                                 
#define DATA "adv.dat" // the data should be in the same directroy as the program
#define DATA2 "gm_tools/ran_adv/adv.dat" // but it might be here if installed incompletely          
#define ESC 27                   
#define MAXLEN 80                

#define MIN_ROLL 1               
#define MAX_ROLL 100             
                                 
FILE *in;                        
                                 
void bail_out(int err, const char* s, ...);
// quit on fatal error            
// PRECONDITION: err is an error code (should be non zero);
//    calling function to provide formatted error message and arguments as per printf    
// POSTCONDITION: messages posted to stderr and err.log; cloze() called; exit with code EXIT_FAILURE
                                                 
                                                 
int roll()                                       
{                                                
  int num, old_num; // numbers read from table   
  int ran; // random roll result                 
  //  int len; // length of a string
  int offset; // offset into the data to the begining of the result (skip the roll number)
  char *data_in; // for reading in a line from the data file 
  char *title, *result; // the table title and the table result
                                                 
        
  data_in = new_str(MAX_INPUT);
  log_event("roll()\n");                         
	                                               
//  strcpy(data_in, "HELLO WORLD"); 
//  log_event("roll() - data_in (test): %s\n", data_in);
                                                 
  // read in the table title                     
  //  log_event("roll() - data_in (prior to read): %s\n", data_in);
  log_event("roll() - MAX_INPUT: %d\n", MAX_INPUT);
  log_event("roll() - strlen(data_in): %d\n", strlen(data_in));
  log_event("roll() - in (data_in file): %d\n", in);
  assert(in != NULL);
  if (fgets(data_in, MAX_INPUT, in) == NULL)
  {                            
		log_event("roll() - end of data file found\n");
    return 0;  // no more lines, end of tables, done creating adventure
  }                                              
  log_event("roll() - data_in (table title): %s\n", data_in); 

	                        
  if (data_in[0] != '*')           
  // not the expected table title line
  {                       
		log_event("roll() - table title not found, checking alternatives\n");
    if (data_in[0] == '#')         
    // create line break           
    {                     
      log_event("roll() - line break found\n");
      setpcolour(pcolRED);           
      title = data_in + 2;         
      title[(strlen(title)-1)] = '\0';
      dwrite("\n\n                    -----%s-----\n", title);
      return 1;                  
    } else {              
			log_event("roll() - garbage line found, ignoring line");
			// ignore garbage line     
      return 1;                  
    }                            
  }//fi                          
             
           
  // Record the table title      
  log_event("roll() - formatting table title\n");
  title = new_str(data_in + 2);    
  title[strlen(title)-1] = '\0';  // change terminating newline to null
  log_event("roll() - Table title: %s\n", title);                               
	                                     
  // roll on the table and find the result
  ran = d_random(MAX_ROLL); // roll on the table 
  log_event("roll() - ran (d% result): %d\n", ran);
  old_num = MIN_ROLL-1; // to make first ascending check against
  
  // read through the table elements until the one matching the roll is found.
  do {                                 
    fgets(data_in, MAXLEN, in);          
    sscanf(data_in, "%d", &num);         
		// boundry check the number        
    if (num < MIN_ROLL)                
    {                                  
      bail_out(errOUT_OF_BOUNDS, "roll() - Table value of %d is less than MIN_ROLL, bailing out", num);
    }                                                                               
    if (num > MAX_ROLL)                                                             
    {                                                                               
      bail_out(errOUT_OF_BOUNDS, "roll() - Table value of %d is greater than MAX_ROLL, bailing out\n", num);
    }                                                                              
		// check the numbers are ascending                                             
    if (num <= old_num)                                                            
    {                                                                              
      bail_out(errINVALID, "roll() - Table values are not ascending %d is not greater than %d, bailing out\n", num, old_num);
      exit(EXIT_FAILURE);
    }                    
    old_num = num;  // keep track of previous range to validate the table
  } while (num < ran);  
	          
  // record the table result
  // remove the leading number (one, two or three didgits)
  offset = 0;
  while (data_in[offset] != ' ') // find the first space
    offset++;
  while (data_in[offset] == ' ') // skip to begining of the first word
    offset++;
  result = new_str(data_in + offset);  // make a copy
  result[strlen(result)-1] = '\0'; // change terminating newline to null
  log_event("roll() - result: %s\n", result);
	  
  setpcolour(pcolGREEN);
  dwrite("\n%25s: ", title);
  setpcolour(pcolLIGHTGRAY);
  dwrite("%s", result);
  drefresh();
               
  //tidy up    
	delete title;
  delete result; 
                
  while (num != MAX_ROLL)  // get to the end of the table
  {                           
    if (fgets(data_in, MAX_INPUT, in) == NULL)
      return 0;               
    assert(sscanf(data_in, "%d", &num) != 0); // ???
  }                           
	log_event("roll() - end\n");
  return 1; // end of table list not yet found
                         
}// roll on tale         
                         
void init()              
// start included services
{                        
  start_log();  // start debugging log
  log_event("init()\n");
  randomize();  // seed the random number generator
  init_danio(); // initialise user interface                      
}// init                 
                         
void setup()             
// setup program state and data
{                       
  log_event("setup()\n");
  
  if ( 
       (in = fopen(DATA, "rt"))  == NULL &&  // if not here
       (in = fopen(DATA2, "rt")) == NULL     // look here
     ) 
  {                                         
    log_event("setup() - Cannot open data file.\n");
    close_danio();         
    end_log();             
    fprintf(stderr, "Fatal error - Cannot open data file: %s\n", DATA);
    dgetch();
    exit(EXIT_FAILURE);              
  } //if
                              
  log_event("%s open for input\n", "adv.dat"/*DATA*/);                       
                                
  dcur(CURSOR_INVIS);                  
}// setup                
                         
                         
void cloze()             
// wrap up before exiting
{                        
  log_event("close()\n");
  fclose(in);            
  close_danio();         
  end_log();             
}// cloze                
            
void bail_out(int err, const char* s, ...) 
// quit on fatal error            
// PRECONDITION: err is an error code (should be non zero);
//    calling function to provide formatted error message and arguments as per printf    
// POSTCONDITION: messages posted to stderr and err.log; cloze() called; exit with code EXIT_FAILURE
{                                 
  va_list ap;  // pointer to the list of variable arguments
                                                         
  va_start(ap, s);  // initialise argument pointer
                                  
  // post error messages          
  vlog_event(s, ap);               
  vfprintf(stderr, s, ap);        
  cloze(); // exit from whatever graphics mode we are in (hopefully)
  va_end(ap);  // clear argument pointer
                                  
  exit(err);  // dump out              
}// bail out                         
                                     
                                     
int main()                          
{                                    
  init();                         
  setup();                        
                                  
	// Make a random adventure      
  do {                            
    dclear();  // clear the screen
    
    // Write a title
    setpcolour(pcolWHITE);               
    dwrite("                        Random Adventure\n");
    log_event("main() - adv title line displayed\n");
                                                    
		// Roll on the tables
    setpcolour(pcolLIGHTGRAY);           
    while (roll())                   
      ; // roll on each table until there are no more tables (roll returns 0)
                         
		// Show a quit/continue prompt
    setpcolour(pcolWHITE);               
    dwrite("\n\n      Press <ESC> to quit, or any other key to re-do.");
		
		// Refresh the screen so we can see it
    drefresh();                           
    log_event("main() - random adventure displayed.\n");
    
    rewind(in); // incase we go again     
  } while (dgetch() != ESC);              
                                          
  cloze();
  
  return 0;
}// main                 
                         

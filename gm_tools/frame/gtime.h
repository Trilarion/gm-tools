/* gtime.h
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

#ifndef GTIME_H
#define GTIME_H

#define YEARS 1
#define DAYS 2
#define HOURS 3
#define MINUTES 4
#define SECONDS 5

class gtime
{
public:
  gtime() {};
  gtime(gtime& source);
    // initialise a gtime from another gtime
  gtime(int new_year, int new_day, int new_hour, int minute, float new_second);
    // initialise a gtime from all the bits tk
  void gtime::normalise();
    // normalise time, eg 63 minutes becomes 1 hour 3 minutes
  void show();
  gtime advanced(int amount, int unit);
  gtime advanced(float amount); // for seconds
    // returns a pointer to a _new gtime which is _this + the amount indicated in the parameters
    // usage: latter = now.advanced(10, YEARS);
  gtime operator+(gtime t);
  gtime operator-(gtime t);
    // addition and subtraction of times (the resultant is normalized)
  int operator<(gtime t);
    // time comparison

  int year;
  int day;
  int hour;
  int minute;
  float second;   // NOTE: The difference between a reaction of 99 and 100 is aprox 0.0001 seconds per rediness point
};//gtime


#endif
// end - gtime.h

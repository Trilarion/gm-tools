/* event.cc
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

#include <stdlib.h>
//#include <stdio.h>
#include <curses.h>
#include <assert.h>

#include "event.h"
#include "char.h"
#include "action.h"
#include "actdef.h"

extern int line;

event::event(character* new_who, action * new_how, gtime new_when, int new_what, event *new_prev, event *new_next)
{
  who = new_who;
  how = new_how;   
  when = new_when;
  what = new_what;
  prev = new_prev;
  next = new_next;
}// constructor

event::~event()
{
  // take care as no components are automaticlly deleted.
  // use remove to delete a member of an event list without disturbing the list
}//destructor

void event::remove()
// PRECONDITION: the event is a member of a doubly linked list with a dummy
//   head node and is not the dummy head node itself
{ 
  assert(prev != NULL);	
  if (next == NULL)
  {
    prev->next = (event *) NULL;
  } else {
    prev->next = next;
    next->prev = prev;
  }
  delete this;
}//remove

void event::add_event(character* new_who, action * new_how, gtime new_when, int new_what)
// insert a new event in the event list
// self is a node in a doubly linked list of events, usually the dummy head node.
// maintain a sorted list
{
  event *new_event, *eventptr;

  // find the insertion point: set eventptr to the node to go before the new node. the next node should be latter than the new node
  eventptr = this; // our starting point  
   
  while (eventptr->prev != NULL)
    eventptr = eventptr->prev; // move to head node
  if (eventptr->next != NULL) 
    eventptr = eventptr->next; // skip dummy head node

  while ((eventptr->next != NULL) && (eventptr->next->when < new_when))
    eventptr = eventptr->next;

  // set up the new event: add it on after eventptr
  new_event = new event(new_who, new_how, new_when, new_what, eventptr, eventptr->next);

  // insert the event					
  if (eventptr->next != NULL)          // if this is not the last node
    eventptr->next->prev = new_event;  //   the next node's prev pointer gets the new node
  eventptr->next = new_event;          // the next node is now the new node
}//add event


void event::show()
{
  mvprintw(line, 1, "Event: ");  // show the event on its own line
  line++;
  
  when.show();
  printw(" ");
  if (who != NULL)
    printw("%s <ID: %d> ", who->name, who->id);
  switch (what)
  {	  
    case(MOVE_NORTH):
    {	  
      printw("moves north.");
      break;
    }// move north
    case(MOVE_SOUTH):
    {	    
      printw("moves south.");
      break;
    }// move south
    case(MOVE_EAST):
    {	    
      printw("moves east.");
      break;
    }//move east
    case(MOVE_WEST):
    {	    
      printw("moves west.");
      break;
    }// move west
    case(REST):
    {	    
      printw("rests.");
      break;
    }//rest 
    case(RANDOM_MONSTER_CHECK):
    {	    
      printw("random monster check.");
      break;
    }// random monster check
    case(RANDOM_MONSTER_ENCOUNTER):
    {	    
      printw("random monster encounter.");
      break;
    }//random monster encounter
    case(ATTACK):
    {	    
      printw("attacks.");
      break;
    }//attack
    default:
    {	    
      printw("unregistered event.");
      break;
    }	    
  }//switch 
}// show    
	    
// end - event.cpp
	    

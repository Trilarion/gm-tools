/* event.h
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

#ifndef EVENT_H
#define EVENT_H

#include "gtime.h"
#include "location.h"
#include "action.h"
#include "char.h"


class event
{
public:
  event(character* new_who, action * new_how, gtime new_when, int new_what, event *new_prev, event *new_next);
  ~event();
  void add_event(character* new_who, action *new_how, gtime new_when, int new_what);
  void remove();
    // deletes self from list whilst maintaining integrity of the list
  void show();

  character    *who;
  action       *how;
  gtime        when;
  int what;
  event        *prev;   // doubly linked list
  event        *next;

};//event


#endif
// end - event.h

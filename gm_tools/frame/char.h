/* char.h
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

#ifndef CHARACTER_H
#define CHARACTER_H

#include "location.h"
#include "action.h"
#include "gtime.h"
#include "/home/dan/source/include/sat.h"

#define MAX_PC 10000

#ifndef TRUE
#define TRUE 1
#define FALSE 0
#endif

class character
{
public:
  character();
  ~character();
  void show_action();
    // displays currently declared actiont
  int declare_action(character *cast);
    // gets/confirms a declaration
  int character::interpret(char* s);
    // interpret commands contained in the string <s>
  void character::insert(character *c);
    // add a new character to the linked list of characters
    // characters are sorted from lowest reaction to highest

  gtime when_ready(gtime start);
  static int new_id;
  int id;
  char * name;
  int str;
  int skill;  // which is used for Reaction
  int body;
  int wounds;
  int rediness; // the amount of rediness remaining
  int xp;
  location loc;
  character *next;
  action *declared;

protected:
  int usr_declare_action(character *cast);
    // gets/confirms a declaration action from the user
    // returns resposes for the program flow control such as quit.
    // valid returns are defined in actdef.h
  int character::AI_declare_action(character *cast); // tk will take an optional parameter for the type of intellegense or orientation of the AI
    // gets/confirms a declaration action from the AI

};//character

#endif
// end - char.h

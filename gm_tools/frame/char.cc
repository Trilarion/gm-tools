/* char.cc
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

#include <stdlib.h>
//#include <stdio.h>
#include <ctype.h>
// #define NDEBUG  // remove comment for finished product
#include <assert.h>		  
#include <curses.h> 

#include "/home/dan/source/include/sat.h"  
#include "char.h"		   
#include "/home/dan/source/prompt/prompt.h"    	       	   
//#include "keydefs.h"
#include "actdef.h"
#include "gtime.h" 
		
extern int line;

character::character()
{      	 
//  assert(new_id < MAX_PC);
  id = new_id++; 
  str = 10;
  skill = 50;
  body = 10;
  wounds = 0;
  name = new_str((char*) "Joe");
  xp = 0;
  loc.lat = 0;
  loc.lng = 0;
  next = (character *) NULL;
  declared = new action();
    
}// character constructor.

character::~character()
{			       	
  if (name != NULL) delete name;
  // need to delete action also ? but what of the event list ? 
  if (declared != NULL) delete declared; // for trial
}//character destructor							    


void character::show_action()
// display the declared action
{
  if (declared == NULL)
  {
    mvprintw(line, 1, "%s is doing nothing.", name);  
    line++;
  }	    
  else	   
  {	   
    mvprintw(line, 1, "%s", name); 
    declared->show();		   
    line++;
  }//fi	    
}//show action
	    
void character::insert(character *c)
// add a new character to the linked list of characters
// characters are sorted from lowest reaction to highest
{	    
  character *before = this; // the character to go before the new character c
	    
  // find the character in the list which the new character is to go after
  while ((before->next != NULL) && (before->next->skill < c->skill))
    before = before->next;
	    
  // insert the new character c into the list
  c->next = before->next;
  before->next = c;
}// insert  							      
	    							      
int character::interpret(char* s)				      
// interpret commands contained in the string <s>		      
// NB: interpret is a member of character as the character may become context
//     for the interpretation in later developments		      
{	    							      
  char *ans;							      
  int response;							      
     	    							      
  ans = new_str(s);  // so we can modify it as need be		      
     	    							      
  //convert ans to standard form				      
  str_upp(ans);      // convert to uppercase			      
  debuf(&ans);       // remove leading and trailing spaces	      
     	    							      
#ifndef NDEBUG							      
  if (ans != NULL) printw("You said: %s", ans);  // for debugging 
#endif	    							      
     	    							      
  if (ans == NULL)  // main interpretation cascade		      
    response = NO_CHANGE;					      
  else if (ans[0] == 'Q')					      
    response = cQUIT;						      
  else if (ans[0] == 'N')					      
    response = MOVE_NORTH;					      
  else if (ans[0] == 'S')					      
    response = MOVE_SOUTH;					      
  else if (ans[0] == 'E')					      
    response = MOVE_EAST;					      
  else if (ans[0] == 'W')					      
    response = MOVE_WEST;					      
  else if (ans[0] == 'R')					      
    response = REST;						      
  else if (ans[0] == 'A')					      
    response = ATTACK;						      
  else  // default: occurs if no other comand causes a return	      
  {  								      
    printw("Declaration not understood."); 	       	       	      
    response = NOT_UNDERSTOOD;					      
  }// main interprtation cascade				      
  delete ans; // delete the local copy of the input		      
     	  							      
  return response;
}// interpret
     	  
     	  
int select_target(int own_id, character *cast)
// stub for locating the target in the cast list 
//  - grabs the first non-self cast member
{    	  
  character *cptr = cast;
     	  
  while (cptr != NULL && cptr->id == own_id)
    cptr = cptr->next;
     	  
  return cptr->id;
}// select target stub
     	  
     	  
     	  
int character::usr_declare_action(character *cast)
// get a usr declaration (which may include a command such as save or quit)
{    	  
  int response; // the action/command code as interpreted
  prompt * ans;   // an input prompt
       	  			    
  do  // input a respose	    
  {    	  			    
    // display the declared action  
    line++;
    show_action();	       	    
       	  		       	    
    mvprintw(line, 1, "Declare a new action or confirm with <Enter>: ");
    line++;
    ans = new prompt();        	       	       	    
    response = interpret(ans->s);
    delete ans;		
  } while (response == NOT_UNDERSTOOD);
  erase();
  line = 1;
     	       		       
  // record the newly declared action
     
  if (is_action(response))
  if (response != declared->what) 
    declared->what = response;	  						  
  else			  	  						  
    response = NO_CHANGE; // ignore re-itteration of previous declarations
     
  if (response == ATTACK)      
    declared->target = select_target(id, cast);
     	       		       
  return response;	       
}// usr_declare action	       
 	       		       
 	       		       
int character::AI_declare_action(character *cast)
{	       		       
  int response;		       
 	       		       
  if (declared->what != ATTACK)
  {	       		       
    response = ATTACK;	       
    declared->what = ATTACK;
    declared->target = 1000;
 	       
    if (response == ATTACK)
      declared->target = select_target(id, cast);
  } else       
    response = NO_CHANGE;
 	       
  return response;
}//AI declare action
 	       
 	       
int character::declare_action(character *cast)
{	       
  int response; // command/action code as interpreted
 	       
  // confirm or get new declaration
  if (id < MAX_PC)
    response = usr_declare_action(cast);
  else	       
    response = AI_declare_action(cast);
  return response;
}// declare action
 	       
 	       
gtime character::when_ready(gtime start)
// returns the time when the character will complete his declared action,
// using standard manuver times and the start time
// this could be done with a standard manuvers table loaded dynamicly!
// this stub uses 1 day for every action
// stub tk     
{ 	       
// this is what I had under dos  
//  if (declared->what == ATTACK)	
//    return start.advanced((50.0 / (float) skill));  // change to reaction tk
//  else       	  		      	
//    return start.advanced(1, DAYS);
  gtime new_time; 
  	       	  
  new_time.year = start.year;
  new_time.day = start.day;
  new_time.hour = start.hour;
  new_time.minute = start.minute;
  new_time.second += 0.25;	 
  new_time.normalise();
 	       	      
  return new_time;
}//when ready  	      
 	       	      
// end - char.cpp
 	       
 

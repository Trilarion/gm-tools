/* action.cc
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

//#include <stdio.h>
#include <curses.h>

#include "action.h"
#include "actdef.h"



action::action()
{
  what = REST;
  target = 0;  // zero is a non-targetable id
}// constructor

action::~action()
{


}// destructor


void action::show()
// usuall preceeded by cprintf("\r\n%s", <char.name>);
{
  switch(what)
  {
    case(REST):
    {  
      printw(" is resting.");
      break;
    }// rest
    case(MOVE_NORTH):
    {	    
      printw(" is moving North.");
      break;
    }//move north
    case(MOVE_SOUTH):
    {	    
      printw(" is moving South.");
      break;
    }//move south
    case(MOVE_EAST):
    {	    
      printw(" is moving East.");
      break;
    }//move east
    case(MOVE_WEST):
    {	    
      printw(" is moving West.");
      break;
    }//move west
    case(ATTACK):
    {
      printw(" is Attacking");
      break;
    }	    
    default:
    {	    
      printw(" is doing nothing.");
    }//default
  }//switch
}// show

void action::redeclare(int new_action)
{
  what = new_action;
}//redeclare


// end - action.cpp

/* frame.cpp - a role playing adventure game.
 *  
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

// BUGS
// screen goes white when monster is slain 
// screen goes white if character changes from attacking a monster to moving north
// crashes if character dies while travelling north 
// crashes if character attacks but no monster exists

// tk - simultatnious attacks: if characters took resposibility for their 
//    actions, i.e. deleting them after death, and death were delayed by .0001
//    seconds after the character being killed and character death was an event
//    she'd be apples. both attacks would be proccessed leading to two 
//    characters being killed generating two character death events which 
//    would then clean up their actions. Is an event dumped after proccessing?


//#include <stdio.h>		   
#include <stdlib.h>		   
// #define NDEBUG  // insert to remove debugging code
#include <assert.h> 
#include <curses.h> 
  	   			   
#include "/home/dan/source/include/sat.h"		   
#include "event.h"		   
#include "gtime.h"		   
#include "location.h"		   
#include "char.h"		   
#include "actdef.h"		   
#include "/home/dan/source/include/keydefs.h"
#include "/home/dan/source/prompt/prompt.h" 
       	    
// boolean
#ifndef BOOLEAN
#define BOOLEAN
#ifndef TRUE
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif 
#endif


// start time (miday, mid-year)
#define START_YEAR 501
#define START_DAY 360
#define START_HOUR 12       // 24 hr time
#define START_MINUTE 0
#define START_SECOND 0.0000


//**************** GLOBALS **************************
int line = 1;
int character::new_id = 1000; // start point for id numbers
//int new_id = 1000;
character *cast;
int done;           // flow control
gtime now(START_YEAR, START_DAY, START_HOUR, START_MINUTE, START_SECOND);
event * event_list;
gtime time_to_go;  // time until next event, periodicly calculated after declarations and event generation

////////////////////  END GLOBALS  //////////////////

void update_events(character *cptr);

void initialization()
{
  character *cptr;

// tk  textmode(C4350); // 50 rows
//  clrscr();
  //ncurses setup 
  initscr();	  
  start_color();	  
  nonl();  	  
  cbreak();	  
  noecho();
  keypad(stdscr, TRUE);	 

  cast = new character();
  done = FALSE;
  // the dummy head node for the event list is set with the game start time. 
  event_list = new event(NULL, NULL, now, DUMMY_NODE, NULL, NULL);	     
  									     
  cptr = cast;								     
  while (cptr != NULL)							     
  {									     
    update_events(cptr);						     
    cptr = cptr->next;							     
  }//while								     
  event_list->add_event(NULL, NULL, now.advanced(0.25), RANDOM_MONSTER_CHECK);  // first random monster check
}//initialization				  
  
void bye()
{    
  character *cptr1, *cptr2;
  event *eptr1, *eptr2;
     	 
  // delete cast
  cptr1 = cast;
  while (cptr1 != NULL)		
  {  	 			
    cptr2 = cptr1->next;	
    delete cptr1;  		  
    printw("character deleted");  
    cptr1 = cptr2;		  
  }//while			  
     	 			  
  // delete event list		  
  eptr1 = event_list;		  
  while (eptr1 != NULL)		  
  {  	 			  
    eptr2 = eptr1->next;	  
    delete eptr1;		  
    printw("event deleteted");	  
    eptr1 = eptr2;		  
  }//while			  

  // clear the screen tk
  mvprintw(1, 1, "Bye...");	  
  endwin(); // terminates ncurses 
//  (void) getch();		  
}//bye	  			  
     	  			  
void description()		  
// third person description of the situation
{    	       	 	  	  
  character *cptr = cast; 	  
  // tk textcolor(WHITE); 	    
  mvprintw(line, 1, "Description: "); line++;
  // tk textcolor(LIGHTGRAY);	  		      
  now.show();  	  		  		      
  	   	 				      
  while (cptr != NULL)				      
  { 	   	 				      
    mvprintw(line, 1, "%s (ID: %d) is at lat: %.2f lng: %.2f",   
    	   cptr->name, cptr->id, cptr->loc.lat, cptr->loc.lng);     	     
    cptr = cptr->next;				      
    line++;  	 				      
  }//while					 
    						 
//  (void) getch();  				 
}//description 	  				 
	       	  
void do_command(int cmd)
{	  	  
  if (cmd == cQUIT)
    done = TRUE;              // terminate game
}//do command				       
					       
void update_events(character *cptr)  
/* Modify the event list for newly declared action of a character. 
 * cptr is a pointer to the character declaring a new action. 
 * His old action is to be removed and new action added	
 */							
{					       
  event *eptr, *eptr2; // tempory pointers to events.	  
     					       	     
  // remove the old declaration (if any) from the event_list. 
  // The event will refrence the character cptr	     
  eptr = event_list->next; // skip the dummy node 
  while(eptr != NULL)			       
  {	  				       
    if (eptr->who != NULL)		       
      if (eptr->who->id == cptr->id)	       
      {	  				       
        eptr2 = eptr;			       
        eptr = eptr->next;
        eptr2->remove();
      }
      else
        eptr = eptr->next;
    else
      eptr = eptr->next;
   }//while
  // add new declaration to the event list.
  event_list->add_event(cptr, cptr->declared, cptr->when_ready(now), cptr->declared->what);
}// update events			       
	  				       
void check_declarations()		       
// check that actions are declared for all the cast and they confirm these actions
{	  				       	     
  int response; // feedback from declaration input (commands/actions)
  character *cptr = cast;		       	     
	  				       	     
  while ( (cptr != NULL) && (!done) )	       	     
  {	  				       	     
    response = cptr->declare_action(cast); // call the declaration rutine - get inputs from the cast
    if (is_command(response))          // commands are inputs not related to declarations and must be executed immediately
      do_command(response);            // (cmd == cQUIT -> done = TRUE)
    else /* not a command */		       	     
    {	  				       	     
      if (is_action(response))         // update event list if required.
      {	  					     
        update_events(cptr);			     
      }//fi: new action declared		     
      cptr = cptr->next;               // advance through the cast
    }// fi: not command				     
  }//while					     
	  					     
}// check_declarations				     
  	  					     
void random_monster_check()			     
/* check for random monsters
 * Precondition: Random monster check is the next event on the event list
 * Postcondition: Random monster check has been made, and a random monster
 *                encounter event generated if required
 *                A new random monster check is inserted in the event list
 */ 		       
{ 	  	       
  static int checks_made = 0; // keeps track of the number of checks made 
  event *this_event = event_list->next;				       	  
  	  	       
  assert(this_event->what == RANDOM_MONSTER_CHECK);    	       	       	     
  // tk textcolor(WHITE);						     
  mvprintw(line, 1, "Random Monster Check %d.", checks_made); line++; 	     
  // tk textcolor(LIGHTGRAY);						     
  checks_made++;       							     
  if (checks_made % 6 == 2)						     
    event_list->add_event(NULL, NULL, this_event->when.advanced(2, HOURS),   
  		       	  RANDOM_MONSTER_ENCOUNTER);			     
  // setup for next random monster check				     
  event_list->add_event(NULL, NULL, this_event->when.advanced(1, DAYS),	     
  		       	RANDOM_MONSTER_CHECK);				     
}//random monster check							     
  	  								     
void random_monster_encounter()						     
// random monster encounter						     
// precondition: the next event is a random monster encounter		     
// postcondition: a monster is generated and placed 'in the game'	     
{ 									     
  event *this_event = event_list->next;					     
  character *monster;							     
	
  assert(this_event->what == RANDOM_MONSTER_ENCOUNTER);
  // tk textcolor(WHITE);
  printw("Random monster encounter.");
  // tk textcolor(LIGHTGRAY);
  // generate a monster, stick it in the cast, have it attack the adventurer(s)
  monster = new character();  // generate   tk
  delete monster->name;
  monster->name = new_str("Monster");	 
  monster->id += MAX_PC;
  monster->skill = 20;
  monster->body = 3;
  monster->str = 5;
  cast->insert(monster);    
  update_events(monster); 
}//random monster encounter
	
void do_attack()
// precondition: the next event is an attack
{	
  character *attacker = event_list->next->who;
  event *this_event = event_list->next;
  event *eptr = event_list->next;  // used to find the targets event when he is slain.
  character *cptr, *targ_char = cast;
  gtime next_attack_time; // tempory for testing
  	
  mvprintw(line, 1, "Attack.");	line++; 
  // find the target in the cast list  
  while ((targ_char != NULL) && (targ_char->id != attacker->declared->target))
    targ_char = targ_char->next;       
  if (targ_char == NULL)	       
    printw("Target has gone.");	       
  else	  			       
  {	  			       
    targ_char->wounds += (attacker->str / 5);
    printw("Target's Body: %d", targ_char->body - targ_char->wounds);
    if (targ_char->wounds >= targ_char->body*2)
    {						
      printw("%s is slain.", targ_char->name); 	// screen turns white here ? 
      // remove declared events tk - simultanious attacks
      while (eptr != NULL)
      {	
        if (eptr->who != NULL)
          if (eptr->who->id == attacker->declared->target) // tk simultanious attacks
          {
	    eptr->remove();  
	    printw("Event removed.");
	  }
        eptr = eptr->next;  	       
      }//while	     	    
      // remove from cast list
      if (cast == targ_char)  // the target is the first character in the cast
      {	    	     	    
        cast = targ_char->next;
        delete targ_char;   
      } else {	     	    
        cptr = cast; 	    
        while ((cptr != NULL) && (cptr->next != targ_char)) // set cptr to the character before targ_char
          cptr = cptr->next; 
	assert(cptr != NULL);  // this should never happen as we have already found the target character in the cast list!
        if (targ_char->next == NULL)
        {
          cptr->next = NULL;
          delete targ_char;
        } else {
          cptr->next = targ_char->next;
          delete targ_char;
        }//fi: last or not
      }//fi: first or not
    }//if killed
  }//fi
  mvprintw(line, 1, "NOW: "); this_event->when.show();
  next_attack_time = this_event->who->when_ready(this_event->when);
  line++; 	    
  mvprintw(line, 1, "Next attack: "); next_attack_time.show();
  line++; 
  event_list->add_event(this_event->who, this_event->how, this_event->who->when_ready(this_event->when), this_event->what);
  			   
}//do attack	     	     
	    	     
	    	     
void do_next()	     
// execute the next event in the event list;
{	    	     
  event *eptr = event_list->next; // the event to be executed
  // tk textcolor(GREEN);
  mvprintw(line, 1, "Executing: "); line++;  
  // tk textcolor(LIGHTGRAY);
  eptr->show();
	    
  if (eptr != NULL)
  {	    
    switch (eptr->what)
    {	    
      case(MOVE_NORTH):
      {	    
        eptr->who->loc.lat++;
        event_list->add_event(eptr->who, eptr->how, eptr->when.advanced(1, DAYS), eptr->what);
        break;
      }//move north
      case(MOVE_SOUTH):
      {	
        eptr->who->loc.lat--;
        event_list->add_event(eptr->who, eptr->how, eptr->when.advanced(1, DAYS), eptr->what);
        break;
      }	
      case(MOVE_EAST):
      {	
        eptr->who->loc.lng++;
        event_list->add_event(eptr->who, eptr->how, eptr->when.advanced(1, DAYS), eptr->what);
        break;
      }// move east
      case(MOVE_WEST):
      {	
        eptr->who->loc.lng--;
        event_list->add_event(eptr->who, eptr->how, eptr->when.advanced(1, DAYS), eptr->what);
        break;
      }// move west
      case(REST):
      {	
        event_list->add_event(eptr->who, eptr->how, eptr->when.advanced(1, DAYS), eptr->what);
        if (eptr->who->wounds > 0)
          eptr->who->wounds -= 1;
        break;
      }//rest
      case(RANDOM_MONSTER_CHECK):
      {	
        random_monster_check();
        break;
      }//random monster check
      case(RANDOM_MONSTER_ENCOUNTER):
      {	
        random_monster_encounter();
        break;
      }// random monster encounter
      case(ATTACK):
      {	
        do_attack();
        break;
      }//attack
    }// switch
    assert(eptr != NULL); // ensure no called function attempts to delete the event first
    eptr->remove(); // this is needed so that the program does not become 'stuck' on the current event
  }//fi: eptr != NULL
} // do next
	
void generate_events()
// generate time / place events
{	
  // tk textcolor(WHITE);
  mvprintw(line, 1, "Generating Locality events."); line++;
  // tk textcolor(LIGHTGRAY);
  // check location events
}// generate events
	
void list_events()
// show a table of all the events
{ 
#ifdef NDEBUG  
  event *eptr = event_list->next;  // the first event after the dummy node
#else 
  event *eptr = event_list; // the dummy node is displayed
#endif
  // tk textcolor(WHITE);
  mvprintw(line, 1, "----------------- Event List --------------------");
  line++; 
  // tk textcolor(LIGHTGRAY); 
  mvprintw(line, 1, "NOW: ");
  line++; 
  now.show();
  	
  while (eptr != NULL)
  {	
    eptr->show();
    eptr = eptr->next;
  }// while
}// list events
	
void main()
{	
  initialization();
	
  while (1) // exit condition is checked after declarations are made
  { 	  							    
    // (begin again)						    
    // description						    
    description();						    
    list_events();						    
    // check declarations					    
    check_declarations();					    
    if (done) break;						    
    								    
    list_events();						    
    // time/place event generated				    
    generate_events();						    
    // determine time to next event				    
    time_to_go = event_list->next->when - now;  // is allocated by operator-
    // advance other events					    
    // do next event						    
    do_next();							    
    now = time_to_go + now; // advance time			    
    	  							    
  }//while
  bye();  
}//main	  
	  

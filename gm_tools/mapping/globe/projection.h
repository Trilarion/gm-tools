/* projection.h - projection header
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

		       
/*		       
Specification	       
		       
This object is a vector from the origin to the surface of a unit sphere,
which is centered on the origin. The vector can initalise from both
3d vectors and latitude-longditude co-ordinates. It maintians its unit
length and can generate information regarding distances and orientation
w.r.t. others of its kind. It can also project itself onto a plane in a
number of ways	       
*/		       
		       
/*		       
notation: anything folowed by an underscore denotes a vector, eg 0_ is the
0 vector [0,0,0]. a_ is a vector.
*/		       
		       
#ifndef PRJCTN_H       
#define PRJCTN_H       
		       
struct fpoint	       
{		       
  double x;	       
  double y;	       
};		       
		       
struct point	       
{		       
  int x;	       
  int y;	       
};		       
		       
struct latlng	       
{		       
  double lat;	       
  double lng;	       
};		       
		       
struct vector3	       
{		       
  double x;	       
  double y;	       
  double z;	       
};		       
		       
double arcdist(vector3 a_, vector3 b_);
  // returns the great arc distance between two 3d vectors
double dist(vector3 a_, vector3 b_);
  // returns the great arc distance between two 3d vectors
		       
		       
double remainder(double x);
// returns the normal fractional portion of a real number
// eg 0.245 == remainder(1.245)
		       
class projection       
{		       
private:	       	       
		       
public:		       
		       	       
  double di;  // 3d vectors    // width
  double dj;	       	       // depth	       	 
  double dk;	       	       // height
		       
  projection(latlng position);
    // init from latitude-longditude
  projection(double lat, double lng);	
    // init from latitude and londitude	
  projection(vector3 position);
    // init from 3d vector
  ~projection();       	  
		       	  
  double arcdist(projection *another);
    // returns the great arc distance between self and another
  double dist(projection *another);
    // returns the cord distance between self and another
  double arcdist(vector3 a_);
    // returns the great arc distance between self and another
  double dist(vector3 a_);
    // returns the cord distance between self and another
  void normalise();    
    // normalise to a unit vector
	       	       
  fpoint mapGlobe(vector3 plane);
    //	       	       
  void i_rotate(double i_rotation);
    // rotate a point w.r.t. di axis
    // rotation is an angle in degrees about the i axis
  void j_rotate(double j_rotation);		  
    // rotate a point w.r.t. dj axis		  
    // rotation is an angle in degrees about the j axis
  void k_rotate(double k_rotation);
    // rotate a point w.r.t. dk axis
    // rotation is an angle in degrees about the k axis
		       
};//projection	       
		       
		       
		       
#endif		       
		       
// end - prjctn.h      
		       
		       
		       

/* tesscal.cc - tesselation calculations
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */


// NB: read "1 << x" as 2^x   ie. 2 to the power of x

#include <assert.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include "tesscal.h"


/*
*  The tesscal (Teselation Calculation) inclusion contains functions used to locate
*  file ofsets in a height variation file assosiated with a regular recusive
*  triangular tesselation of the surface of a sphere.  See written notes for
*  more details (see Random Terrain Generation - GM Tools)
*/

////////// Imediate Calculations /////////////
/*
*  These functions calculate a value in a strait-forward maner and
*  are used by other functions in this inclusion
*  files_size;  rowsT_Dn;  rowsP_Dn;  equator_row; invert_row
*/

big_num file_size(unsigned char depth)
// returns the number of height variences verticies for a division with (depth) recusions
// see written notes on regular recursive triangular tesselation
// file_size <- 2 +4^(depth+1)  NB: not proven, only from inspection
// = 2 + 2 ^[ (depth + 1)*2 ]
// shift left used to find powers of 2
{
  assert(depth < 15); // avoid overflow
  return (2 + (1 << ((depth+1)*2)));
}// file size

unsigned int rowsT_Dn(unsigned int depth)
// returns the number of rows of triangles, given a tesselation depth
// = 2^(depth+1) ; by inspection
// shift left used to find power of 2
{
  assert(depth < 31); // avoid overflow
  return (1 << (depth+1));
} //rows of triangles (Dn)

unsigned int rowsP_Dn(unsigned int depth)
// returns the number of rows of points, given a tesselation depth
// = 2^(depth+1) + 1 ; by inspection
// shift left used to find power of 2
{
  assert(depth < 31); // avoid overflow
  return ((1 << (depth+1)) + 1);
} //rows of points (Dn)

big_num triangles_Dn(unsigned int depth)
// returns the number of triangles, given a tesselation depth
// = 8 * 4^depth
// shift left used to find power of 4
// = 8 * 2^(depth*2)
{
  assert(depth < 15); // avoid overflow
  return (8 * (1 << (2 * depth)));
}  // triangles Dn

unsigned int equator_row(unsigned char depth)
// returns the row number (rows of points) of the equator,
// given the tesselation depth
// (2^depth) + 1
// shift left is used to find the power of 2
{
  assert(depth < 32);  // else overflow
  return ((1 << depth) + 1);   // (2^depth) + 1;
}// equator row

unsigned int invert_row(unsigned int row, unsigned int depth)
// returns the number of the row (row) counting from the southern hemisphere,
// i.e. as if the globe were inverted.
// R^p(Dn) - row + 1
// = 2^(depth+1) - row + 2;  // substitution into rowsP_Dn
// shift left used to find power of 2
{
  assert(depth < 31); // avoid overflow
  return ((1 << (depth+1)) - row + 2);
}// invert row.

big_num invert_point(big_num p, unsigned char depth)
// returns the number of the point, counting from the southern hemisphere
// i.e. as if the globe were inverted.
// [P(Dn)] - p + 1
// 2 + 2 ^[ (depth + 1)*2 ] - p + 1 -- substitution of file_size
// = 3 - p + 2 ^[ (depth + 1)*2 ]
{
  assert(depth < 16);
  return 3 - p + (1 << ((depth+1)*2));
}// invert point


///////// Branching Calculations //////////////////////////////////////////////
/*
*  These simple calculations contain if's to deal with boundry conditions.
*  points_in_row;
*/

unsigned int rowP(big_num p, unsigned char depth)
// returns the row of a point
{
  float row;               // calculation tempory row
  unsigned int row_;       // claculation tempory row

  if (p < (file_size(depth) / 2))
  {
     row = (2 + sqrt((8 * ((float) p)) - 4)) / 4;
     return ((int) row == row) ? (unsigned int) row : (unsigned int) row + 1;
  } else {
     row = (2 + sqrt((8 * ((float) invert_point(p, depth))) -4 )) / 4;
     row_ = ((int) row == row) ? (unsigned int) row : (unsigned int) row + 1;
     return invert_row(row_, depth);
  }//fi
} // row P

unsigned int points_in_row(unsigned int row, unsigned int depth)
// returns the number of points in a given row* (row),
// for a given tesselation depth (depth) < 31
// *the row is a row of points (not triangles)(see Definitions - Random Terrain Generation - GM Tools)
// output undefined for rows not on D(n)
/*
*  essentially the answer is the (row-1) * 4, with the added complication of
*  dimminishing numbers after the equator. This is delt with by inverting
*  the row number (as if counting from the southern hemisphere), for rows
*  after the equator.
*
*  also we must deal with the case of the first and last rows where the
*  number of points is 1, this is done with a simple test.
*/
{
  unsigned int row_temp;
  row_temp = row;
  if (row > equator_row(depth))           // if in southern hemisphere
    row_temp = invert_row(row, depth);    //   invert
  if (row_temp == 1)								// first and last row
    return 1;
  return (row_temp-1)*4;                  //   just multiply row by 4
}// points in row

big_num sum_rowsP(unsigned int row, unsigned int depth)
// sums the number of points in the rows from row 1 to (row) inclusive
// given the tesselation (depth)
// given (row) 0 returns 0; other non-physical rows are undefined
/*
*  the basic calculation is 1+(2*row*(row-1))
*  however the southern hemisphere is different as the equator marks the end of
*  the regular pattern.  The solution in the southern hemisphere is found
*  by considering the total points in the tesselation, less the sum of points
*  in all rows following (row) -- the latter is found using a recursive call.
*/

{
  if (row == 0) return 0;           // required for well defined recursion on row = rowsP_Dn()

  if (row <= equator_row(depth))    // northern hemisphere is trivial
  {
    return (1+(2*row*(row-1)));       // northern hemisphere calculation
  } else {                          // southern hemisphere
    return (
       file_size(depth)               //   total all points in Dn
       									     // - all points in all following rows
     - sum_rowsP(                       // (which are summed by a recusive call
          (invert_row(row, depth) -1),  //  with the inverted row, minus 1)
          depth
       )// recurse sum rowsP
    );//return
  }//fi
}//  sum rowsP

big_num sum_rowsP(unsigned int row1, unsigned int row2, unsigned int depth)
// sums the number of points in the rows from (row1) to (row2) inclusive
// given the tesselation (depth)
{
  return (sum_rowsP(row2, depth))-(sum_rowsP((row1-1), depth));
}//  sum rowsP


/////////////  NON TRIVIAL CALCULATIONS //////////////////////
/*
*  This section is reserved for calculation of a non-trivial nature. The
*  methods used in these calculations are outlined else-where. Many of these
*  functions accept parameters which are used to return multiple results.
*/



void find_seed_corners(
                       unsigned char depth, big_num *s1, big_num *s2,
                                            big_num *s3, big_num *s4,
                                            big_num *s5, big_num *s6
                      )
// finds the seed corner locations: s1(90,0) s2(0,0) s3(0,90) s4(0,180)
// s5(0, -90) s6(-90,0), given the depth (depth) of the tesselation.
// Precondition: 0 <= depth <= 15, memory already allocated to the seed corners
// Poscondition: the other parameters point to file offset magnitudes for the
// 'seed corners' i.e. the verticies of a D(0) tesselation within D(depth)
{
  unsigned int eq_row; // to save recalculation of the equator row
  unsigned int eq_points;  // number of points on the equator
  unsigned int quarter;  // eq_points / 4

  // s1 if the first vertex in the file - the north pole
  *s1 = 1;
  // s6 is the last vertex in the file - the south pole
  *s6 = file_size(depth);
  // s2 is the first vertex on the equator
  eq_row = equator_row(depth);
  *s2 = sum_rowsP((eq_row-1), depth) + 1;
  // the others are evenly spaced, � of the way around the equator between each.
  eq_points = points_in_row(eq_row, depth);
  assert(eq_points % 4 == 0);
  quarter = eq_points / 4;
  *s3 = *s2 + quarter;                     // now just step them off
  *s4 = *s3 + quarter;
  *s5 = *s4 + quarter;
}// find seed corners

unsigned int column(big_num p, unsigned char depth)
// determines the column of p, relative to its octant, or octant 1 if unspecified.
{
  if (p != 1)
  {
    return p - sum_rowsP((rowP(p, depth)-1), depth);
  } else
    return 1; // the fiirst point is also the first row.
}// column

void sort(triangle *t)
// ensure the verticies are sorted - they usually will be
{
  big_num temp;
  assert(t->p1 != t->p2);
  assert(t->p2 != t->p3);
  if (t->p1 > t->p2)
  {
    temp = t->p1; t->p1 = t->p2; t->p2 = temp;
    sort(t);
  }//fi
  if (t->p2 > t->p3)
  {
    temp = t->p2; t->p2 = t->p3; t->p3 = temp;
    sort(t);
  }//fi
} // sort

double latitude(big_num p, unsigned char depth)
// returns the lattitude of a point by its file order (and the depth of the division).
{      
  double division;  
  int row_of_p, row_of_equator;
       		   
  division = 180.0 / rowsT_Dn(depth);
  row_of_equator = equator_row(depth);
  row_of_p = rowP(p, depth);
  if (row_of_p == row_of_equator)
    return 0;	   
//  if (row_of_p < row_of_equator)
//    return ((float) (row_of_equator - row_of_p) * division);
  else 		   
    return ((double) (row_of_equator - row_of_p) * division);
}// latitude
      	
double longditude(big_num p, unsigned char depth)
// returns longditude by file order and depth or recursion
{	
  double division;
  int row_of_p, column_of_p, points_in_row_of_p;
  int m180;
  float lng;
  	
  row_of_p = rowP(p, depth);
  points_in_row_of_p = points_in_row(row_of_p, depth);
  division = 360.0 / points_in_row_of_p;
  	
  m180 = (points_in_row_of_p) / 2 + 1;
  column_of_p = column(p, depth); // need i specify the octant?
  if (column_of_p == 1)
    lng = 0;
  else if (column_of_p > m180)
    lng = ((double) column_of_p - 1) * division - 360.0; 
  else 	  	  
    lng = ((double) column_of_p - 1) * division;
  if (lng < -180.0) lng = -180.0;
  if (lng > 180.0) lng = 180.0;
  return lng;
}// longditude
	
	
// end - tesscal.cpp

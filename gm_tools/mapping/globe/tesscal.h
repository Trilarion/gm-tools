/* tescal.h - tesselation calculations
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */


#ifndef TESSCAL_H
#define TESSCAL_H

#define NORTH 1
#define SOUTH 0
#define big_num long unsigned int

/*
*  The tesscal (Teselation Calculation) inclusion contains functions used to 
*  locate
*  file ofsets in a height variation file assosiated with a regular recusive
*  triangular tesselation of the surface of a sphere.  See written notes for
*  more details (see Random Terrain Generation - GM Tools)
*/ 
   
////////// Imediate Calculations /////////////
/* 
*  These functions calculate a value in a strait-forward maner and
*  are used by other functions in this inclusion
*  files_size;  rowsT_Dn;  rowsP_Dn;  equator_row; invert_row
*/ 
   
class triangle {
public:
  big_num p1;
  big_num p2;
  big_num p3;
  char orientation;
  char octant;
}; 
   
big_num file_size(unsigned char depth);  // tested oct 98 Dan V.
// returns the number of height variences verticies for a division with (depth) recursions
// see written notes on regular recursive triangular tesselation
   
unsigned int rowsT_Dn(unsigned int depth);  //  tested oct 98 Dan V.
// returns the number of rows of triangles, given a tesselation depth
   
unsigned int rowsP_Dn(unsigned int depth);  // tested oct 98 Dan V.
// returns the number of rows of points, given a tesselation depth
   
big_num triangles_Dn(unsigned int depth);  // tested oct 98 Dan V.
// returns the number of triangles, given a tesselation depth
   
unsigned int equator_row(unsigned char depth);            // tested oct 98 Dan V.
// returns the row number (rows of points) of the equator,
// given the tesselation depth
   
unsigned int points_in_row(unsigned int row, unsigned int depth); // tested oct 98 Dan V.
// returns the number of points in a given row* (row),
// for a given tesselation depth (depth)
// *the row is a row of points (not triangles)(see Definitions - Random Terrain Generation - GM Tools)
   
unsigned int invert_row(unsigned int row, unsigned int depth);  // tested oct 98 Dan V.
// returns the number of the row (row) counting from the southern hemisphere,
// i.e. as if the globe were inverted.
   
big_num invert_point(big_num p, unsigned char depth); // tested oct 98 Dan V.
// returns the number of the point, counting from the southern hemisphere
// i.e. as if the globe were inverted.
   
big_num sum_rowsP(unsigned int row, unsigned int depth);   // tested oct 98 Dan V.
// sums the number of points in the rows from row 1 to (row) inclusive
// given the tesselation (depth)
// given (row) 0 returns 0; other non-physical rows are undefined
   
big_num sum_rowsP(unsigned int row1, unsigned int row2, unsigned int depth); // tested oct 98 Dan V.
// sums the number of points in the rows from (row1) to (row2) inclusive
// given the tesselation (depth)
   
unsigned int rowP(big_num p, unsigned char depth); // tested oct 98 Dan V.
// returns the row of a point
   
unsigned int column(big_num p, unsigned char depth);  // tested oct 98 Dan V.
// determines the column of p, relative to its octant, or octant 1 if unspecified.
   
void find_seed_corners(   // tested oct 98 Dan V.
                        unsigned char depth, big_num *s1, big_num *s2,
                        big_num *s3, big_num *s4,
                        big_num *s5, big_num *s6
                      );
// finds the seed corner locations: s1(90,0) s2(0,0) s3(0,90) s4(0,180)
// s5(0, -90) s6(-90,0), given the depth (depth) of the tesselation.
// Precondition: 0 <= depth <= 15, memory already allocated to the seed corners
   
void sort(triangle *t);
// ensure the verticies are sorted - they usually will be

double latitude(big_num p, unsigned char depth);
// returns the lattitude of a point by its file order (and the depth of the division).
      					       
double longditude(big_num p, unsigned char depth);
// returns longditude by file order and depth or recursion

#endif

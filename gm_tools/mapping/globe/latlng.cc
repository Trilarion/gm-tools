/* latlng.cc - test bed for lattitude and longditude functions
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */


#include <stdio.h>
#include "tesscal.h"
/*
float latitude(big_num p, unsigned char depth)
// returns the lattitude of a point by its file order (and the depth of the division).
{
  float division;
  int row_of_p, row_of_equator;

  division = 180.0 / rowsT_Dn(depth);
  row_of_equator = equator_row(depth);
  row_of_p = rowP(p, depth);
  if (row_of_p == row_of_equator)
    return 0;
//  if (row_of_p < row_of_equator)
//    return ((float) (row_of_equator - row_of_p) * division);
  else
    return ((float) (row_of_equator - row_of_p) * division);
}// latitude

float longditude(big_num p, unsigned char depth)
// returns longditude by file order and depth or recursion
{
  float division;
  int row_of_p, column_of_p, points_in_row_of_p;
  int m180;

  row_of_p = rowP(p, depth);
  points_in_row_of_p = points_in_row(row_of_p, depth);
  division = 360.0 / points_in_row_of_p;

  m180 = (points_in_row_of_p) / 2 + 1;
  column_of_p = column(p, depth); // need i specify the octant?
  if (column_of_p == 1)
    return 0;
  if (column_of_p > m180)
    return ((float) column_of_p - 1) * division - 360.0;
  else
    return ((float) column_of_p - 1) * division;
}// longditude
*/

int main()
// test the above functions
{ 				   
  int order, depth;	 
  big_num total_points;		
  float lat, lng;	  
   		  	  	   
  printf("Test the file order to lat/lng functions\n");
  printf("Enter the test depth: ");
  scanf("%d", &depth);	  	  
//  printf("\nEnter a file order: ");
//  scanf("%d", &order);  	  
  total_points = file_size(depth);   
  printf("\n");		  
  for (order = 1; order <= total_points; order++)       	      
  {	       	 			
    lat = latitude(order, depth);	
    lng = longditude(order, depth); 	
    if (lng == 0) printf("\n");		  		   			      
    printf("%.1f %.1f; ", lat, lng);
  }	       	  			     
  return 0;
}  	       	  
	       	  

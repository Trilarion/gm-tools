/* bumps.cc - terrain generator
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */
 
/* uses a brownian height varience on a regular recursive triangular tesselation of 
 * a spherical surface.
 */
 
// note map[0] is a dummy node
 
// this program produces an ordered height variation array, using ordering
// functions of tescal.h, containg brownian motion height variations
// representing a surface map on a sphere - random terrain
 
//#include <stddef.h>
//#include <stdlib.h>
//#include <ncurses.h>
//#include <time.h>
 
#define NDEBUG	    
#include <assert.h> 
 		    
#include "tesscal.h"
#include "projection.h"	
//#include "dandefs.h"
#include "prompt.h"
#include "danio.h"

#define MAX_DEPTH 14
#define VARIATION 2000 // km
#define P_RADIUS 6000 // planet radius km 
#define ROUGH 0.9 // ROUGHNESS


/* obsolete - this stuff is all in dandefs, delete after regresion test FIX ME
#define random_roll(x) (1+(int) ((float) (x)*random()/(RAND_MAX + 1.0)))
		   
void randomize()  		 
{ 		      
  srandom((unsigned int) time(NULL));
  printw("random: %d\n", random_roll(100));
} 
*/    	       	      		 
			
void mem_err()	   	
// fail on memory allocation error
{			
  dwrite("Could not allocate enough memory. bye...");
  exit(EXIT_FAILURE);				    
}						    
						    
int get_depth()					    
// get the recursion depth to be generated	    
{						    
  int depth;					    
  prompt *p;		
  			
  dwrite("Enter the recursion depth (0 - %d) ", MAX_DEPTH);
  //  p = new prompt();  	       	
  //  sscanf(p->s, "%d", &depth);
  depth = 5; dwrite("5\n");
  // test validity based on MAX_DEPTH				
  while (depth > MAX_DEPTH) 					
  {		      	    
    dwrite("\nInvalid depth\n");
    dwrite("Enter the recursion depth (0 - %d)", MAX_DEPTH);
    p = new prompt();	      	
    sscanf(p->s, "%d", &depth);
  }// while	      	
  dwrite("\n");	

  log_event("get_depth() - depth %d\n", depth);  		      	
  return depth;	      	
}// get depth	      	
  	 	      	
int set_varience(int *map, int v1, int v2, int focus, int depth)
// set varience value	
// Precondition: v1 and v2 are the points in map[] either side of this point
//               depth is the recursion depth
//               focus ?is the depth so far reached?	
{ 	 	       	       	       	  	 
  float dist;	   	
  projection *p, *q;	

  if ((p = new projection(latitude(v1, depth), longditude(v1, depth))) 
      == NULL)  mem_err();
  if ((q = new projection(latitude(v2, depth), longditude(v2, depth))) 
      == NULL)  mem_err();

  dist = p->arcdist(q) * P_RADIUS;       
  delete p; delete q;	      
  
  return (int) ( (map[v1]+map[v2])/2 - (dist * ROUGH / 2) + d_random(ROUGH * dist) );
}  // set varience   		   
  		     	
int *init_map(big_num size)
// initalize the map	
{ 			
  big_num i;		
  int * map;		
  			
  if ((map = new int[size+1]) == NULL) mem_err();        // allocate memory
  dwrite("Memory allocated.\n");
  for (i = 0; i <= size; i++)							// zero the map
    map[i] = 0;		
  dwrite("Map zeroed\n");
  return map;		  
}// init map		  
  			  
triangle *setup_octant( big_num p1, big_num p2, big_num p3, char orientation, char num)
//  create and initalise a triangle representing the boundry of the octant
//  returns a pointer to a triangle with verticies p1, p2, p3,
//  .orientation = orientation and .octant = num
{ 			  
  triangle *octant;	  
  if ((octant = new triangle) == NULL) mem_err();
  octant->p1 = p1; octant->p2 = p2; octant->p3 = p3;
  octant->orientation = orientation; octant->octant = num;
  return octant;	  
}// setup octant	  
    			  
    			  
void vary_height(big_num p1, big_num p2, big_num p3,
    		 big_num p1_, big_num p2_, big_num p3_,
                 int * map, int count, int depth)
{   
  assert(map[p1] != 0); assert(map[p2] != 0); assert(map[p3] != 0);
  if (map[p1_] == 0)	  
    map[p1_] = set_varience(map, p2, p3, count, depth);
  if (map[p2_] == 0)	  	 
    map[p2_] = set_varience(map, p1, p3, count, depth);
  if (map[p3_] == 0)	  	 
    map[p3_] = set_varience(map, p1, p2, count, depth);
}// vary height		  
    	       		  
void find_sub_triangles(unsigned char depth, class triangle *t0, class triangle *t1,
                        class triangle *t2, class triangle *t3, class triangle *t4, int * map, int count)   
//  locate the file ofsets for the four sub-traingles
//  Precondition: t0 points to a sorted list of file ofsets, defining a triangle
//   on the surface of a regular triangular recursive tesselation of a sphere,
//   as produced by one of the recursive processes i.e. not offset in anyway.
//   such as produced by this function or find_seed_corners()
//   depth is the recursive depth of the whole tesselation.
//   see notes.		  	 
//  			  	 
// Postcondition: *to is unchanged.  *t{1, 2, 3, 4} contain the four regualr
//   triangular recursive tesselations of *t0.
{   			  	 
  // there are 6 types of triangles to consider
  // on the meridian seam 	 
    // north hemisphere: upright --(1)
    // north hemisphere: inverted  (1.1)   -- oops
    // south hemisphere: inverted -(2)
    // south hemisphere: upright   (2.1)   -- oops
  // not on the meridian seam	 
    // north hemisphere	  	 
      // upright ----------------- (3)
      // inverted ---------------- (4)
    // south hemisphere	  	 
      // upright ----------------- (5)
      // inverted ---------------- (6)
  			  	 
  			  	 
  unsigned int rowP1, rowP2, rowP3;
  big_num p1_, p2_, p3_;  // three new points are to be found
  unsigned int row;  // used in the calculation of points
  unsigned int length; // the length of the side of a sub-triangle in file ofsets
  			  	 
  // find the rows	  	 
  rowP1 = rowP(t0->p1, depth);	 
  rowP2 = rowP(t0->p2, depth);	 
  rowP3 = rowP(t0->p3, depth);	 
//  dwrite(" %d %d %d ", t0->p1, t0->p2, t0->p3);
  			  	 
  if (((t0->octant == 4) || (t0->octant == 8)) && ((column(t0->p1, depth) == 1) || (column(t0->p2, depth) == 1))) // chech for triangles on the meridian seam
  {			  	 
  //  meridian seam	  	 
    if (t0->orientation == NORTH)
    {// upright triangle on meridian seam
      if (t0->octant == 4)	 
      {			  	 
//        dwrite("\nUpright triangle on meridian seam, north hemisphere.\n");// ok     	       
        big_num p2__; // false p2
        p2__ = sum_rowsP(rowP2, depth) +1;
        p1_ = (t0->p3 + p2__) / 2;
        length = p1_ - t0->p3;	 
        row = (rowP1 + rowP2) / 2;
        p3_ = sum_rowsP((row-1), depth) + 1;
        p2_ = sum_rowsP(row, depth) + 1 - length;
        vary_height(t0->p1, t0->p2, t0->p3, p1_, p2_, p3_, map, count, depth);
      } else {			 
//        dwrite("\nUpright triangle on meridian seam, South Hemisphere\n");// ok
        big_num p2__;		 
        p2__ = sum_rowsP(rowP2, depth) + 1;
        p1_ = (t0->p3 + p2__) /2;
        length = p1_ - t0->p3;	 
        row = (rowP1 + rowP2) /2;
        p3_ = sum_rowsP(row, depth) - length + 1;
        p2_ = p3_ - length;	 
        vary_height(t0->p1, t0->p2, t0->p3, p1_, p2_, p3_, map, count, depth);
      }//fi			 
    }// fi: upright triangle on meridian seam :
    else			 
    {// inverted triangle on meridian seam : (2)
      if (t0->octant == 8)	 
      {				 
//        dwrite("\nInverted triangle on meridian seam, South hemisphere.\n");// ok
        big_num p1__; // false p1
        p1__ = sum_rowsP(rowP1, depth) +1;
        p3_ = (t0->p2 + p1__) / 2;
        length = p3_ - t0->p2;	 
        row = (rowP1 + rowP3) / 2;
        p2_ = sum_rowsP((row-1), depth) + 1;
        p1_ = sum_rowsP(row, depth) + 1 - length;
        vary_height(t0->p1, t0->p2, t0->p3, p1_, p2_, p3_, map, count, depth);
      } else {			 
        assert(t0->octant == 4); 
//        dwrite("\nInverted triangle on meridian seam, North hemisphere.\n");// ok
        big_num p1__; // false p1
        p1__ = sum_rowsP(rowP1, depth) +1;
        p3_ = (t0->p2 + p1__) /2;
        length = p3_ - t0->p2;
        row = (rowP1 + rowP3) /2;
        p2_ = sum_rowsP(row, depth) - length + 1;
        p1_ = p2_ - length;
        vary_height(t0->p1, t0->p2, t0->p3, p1_, p2_, p3_, map, count, depth);
      }// fi
    }//fi: inverted triangle on meridian seam :
  } else {  // not on meridian seam
    if (t0->octant < 5)
    {  // north hemisphere
       if (t0->orientation == NORTH)
       // upright triangle not on meridian seam, north hemisphere           (3)
       {
//         dwrite("\nUpright triangle, not on meridian seam, north hemisphere.\n");  //ok
         p1_ = (t0->p2 + t0->p3) / 2;
         row = (rowP1 + rowP2) / 2;
         length = p1_ - t0->p2;
         p3_ = sum_rowsP((row-1), depth) + column(t0->p1, depth) + (t0->octant-1)*length;
         p2_ = p3_ + length;
         vary_height(t0->p1, t0->p2, t0->p3, p1_, p2_, p3_, map, count, depth);
       } else { // inverted triangle not on meridian seam, north hemisphere (4)
//         dwrite("\nInverted triangle, not on meridian seam, north hemisphere.\n");     // ok
         p3_ = (t0->p1 + t0->p2) / 2;
         row = (rowP2 + rowP3) / 2;
         length = p3_ - t0->p1;
         p1_ = sum_rowsP((row-1), depth) + column(t0->p2, depth) + (t0->octant-1)*length;
         p2_ = p1_ - length;
         vary_height(t0->p1, t0->p2, t0->p3, p1_, p2_, p3_, map, count, depth);
       }//fi: not on meridian seam
     } else { // south hemisphere
       if (t0->orientation == NORTH)
       // upright triangle not on meridian seam, south hemisphere           (5)
       {
//    	 dwrite("\nUpright triangle, not on meridian seam, south hemisphere.\n");            // ok
         p1_ = (t0->p2 + t0->p3) / 2;
         row = (rowP1 + rowP2) / 2;
         length = p1_ - t0->p2;
         p2_ = sum_rowsP((row-1), depth) + column(t0->p1, depth) - (t0->octant-5)*length;
         p3_ = p2_ - length;
         vary_height(t0->p1, t0->p2, t0->p3, p1_, p2_, p3_, map, count, depth);
       } else { // inverted triangle not on meridian seam, south hemisphere (6)
//         dwrite("\nInverted triangle, not on meridian seam, south hemisphere.\n");       // ok
         p3_ = (t0->p1 + t0->p2) / 2;
         row = (rowP2 + rowP3) / 2;
         length = p3_ - t0->p1;
         p2_ = sum_rowsP((row-1), depth) + column(t0->p1, depth) - (t0->octant-5)*length;
         p1_ = p2_ + length;
         vary_height(t0->p1, t0->p2, t0->p3, p1_, p2_, p3_, map, count, depth);
       }//fi: not on meridian seam
     }// south hemisphere
  }//fi: meridian seam?

  // carry octant
  t1->octant = t2->octant = t3->octant = t4->octant = t0->octant;
  t1->orientation = t2->orientation = t3->orientation = t0->orientation;
  if (t0->orientation == NORTH) {
    t4->orientation = SOUTH;
  } else {
    t4->orientation = NORTH;
  }//fi

  // assignments of verticies for sub-triangles;
  t1->p1 = t0->p1; t1->p2 = p3_;    t1->p3 = p2_;
  t2->p1 = p3_;    t2->p2 = t0->p2; t2->p3 = p1_;
  t3->p1 = p2_;    t3->p2 = p1_;    t3->p3 = t0->p3;
  t4->p1 = p3_;	   t4->p2 = p2_;    t4->p3 = p1_;
  // sort verticies: ensure they are lowest to highest
  sort(t1); sort(t2); sort(t3); sort(t4);
  assert(t1->p1 < t1->p2);
  assert(t1->p2 < t1->p3);
  assert(t2->p1 < t2->p2);
  assert(t2->p2 < t2->p3);
  assert(t3->p1 < t3->p2);
  assert(t3->p2 < t3->p3);
  assert(t4->p1 < t4->p2);
  assert(t4->p2 < t4->p3);
}// find sub triangles
                 
void generate_octant(int * map, int depth, triangle t, int count)
// generate the height field of an octant - recusivly
{
//  dwrite("Gen %d: ", count);
  triangle *t1, *t2, *t3, *t4; // sub triangles
  if ((t1 = new triangle) == NULL) mem_err();
  if ((t2 = new triangle) == NULL) mem_err();
  if ((t3 = new triangle) == NULL) mem_err();
  if ((t4 = new triangle) == NULL) mem_err();
			   
  // get sub triangles	   
  find_sub_triangles(depth, &t, t1, t2, t3, t4, map, count);
  // recurse sub triangles as neccessary
//  if ((t3->p2 < t3->p3-1) && (t2->p1 < t2->p2-1)) // see footnote
  if (count > 1)	   
  {			   
    generate_octant(map, depth, *t1, count-1);
    generate_octant(map, depth, *t2, count-1);
    generate_octant(map, depth, *t3, count-1);
    generate_octant(map, depth, *t4, count-1);
  }//fi: recurse	   
  delete t1;		   
  delete t2;		   
  delete t3;		   
  delete t4;		   
} // generate octant	   
      
void init()
{
  start_log();
  init_danscr();
}//init

      			   
      			   
int main()		   
{     
  FILE *out;
  int depth;  // recursion depth
  int *map;   // the map   
  big_num size;    // size of the map
  big_num seed1, seed2, seed3, seed4, seed5, seed6;  // seed corners
  triangle *octant[8];	   
      
  init();
  randomize();              // initalise random number generation
  depth = get_depth();      // get the depth to be generated
  size = file_size(depth);  // get the size needed
  dwrite("Size: %d\n", size);
  map = init_map(size);      // assign memory and zero the map
      		  	      
  // generate bump map	      
  find_seed_corners(depth, &seed1, &seed2, &seed3, &seed4, &seed5, &seed6);
  dwrite("Found seed corners: \n");				     
  map[seed1] = d_random(VARIATION);    map[seed2] = d_random(VARIATION);  // initalize height
  map[seed3] = d_random(VARIATION);    map[seed4] = d_random(VARIATION);  // of seed corners
  map[seed5] = d_random(VARIATION);    map[seed6] = d_random(VARIATION);  
      	 		      
  // setup the octants as triangles
  // the index number = octant-1 (indecies start at 0)
  octant[0] = setup_octant(seed1, seed2, seed3, NORTH, 1); // 1st octant
  octant[1] = setup_octant(seed1, seed3, seed4, NORTH, 2); // 2nd octant
  octant[2] = setup_octant(seed1, seed4, seed5, NORTH, 3); // 3rd octant
  octant[3] = setup_octant(seed1, seed2, seed5, NORTH, 4); // 4th octant
  octant[4] = setup_octant(seed2, seed3, seed6, SOUTH, 5); // 5th octant
  octant[5] = setup_octant(seed3, seed4, seed6, SOUTH, 6); // 6th octant
  octant[6] = setup_octant(seed4, seed5, seed6, SOUTH, 7); // 7th octant
  octant[7] = setup_octant(seed2, seed5, seed6, SOUTH, 8); // 8th octant
  dwrite("Octant setup ok\n");    	 
  // recursivly generate each of the eight octants
  for (int i = 0; i < 8; i++)
    generate_octant(map, depth, *octant[i], depth); // octant # = index+1
      	 		    
  dwrite("Generation complete\n");
  // save map as a file	    	  
  if ((out = fopen("TEMP.DAT", "w"))                  // open output file
    == NULL)	    	    	  	    
  {   	  	    	    	  	    
    log_event("Cannot open input file./n");
  } else  	    	    	  	    
  {   	  	    	    	  
    fprintf(out, "%d ", depth);	  
    for (unsigned int i = 1; i <= size; i++)
      fprintf(out, "%d ", map[i]);
  }	 		 	  
  fclose(out); 	       	       	  
  dwrite("File saved\n");
  	    		 	  
  // deallocate		 	  
  delete[] map;		 	  
  	 
  dwrite("Press any key to end.");
  (void) dgetch();
  close_danscr();		 
  end_log(); // close the log file

  return 0;
}// main 		 
  	 		 
// bumps.cpp		 
  	 		 
/*	 		 
footnote 1		 
This test checks to see if the length of the side of the triangle == 1, and if
so abort recursion;  the test is based on sub-triangles t2 adn t3, to avoid
ambiguities created by the meridian seam
*/	 		 
	 		 
			 

/* globe_view.cc - view a terrain generation
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */
   					  
//#include <vga.h>			  
//#include <vgagl.h>			  
#include <assert.h>			  
#include <stdlib.h>			  
#include <time.h> 			  
#include <stdio.h>			  
#include <math.h> 			  
      	    	  			  
#include "projection.h"
#include "tesscal.h"
#include "danio.h"
#include "keys.h"
 		     
// Screen Properties 
/* This should all be handled under danio - delete after regression testing. FIX ME
//#define SCREEN_MODE G1024x768x256 // Only one SCREEN_MODE to be un-commented
#define SCREEN_MODE G640x480x256
//#define SCREEN_MODE G320x400x256
#define BACKGROUND 0     	
#define WHITE  	      15     
#define BLACK	      0	     		
#define LIGHTGRAY     7	     		
#define DARKGRAY      8	     		
#define BLUE   	      32
#define GREEN         2
*/

// Globe properties 			
#define ORIENT_LNG 90 	     		
#define WATER 1
#define LAND 2 
#define WATER_LINE     2500
#define START_LAT 0.0
#define START_LNG 0.0
	
// Globe Manipulations
#define SCREEN_STEP 20
#define SPEED_INC 0.01   	     		
	
// Misc Defs
#define DEGREE 0.0087266463 // 1 degree in radians

// Debugging
#undef DEBUG
#define TOGGLEOFF
		 
//keys	
#define ESC 255
#define LF_ARROW 414
#define RT_ARROW 413
#define UP_ARROW 411
#define DN_ARROW 412

// global varialbes   	     		
int maxx, maxy;  // sudo-constants to be determined at runtime
int radius;    	 // of the globe in screen pixels, determined at runtime
float dynamic_scale;
point centre;    // center of the screen 
point view_centre; // centre of view 
float ratio;     // height / width for circle correction
float i_fast, j_fast, k_fast; // rotation speed 
int water_line;
int level_land, level_water;
		      
projection **verticies;	 // array of pointers to the verticies
projection **vert_copy;	
projection *all_copy; 
int *varience;	      	
big_num size;         	    // nummber of points
unsigned char depth;  
projection * orientation;  // for positioning the sphere
		      	     
// GraphicsContext *physical_screen;  // for screen paging	see vgagl
dwindow *physical_screen;
       	       	      	     
int page; // the active display page, not the visuale display page.
 		 	     
long int random_roll(unsigned long int range)
// returns a random number from 0 to range-1
{    	       		     		   			       
  return (long int) (( (double) random() / (double) RAND_MAX ) * range); 
} // random roll		     					       
  			     					       
void togglepage()
// makes the active page visible and sets the active page to a new virtual 
//   screen
{
#ifndef TOGGLEOFF  
  gl_copyscreen(physical_screen);
#endif  
}// toggle page	    						       
      	    	    						       
void init() 
// initialise random number generator
// initialise vga and vgagl 
// initialise virtual context for vga_gl to enable screen flipping 
{     	    	    
  init_danio();
  /* this should all be handled by danio. delete after regression testing. FIX ME						       
  int errorcode;
   
  // tk initialise the random number generator
   	    	       	       					       
  // initialize vga

  errorcode = vga_init();
  // read result of initialization
  if (errorcode)  // an error occurred 
  {	      	      	       					       
    printf("Vga graphics error: %d\n", errorcode);			       
    exit(errorcode);  	       					       
  }// vga_init fail   	       
  vga_setmode(SCREEN_MODE);    					       
  		      
  // initialise vga_gl 
  errorcode = gl_setcontextvga(SCREEN_MODE);
  if (errorcode)  // an error occurred 
  {	      	      	       					       
    printf("Vga_gl graphics error: %d\n", errorcode);			       
    exit(errorcode);   	       		  			       
  }// gl_setcontextvga failed    	  
  
  // save a pointer to the physical screen
  physical_screen = gl_allocatecontext();
  gl_getcontext(physical_screen);	 
  
  // initialise the virtual screen we will use for page flipping
#ifndef TOGGLEOFF  
  gl_setcontextvgavirtual(SCREEN_MODE);
#endif  
  
  gl_enableclipping();
  
  // initialise runtime detirmined sudo-constats
  maxx = WIDTH; maxy = HEIGHT; // screen dimentions 
  ratio = (float) HEIGHT / (float) WIDTH; // ratio used for circle correction
  radius = (int) (((WIDTH > HEIGHT) ? HEIGHT : WIDTH) * 0.40); // size of the globe
  centre.x = maxx / 2; centre.y = maxy / 2; // screen centre
  */
}//init	 	       	       		 
     	    	       	  		 
void setup()   	       	  		 
// build the sphere    	  	      	 
{      	    	       	  	      	 
  unsigned int point; // counter    	      		 
  FILE *in; 	       		      	 
  int temp;  
   
  // open input file for reading      	 
  if ((in = fopen("TEMP.DAT", "r"))                  // open output file
    == NULL)   	       	    	      	    
  {   	       	       	    	      	    
    dwrite("Fatal Error: Cannot open input file - aborting.\n");
    log_event("Cannot open input file./n");  
    close_danio();
    exit(0);   	       	    	      	 
  } 	       	       	    	      	 
  log_event("input file open\n");	 
    	       	       	    	       	 
  // find size  	    	      	 
  fscanf(in, "%d", &temp); depth = (unsigned char) temp; 
  log_event("depth %d\n", (int) depth);			
  size = file_size(depth);	      	 
  log_event("size %d\n", (int) size); 

  // allocate array for points
  if ((verticies = (projection**) malloc(sizeof(int)*size)) == NULL)  
  {	      	 		
    log_event("out of memory");
    exit(0);			
  }				
  if ((vert_copy = (projection**) malloc(sizeof(int)*size)) == NULL)  
  {	      	 		
    log_event("out of memory");
    exit(0);			
  }				
  // set verticies at applicable lat-lng
  for (point = 0; point < size; point++)
  { 	       			     
    verticies[point] = new projection((double) latitude((point+1),   depth), 
   				      (double) longditude((point+1), depth) );
  }
  // load height varience data
  if ((varience = new int[size]) == NULL)
  {
    log_event("out of memory");
    exit(0);
  }
  for (point = 0; point < size; point++)
  {			      
    fscanf(in, "%d", &varience[point]);
//    if (varience[point] < water_line) varience[point] = water_line-1; // smooth water
  }			      
  log_event("varience of point 1: %d\n", varience[0]);
  // close input file  	      	       
  fclose(in);  	       		       
  log_event("input file closed\n");
  	       	       		
  // set refrence points			     
  orientation = new projection(0, ORIENT_LNG); 	     
}// setup 	    
     	  	   
void cloze()	    	       
{     	    	   	       
  close_danio();
}//cloze    	   
      	    	   
point calc_screen_coords(projection *p1, big_num offset, int terrain)
{			    			       		    
  float v_leng;					       		    
  point screen_point;				       		    
  
  if ( ((terrain == WATER) && (level_water)) || ((terrain == LAND) && (level_land)) )
    v_leng = radius * dynamic_scale * (50000 + water_line) / 50000; // by varience 
  else	     		     	    	 		 
    v_leng = radius * dynamic_scale * (50000 + varience[offset]) / 50000; // by varience 
  screen_point.x = centre.x + view_centre.x + (int) ( p1->di * v_leng);
  screen_point.y = centre.y + view_centre.y - (int) ( p1->dk * v_leng * ratio);
  			       					    
  return screen_point;	       					    
} 			       					    
  								    
double inc_rotation(double rotation, double fast)
// increment rotation 	   	 		
{     	 	   	     	 		
  double new_rotation;	   	 
  			   	 
  new_rotation = rotation + fast;      	
  if (new_rotation > 180) new_rotation -= 360; 
  if (new_rotation < -180) new_rotation	+= 360;
  return new_rotation;			 
  
}// inc_rotation	      				      
  	       		    				      
void do_edge(int a, int b)
// draw a line between adjacent points if vissible
// precondition a and b are adjacent points in the vert_copy array
{   					 
  point screen_a, screen_b;		 		       
  					 
  if ((vert_copy[a]->dj >= -0.001) && (vert_copy[b]->dj >= -0.001)) 
  // (so the top and bottom don't dissapear we don't use 0.0)
  // draw front only (comment out the if for front and back)
  { 	       		  		 	
    if ((varience[a] < water_line) && (varience[b] < water_line))
    { 	
      screen_a = calc_screen_coords(vert_copy[a], a, WATER);
      screen_b = calc_screen_coords(vert_copy[b], b, WATER);  	  
      dline(screen_a.x, screen_a.y, screen_b.x, screen_b.y, BLUE);
    } else { 				 		  
      screen_a = calc_screen_coords(vert_copy[a], a, LAND);  	  
      screen_b = calc_screen_coords(vert_copy[b], b, LAND);  	 
      dline(screen_a.x, screen_a.y, screen_b.x, screen_b.y, GREEN);
    } //fi 
  }// fi								   
} // do edge			 		    
    	    		 			    
    	    	     	 			    
void draw_globe(double i_rotation, double j_rotation, double k_rotation)
  // draw the rotating spere   	    	    	    	      	
  // NOTE - drawing page is not the viewing page, see togglepage()
{   	       	      	 			    	      
  unsigned int count;  	      	 			    
  int this_row, points_in_this_row;		    
  int next_row, points_in_next_row;		    
  int this_point, point_below_this;
  int first_point_in_this_row;
  int last_point_in_this_row; 			    
  int first_point_in_next_row; 	       	       	    
  int last_point_in_next_row; 			    
  int total_rows;
  int quarter_points_in_this_row;
  int quarter_points_in_next_row;       	
  int middle_row;	      			    
  int above_equator;	      			    
    			      			    
// copy the points ***	      			    
  for (count = 0; count < size; count++) 	    	      
  {    	       	      	      	    	 	    	      
    if ((vert_copy[count] = new projection(*verticies[count])) == NULL)
    { 	 		      			    
      log_event("out of memory");		    
      exit(0);		      			    
    }			      			    
    vert_copy[count]->i_rotate(i_rotation);  // rotate the points
    vert_copy[count]->j_rotate(j_rotation);
    vert_copy[count]->k_rotate(k_rotation);
    vert_copy[0]->normalise();
  } 			      			    
//  log_event("point 1 dj: %.2f ", vert_copy[0]->dj);
//  log_event("point 1 dk: %.2f ", vert_copy[0]->dk);
//  log_event("point 2 dj: %.2f ", vert_copy[1]->dj); 
//  log_event("point 2 dk: %.2f ", vert_copy[1]->dk);
//  log_event("point 3 dj: %.2f ", vert_copy[2]->dj); 
//  log_event("point 4 dj: %.2f ", vert_copy[3]->dj); 
//  log_event("point 5 dj: %.2f\n ", vert_copy[4]->dj); 
// draw conecting lines    	       	 	    	      
  // NB: vissible points have j > 0 (except top and bottom?) 
  // top	    	  	     		    	     
  do_edge(0, 1); do_edge(0, 2); do_edge(0, 3); do_edge(0, 4);
// *** do intermaediate rows ***     	       	       	       	 
  total_rows = rowsP_Dn(depth);	     		       	     
  middle_row = (total_rows + 1) / 2; 		       	     	       
  this_row = 2;	    		     		       	     	       
  this_point = 2; // in tesscal terms, 1 in array terms	     	       
  above_equator = 1; // start with the upper hemisphere		       
  do   	       	       	     	     	 	       	     	       
  // loop through the rows   	     	 	       	     	       
  {   	     	    	     	     	 	       	     	       
    points_in_this_row = points_in_row(this_row, depth);     	       
    next_row = this_row +1;    	     	 	       	     	       
    points_in_next_row = points_in_row(next_row, depth);     	       
    point_below_this = sum_rowsP(this_row, depth) + 1; 		       
    first_point_in_this_row = this_point;      	       	       	       
    if (first_point_in_this_row != sum_rowsP(this_row-1, depth)+1)
    // but why do I need this kludge ???
    {  	 			     
      first_point_in_this_row = sum_rowsP(this_row-1, depth) + 1;
//      log_event("This row: %d This point: %d First point in this row: %d Point below: %d\n",
//       	      this_row, this_point, first_point_in_this_row, point_below_this);
      this_point = first_point_in_this_row;
    }//fi
    last_point_in_this_row = point_below_this - 1;		       
    first_point_in_next_row = point_below_this;			       
    // log_event(" %d ", first_point_in_next_row);		       
    last_point_in_next_row = sum_rowsP(next_row, depth);	       
    quarter_points_in_this_row = points_in_row(this_row, depth) / 4;   
    quarter_points_in_next_row = points_in_row(next_row, depth) / 4;   
    above_equator = this_row < middle_row;	   		       
    do_edge(point_below_this-2, this_point-1); // do seam horizontal: this point and the last in the row
//    assert(last_point_in_this_row != first_point_in_next_row);
    if (above_equator)			 	       // do vertical seam
      do_edge(this_point-1, last_point_in_next_row-1); // the two hemispheres 
    else					       // slope opposit at the
      do_edge(last_point_in_this_row-1, first_point_in_next_row-1); // seam
    do 	      	      	       	     	 	      	     	       	     
    // loop through points in the row	 	    	     	       	     
    {  	       	       	       	       	       	       	     	       	     
      // draw horizontal lines in row		    	 	       	     
      if (this_point != last_point_in_this_row)	    	 	       	     
        do_edge(this_point-1, this_point); // this point and the next  	     
      // draw vertical lines down from row	    	  	       	     
      if (above_equator)  	     		    	  	       	     
      {	      	     	  	     		    	  	       	     
        do_edge(this_point-1, point_below_this-1); // this and the one below
       	do_edge(this_point-1, point_below_this); // and the next       
        // adjust for meridians	     		    	  	       
        if (((point_below_this - first_point_in_next_row + 1) %        
  	      quarter_points_in_next_row) == 0)	   	 	       
  	// point before the meridian line 	   	 	       
  	{	    		  		   	 	       
//    	  log_event("point below this: %d", point_below_this);   
          point_below_this++;	     		   	   	       
     	  do_edge(this_point-1, point_below_this); 	 	       
   	}	     	  	     		   	 	       
      }	else { // equator or below  				 
     	if (this_point != first_point_in_this_row)		 
          do_edge(this_point-1, point_below_this-2); // this and the one below
  	if (point_below_this <= last_point_in_next_row)		 
       	  do_edge(this_point-1, point_below_this-1); // and the next
        // adjust for meridians	     		    	  	       
        if (((this_point - first_point_in_this_row + 1) % 	       
       	      quarter_points_in_this_row) == 0)	   	 	       
    	// point before the meridian line     	   	 	       
    	{	    		    		   	 	       
//       	  log_event("point below this: %d", point_below_this);
          this_point++;
    	  do_edge(this_point-1, this_point); // horizontal line before meridian
    	  if (this_point < last_point_in_this_row)  
    	  { 
            do_edge(this_point-1, point_below_this-1); // vertical meridian lines
      	  }
      	}	     	  	     		   	 	       
      }	//fi  	      	  	     		   	  	       
      // move to next point    	     	 	      	  	       
      this_point++;   	     	     	 	   	  	       
      point_below_this++;    	     	 	   	  	       
    } while (this_point <= last_point_in_this_row);	  	       
    this_row++;	       	// go to next row  	   	 	       	  
  // while: loop through points in the row     	       	  	       
  } while (this_row < total_rows-1); // dont need to do last row - only one point    	 		  
       	       	    	  		 	   		       
// bottom    	    	     		 	   		       
  do_edge(size-1, size-2); do_edge(size-1, size-3); 		       
  do_edge(size-1, size-4); do_edge(size-1, size-5);		       
// delete the copy
  for (count = 0; count < size; count++) 		      
  {    	       	      	     	    	 		      
    delete vert_copy[count];      
  }    
}   	    	      	       	       		   
       	    		       	       		   
int main() 	     	       	    		   
{      	       	     	       	    		   
  int key;
  int done = 0;
  double i_rotation = 0, j_rotation = 0, k_rotation = 0.0;
  				   
  				   
  init();      	       	       	   
  log_event("init ok\n");	   
  setup();		    	   
  water_line = WATER_LINE;	   
  level_land = 0; level_water = 1; 
  view_centre.x = 0; view_centre.y=0;
  dynamic_scale = 2.5; 	       	   
  k_fast = 0; 	    	   
  	   		    	   
  log_event("setup ok\n");   
  // draw the rotating globe  	    	    			
  do {
    dclear(BACKGROUND); // clear the page
    draw_globe(i_rotation, j_rotation, k_rotation);
    // increment rotation
    i_rotation = inc_rotation(i_rotation, i_fast);
    j_rotation = inc_rotation(j_rotation, j_fast); 
    k_rotation = inc_rotation(k_rotation, k_fast); 
    togglepage();  // show the newly drawn page and reset active page
    do 	   			   
    { 	   			   
      key = dgetch(); // it might be better to use prompt to catch extended characters
      if (key == 27)  		   			  
      {	   	     		   			  
        key = 255 + (int) dgetch() + (int) dgetch(); 
        log_event("key pressed coded as: %d", key);	  
//        exit(0);		   
      }	   			   
    	   			   
      switch(key)		       
      { // vertical (k) axis rotation
	case('K'):		   
        case(','):  // reduce or reverse rotation	
        {      	      	    	       
          k_fast -= SPEED_INC;     
          break;  	    	       
        }      	  		   
	case('k'):		   
        case('.'): // increase rotation
        {      	  	    	    
          k_fast += SPEED_INC;     
          break;  	    	    
        }      	  		   
	// depth (j) axis rotation 
	       			   
	case('J'):
	{      
	  j_fast -= SPEED_INC;
	  break;    
	}      	    
	case('j'):  
	{      	    
	  j_fast += SPEED_INC; 
	  break; 
	}      
	// horizontal (i) axis rotation	
	case('I'):			
	{      				
	  i_fast -= SPEED_INC;		
	  break;			
	}      				
	case('i'):			
	{      				
	  i_fast += SPEED_INC;		
	  break;			
	}  				
        case(' '):  // stop all rotation
        {  		    		
          i_fast = 0.0;
	  j_fast = 0.0;
	  k_fast = 0.0;
          break;	    		
        }
	case('0'):
	{    	
	  i_rotation = 0.0; j_rotation = 0.0; k_rotation = 0.0;	
	  break;
	}
       	case(ESC):
        case('q'):	    	
        case('Q'):  // quit 
        {      			
          done = 1;    				
  	  break;       				
        }      	       				
        case('Z'): // zoom out 			
        {     					
       	  dynamic_scale = (dynamic_scale * 0.9);
	  view_centre.x = (int) (view_centre.x * 0.9);
	  view_centre.y = (int) (view_centre.y * 0.9);
	  break;				
        }      	     	      		     		 	  
        case('z'):  // zoom in			
        {  	   	      			
       	  dynamic_scale = (dynamic_scale / 0.9);
	  view_centre.x = (int) (view_centre.x / 0.9);
	  view_centre.y = (int) (view_centre.y / 0.9);
	  break;
        }  	   	      		     		 	  
	case('w'): // raise water level 	
	{  	       				
       	  water_line += 100;			
       	  break;       	    		     
	}  	       	    		     
	case('W'): // lower water level	 
	{  	       	    		 
	  water_line -= 100;		 
  	  break;       
  	}  	       
	case('l'): // level water
	{  	       
	  level_water = (!level_water);
	  break;       		       
	}  	       		       
      	case('L'): // level land       
	{  	       		       
	  level_land = (!level_land);
	  break;       		       
	}  	       
	case(LF_ARROW):
	{  	       
	  view_centre.x += SCREEN_STEP;
	  break;       		      
	}  	       		      
	case(RT_ARROW):		      
	{  	       		      
	  view_centre.x -= SCREEN_STEP; 	      
	  break;		      
	}  			      
	case(UP_ARROW):		      
	{  			      
	  view_centre.y += SCREEN_STEP; 	      
	  break;		      
	}  			      
	case(DN_ARROW):		      
	{  			      
	  view_centre.y -= SCREEN_STEP; 	      
	  break;
	}
      } // switch
    } while (key != 0);
    
  } while (!done);  // until <Keypress>	   
//  } while(0); // draw once and quit (for debbuging) 
  log_event("Waterline %d\n", water_line);
  
  cloze();     		 				  

  return errNO_ERROR;
}//main	      		 				  
    	      		 				  
    							  
  	
  
  

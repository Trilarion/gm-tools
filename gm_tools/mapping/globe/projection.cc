/* projection.cc - projection 	 
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

		       
#include <assert.h>    
#include <math.h>      
#include <stdio.h>     
		       						      
#include "projection.h"    						      
		       						      
#define PRECISION 0.00001					      
		       						      
/*        FINDING THE GREAT ARC					      
 * NB: the following mathimatical preamble is included for intrests sake only
 * to describe how the function finds the great arc distance between the ends 
 * of two vectors
The great arc lenght is the radius times the angle (in radians) subtended
by the two ends indicated by the vectors. This angle can be found by  
substituting the chord length and the radius into the cosign rule for 
non right angled triangles and solving for the subtended angle thus:  
		       						      
c^2 = a^2 + b^2 - 2*a*b*cos(C)               --( cosign rule )	      
// solving for the angle C					      
C = arc_cos( (a^2 + b^2 - c^2) / (2*a*b) )   --( 1 )		      
		       						      
And so:		       						      
		       						      
Circumfrence = 2 * PI * radius  --( circumfrence of a circle )	      
Arc = Circufrence * subtended_angle / (2*PI)			      
    = 2 * PI * radius * subtended_angle / (2*PI)		      
    = subtended_angle * radius               --( 2 )		      
		       						      
Consider the circle ( a meridian of the globe ) containing the two endpoints
of the vectors. The triangle formed by two radii and the chord is equalateral.
Let a = b = radius = r in (1)					      
Let chord = c	       						      
		       						      
C = arc_cos( (2* r^2 - c^2) / 2* r^2 )				      
  = arc_cos( 1 - (c^2 / 2* r^2) )				      
		       						      
substituting into (2) we get the arc length in terms of the radius and the
chord:		       						      
		       						      
Arc = r * arc_cos( 1 - (c^2 / 2* r^2) )				      
								      
*/								      
								      
								      
								      
inline double get_magnitude(vector3 a_)				      
// returns the magnitude of a vector				      
{								      
  return sqrt(a_.x*a_.x + a_.y*a_.y + a_.z*a_.z);		      
}

double remainder(double x)
// returns the normal fractional portion of a real number
// eg 0.245 == remainder(1.245)
{
  double temp = (int) x;
  x -= temp;
  return x;
}

void projection::normalise()
  // normalise to a unit vector (divide each vector component by the magnitude of the vector
{
  double magnitude = sqrt(di*di + dj*dj + dk*dk);
  if (magnitude == 0) return; // [0,0,0], ie 0_
  di /= magnitude;
  dj /= magnitude;
  dk /= magnitude;
}   
    
projection::projection(latlng position)
  // init from latitude-longditude
  // precision may be reduced by trig functions operating on pi
{   
  double cosphi;                  // cos of the elevation
    
// preconditions
  assert(position.lat <= 90);
  assert(position.lng >= -180);
  assert(position.lng <= 180);
    
// convert angle to radians
  position.lat *= (M_PI / 180.0);
  position.lng *= (M_PI / 180.0);
    
  // horizontal component of the latitude
  cosphi = cos(position.lat);
    
  // i and j components of the longditude,
  // times horizontal component of latitude
  di = cosphi * cos(position.lng);
  dj = cosphi * sin(position.lng);
    
  // verticle component of the latitude
  dk = sin(position.lat);
    				    
  // nomalisation is unneccesary    
}// init with latitude-longditude   
    				    
projection::projection(double lat, double lng)	
  // init from latitude and londitude
{ 				    
  double cosphi;                  // cos of the elevation
 				    
// preconditions		    
  assert(lat <= 90);  	    
  assert(lng >= -180);	    
  assert(lng <= 180);	    
 	 			    
// convert angle to radians	    
  lat *= (M_PI / 180.0);   
  lng *= (M_PI / 180.0);   
  				    
  // horizontal component of the latitude
  cosphi = cos(lat);	    
 	       			    
  // i and j components of the longditude,
  // times horizontal component of latitude
  di = cosphi * cos(lng);  
  dj = cosphi * sin(lng);  
 	       	    		    
 	       	    		    
  // verticle component of the latitude
  dk = sin(lat);	    
 	   	    		    
  // nomalisation is unneccesary    
}// init with latitude and londitude 				 	
	   			 
projection::projection(vector3 position) 				 
// init from 3d vector	 
{	   
  di = position.x;
  dj = position.y;
  dk = position.z;
  normalise();
}// init with vector3

projection::~projection()
{
}

double projection::arcdist(projection *another)
  // returns the great arc distance between self and another
{				       
// Precondition: the vectors are direction vectors for a unit sphere.
				       
// Arc = r * arc_cos( 1 - (c^2 / 2* r^2) )  --( see head of file for derivation )
// r = 1 so Arc = arc_cos( 1 - c^2 / 2 )
				       
  double chord, chordsq;	       
				       
  chord = dist(another);            // find the chord
  chordsq = chord*chord;           // find the square of the chord
	       			       
  return acos( 1 - (chordsq / 2));
}	       
	       
double projection::arcdist(vector3 a_)
  // returns the great arc distance between self and another
{	       
// Precondition: the vectors are direction vectors for a unit sphere.
	       
// Arc = r * arc_cos( 1 - (c^2 / 2* r^2) )  --( see head of file for derivation )
// r = 1 so Arc = arc_cos( 1 - c^2 / 2 )
	       
  double chord, chordsq;
	       
  chord = dist(a_);            // find the chord
  chordsq = chord*chord;           // find the square of the chord
	       
  return acos( 1 - (chordsq / 2));
}	       
double arcdist(vector3 a_, vector3 b_)
  // returns the great arc distance between two 3d vectors
{	       
// Precondition: the vectors are direction vectors for a unit sphere.
	       
// Arc = r * arc_cos( 1 - (c^2 / 2* r^2) )  --( see head of file for derivation )
// r = 1 so Arc = arc_cos( 1 - c^2 / 2 )
	       
  double chord, chordsq;
	       
  chord = dist(a_, b_);            // find the chord
  chordsq = chord*chord;           // find the square of the chord
	       
  return acos( 1 - (chordsq / 2));
}// arc distance
	       
double dist(vector3 a_, vector3 b_)
  // returns the cord distance between two 3d vectors
{	       
  vector3 diference;
	       
  // find the diference vector
  diference.x = a_.x - b_.x;
  diference.y = a_.y - b_.y;
  diference.z = a_.z - b_.z;
	       
  // return the magnitude
  return get_magnitude(diference);
}// dist (two vector3's)
	       
double projection::dist(projection *another)
  // returns the cord distance between self and another
{	       			    
  vector3 diference;		    
	       			    
  // find the diference vector	    
  diference.x = di - another->di;    
  diference.y = dj - another->dj;    
  diference.z = dk - another->dk;
	       		      
  // return the magnitude     
  return get_magnitude(diference);
}			      
			      
double projection::dist(vector3 a_)
  // returns the cord distance between self and another
{			      
  vector3 diference;	      
			      
  // find the diference vector
  diference.x = di - a_.x;    
  diference.y = dj - a_.y;    
  diference.z = dk - a_.z;    
			      
  // return the magnitude     
  return get_magnitude(diference);
}			      
			      
fpoint projection::mapGlobe(vector3 plane)
{			      
  fpoint p;		      
  p.x = 0.0;	   	      
  p.y = 0.0;	   	      
 		   	      
  return p;	   	      
}		   
   		   
void projection::i_rotate(double i_rotation)
{      	      
  double phi; 
  double leng;
  	      
  if (i_rotation != 0)
  {	      	      	
    	      	    	
  // leave the i value alone, rotate j and k
    // calculate the cartesian angle of the (j,k) vector - phi
      // tan(phi) = k / j ; therfore   	      
      phi = atan(dk/dj); 
    // calculate the vector's length
      leng = sqrt(dk*dk+dj*dj); 
    // add the rotaion angle 	
      phi += (i_rotation);       
    // recalculate i and k
      if (dj < 0)   		
        phi+=M_PI;	
      dj = cos(phi) * leng;	
      dk = sin(phi) * leng;   
  } //fi      	    	      	      
}// i_rotation  		  	 
    	  	      	
void projection::j_rotate(double j_rotation)
{ 		      		 
  double phi;	      	
  double leng;	      	
  		      	
  if (j_rotation != 0)	
  {		      	
    		    	
  // leave the j value alone, rotate i and k
    // calculate the cartesian angle of the (i,k) vector - phi
      // tan(phi) = k / i ; therfore 
      phi = atan(dk/di);
    // calculate the vector's length
      leng = sqrt(dk*dk+di*di); 
    // add the rotaion angle 	
      phi += (j_rotation);       
    // recalculate i and k
      if (di < 0)   		
        phi+=M_PI;	
      di = cos(phi) * leng;	
      dk = sin(phi) * leng;   
  } //fi	    	      	      
}// rotation (j axis)	      		 
 		    	      
       		     	       
void projection::k_rotate(double k_rotation)
{      		     	      		 
  float lat, lng;    	      		 
  double coslat;     	      		 
  //recalculate lat and lng   		 
  lat = asin(dk);    	      		 
  coslat = cos(lat); 	      		 
       		     	      		 
  // fuzz controll //	      		 
  if (fabs(fabs(di/coslat)-1) < PRECISION)
  {    		     	      		 
    if ((di * coslat) < 0)    		 
      lng = M_PI;    	      		 
    else	     	      		 
      lng = 0.0;     	      		 
  }    		     	      		 
  else 		     	      		 
  // end fuzz controll //     		 
    lng = acos(di/coslat);  // this is what gets fuzzy,
                            // -1 < (di/coslat) < 1 or BOOM!
       		     	      		 
  // correct for loss of back hemisphere: cos periodicy
  if (dj < 0) lng *= -1;      		 
       		     	      		 
  // add current rotation     		 
  lng += k_rotation; 			 
       		     	      		 
  // recalculate 3d cosign vectors	 
  di =  cos(lng)*cos(lat);    		 
  dj =  sin(lng)*cos(lat);    		 
}// rotation (angle)			 
       		    			 
       		    
       
  
 
// end - prjctn.cpp	      
 			      
 			      
 			      
 

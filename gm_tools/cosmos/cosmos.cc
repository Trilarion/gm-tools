/* cosmos.cc - To calculate cosmological data for Acifigus
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */
 
// uses cicular instead of eliptical orbits 

#include <vga.h>
#include <vgagl.h> 		
#include <stdio.h>
#include <string.h>
#include <math.h>

#define SCREEN_MODE G640x480x256
#define DIST_SCALE 1.0 	// x10^9 metres	
#define S_SIZE_SCALE 0.01 // x10^6 metres
#define P_SIZE_SCALE 0.01  	 
#define M_SIZE_SCALE 0.01
#define SCALE_INC 0.1
#define CENTRE_X 320
#define CENTRE_Y 240   
		   
//keys
#define ESC 255
#define LF_ARROW 414
#define RT_ARROW 413
#define UP_ARROW 411
#define DN_ARROW 412

struct body	   							      
{     									      
  void add(body *p);							      
  body *find(char *c);							      
  void move(int speed);
  void show(body * centre, float dynamic_scale, int x_offset, int y_offset, int show_name);
  float angletox(body *centre, float dynamic_scale);		       	      
  float angletoy(body *centre, float dynamic_scale);			      
      	      								      
  char * name;	      							      
  float diameter;      	 // metres x 10^6      	(Earth 12.7)		      
  float size_scale;							      
  int colour;         	 	 			    		      
  body *orbits;	       	 	 			    		      
  float O_duration;  // in hours      	 			    	      
  float O_distance;  // in metres x 10^9  (Earth 144 from Sol)    	      
  float O_angle; // position in orbit 					      
  body *next; // so we can link them as a list.				      
};   	      	      		      					      
			     						      
float body::angletox(body *centre, float dynamic_scale)			      
// returns the x ordinate   	 	     		    		      
{ 		     	     	 	     		    		      
  float x;	     	     	 	     		    		      
  x = cos(O_angle) * O_distance * DIST_SCALE * dynamic_scale;		      
  if ((orbits != NULL)&&(orbits != centre))    				      
    x += orbits->angletox(centre, dynamic_scale);			      
  return x;	     	     	 	       				      
}	   	     	     	 	       				      
		     	     	 	       				      
float body::angletoy(body *centre, float dynamic_scale)	     		      
// returns the y ordinate 	 	       	      			      
{			     	 	       	      			      
  float y;		     	 	       	      			      
  y = sin(O_angle) * O_distance * DIST_SCALE * dynamic_scale;		      
  if ((orbits != NULL)&&(orbits != centre))    				      
    y += orbits->angletoy(centre, dynamic_scale);			      
  return y;		     		       				      
}			     		       				      
			     		     	    			      
void body::show(body * centre, float dynamic_scale, int x_offset, int y_offset, int show_name)
{   			     	      	     	    
  float x, y;		     		     	    
  body *p;		     		     	    
    	  		     		     	    
  x = angletox(centre, dynamic_scale); y = angletoy(centre, dynamic_scale);
  x += CENTRE_X + x_offset; y += CENTRE_Y + y_offset;
  gl_circle((int) x , (int) y, (int) (diameter * size_scale * dynamic_scale), colour);
  switch (show_name)
  {
    case(0): break;
    case(1):
    {
      gl_printf((int) (x + diameter * size_scale * dynamic_scale + 8), (int) y, "%c", name[0]);
      break;
    }
    case(2):
    {
      gl_printf((int) (x + diameter * size_scale * dynamic_scale + 8), (int) y, "%s", name);
      break;
    }

  }

  if (next != NULL) next->show(centre, dynamic_scale, x_offset, y_offset, show_name);
}//show	    			      	     
				      	     
				      	     
void body::move(int speed)
{   		    	 	      	    
  if ((O_distance > 1.0) && (O_duration > 1.0))
  { 		      	      	      	    
    O_angle += (speed / (float) O_duration) * 2.0 * M_PI;	   
    if (O_angle > (2*M_PI)) O_angle -= 2*M_PI;
    if (O_angle < 0) O_angle += 2*M_PI;
  }		     	       	      	    
  if (next != NULL)  	      	      	    
    next->move(speed);    	      	      	    
}		    	      	      	    
			      	      	    
			      		    
body *body::find(char * c)    
{	      	  	      
  if (!strcmp(name,c)) 	      
    return this;	      
  else		  	      
    if (next != NULL)	      
      next->find(c);	      
    else	  	      
      return NULL;	      
}		  
		  
		  
void body::add(body *p)
{ 		      
  if (next == NULL)
    next = p;
  else
    next->add(p);
}// body::add 
	  	   
char* new_str( char* str )
// allocates a copy of str
{ 	  	   
  char *tmp;	   
  int len;	   
  	  	   
  if (str == NULL) return (char *) NULL;
  len = strlen(str) + 1;   	
  if (len == 1) return (char *) NULL;
  tmp = new char[len];
  memcpy(tmp, str, len);
}// new_str (char*)
		   
		   
GraphicsContext * init()
{
  GraphicsContext *physical_screen;
  (void) vga_init();   	       
  vga_setmode(SCREEN_MODE); 	
  gl_setcontextvga(SCREEN_MODE);
  // save a pointer to the physical screen
  physical_screen = gl_allocatecontext();
  gl_getcontext(physical_screen);	 
  
  // initialise the virtual screen we will use for page flipping 
  gl_setcontextvgavirtual(SCREEN_MODE);     	       	  
		   
  gl_setfont(8, 8, gl_font8x8);	   
  gl_setwritemode(FONT_COMPRESSED + WRITEMODE_OVERWRITE);
  gl_setfontcolors(0, vga_white());
  gl_enableclipping();
  gl_clearscreen(0); 	       	  
  fprintf(stderr, "init ok\n");	
  return physical_screen;  	      	   
}//init	      	   
  	      	   
body * setup()     
{ 		   
  body *p, *c;     	   
  // centre of solar system
  c = new body;	   
  c->name = new_str("Centre");  
  c->diameter = 0; 
  c->size_scale = 0; 
  c->colour = 0;     
  c->orbits = NULL;  
  c->O_duration = 0; 
  c->O_distance = 0; 
  c->O_angle = 0;    
  c->next = NULL;    	      
  // Halar	       	       
  p = new body;	       	       
  p->name = new_str("Halar");  
  p->diameter = 2000;
  p->size_scale = S_SIZE_SCALE;
  p->colour = 12;      	       
  p->orbits = c;   	       
  p->O_duration = 2*480*10;    
  p->O_distance = 30; 	       
  p->O_angle = 0;    	       
  p->next = NULL;   	       
  c->add(p);        	       
  // Yener     	    	       
  p = new body;	       	       
  p->name = new_str("Yener");  
  p->diameter = 1000;  	       
  p->size_scale = S_SIZE_SCALE;
  p->colour = 15;      
  p->orbits = c;    
  p->O_duration = 2*480*10;
  p->O_distance = 40; 
  p->O_angle = M_PI;    
  p->next = NULL;  
  c->add(p);   	      
  // Shalar    	     
  p = new body;	       
  p->name = new_str("Shalar");
  p->diameter = 300;  	   
  p->size_scale = P_SIZE_SCALE;
  p->colour = 47;      
  p->orbits = c;     
  p->O_duration = 480*10;
  p->O_distance = 120; 
  p->O_angle = 0;     
  p->next = NULL;     
  c->add(p);	     
  // Melar	     
  p = new body;	       
  p->name = new_str("Melar");
  p->diameter = 2;   	   
  p->size_scale = M_SIZE_SCALE;
  p->colour = 12;      
  p->orbits = c->find("Shalar");     
  p->O_duration = 10*10;    
  p->O_distance = 4; 
  p->O_angle = 0;     
  p->next = NULL;     
  c->add(p);	     
  // Sorus	     
  p = new body;	       
  p->name = new_str("Sorus");
  p->diameter = 4;     	   
  p->size_scale = M_SIZE_SCALE;
  p->colour = 25;      
  p->orbits = c->find("Shalar");     
  p->O_duration = 18*10;    
  p->O_distance = 8; 
  p->O_angle = 0;     
  p->next = NULL;     
  c->add(p);	     
  // Acifigus	      
  p = new body;	       
  p->name = new_str("Acifigus");
  p->diameter = 10;   	     
  p->size_scale = M_SIZE_SCALE;
  p->colour = 10;      
  p->orbits = c->find("Shalar");  
  p->O_duration = 24*10;
  p->O_distance = 12; 
  p->O_angle = 0;    
  p->next = NULL;    
  c->add(p);	     
  // Torad	    
  p = new body;	       
  p->name = new_str("Torad");
  p->diameter = 3;     	   
  p->size_scale = M_SIZE_SCALE;
  p->colour = 25;      
  p->orbits = c->find("Shalar");     
  p->O_duration = 49*10;    
  p->O_distance = 20; 
  p->O_angle = 0;     
  p->next = NULL;     
  c->add(p);	     
  // Fener	   
  p = new body;	       
  p->name = new_str("Fener");
  p->diameter = 6;     	   
  p->size_scale = M_SIZE_SCALE;
  p->colour = 15;      
  p->orbits = c->find("Shalar");     
  p->O_duration = 78*10;    
  p->O_distance = 24; 
  p->O_angle = 0;     	   
  p->next = NULL;     	   
  c->add(p);	     	   
  // Shifar		   
  p = new body;	       	   
  p->name = new_str("Shifar");
  p->diameter = 3;   	   
  p->size_scale = M_SIZE_SCALE;
  p->colour = 25;      	   
  p->orbits = c->find("Shalar");     
  p->O_duration = 111*10;    
  p->O_distance = 28;	   
  p->O_angle = 0;     	   
  p->next = NULL;     	   
  c->add(p);	     	   
  // Tol	     	   
  p = new body;	       
  p->name = new_str("Tol");
  p->diameter = 210;     	   
  p->size_scale = P_SIZE_SCALE;
  p->colour = 1;      
  p->orbits = c;
  p->O_duration = 792*10;    
  p->O_distance = 120; 
  p->O_angle = 0;     
  p->next = NULL;     
  c->add(p);   	     
  // Morad	     	   
  p = new body;	       
  p->name = new_str("Morad");
  p->diameter = 470;      	   
  p->size_scale = P_SIZE_SCALE;
  p->colour = 12;      
  p->orbits = c; 
  p->O_duration = 1015*10;    
  p->O_distance = 180; 
  p->O_angle = 0;     
  p->next = NULL;     
  c->add(p);   	     
  // Vain	
  p = new body;	       
  p->name = new_str("Vain");
  p->diameter = 470;     	   
  p->size_scale = P_SIZE_SCALE;
  p->colour = 25;      
  p->orbits = c;
  p->O_duration = 1675*10;    
  p->O_distance = 210; 
  p->O_angle = 0;     
  p->next = NULL;     
  c->add(p);   	     
  
  // Yoleth	     
  p = new body;	       
  p->name = new_str("Yoleth");
  p->diameter = 14;    	     
  p->size_scale = P_SIZE_SCALE;
  p->colour = 25;      
  p->orbits = c;     
  p->O_duration = 2256*10;
  p->O_distance = 300; 
  p->O_angle = 0;    
  p->next = NULL;  
  c->add(p);	   
   	       	   	      
  return c;    	   
  fprintf(stderr, "setup ok");
}//setup       	   
   	       	   
void cleanup() 
{  	       			       
  vga_setmode(TEXT);		       
}//cleanup    			       
   	      	 		       
   				       
void main()   	    		       
{  	   	    		       
  GraphicsContext *physical_screen;  // for screen paging	see vgagl
  body *centre, *view;	      		       
  int key;    	      		      
  float dynamic_scale = 1.00;
  int x_offset = 0;   
  int y_offset = 0;   
  int speed = 5;
  int show_name;
     		      		      		       	       	 
     	   	      		       		      		 
  physical_screen = init();  //initialise drivers, utilities etc.
  centre = setup(); // setup solar system	      		 
  view = centre; // start viewing from centre 	      		 
  do {	       	      	 	       		      	    	 
    //process 1 hr    	 	       		      	    	 
    centre->move(speed);			      		 
    //display  	       	       	       		      	    	 
    centre->show(centre, dynamic_scale, x_offset - view->angletox(centre, dynamic_scale), y_offset - view->angletoy(centre, dynamic_scale), show_name);
    gl_copyscreen(physical_screen); // print to screen	  
    gl_clearscreen(0); // blank the virtual screen    	  
    key = (int) vga_getkey();	     		 	  
    if (key == 27)  					  
    {		     					  
      key = 255 + (int) vga_getkey() + (int) vga_getkey(); 
      fprintf(stderr, "key pressed coded as: %d", key);	  
//      exit(0);
    }

    switch (key)
    {
      case('s'): // show name 
      case('S'):
      {	      
	show_name++;
	if (show_name > 2) show_name = 0;
	break;
      }
      
      case('-'):
      {	   
       	x_offset = (int) (x_offset * (dynamic_scale - SCALE_INC) / dynamic_scale);
       	y_offset = (int) (y_offset * (dynamic_scale - SCALE_INC) / dynamic_scale);     	  
       	dynamic_scale -= SCALE_INC;  		 	  				  
      	break;	     		     		 	  
      }	     	     		     		 	  
      case('+'):     		     		 	  
      {	   
       	x_offset = (int) (x_offset * (dynamic_scale + SCALE_INC) / dynamic_scale);
       	y_offset = (int) (y_offset * (dynamic_scale + SCALE_INC) / dynamic_scale);     	  
      	dynamic_scale += SCALE_INC;  		 	  
      	break;	     		     		 	  
      }	   	     		     		 	  
      case(LF_ARROW):		     		 	  
      {	   	     		     		 	  
      	x_offset += 1; break;
      }	   	     	     
      case(RT_ARROW):
      {	   	  
      	x_offset -= 1; break;
      }	   	  
      case(UP_ARROW):
      {	   	  
      	y_offset += 1; break;
      }	   	  
      case(DN_ARROW):
      {	   	  
       	y_offset -= 1; break;
      }	   	  
      case(' '):  
      {	   	  
      	speed = 0; break;
      }	   	  
      case(','): 
      {	    
       	speed -= 1;  break;
      }	   		   
      case('.'):	   
      {	   		   
      	speed += 1;  break;  	   
      }	        
      case('n'):   
      case('N'):   
      {		   
	if (view->next != NULL)
	  view = view->next;
	else	   	 
	  view = centre; 
      	break; 	   	 
      }	       	   	 
      case('p'):   	 
      case('P'):   	 
      {	       	   	 
	break; 	   
      }	       	   
      	     	     		     		 	  
    }//switch 	     		     		 	  
       	       	     		     		 
  } while (key != ESC);	       	       	       	 
  cleanup();   			     
  fprintf(stderr, "\nGood Bye...\n");
    	      		 			 
}//main	      		 
    	      
  

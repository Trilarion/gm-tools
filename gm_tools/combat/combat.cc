/* combat.cc - combat manager program for gm tools projects
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */


/*
 *
 * This program is intended to be both the development bed for combat rutines for the gm tools projects and
 *  a tool for managing combat as the gm in a game.
 *
 */

/*
 * Intended Features / Design
 *
 * Load and Save characters
 * Track segments / Phases / Turns or whatever (Hero and Frame), with Undo / Forward / Backward / 
 * Scripting of events, allow script to be eddited, rewind, play back, save, load, etc
 * Track DCV / OCV / ToHit scores 
 * Calculate damage / Armour etc.
 * Track wounds and healing.
 * Make rolls or allow rolls or results to be entered at any point - Unlimited GM override
 * Track endurance, stun, mana, fatigue, stress, body, actions, unconcousness, bleeding, wounding, etc
 *
 *
 * Layout
 * - menu
 * - log / script
 * - character list
 *
 */

/*
 * Specification
 * Use two layers and a scripting interface between them. The application layer presents results / information to the user and provides a variety of 
 * input options - menu / direct scripting / load / save / etc. The application layer produces and stores a script which it plays to the management layer.
 * The management layer processes the script to determine results. The management layer assumes nothing and rolls nothing. It is just a combat calculator.
 * 
 *
 *
 *
 *
 */

//#include <stdio.h>    			       
#include <sys/time.h>	 // get_time_of_day()      	  
#include <time.h>      	 // time()	       
//#include <stdlib.h>   		 
#include <assert.h>   	// assert		       
//#include <string.h>   			       
//#include <errno.h>    			       
//#include <math.h>  


#include "menu.h"
#include "adv_character.h"
#include "status_line.h"
#include "keys.h"
#include "dandefs.h"
#include "sat.h"
#include "adv_cmd.h"
#include "adv_clock.h"
#include "event.h"
#include "prompt.h"
#include "err_log.h"
#include "danio.h"

using namespace std;

/*
 *
 *  Global Constants
 *
 */

const int MAX_CHARACTERS = 1000;

// stuff for the game prompt		       
// x ordinates for menu questions	       
#define xoMAIN_MENU 23	 		       
#define xoPOTION 46   	    		       
#define xoSPEND_XP 43	    		       
#define xoCHANGE_NAME 19    		       
#define xoSHIELD 50    	    		       
#define xoBUY 34 	    		       
#define xoARMOUR 50	    		       
#define xoSWORD 45	    		       

// menu states - actually a misnomer for prompt states
#define msMAIN_MENU    0  		       
#define msSPEND_XP     1   		       
#define msCHANGE_NAME  2		       


/*
 *
 *  Global Variables (Used sparingly)
 *
 */


adv_character* character_list[MAX_CHARACTERS];
int next_character;


// ********************** INPUT HANDLING **************************
      
void take_input_control(char **question, const char *new_question, 
       	       	       	char **old_question, int& menu_state, 
       	       	       	const int new_menu_state, int& old_menu_state,
       	       	       	prompt *main_prompt, const int new_x, int *old_x)
// do a handover for functions that take control of input
// This function is used both to take control and to relinquish control
// To take control:		      	   	   
//   PRECONDITIONS:			   	   
//                        * QUESTIONS *	   	   
//     question is a pointer to the string which holds the prompt question, 
//       from the previous prompt      	   	   
//     new_question is the new question to be possed, posibly a constant
//     old_question is a static variable of the calling function which
//       points to a string (the string may be NULL, but
//       old_question must not be NULL)	   	   
//             	       	   * MENU STATES *     	   
//     menu_state points to menu::state member of main_menu, which holds the 
//       menu state from the previous input controler (usually msMAIN_MENU)
//     new_menu_state holds the state identifier for the calling function
//     old_menu_state points to a static variable of the calling function to
//       store the old value of main_menu::state   
//     	       	       	   * PROMPT/X *  	       	   
//     main_prompt is the game prompt, with prompt::col appropriate to 
//       previous question	    
//     new_x is an x-ordinate suitable for the input window with the 
//       new_question   		   
//     old_x points to a static variable of the calling function to hold the 
//       old value of main_prompt::col   	       	   
//   POSTCONDITIONS:	       	      	       	 
//                        * QUESTIONS *	   
//     question will point to a copy of new_question
//     new_question will be unmodified	       		
//     old_question will point to a string containing a copy of the previous
//       question	       	      	       
//             	       	   * MENU STATES *
//     menu_state points to menu::state member of main_menu, which now contains
//       the value from new_menu_state, to identify the calling function as the
//       input controller      	     
//     new_menu_state is unchanged   	  
//     old_menu_state points to a copy of the value previously pointed to by 
//       menu_state		     
//             	       	  * PROMPT/X * 
//     main_prompt::col is changed to suit new question
//     new_x is unchanged
//     old_x points to a copy of the previous value of main_prompt::col
{   	     	 	       	      	       	       	       	   
  // change the question       	     	       
  assert(old_question);	       	     	       
  if (*old_question) delete *old_question;
  (*old_question) = new_str(*question);
  (*question) = new_str(new_question);   
  // change the menu state     
  old_menu_state = menu_state;
  menu_state = new_menu_state;
  // change the x ordinate of the input window
  *old_x = main_prompt->get_col();
  main_prompt->pmove(new_x, PROMPT_LINE); 
} // input control change      
	
void restore_input_control(char **question, char *old_question, 
		       	   int& menu_state, int old_menu_state, 
       	       	       	   prompt *main_prompt, int old_x)
// do a handover for functions that are done with control of input
{ 		  	   			    
  if (*question) delete *question;	     // out with the old 
  *question = new_str(old_question);	     // in with the older
  menu_state = old_menu_state;
  main_prompt->pmove(old_x, PROMPT_LINE);
}// restore input control      		 
  		 	       
void cleanup_prompt(prompt *main_prompt, char *question)
      // clear any information remaining from previous entries
{ 			       
  setpcolour(PROMPT_COLOUR);		// make sure the colour is right
  dcur_move(1, PROMPT_LINE);
  dwrite(BLANK_LINE);  	       	       	// clear garbage  
  main_prompt->setpstr(NULL);           // get rid of the old input 
  main_prompt->redraw();                // redraw the input window
  dcur_move(1, PROMPT_LINE);  	
  dwrite(question);                     // redraw the question
} // cleanup prompt	   			



void handle_user_selection(int ans, adv_display *display, menu_el *main_menu,
     	    	      	   char **question, bool& play, prompt *main_prompt, 
			   int& prompt_state)

  //void handle_user_selection(int ans, int& play, int& prompt_state)
{ 
  log_event("handle_user_selection()\n");
  log_event("handle_user_selection - prompt_state: %d\n", prompt_state);
  log_event("handle_user_selection - ans: %d\n", ans);
	 				 			      
  switch (prompt_state)    	     	       	    	    
  {	    	      	       	     	       	    	    
    case msMAIN_MENU: 	   	     	       	    	    
    {	    	      	   	     	       	    	    
       	    	      	     	     	 	    	    	    
      switch (ans)     	    // carry out actions    	    
      {                        	      	       	    	    	    
        case advcmdNO_COMMAND: break;      // impossible action selected   
        case advcmdQUIT: play = FALSE; break;         // end the game	    

	case advcmdSAVE_GAME:		 	    
	{ 				 	    
       	  say_status("Save Battle");
	  break;	     		 	    		 
	} 		     		 	    		 
	case advcmdLOAD_GAME:		 	    		 
	{ 	   	     		 	    		 
          say_status("Load Battle");
	  break;				    
	} 					    
        case advcmdSHOW_CHAR: 			    
	{ 					    
	  character_list[0]->show();		    
	  display->detail_state = detCHAR;	    
	  break; // show the character		    
	}
	  	   
	case advcmdPGUP:	  		    
	{   	   				    
	  if (display->detail_state == detCHAR)
	  { 	   
       	    character_list[0]->change_page(-1);
	    character_list[0]->show();
	  }
	  break;    	   		   
	}   	    	   		   
       	case advcmdPGDN:	  	   
	{   	    	    	  	   
	  if (display->detail_state == detCHAR)
	  { 	    	   
       	    character_list[0]->change_page(+1);
	    character_list[0]->show();
       	  } 	   
	  break;    	    		 
	} 	    	   		 
       	case advcmdCURSOR_UP:	 
 	{ 	    	   		 
 	  if (display->detail_state == detCHAR) // could replace this with a switch too
	  {	    	  
       	    character_list[0]->cursor_up();
	    character_list[0]->show();
	  }   	  
	  break;   	    		 
	} 	   	   
       	case advcmdCURSOR_DOWN:  	 
 	{ 	   	    		 
 	  if (display->detail_state == detCHAR)
	  {	   
	    	   
       	    character_list[0]->cursor_down();
	    character_list[0]->show();
       	  } 	   
	  break;   	    		  
	}	   			  
		   			  
	  	   			  
        case advcmdDRINK_POTION: // drink potion	
  	{      	      	      		     				  
          character_list[0]->drink_potion();
  	  if (display->detail_state == detCHAR) character_list[0]->show();
  	  break;      	      		     	     	       		  
  	}      	      	      			     	

        case advcmdREST: // rest	    
  	{	       	 	   	                  
  	  character_list[0]->rest();      	   	     
  	  if (display->detail_state == detCHAR) character_list[0]->show();
  	  break;       	 	      	        
  	}	       	 		                    

	case advcmdSAVE_CHAR:	   	          
  	case danioKEY_F2:  	 // save      	       	     
  	  character_list[0]->adv_save(); break;
  	case advcmdLOAD_CHAR:	   	        
  	case danioKEY_F3: // load	   	        
  	{	   	   	   	                    
  	  character_list[next_character]->adv_load(); // load the next character
          next_character++;                           // increment the end of list index
	  //  	  if (display->detail_state == detCHAR) character_list[0]->show();
       	  break;     		   	          
  	}	                                
  	case advcmdTOGGLE_DISPLAY:        
  	{	                                
  	  if (display->is_combined())     
  	    display->expand();            
  	  else	                          
  	    display->combine();           
  	  if (display->detail_state == detMENU) main_menu->show();   
  	  break;     						          
  	}	     						                
  	case advcmdCOMBINE_DISPLAY: // combine display		     
  	{	     		   	  			            
       	  if (!display->is_combined()) display->combine();	     
  	  if (display->detail_state == detMENU) main_menu->show();   
      	  break;     		   	  			     
  	}	     		   	  			            
      	case advcmdEXPAND_DISPLAY: // expand display	 	     
        { 	     		   	  			      
          if (display->is_combined()) display->expand(); 	     
  	  if (display->detail_state == detMENU) main_menu->show();   
  	  break;     		   	  			      
  	}	     		   	  			            
       	case advcmdSHOW_MENU: // show menu			     
  	{	     						                
	  log_event("handle user selection - show menu\n");	     
  	  main_menu->show();					     
  	  display->detail_state = detMENU;			     
  	  break;     						          
  	}	     						                
	                                    
	case UP_MENU_TREE: break; // return from a sub menu 	     
        default : bad_input();       	       	 	 	     

      }//switch: ans 		   	       		 	     
      break;	     		   	       		 	     
    }// case : main menu	   	       			     

    default: prompt_state = msMAIN_MENU; break;			     
  }// switch : prompt_state	   	   		  	     	   
} // user input			   	    			     	   


			   	   			     	   
void user_input(char **question, prompt *main_prompt, 	       	   
     	     	adv_display *display, menu_el *main_menu, int& prompt_state, 
		bool& play)
// Track and handle user input.	    		    		       	   
// Keeps track of the prompt and looks for input in the form of a terminated
// prompt (cmd == CMD_QUIT) or a hot-key.	    		       	   
// Input is then passed to an interpreter which converts raw input to a
// mnemonic command. Commands are then compared to the context to dissalow 
// improper commands (eg go up when there is no way up). Finally commands are
// passed to the handler. 	    	 	    		       	   
{      	     	       	       	     	 	    		       	   
  int usr_cmd;    // a user selection 	       	       	       	       	   
  int prompt_cmd; // an internal control command from the prompt, used to  
                  // detect prompt termination, usually by pressing <Enter>
       	       	       	       	    	 	    		       	   
  prompt_cmd = main_prompt->get_ed_cmd(); // look for input    	       	   
  if (prompt_cmd != NO_COMMAND) // there was a keystroke	   	   
  {   	       	       	       	    	 	 	      		   
    log_event("Prompt command: %d\n", prompt_cmd); 			   
    log_event("Prompt key: %d\n", main_prompt->key); 			   
    main_prompt->do_ed_cmd();  	// make the prompt work			   
    
    clear_status_line();     	                // remove old notices	   
    	       		      	   					   
    usr_cmd = NO_COMMAND; // no command found yet  			   
    if (prompt_state == msMAIN_MENU) // check that the menu has authority 
                                     // to use the prompt
    {	       		      	   				   	   
      if (prompt_cmd == CMD_QUIT) // non hot key entries	   	   
        usr_cmd = main_menu->interpret_std_key((int) main_prompt->s[0]);
      // catch hot keys; but don't want to overwrite normal entries
  	// Note: hot keys may include prompt quit keys        
     	//   (i.e. those that generate CMD_QUIT and cause a call to the above 
  	//   interpret) thus the if	   			     
    	if (usr_cmd == NO_COMMAND)  		      
        usr_cmd = main_menu->interpret_hot_key(main_prompt->key);
	//      usr_cmd = filter_cmd(usr_cmd, location_list[0], dungeon_level);	   // filter not required as there is no context ??? FIXME
    }// fi   		      	   
    if (usr_cmd != NO_COMMAND)
    {
      handle_user_selection(usr_cmd, display, main_menu, question, 		   
     	       	       	    play, main_prompt, prompt_state);
      //       	description(display); 
      cleanup_prompt(main_prompt, *question);
    }	// fi: proccess cmd
  } // input made      	      	     		       		 
} // user input 	       	      	     		    
    	       		      	    
// ***************** END INPUT HANLING




/*
 *
 *  INIT, SETUP, CLOZE
 *
 */

void init(adv_display **display, adv_clock **time_keeper)
{
  start_log();		      		  	       
  int err;
  log_event("adv_game.cc:init\n");	      
  //initialise random number generator	      	 
  randomize();       	      		      	 
  log_event("adv_game.cc:init - randomize ok\n");
  // initialise danio
  err = init_danio();
  if (err)      	      	      	 	 
    log_event("adv_game.cc:init - danio err: %d\n", err);
  else	       	     	      		 	 
    log_event("adv_game.cc:init - danio ok\n");
  dset_scroll(OFF);			  
  dcur(CURSOR_INVIS); // cursor invisible
  // initilise adv_display.h					 
  *display = new adv_display();
  log_event("adv_game.cc:init - display ok\n");
  // initilises the game clock	
  *time_keeper = new adv_clock();
  log_event("adv_game.cc:init - time keeper ok\n");
  log_event("adv_game.cc:init - end\n");

}//init


void setup(adv_display *display,
       	   menu_el **main_menu, prompt ** main_prompt, char ** question,
	   event **event_head )
{
  event *new_event;				       
  // initialise the event list with dummy head and tail nodes
  *event_head = new event(NULL, NULL, 0, 0, 0);  // create head
  new_event = new event(NULL, NULL, 0x7FFFFFFF, 99, 0);	// create tail       
  (*event_head)->next = new_event; // link head to tail
  						       
  // initialise the character list	  	       
  for (int i = 0; i < MAX_CHARACTERS; i++)
    character_list[i] = NULL; 	
  log_event("setup - character list nulled\n");
  // make player      	      	      	       	 
  character_list[0] = new adv_character(display);   
  character_list[0]->new_player(); 	       
  log_event("setup - player instatiated\n");
  // the menu	     	      	   	    
  *main_menu = new menu_el("   ~Main Menu~",   	     
       	      new menu_el("~F1~ Show menu", 0, danioKEY_F1, advcmdSHOW_MENU,
       	      new menu_el("~F2~ Save Battle", 0, danioKEY_F2, advcmdSAVE_GAME, 
       	      new menu_el("~F3~ Load Battle", 0, danioKEY_F3, advcmdLOAD_GAME,   
	      new menu_el("~F4~ Toggle display mode", 	     
		     	  0, danioKEY_F4, advcmdTOGGLE_DISPLAY,
       	      new menu_el("~F5~ Show Character", 0, danioKEY_F5, advcmdSHOW_CHAR,
       	      new menu_el("~F6~ Drink ~P~otion", 'p', danioKEY_F6, advcmdDRINK_POTION,
       	      new menu_el("~PgUp/PgDn~ Char Page",     
       	      new menu_el("", 0, danioKEY_PGDN, advcmdPGDN, 
	      new menu_el("", 0, danioKEY_PGUP, advcmdPGUP, 	       	  
       	      new menu_el("", 0, danioKEY_UP_ARROW, advcmdCURSOR_UP,
       	      new menu_el("", 0, danioKEY_DOWN_ARROW, advcmdCURSOR_DOWN,
       	      new menu_el("~R~est", 'r', advcmdREST, 
       	      new menu_el("", 0, danioKEY_TAB, advcmdTAB, // just a pure hot key, not a sub menu
       	      new menu_el("~I~mport Character", 'i', 0, advcmdLOAD_CHAR,
       	      new menu_el("Expor~t~ Character", 't', 0, advcmdSAVE_CHAR,
       	      new menu_el("~Q~uit", 'q', danioKEY_ESC, advcmdQUIT,		  
       	      new menu_el("", 0, danioKEY_ENTER, advcmdENTER, 	       	  
       	      NULL ))))) ))))) ))))) )));
  log_event("setup - menu instantiated\n");
  (*main_menu)->set_win(display->detail_win);
  (*main_menu)->show();        	       	    
  log_event("show menu\n");   	   	    
  setpcolour(MENU_COLOUR); 	   	    
  dcur_move(1, MENU_LINE); 	   		    
  dwrite("F1 for Menu.                                                                    \n");
  //   	  12345678901234567890123456789012345678901234567890123456789012345678901234567890
  drefresh();	     			    
  		     			    
  // create the prompt	      		    
  *main_prompt = new prompt(ADV_PROMPT_LEN, NULL, 
  		     	   PROMPT_COLOUR, NO_AUTHORITY);
  (*question) = new_str("Enter your selection: ");
  setpcolour(PROMPT_COLOUR);  		  
  dcur_move(1, PROMPT_LINE);
  dwrite(*question);	  
  (*main_prompt)->pmove(xoMAIN_MENU, PROMPT_LINE); 	 // NB:  x, y order
  (*main_prompt)->redraw();   		  
}//setup

void intro()	      	      		  		  		 
{  		     	      		  
  say_status("GM Tools: Combat Controller.");
} // intro	     	      		  


void cloze()
{

  close_danio();
  end_log();    	       	      	      	   	    

}//cloze

int main()
{
  adv_display *display;	          // tracks the window regions of the display
  menu_el *main_menu = NULL;      // the main menu	   
  adv_clock *time_keeper = NULL;  // the game clock
  event *event_head = NULL;       // event/action tracking
  bool play = true;               // main loop flag

  // prompt stuff	      		  	       
  prompt *main_prompt; 	       // the main prompt      
  int prompt_state = msMAIN_MENU; // allows the prompt to be comandeared
  char *question;      	       // the posed question   
  void *caller;	       	       // the function waiting for input


  init(&display, &time_keeper);  // initialise services
  setup(display,                 // set up the program ellements
       	&main_menu, &main_prompt, 
	&question, &event_head);
  intro();                       // display an introduction

  do  // play	       	       	      	       	    	   	    
  {	    	       	       	      	  	    
//    log_event("begin main loop\n");     	    
//    wait_sec(1);             		      	    
//    log_event("before time update\n");  	    
    time_keeper->update();     	       	       	  // track time
//    log_event("time\n");     	      		    
//    wait_sec(1);  	       			    
    user_input(&question, main_prompt,
       	       display, main_menu, prompt_state, play);
//    log_event("input\n");    	      	       	    
//    wait_sec(1);  	       	     	       	    
  } while (play); //do main menu loop 	    	   


  cloze();
  return 0;
}//main

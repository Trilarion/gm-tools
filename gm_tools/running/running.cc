/* running.cpp - test bed for functions dealing with character's running and travelling
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

#include <stdio.h>
#include <math.h>
    
#define END_MAX_RUN 0.1 // endurance factor remaining
#define MAX_ENDURANCE 100 // maximum normal Endurance value
#define MAX_BASE_MOVE 30 // maximum base movement
#define DEF_END 40 // default value of Enndurance
#define DEF_BASE_MOVE 20 // default value for base movement 
#define A 2    	       
#define B 60   
#define C 0.67  
	       
struct run     	       	       	      
{ 	       			      
  float s; float t; float f; float v; 
};	       
	       
float get_max_v(int base_move, int non_combat_multiple = 2);
struct run best_run(int s, int e, int bm, int n); 
float get_top_v(int endurance, float fatigue = 0);
float calc_fatigue(float v, int t = 6);	
void do_max_run(); 
       	       
struct run do_run(float v, int e)			  
// perform a run at given velocity v, and enndurance e 
// until v cannot be maintained
{ 
//  printf("\ndo_run(v = %.2f, e = %d)", v, e);
  struct run r;	   
  r.s = 0; r.t = 0; r.v = v;
  r.f = e - (r.v * C); // total endurance, less that need to mainntain v
//  printf("\nbefore run: s = %.2f, t = %.2f, v = %.2f, f = %.2f", r.s, r.t, r.v, r.f);
  float fatigue_rate = calc_fatigue(r.v);
  while (r.f > 0)    		   
  {    	       	     		   
    r.s += r.v * 6; r.t += 6; // move for 1 segment
    r.f -= fatigue_rate; // tire 	   
  }//while     	   		 
  r.t -= 6; r.s -= v * 6; r.f += calc_fatigue(r.v);// go to just before using all the fatigue 
  r.f =	e - r.f + r.v * C; // want to return the fatigue used.
  return r;
}// do run 
  	
  		    
struct run best_run(int s, int e, int bm, int n)
// find the best time a character can do for the distance s,
// with a given endurance e, base move bm, 
// to n decimal places 	    		  	     
// uses a half split search 
{    	       	    	    		  	     
  float v_high, v_low, f, dv; // velocity, fatigue, change in velocity per itteration
  struct run r; // for attempted runs
  v_low = 0.0; // low boundry for v
  v_high = (float) bm * 2.0 / 6.0;
  float precision = 1.0;       	      		   
  for (int i = 0; i < n; i++) // 0.1 ^ n 
    precision *= 0.1;  	    	   
  do   	       	       	     	      	   	      
  {    	     	       	    	   	   
    r.v = (v_high + v_low) / 2.0;  	   
    // do a run at r.v until r.v cannot be maintained. 
    r = do_run(r.v, e);     	   	   
    // record new boundry on v	       	 
    if (r.s < (float) s)       	    		   
    {		       	    		   
      v_high = r.v; // didn't make the distance
    } 	      	       	    	      	   
    else       	       	    	      	   
    { 	      	       	    	      	   
      v_low = r.v; // still got more to give
    }         

//    printf("\n%.6f %.6f %.6f", v_low, r.v, v_high);   
//    printf("\ndv = %f", dv);		      
//    printf("\nran %.0f meters, at %.0f m/s in %.0f seconds", r.s, r.v, r.t);
//    printf("\ndv = %.0f", dv);	      
  dv = v_high - v_low;
  } while ((precision < dv) && (v_low < 9.99)); 		   
  return r;   	    	    	 	   
} // best run  	    	    		  
    	       	    
  	       	    
float get_max_v(int base_move, int non_combat_multiple = 2)	
// find a characters maximum possible speed (m/s) given his base move
// = base_move *non combat multiple / 6 seconds per segment 
{    	       	       		   	    	  
  return base_move * non_combat_multiple / 6;	  
}// get max v  	       	       	       	       	  
     	       	 	
float get_top_v(int endurance, float fatigue = 0)
// find a characters maximum speed (m/s) based on the endurance cost 
// (movement costs Endurance. A character can only move as fast as he can 
//  pay for)   	 
// = (endurance - fatigue) / 10 segments per turn * C (the cost factor)
{ 	      	 
  return (endurance - fatigue) / 10 * C;
}// get top v 	 			
       		 
float calc_fatigue(float v, int t = 6)  
// calculate fatigue given velocity and time 
// = t / 6 * B^[log2(vC)-A]	     
{ 	     	   		     
  return (t / 6 * exp((log(v * C) / log(2) - A) * log( B ))); 
}// calc fatigue 		     
  		       
void do_max_run()
{ 		 
  float f, v, s, t, top_v; 	     	 	  
  float max_v = get_max_v(MAX_BASE_MOVE);		  
  // loop through a maximal run	       		  
  f = v = s = t = 0;   	      	       		  
  while (f < ((1 - END_MAX_RUN) * MAX_ENDURANCE))	
  {    	      	       	      	     		  
    top_v = get_top_v(MAX_ENDURANCE, f); 
    v = (top_v > max_v) ? max_v : top_v; // take the smaller  
    s += v * 6; t += 6; // move for six seconds		  
    f += calc_fatigue(v);
//    printf("Time: %.0f Distance %.0f Fatigue %.0f Velocity: %.1f Mean Velocity: %.2f\n", t, s, f, v, (s/t));
  }//while 	      			     	
}// do max run       
  		     
void show_best_run(int s, int e, int bm, int p)   	    
{ 			    		      
  struct run r;		    		      
  r = best_run(s, e, bm, p);		      
  printf("\n%.0fm in %.2f sec, at %.2f m/s", r.s, (r.s / r.v), r.v);
}

  		     
int main()	     
{ 		     
  struct run r;	     
  printf("                      Running Modler \n");
  show_best_run(100, MAX_ENDURANCE, MAX_BASE_MOVE, 4);
  show_best_run(200, MAX_ENDURANCE, MAX_BASE_MOVE, 4);
  show_best_run(800, MAX_ENDURANCE, MAX_BASE_MOVE, 6);
  show_best_run(5000, MAX_ENDURANCE, MAX_BASE_MOVE, 4);
  show_best_run(10000, MAX_ENDURANCE, MAX_BASE_MOVE, 4);
  show_best_run(21101, MAX_ENDURANCE, MAX_BASE_MOVE, 4);
  show_best_run(29090, MAX_ENDURANCE, MAX_BASE_MOVE, 4);
  show_best_run(30000, MAX_ENDURANCE, MAX_BASE_MOVE, 4);
  printf("\n");
  return 1;   		 	     	
}   	      
  	    
  	    

/* stat.cc - Statistics definitions for auto-char
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

   		  
#include <string.h>
#include <assert.h>
#include <stdio.h>
   		  
#include "stat.h" 
#include "sat.h"  
   				   
/* 				   
void statistic::figured_base(int (*newbase)(statistic * a, statistic * b, statistic * c),
                             statistic *a,statistic *b, statistic * c)
// stores a pointer to a function for calculating the base
{  				   
  get_base = newbase;		   
  base_a = a;			   
  base_b = b;			   
  base_c = c;			   
}//figured base			   
   				   
*/ 				   
statistic::statistic(ability * is_after, char* new_name, int new_pts, int new_cost,
                     int new_max) :
  ability(is_after, new_name, new_pts)
{  				   
  cost = new_cost;		   
  maximum = new_max;		   
}  				   
   
statistic::~statistic()
{  
}  
   
int statistic::get_base()
// default for stats
{  	       	       	       	       	       	  
  return 10;					  
}	      					  
 	 	 				  
int statistic::value_to_pts(int val)
// redefined to take care of maximums
{ 	       	 				  		     
  int temp;    	 	    			  		     
	       	 	    			  		     
  if (val <= maximum)				  		     
    return (val - get_base()) * cost;				     
  else	 	 						     
    return (maximum - get_base()) * cost + (val - maximum) * 2 * cost; 
} // value to pts
      	      		      	 
void statistic::set_value(int new_val)
// needs to be redefined here so that the redefined value_to_pts is used  
{     	     		      
  pts = value_to_pts(new_val);
}     			      
 	       
int statistic::pts_to_value()
// convert point to a value given points spent, cost, base and maximum
// uses mathimatical rounding, inapropriate for speed and primary stats, which
// must be paid for in full.
{	       		    
  int temp;    		    
	       		    
  assert(cost > 0);	    
  temp = (int) ((float) (pts + get_base() * cost) / (float) cost);  
  	       		    
  if (temp > maximum)	    
  {	       		    
    temp = (pts - ((maximum - get_base())*cost));
    temp = (int) ((float) temp / (float) (cost*2));
    temp += maximum;
  }	       
  return temp; 
} // pts to value	       
    	       
void statistic::show(int x, int y, dwindow *win, int options)
{   		 	 
  char *temp_name;
    
  dcur_move(x, y, win);	     
  dwrite(win, "%3d", pts_to_value());
  if (win != NULL)
  { 
    temp_name = new_str(3);
    strncpy(temp_name, name, 3);
    str_upp(temp_name);
    dwrite(win, " %-5s", temp_name);
    dwrite(win, "%4d", get_base()); 
    dwrite(win, "%5d", cost); 
    dwrite(win, "%4d", maximum);
    dwrite(win, "%4d\n", pts); 	     
  } else { 	   
    dwrite(win, " %-14s", name);
    dwrite(win, "%6d", get_base()); 
    dwrite(win, "%6d", cost); 
    dwrite(win, "%5d", maximum);
    dwrite(win, "%5d\n", pts); 	     
  } 		   
}   	      	      	  			     
    	      	      	  
void statistic::print(FILE *stdprn)
{	      	      	  
  fprintf(stdprn, "\r\n");
  fprintf(stdprn, "%3d", pts_to_value());
  fprintf(stdprn, " %-14s", name);
  fprintf(stdprn, "%6d", get_base());
  fprintf(stdprn, "%6d", cost);
  fprintf(stdprn, "%5d", maximum);
			  
  fprintf(stdprn, "%5d", pts);
}			  
///////////////////// miscellanious functions
			  
// end - stat.cc
   

/* perk.cc -- perks for character
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

		   
#include <stdlib.h>
#include <stdio.h> 		      
#include <string.h>
		   		      
#include "disadv.h"		      
#include "perk.h"  
#include "common.h"
#include "prompt.h"
#include "sat.h"
#include "err_log.h"

perk::perk(perk* is_after, char * new_name, int new_pts) :
  disadvantage(is_after, new_name, new_pts)
{
  debuf(&name);
}

int perk::load(char * data)
{
  perk * new_perk;
  char * new_name;
  int new_pts;
  int new_cost; // place holder only

  sscanf(data, "%d%d", &new_pts, &new_cost);
  strtok(data, "\"");                      // move to begining of name
  new_name = new_str(strtok(NULL, "\""));  // read in name

  delete data;	       
		       
  if (this->prev == NULL)
  // this is the dummy head node
    new_perk = new perk(this, new_name, new_pts);
  else		       
    new_perk = new perk((perk*)prev, new_name, new_pts);
		       
  delete new_name; // is reallocated for in constructor
  log_event("perk loaded: %d %s %d %d \n",     
   	  new_perk->pts_to_value(), new_perk->name, 
   	  new_perk->cost, new_perk->pts);
  return 0;
}//load

perk::~perk()
{     
  if (prev != NULL) prev->next = next;
  if (next != NULL) next->prev = prev;
  if (name != NULL)
  {
    delete name;
    name = NULL;
  }

}     

// virtual redefinitions
void perk::show(int x, int y, dwindow *win, int options)
{	       
  if (name == NULL)
  {	       
    dcur_move(x, y, win);
    dwrite(win, "---End of Perks---");
  } else {		   
    dcur_move(x, y, win);	   
    dwrite(win, "%-20s  %d", name, pts);
  }	 		   
}//show	    		   
	    	
void perk::print(FILE *stdprn)
{	    	
  if (name == NULL)    
    fprintf(stdprn, "\r\n--End of Perks---");
  else	    	       
    fprintf(stdprn, "\r\n%-20s  %d", name, pts);
}// print   	       
	    	       
int perk::make_new(ability* based_on)
// returns non-zero on failure, eg user cancel
{	    	       
  char *new_name;      
  int new_value;       
  prompt *sd;         
  perk *new_perk;      
      	    	       
// get a name	       
  new_name = new_str(30);
  say_prompt("Enter a description of the perk: ");
  sd = new prompt();  	   
  new_name = new_str(sd->s);
  delete sd; 	       	    
  if (is_empty(new_name)) return 1;
    	     	       	   
// get value  
  clear_status_line();
  say_prompt("Enter the cost of the perk: ");
  sd = new prompt();
  if (is_empty(sd->s)) return 1;
  sscanf(sd->s, "%d", &new_value);
  delete sd;	  	  
		       	  
// record new perk     	  
  if (this->prev == NULL) 
  // first perk in list
    new_perk= new perk(this, new_name, new_value);
  else		       
    new_perk= new perk((perk*)this->prev, new_name, new_value);
		 
  return 0;	 
}//make new	 
		 
// end - perk.cc
		 
		 

// character.h - character object for auto-char
// Daniel Vale APR 2001

#ifndef CHARACTER_H
#define CHARACTER_H 	 

#include <stdio.h>

#include "stat.h"
#include "w_rank.h"
#include "skill.h"
#include "disadv.h"
#include "perk.h"
#include "pow.h"
#include "gear.h"

#define FILE_FIELD 20 // size of the input field for the file name
#define NUM_STATS 18 // number of character statistics

#define MALE 0x0B        //
#define FEMALE 0x0C      //
#define ANDROGENOUS 0x3F // ?

class character
{

private:

protected:
  int page;
  int stat_top; // cursor y position of displayed strength
  int stat_btm;
  int cury;
  char * file_name;

  ability * current;
  ability * field_default(int page);
    // returns a default field for the page to be activated when
    // changing page.

// stats etc. preceeded with ch_ (characteristic)

// Bio
  char *ch_name;
  char *ch_living;
  char *ch_race;
	
  int ch_age;
  char *ch_sex;

  int ch_base; // base points
  int ch_max_disadvantages; // maximum points from disadvantages
  int ch_experience;

// points in stats
  statistic *ch_stats;   // dummy head node

  statistic *ch_str;
  statistic *ch_dex;
  statistic *ch_con;	       	     
  statistic *ch_bod;		     
  statistic *ch_int;	
  statistic *ch_tech; // Frame only 
  statistic *ch_dis;  // discipline == Ego / Will
  statistic *ch_pre;		     	
  statistic *ch_com;  // or Apperance 	       	       	
		    		     
  statistic *ch_pd;    	       	     
  statistic *ch_ed;    	 	     
  statistic *ch_md;    // Frame only 
  statistic *ch_spd;	 
  statistic *ch_rec;   // Hero only	 
  statistic *ch_end;	       	   
  statistic *ch_stn;	   	   
  statistic *ch_mrc;   // Hero only	   
  statistic *ch_man;		   
  statistic *ch_run;		   
  statistic *ch_swm;		   
				   
//  statistic *ch_stats[NUM_STATS]; // array of pointers to the stats for traversal
				   
  weapon_rank * combat;// ptr to dummy head of doubly linked list;
  weapon_rank * c_tail;// ptr to dummy tail node.

  skill * skill_head;
  skill * skill_tail;

  disadvantage * disadvantage_head;
  disadvantage * disadvantage_tail;

  perk * perk_head;
  perk * perk_tail;

  gear * gear_head;
  gear * gear_tail;

  power * power_head;
  power * power_tail;

//  equiptment * equiptment_head;
//  equiptment * equiptment_tail;


  int sum_stats();  // returns total cost of stats
  int sum_combat(); // returns total cost of combat
  int sum_skills(); // returns total cost of skills
  int sum_disadvantages(); // returns total value of disadvantages
  int sum_perks();  // returns total cost of perks
  int sum_gear();  // returns total cost of gear
  int sum_powers();  // returns total cost of powers

  void show_title_page();
  void show_stat_page();
  void show_combat_page();
  void show_skill_page();
  void show_perk_page();
  void show_gear_page();
  void show_power_page();
  void show_disadvantage_page();

//  void setname(char * new_name);
  ability * get_base(char * data);

  void goset(int y);

  char * dialog_title(char * field);
  void edit_name();
  void edit_age();
  void edit_race();
  void edit_sex();
  void edit_living();
  void edit_base();
  void edit_xp();
  void edit_disadv();
  void bio_edit();
  
public:
  character();
  ~character();

  void del_stats();
  void del_weapon_ranks();
  void del_skills();
  void del_perks();
  void del_gear();
  void del_powers();
  void del_disadvantages();

  void show();
  void show_subtotal_stats();
  void print_subtotal_stats(FILE *stdprn);

  void print();

  void edit(int cmd);

  void comment();

  int save();
  int load();
  void print_character();

};//character (object)


#endif
// end - character.h

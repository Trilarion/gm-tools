// figured.cpp - figured stats for char(acter)

#include <stdlib.h>
		 
#include "figured.h"
#include "cost.h"
      		 
int normal_def::get_base()
{ 
#ifdef HERO				  
  // round(base / 5)			  
  int temp;				  
  temp = (int) ((float) base_stat->pts_to_value() / 5.0 + 0.5);
  return temp; 				  
#else 					  
  return 0;
#endif  
}     		 
      		 
normal_def::normal_def(ability * is_after, char* new_name, int new_pts, 
      		       ability * new_base_stat)
  : statistic(is_after, new_name, new_pts, cPD, mPD)
{     		       	   		      
  base_stat = new_base_stat;       	      
}     	   	       			      
      	   	       			      
int spd::get_base()    			      
{
#ifdef HERO
  return 1 + dex->pts_to_value() / 10;
#else 				  
  return dex->pts_to_value() + 30;
#endif  
}		       			      
		       			      
spd::spd(ability * is_after, char* new_name, int new_pts, ability * new_dex)
  : statistic(is_after, new_name, new_pts, cSPD, mSPD)
{  		       			      
  dex = new_dex;       			      
}  		       			      
   		       			      
int rec::get_base()    			      
{  		       
//round(str/5) + round(con/5)
  int temp;	       
  temp = (int) ((float) str->pts_to_value() / 5.0 + 0.5);
  temp += (int) ((float) con->pts_to_value() / 5.0 + 0.5);
  return temp;	 
}  
   
rec::rec(ability * is_after, char* new_name, int new_pts, ability * new_str, ability * new_con)
  : statistic(is_after, new_name, new_pts, cREC, mREC)
{  
  str = new_str;
  con = new_con;
}  
   
int end::get_base()
{  
#ifdef HERO
  return con->pts_to_value() * 2;
#else
  return con->pts_to_value() * 4;
#endif  
}     
      
end::end(ability * is_after, char* new_name, int new_pts, ability * new_con)
  : statistic(is_after, new_name, new_pts, cEND, mEND)
{     
  con = new_con;
}     
      
int stn::get_base()
{     
#ifdef HERO  
  //bod + round(str/2) + round(con/2)
  int temp;  		     
  temp = bod->pts_to_value();
  temp += (int) ((float) str->pts_to_value() / 2.0 + 0.5);
  temp += (int) ((float) con->pts_to_value() / 2.0 + 0.5);
  return temp; 		     
#else			     
  // bod * 2 + str + con     
  int temp;  		     
  temp = bod->pts_to_value() *2;
  temp += str->pts_to_value();
  temp += con->pts_to_value();
  return temp; 
#endif       
}     
      
stn::stn(ability * is_after, char* new_name, int new_pts, ability * new_bod, ability * new_str, ability * new_con)
  : statistic(is_after, new_name, new_pts, cSTN, mSTN)
{     
  bod = new_bod;
  str = new_str;
  con = new_con;
}     

int run::get_base()
{ 
#ifdef HERO
  return 6;
#else   
  return 20;
#endif  
}     
      
run::run(ability * is_after, char* new_name, int new_pts)
  : statistic(is_after, new_name, new_pts, cRUN, mRUN)
{     
}     
      
int swm::get_base()
{     
#ifdef HERO
  return 2;
#else  
  return 5;
#endif  
}     
      
swm::swm(ability * is_after, char* new_name, int new_pts)
  : statistic(is_after, new_name, new_pts, cSWM, mSWM)
{     
}     
      
swm::~swm() {}
run::~run() {}
stn::~stn() {}
end::~end() {}
rec::~rec() {}
spd::~spd() {}
normal_def::~normal_def () {}

// end figured.cpp

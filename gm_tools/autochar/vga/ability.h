// ABILITY.H - virtual base class for char(acter) abilities
#ifndef ABILITY_H
#define ABILITY_H

#include <stdlib.h>
#include <stdio.h>
#include <vga.h>
#include <vgagl.h>

#include "sat.h"

class ability
{
private:

protected:
  int pts; // experience point spent in ability
  int cost; // in experience points

  virtual int get_base(); // returns a constant for some derived classes

public:
  char * name;
  ability * next;
  ability * prev;

  ability(ability * after_line, char * new_name, int new_pts = 0, int new_cost = 10);
  ~ability();

  int sum();
    // sums up abilities from this one to the terminating NULL

  virtual int make_new(ability * based_on = NULL);

  virtual void change_pts(int amt);
  virtual int get_pts();

  virtual char* new_prn_pts();
  virtual char* new_prn_pts(int num);
    // dynamicly allocates char*

  virtual void change_value(int amt);
  virtual int pts_to_value();
  virtual void save(FILE * out);
  virtual void edit() {gl_write(8,8, "Edit ability            " );}
    // load must delete char * data

  // pure vertual methods
  virtual void show(int x, int y) = 0;
  virtual void print(FILE *stdprn) = 0;
  virtual int load(char * data) = 0;
};//ability

#endif
// end - ABILITY.H

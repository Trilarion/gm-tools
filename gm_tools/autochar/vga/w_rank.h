// W_RANK.H - weapon ranks for char(acter)
#ifndef W_RANK_H
#define W_RANK_H

#include <stdio.h>

#include "ability.h"

class weapon_rank : public ability
{
private:
protected:

public:
  weapon_rank(weapon_rank * is_after, char * new_name, int new_cost, int new_pts = 0);
  ~weapon_rank();

  // virtual redefinitions
//  int pts_to_value();
  void show(int x, int y);
  virtual void print(FILE *stdprn);
    // output to stdprn
  int make_new(ability* based_on = NULL);
    // returns non-zero on failure, eg user cancel

  void save(FILE*out);
  virtual int load(char* data);

};// weapon rank

#endif
#// end W_RANK.H

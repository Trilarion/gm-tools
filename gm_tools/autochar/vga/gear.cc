// gear.cpp -- gears for char(acter)


#include <stdlib.h>
#include <stdio.h>

#include "disadv.h"
#include "sat.h"
#include "string.h"
#include "gear.h"
#include "vga_prompt.h"

gear::gear(gear* is_after, char * new_name, int new_pts) :
  disadvantage(is_after, new_name, new_pts)
{
  debuf(&name);
}

gear::load(char * data)
{
  gear * new_gear;
  char * new_name;
  int new_pts;
  int new_cost; // place holder only

  sscanf(data, "%d%d", &new_pts, &new_cost);
  (void) strtok(data, "\"");
  new_name = new_str(strtok(NULL, "\""));
  delete data;

  if (this->prev == NULL)
  // this is the dummy head node
    new_gear = new gear(this, new_name, new_pts);
  else
    new_gear = new gear((gear*)prev, new_name, new_pts);

  delete new_name; // is reallocated for in constructor
  fprintf(stderr, "new gear loaded: %d %s %d %d\n",
	  new_gear->pts_to_value(), new_gear->name,
	  new_gear->cost, new_gear->pts);
  return 0;

}//load

gear::~gear()
{
}

// virtual redefinitions
void gear::show(int x, int y)
{
  if (name == NULL)
    gl_printf(8*x, 8*y, "---End of gear---");
  else			
    gl_printf(8*x, 8*y, "%-20s  %d", name, pts);
}//show	       	       	
       
void gear::print(FILE *stdprn)
{      
  if (name == NULL)
    fprintf(stdprn, "\r\n--End of gear---");
  else 
    fprintf(stdprn, "%-20s  %d", name, pts);
}// print
       
int gear::make_new(ability* based_on)
// returns non-zero on failure, eg user cancel
{      
  char * new_name;
  int new_value; 
  gear* new_gear;
  vga_prompt *p;
       		
// get a name	
  gl_write(8,8, "Enter a description of the gear");
  p = new vga_prompt(8, PROMPT_LINE);
  new_name = new_str(p->s);	    
  delete p;   	       	 	    
  if (is_empty(new_name)) return 1; 
       	       			    
// get value   			    
  gl_write(8,8,"Enter the cost of the gear");
  p = new vga_prompt(8, PROMPT_LINE);    	    
  if (is_empty(p->s)) return 1;
  sscanf(p->s, "%d", &new_value);
  delete p;
       
// record new gear
  if (this->prev == NULL)
  // first gear in list
    new_gear= new gear(this, new_name, new_value);
  else 
    new_gear= new gear((gear*)this->prev, new_name, new_value);
       
  return 0;
}//make new
       
// end - gear.cpp
       

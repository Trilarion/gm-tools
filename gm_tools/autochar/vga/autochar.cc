// autochar.cc - automated character sheet
// Daniel Vale APR 2001	 			     

#include <string.h>	 	
#include <stdio.h>	 	
#include <stdlib.h>    	 	
#include <vga.h>	 	
#include <vgagl.h>
#include <vgakeyboard.h>
  		       	 	
#include "sat.h"  	 	
#include "character.h" 	 	
#include "stat.h"  	 
#include "dan_keyboard_buffer.h"
#include "cmd.h"   	 
  		   	 	
#define SCREEN_MODE G640x480x256
		    
dan_keyboard_buffer *key_buffer;
				
void main()	   	 	
{  		   	 	
  //exit(EXIT_SUCCESS);	 	
  class character *dummy; 	
  (void) vga_init();	  	
  vga_setmode(SCREEN_MODE);	
  gl_setcontextvga(SCREEN_MODE);	 
  gl_setfont(8, 8, gl_font8x8);		 
  gl_setwritemode(FONT_COMPRESSED + WRITEMODE_OVERWRITE);
  gl_setfontcolors(0, vga_white());	 
  gl_enableclipping();	  		 
  // Show the background  		 
  gl_clearscreen(0); 	  		 
  fprintf(stderr, "vga init ok\n");	 
  // Status Line     	  		 
  gl_write(20*8, 52*8, "[ESC] to quit.   [F1] for help.");
   	 	       	  		 
  // create the keyboard buffer		 
  key_buffer = new dan_keyboard_buffer();
  // create the character 		 
  dummy = new character();		 
  fprintf(stderr, "new character created\n");
  dummy->show();       	   		  
  fprintf(stderr, "show\n");	
   	 	       		
  // input loop	       		
  int cmd = user_cmd();		
  fprintf(stderr, "input\n");
  while (cmd != KEY_ESCAPE)   	
  {   	 	       		
    if (cmd != KEY_F1)     	
    { 	 	       		
      dummy->edit(cmd);		
    } 	 	       		
    else 	       		
    { 	 	       		
//      show_help(); tk		
//      dummy->show(); 		
    } 	 	       
    cmd = user_cmd();  
  }   	 	       
      		     
  // clean up	       
  delete dummy;	       
//  (void) vga_getch(); // debug  
  keyboard_close();
  vga_setmode(TEXT);   
}// main 	       
  	 	       

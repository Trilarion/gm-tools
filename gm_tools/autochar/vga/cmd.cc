// cmd.cc - input processor  
// Daniel Vale Jan 2001	     	 
#include <vga.h>	     
    	     		     
#include "cmd.h"	     	 
#include "dan_keyboard_buffer.h"		 
#include "editcmds.h"	     	 
    	     		     	 
enum mode{ normal, win_mode };	 
    	     		     	 
mode in_mode = normal;
extern dan_keyboard_buffer *key_buffer;// = new dan_keyboard_buffer();
       	     		     	 
int user_cmd()		     	 
{      	   		     
  int c;
    do   	   	    
    {    	   	    
      key_buffer->update();
      c = key_buffer->pull();
    } while (c == -1);
  return c;	     
} // user_cmd	  		     			 
       	  		     			 
int edit_cmd()		     			 
{ 	  		     			 
  switch ( in_mode )	     		 
  {	  		     		 
    case win_mode:	     
    {	    		     
      return win_cmd();	     
      break;		     
    }	    		     
    default:		     	 
      return plain_cmd();	 
  } // switch 
}//edit cmd 
  	    
int plain_cmd()
{ 	    
unsigned int c = user_cmd();
// typing   
  if ( (c >= ' ') && (c < 256) )
    return c + 0x7F00; 	// signal that this is a normal typing character 
      	     		 
  switch ( c )	 
  {   	     	 
    case KEY_ENTER:	 
      return DO_ENTER;	 
    case KEY_ESCAPE:
      return EXIT;
// moveing   		 
    case KEY_CTRL_LF_ARROW:	 
      return WORD_LF;	 
    case KEY_CTRL_RT_ARROW:	 
      return WORD_RT;	 
    case KEY_UP_ARROW:
      return CURSOR_UP;
    case KEY_DN_ARROW:
      return CURSOR_DN;
    case KEY_LF_ARROW:
      return CURSOR_LF;
    case KEY_RT_ARROW:
      return CURSOR_RT;
    case KEY_PGUP:
      return GO_PGUP;
    case KEY_PGDN:
      return GO_PGDN;
    case KEY_CTRL_PGUP:
      return GO_BOF;
    case KEY_CTRL_PGDN:
      return GO_EOF;
    case KEY_END:
      return GO_END;
    case KEY_HOME:
      return GO_HOME;
// deleating 
    case KEY_DEL:
      return DEL_CHAR;
    case KEY_BACKSPACE:
      return DEL_PREV_CHAR;
    case KEY_CTRL_T:
      return DEL_WORD;
    case KEY_CTRL_Y:
      return DEL_LINE;
// modal changes
    case KEY_F5: 
      return TOGGLE_SCREEN;
    case KEY_INS:
      return TOGGLE_INSERT;
    case KEY_CTRL_F5:
    {
      in_mode = win_mode;
      return win_cmd();
    }
    default:
      return NOP;
  } // switch   	     
} //plain cmd 
	      
int win_cmd() 
{	      
  int cmd = user_cmd();
  	      
  switch ( cmd )
  {     
    case KEY_LF_ARROW:
      return WIN_MOVE_LF;
    case KEY_RT_ARROW:
      return WIN_MOVE_RT;
    case KEY_UP_ARROW:
      return WIN_MOVE_UP;
    case KEY_DN_ARROW:
      return WIN_MOVE_DN;
    case KEY_ALT_LF_ARROW:
      return WIN_SIZE_LF;
    case KEY_ALT_RT_ARROW:
      return WIN_SIZE_RT;
    case KEY_ALT_UP_ARROW:
      return WIN_SIZE_UP;
    case KEY_ALT_DN_ARROW:
      return WIN_SIZE_DN;
    case KEY_ENTER:
    case KEY_ESCAPE:
    {
      in_mode = normal;
      return plain_cmd();
    }
    default:
      return NOP;
  } // switch   	  
} // win_cmd     
     
   
int yn_cmd()
{     
  int retval;
      
  while ( TRUE )
  {
    char c = (char) user_cmd();
    if ( c == 'y' || c == 'Y' )
    {
      retval = TRUE;
      break;
    }
    else if ( c == 'n' || c == 'N' )
    {
      retval = FALSE;
      break;
    }
  }
  return retval;
} // yn_cmd
   	   
// end of TECMD.CPP
   	   

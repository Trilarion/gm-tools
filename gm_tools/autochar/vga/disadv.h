// disadv(antage).h -- disadvantages for char(acter)
#ifndef DISADV_H
#define DISADV_H

#include <stdio.h>

#include "ability.h"

class disadvantage : public ability
{
private:
protected:

public:
  disadvantage(disadvantage * is_after, char * new_name, int new_pts = 0);
  ~disadvantage();

  // virtual redefinitions
  void show(int x, int y);
  void print(FILE *stdprns);
    // output to stdprn
  int pts_to_value();
  int make_new(ability* based_on = NULL);
  // returns non-zero on failure, eg user cancel
  int load(char * data);
  
  // new weapon rank methods
};
#endif
// end - disadv.h

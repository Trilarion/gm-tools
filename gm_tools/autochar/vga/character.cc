///////////////////////////////////////////////////////////////////////////
// character.cc - character object definitions for autochar
// Daniel Vale JUN 2001	       	       	
///////////////////////////////////////////////////////////////////////////
	  				
   	  				
   	  				
#include <assert.h>		     
#include <string.h>		     
#include <stdio.h> 		     
#include <vga.h>
#include <vgagl.h>
   						 
#include "character.h"				 
#include "dan_keyboard_buffer.h"		 
#include "ability.h"				 
#include "cost.h"				 
#include "figured.h"				 
#include "stat.h"				 
#include "vga_prompt.h"				 
						 
   						 
#define MAX_LINE 80    				 
   		       
#define NORMAL 0x07    
#define LINE_HIGHLIGHT 0x7F
   		       
// PAGES title, stats, combat, skills, perks, gear, powers, disadvantages
#define PAGES 8
#define TITLE_PAGE  1  
#define STAT_PAGE   2  
#define COMBAT_PAGE 3  
#define SKILL_PAGE  4  
#define PERK_PAGE   5  
#define GEAR_PAGE   6  
#define POWER_PAGE  7  
#define DISADVANTAGE_PAGE 8
	 
// Lines on the title/bio page (bio_edit())
#define NAME_LINE   5  
#define AGE_LINE    6  
#define RACE_LINE   7  
#define SEX_LINE    8  
#define LIVING_LINE 9
#define	DISADV_LINE 20   
#define BASE_LINE   21
#define EXPERIENCE_LINE	22
#define END_TITLE_PAGE 22
 		       
// replace the box draw variables this can be replaced when windowing
//   functionality is added 
#define lft 8		    
#define top 3 		    
#define btm 48		    

///////////////////////////////////////////////////////////////////////////
////////////////    CONVERSIONS AND LOOKUP     ////////////////////////////
///////////////////////////////////////////////////////////////////////////
		       
// curser y defaults for pages
const int cur_y_default[PAGES+1] = {0, 5, 6, 6, 6, 6, 6, 6, 6}; // page zero unused
		       		
const char TITLE_SKIP[11] = {10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 0};
  // Null terminated string containg numbers of blank lines
  // on the title page to be skipped in cursor movement
		     		
		     	 	
ability * character::get_base(char * temp)
// finds and returns a pointer to the base ability (stat) for a skill
{		       		
  // insuficent input: general skill
  if (temp == NULL) return NULL;
  if (strlen(temp) < 3) return NULL;
		       
  // standard cases    
  if (strcmp(temp, "STR") == 0)
    return ch_str;     
  else if (strcmp(temp, "DEX") == 0)
    return ch_dex;     
  else if (strcmp(temp, "CON") == 0)
    return ch_con;     
  else if (strcmp(temp, "BOD") == 0)
    return ch_bod;     
  else if (strcmp(temp, "INT") == 0)
    return ch_int;     
  else if (strcmp(temp, "EGO") == 0) // old name for disipline
    return ch_dis;     
  else if (strcmp(temp, "DIS") == 0)
    return ch_dis;     
  else if (strcmp(temp, "PRE") == 0)
    return ch_pre;     
  else if (strcmp(temp, "COM") == 0)
    return ch_com;     
  else		       
  // default to general skill
    return NULL;       
} // get base;	       
		       
		       
ability * character::field_default(int page)
// ability defaults for pages (default cursor position)
{		       
  switch (page)	       
  {		       
    case (TITLE_PAGE): 
    {		       
      return NULL;     
    }		       
    case (STAT_PAGE):  
    {		       
      return ch_stats->next;
    }		       
    case (COMBAT_PAGE):
    {		       
      if (combat->next == NULL)
        return c_tail; 
      else	       
        return combat->next;
    }		       
    case (SKILL_PAGE): 
    {		       
      if (skill_head == NULL)
        return skill_tail;
      else	       
        return skill_head->next;
    }		       
    case (PERK_PAGE):  
    {		       
      if (perk_head == NULL)
        return perk_tail;
      else	       
        return perk_head->next;
    }		       
    case (GEAR_PAGE):  
    {		       
      if (gear_head == NULL)
        return gear_tail;
      else	       
        return gear_head->next;
    }		       
    case (POWER_PAGE): 
    {		       
      if (power_head == NULL)
        return power_tail;
      else	       
        return power_head->next;
    }		       
		       
    case (DISADVANTAGE_PAGE):
    {		       
      if (disadvantage_head == NULL)
        return disadvantage_tail;
      else	       
        return disadvantage_head->next;
    }		       
    default:	       
    {		       
      gl_write(8,8," ERROR: page out of range ");
      return NULL;     
    }		       
  }//switch	       
}//field default       
		       
int character::sum_stats()
{		       
  int count;	       
  ability * look;      
		       
  look = ch_stats->next; // the first statistic;
		       
  // sum the points spent (in xp)
  count = 0;	       
  while (look != NULL) 
  {		       
    count += look->get_pts();
    look = look->next; 
  }		       
		       
  return count;	       
}// character:: sum stats
		       
int character::sum_combat()
{		       
  // check there is at least one combat ability to sum
  assert(combat != NULL);
  if (combat->next == NULL)
    return 0;	       
  else		       
    return combat->next->sum();  // call ability::sum - base class method
}// sum combat	       
		       
int character::sum_skills()
{		       
  // check there is at least one skill to sum
  assert(skill_head != NULL);
  if (skill_head->next == NULL)
    return 0;	       
  else		       
    return skill_head->next->sum();  // call ability::sum - base class method
}// sum skills	       
		       
int character::sum_perks()
  // returns total cost of perks
{		       
  assert(perk_head != NULL);
  if (perk_head->next == NULL)
    return 0;	       
  else		       
    return perk_head->next->sum();
}// sum perks	       
		       
int character::sum_gear()
  // returns total cost of gear
{		       
  assert(gear_head != NULL);
  if (gear_head->next == NULL)
    return 0;	       
  else		       
    return gear_head->next->sum();
}// sum gear	       
		       
int character::sum_powers()
  // returns total cost of powers
{		       
  assert(power_head != NULL);
  if (power_head->next == NULL)
    return 0;	       
  else		       
    return power_head->next->sum();
}// sum powers	       
		       
		       
int character::sum_disadvantages()
{		       
  assert(disadvantage_head != NULL);
  if (disadvantage_head->next == NULL)
    return 0;	       
  else		       
    return disadvantage_head->next->sum();
}//sum disadvantages   
		       
///////////////////////////////////////////////////////////////////////////
//////////    CONSTRUCTOR & INITAILIZATION    /////////////////////////////
///////////////////////////////////////////////////////////////////////////
		       
character::character() 
{		       
// important places    
  page = 1;	       
  stat_top = top + 3;
  stat_btm = stat_top + NUM_STATS - 1;  // record the bottom of the stats
  cury = cur_y_default[page];
		       
// characteristics     
  ch_name = new_str("Joe");
//  title = buff_str(new_str(ch_name));
		       
  file_name = new_str(FILE_FIELD);
  strncpy(file_name, ch_name, strlen(ch_name));
  strncpy((file_name+strlen(ch_name)), ".chr", 4);
		       
  ch_age = 25;	       
  ch_living = new_str("Soldure");
  ch_race = new_str("Human");
  ch_sex = new_str("Male");
		       
  ch_base = 750;       
  ch_max_disadvantages = 750;
  ch_experience = 0;   		    
		       		    
  ch_stats = new statistic(NULL, NULL, 0, 0); // dummy head node
  // set up the stats depending on HERO rules or Frame rules
#ifdef HERO 			    
// HERO stats			    
  ch_str = new statistic(ch_stats, "Stength", 0, cSTR);
  ch_dex = new statistic(ch_str, "Dexterity", 0, cDEX);
  ch_con = new statistic(ch_dex, "Constitution", 0, cCON);
  ch_bod = new statistic(ch_con, "Body", 0, cBOD);
  ch_int = new statistic(ch_bod, "Intelegence", 0, cINT);
  ch_dis = new statistic(ch_int, "Ego", 0, cDIS);
  ch_pre = new statistic(ch_dis, "Presence", 0, cPRE);
  ch_com = new statistic(ch_pre, "Comliness", 0, cCOM);
  ch_pd  = new normal_def((ability*) ch_com, "PD", 0, (ability*) ch_str);
  ch_ed  = new normal_def((ability*) ch_pd,  "ED", 0, (ability*) ch_con);
  ch_spd = new spd((ability*) ch_ed, "Speed",     0, (ability*) ch_dex);
  ch_end = new end((ability*) ch_spd, "Endurance", 0, (ability*) ch_con);
  ch_stn = new stn((ability*) ch_end, "Stun", 0, (ability*) ch_bod, (ability*) ch_str, (ability*) ch_con);
  ch_rec = new rec((ability*) ch_stn, "Recovery", 0, (ability*) ch_str, (ability*) ch_con);
  ch_man = new end((ability*) ch_rec, "Mana",     0, (ability*) ch_dis);
  ch_mrc = new rec((ability*) ch_man, "Mana Recovery",0, (ability*) ch_int, (ability*) ch_dis);
  ch_run = new run((ability*) ch_mrc, "Running", 0);
  ch_swm = new swm((ability*) ch_run, "Swimming", 0);
  	     			    
#else 	      			    
// Frame stats			    
  ch_str = new statistic(ch_stats, "Stength", 0, cSTR);
  ch_dex = new statistic(ch_str, "Dexterity", 0, cDEX);
  ch_con = new statistic(ch_dex, "Constitution", 0, cCON);
  ch_bod = new statistic(ch_con, "Body", 0, cBOD);
  ch_int = new statistic(ch_bod, "Intelegence", 0, cINT);
  ch_tech= new statistic(ch_int, "Tech", 0, cTECH); 
  ch_dis = new statistic(ch_tech,"Will", 0, cDIS);
  ch_pre = new statistic(ch_dis, "Presence", 0, cPRE);
  ch_com = new statistic(ch_pre, "Apperance", 0, cCOM);
  ch_pd  = new normal_def((ability*) ch_com, "PD",        0, (ability*) ch_str);
  ch_ed  = new normal_def((ability*) ch_pd,  "ED",        0, (ability*) ch_con);
  ch_md  = new normal_def((ability*) ch_ed,  "MD", 0, (ability*) ch_dis);
  ch_spd = new spd((ability*) ch_md, "Reflex", 0, (ability*) ch_dex);
  ch_end = new end((ability*) ch_spd, "Endurance", 0, (ability*) ch_con);
  ch_stn = new stn((ability*) ch_end, "Stun", 0, (ability*) ch_bod, (ability*) ch_str, (ability*) ch_con);
  ch_man = new end((ability*) ch_stn, "Mana",     0, (ability*) ch_dis);
  ch_run = new run((ability*) ch_man, "Running", 0);
  ch_swm = new swm((ability*) ch_run, "Swimming", 0);
  				    				 
#endif  			    				 
  current = field_default(page);    	      
      		       		    	      
// weapon ranks dummy head node	    	      
  combat = new weapon_rank(NULL, NULL, 0); 
  c_tail = new weapon_rank(combat, NULL, 0);
      		       
// skill dummy nodes   
  skill_head = new skill(NULL, NULL, 0, NULL);
  skill_tail = new skill(skill_head, NULL, 0, NULL);
      		       
// perk dummy nodes    
  perk_head = new perk(NULL, NULL, 0);            // up to here in recompiling for win95
  perk_tail = new perk(perk_head, NULL, 0);
      		       
// gear dummy nodes    
  gear_head = new gear(NULL, NULL, 0);
  gear_tail = new gear(gear_head, NULL, 0);
      		       
// power dummy nodes   
  power_head = new power(NULL, NULL, 0);
  power_tail = new power(power_head, NULL, 0);
      	       	       
// disadvantage dummy nodes
  disadvantage_head = new disadvantage(NULL, NULL, 0);
  disadvantage_tail = new disadvantage(disadvantage_head, NULL, 0);
      	       	       
}// character constructor
      	       	       
///////////////////////////////////////////////////////////////////////////
///////////     DESTRUCTOR AND DELEATION     //////////////////////////////
///////////////////////////////////////////////////////////////////////////
	       	       
	       	       
void character::del_stats()
{	       	       
  statistic * look, *prev;
	       	       
  look = ch_stats;     
  while (look != NULL) 
  {	       	       
    prev = look;       
    look = (statistic*) look->next;
    delete prev;
  }	       
  ch_stats = NULL;
}//del stats   	       
	       	       
void character::del_weapon_ranks()
{	       	       
  weapon_rank * look, *prev;
	       	       
  look = combat;       
  while (look != NULL) 
  {	       	       
    prev = look;       
    look = (weapon_rank*) look->next;
    delete prev;  
  }	       	  
  combat = NULL;
  c_tail = NULL;
}//del weapon ranks    
	       	       
void character::del_skills()
{	       	       
  skill *look, *prev;  
	       	       
  look = skill_head;   
  while (look != NULL) 
  {	       	       
    prev = look;       
    look = (skill*) look->next;
    delete prev;
  } 
  skill_head = NULL; skill_tail = NULL;
}//del weapon ranks    		       
    	       	       		       
void character::del_perks()	       
{	       	       		       
  perk *look, *prev;   		       
	       	       		       
  look = perk_head;    		       
  while (look != NULL) 		       
  {	       	       		       
    prev = look;       		       
    look = (perk*) look->next;	       
    delete prev;
  }	       	
  perk_head = NULL;
  perk_tail = NULL;
}//del weapon ranks    		       
  	       	       		       
void character::del_gear()	       
{ 	       	       		       
  gear *look, *prev;   
  	       	       
  look = gear_head;    
  while (look != NULL) 
  {	       	       
    prev = look;       
    look = (gear*) look->next;
    delete prev;
  } 
  gear_head = NULL;
  gear_tail = NULL;
  
}//del gear	       
  		       
void character::del_powers()
{ 		       
  power *look, *prev;  
  		       
  look = power_head;   
  while (look != NULL) 
  {		       
    prev = look;       
    look = (power*) look->next;
    delete prev;       
  }
  power_head = NULL;
  power_tail = NULL;
}//del weapon ranks    
  		       
void character::del_disadvantages()
{ 		       
  disadvantage *look, *prev;
  		       
  look = disadvantage_head;
  while (look != NULL) 
  {		       
    prev = look;       
    look = (disadvantage*) look->next;
    delete prev;       
  }
  disadvantage_head = NULL;
  disadvantage_tail = NULL;
}//del weapon ranks    
  		       
  		       
  		       
character::~character()
{ 		       
// delete abilities    
  del_stats();	       
  del_weapon_ranks();  
  del_skills();	       
  del_perks();	       
  del_gear();	       
  del_powers();	       
  del_disadvantages(); 
  		       
  if (ch_name != NULL) delete ch_name;
  if (ch_living != NULL) delete ch_living;
  if (ch_race != NULL) delete ch_race;
  if (ch_sex != NULL) delete ch_sex;
  if (file_name != NULL) delete file_name;
}//char destructor     
		       
///////////////////////////////////////////////////////////////////////////
///////////    DISPLAY        /////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
		       
		       
void character::show_subtotal_stats()
{		       
// redisplay the subtotal for stats
  int sub_total = sum_stats();
  text_norm(); 
  gl_printf((lft+2)*8, (stat_btm+2)*8, "          Total Statistics Costs: %6d     ", sub_total);
}	       	       
	       	       
void character::print_subtotal_stats(FILE * stdprn)
{	       	       
// redisplay the subtotal for stats
  int sub_total = sum_stats();
  fprintf(stdprn, "          Total Statistics Costs: %6d     \r\n", sub_total);
}//print subtotal stats
       	       	       
void character::show_stat_page()
{      	       	       
  gl_clearscreen(0);
  // heading for stats columns
    text_norm();       
    gl_write((lft+2)*8, (top+2)*8, "Val Characteristic  Base  Cost  Max  Pts");
       	       	       		   
  // show the stats		   
       	       			   
    ability * look;		   
    look = ch_stats->next; // first ability in list;
    int i = 0; 			   
    while (look != NULL)	   
    {  	       			   
      look->show(lft+2, stat_top+i);
      look = look->next; i++;	   
    }  	       			   
    show_subtotal_stats();	   
       	       			   
  // show highlighted stat	   
    text_highlight();
    // assert(cury > stat_top-1);	   
    current->show(lft + 2, cury);
    text_norm();
}//show_stat_page		   
  	       			   
void character::show_combat_page() 
{ 	       
  int total = 0; //total combat cost
  gl_clearscreen(0);
  	       
  text_norm(); 
  gl_write((lft+2)*8, (top+2)*8, "Ranks  Field             @cost  pts  Notes");
  	       
  ability * look = combat->next;
  int i = cur_y_default[COMBAT_PAGE];
  while (look != NULL)
  {	       
    if (look != current)
    {	       
      look->show(lft+2, i);
      total += look->get_pts();
    }	       
    else       
    {	       
      text_highlight();
      look->show(lft+2, i);
      text_norm();
      total += look->get_pts();
    } 	       
    look = look->next;
    i++;       
      	       
  }//while     
  total -= c_tail->get_pts(); // not a real node
      	       
  gl_printf((lft+2)*8, (i+1)*8, "Total Combat Cost: %d", total);
}//show_combat_page		
      	       			
      	       			
void character::show_skill_page()
{     	       			
  int total= 0;			
      	       			
  gl_clearscreen(0);
  text_norm(); 
  gl_printf((lft+2)*8, (top+2)*8, 
	    "Val  Skill                Base Stat    @cost  pts  Notes");
	       			
  ability * look = skill_head->next;
  int i = cur_y_default[SKILL_PAGE];
  while (look != NULL)		
  {	       			
    if (look != current)	
    {	       			
      look->show(lft+2, i);	
      total += look->get_pts();	
    }	       			
    else       			
    {	       			
      text_highlight();
      look->show(lft+2, i);	
      text_norm();
      total += look->get_pts();	
    }  	       
    look = look->next;
    i++;
  }

  gl_printf((lft+2)*8, (btm-2)*8, "Total Skill Cost: %d", total);
}// show skill page

void character::goset(int y)
{
  if (y == cury)
    text_highlight();
  else
    text_norm();
}

void character::show_title_page()
{
  int x, y; // my cursor
  int stat_total, combat_total, skill_total;
  int gear_total, perk_total, power_total;
  int disadvantage_total, disadvantage_contribution;
  int total_spent, balance;

  gl_clearscreen(0);
  x = lft+2;
  y = top+2;

  total_spent = 0;
  total_spent += stat_total = sum_stats();
  total_spent += combat_total = sum_combat();
  total_spent += skill_total = sum_skills();
  total_spent += perk_total = sum_perks();
  total_spent += gear_total = sum_gear();
  total_spent += power_total = sum_powers();

  disadvantage_total = sum_disadvantages();
  disadvantage_contribution = (disadvantage_total > ch_max_disadvantages) ? 
                              ch_max_disadvantages : disadvantage_total;
  balance = total_spent - ch_base - disadvantage_contribution - ch_experience;
									     
  goset(y); gl_printf(x*8, y*8, "Name: %s", ch_name); y++;
  goset(y);
  if (ch_age < 0)
    gl_printf(x*8, y*8, "Age: Unknown / Ageless");
  else
    gl_printf(x*8, y*8, "Age: %d", ch_age);
  y++;
  goset(y); gl_printf(x*8, y*8, "Race: %s", ch_race); y++;
  goset(y); gl_printf(x*8, y*8, "Sex: %s", ch_sex); y++;
  goset(y); gl_printf(x*8, y*8, "Living: %s", ch_living); y++;

  y++;
  goset(y); gl_printf(x*8, y*8, "Stat Cost:    %5d", (stat_total));   y++;
  goset(y); gl_printf(x*8, y*8, "Combat Cost:  %5d", (combat_total)); y++;
  goset(y); gl_printf(x*8, y*8, "Skill Cost:   %5d", (skill_total));  y++;
  goset(y); gl_printf(x*8, y*8, "Perk Cost:    %5d", (perk_total));   y++;
  goset(y); gl_printf(x*8, y*8, "Gear Cost:    %5d", (gear_total));   y++;
  goset(y); gl_printf(x*8, y*8, "Power Cost:   %5d", (power_total));  y++;
  goset(y); gl_printf(x*8, y*8, "-------------------" ); y++;
  goset(y); gl_printf(x*8, y*8, "Total:        %5d", (total_spent));        y++;
  y++;
  if (disadvantage_total > ch_max_disadvantages)
  {
    goset(y); 
    gl_printf(x*8, y*8, "Disadvatages: %5d (Total: %d)", disadvantage_contribution, disadvantage_total); 
    y++;
  }
  else
  {
    goset(y); 
    gl_printf(x*8, y*8, "Disadvatages: %5d (Max: %d)", disadvantage_total, ch_max_disadvantages); 
    y++;
  }
  goset(y); gl_printf(x*8, y*8, "Base:         %5d", (ch_base));      y++;
  goset(y); gl_printf(x*8, y*8, "Experience:   %5d", (ch_experience)); y++;
  goset(y); gl_printf(x*8, y*8, "-------------------" ); y++;
  goset(y);			
  if (balance > 0)		
    gl_printf(x*8, y*8, "Overspent:    %5d", balance);
  else			
    gl_printf(x*8, y*8, "Unspent:      %5d", (-balance));
}// show title page	
     			
void character::show_perk_page()
{    			
  int row;		
  int total= 0;		

  gl_clearscreen(0);
  row = cur_y_default[PERK_PAGE];
			
  text_norm();
  gl_printf((lft+2)*8, (top+2)*8, "Perk                  Pts");
  			
  ability * look = perk_head->next;
  while (look != NULL)	
  {
    if (look != current)
    {
      look->show(lft+2, row);
      total += look->get_pts();
    }
    else
    {
      text_highlight();
      look->show(lft+2, row);
      text_norm( );
      total += look->get_pts();
    } 		
    look = look->next;
    row++;	
  }   		
      		
  gl_printf((lft+2)*8, (row+1)*8, "Total Perks: %d", total);
}// show perk page		  
				  
void character::show_gear_page()  
{				  
  int total= 0;			  
				  
  gl_clearscreen(0);
  text_norm();      	  
  gl_printf((lft+2)*8, (top+2)*8, "Gear                  Pts");
	    			  
  ability * look = gear_head->next;
  int i = cur_y_default[GEAR_PAGE];
  while (look != NULL)		  
  {				  
    if (look != current)	  
    {				  
      look->show(lft+2, i);	  
      total += look->get_pts();	  
    }				  
    else			  
    {				  
      text_highlight(); 
      look->show(lft+2, i);	  
      text_norm(  );	  
      total += look->get_pts();	  
    }	       			  
    look = look->next;		  
    i++;       			  
  }	       
	       
  gl_printf((lft+2)*8, (i+1)*8, "Total Gear: %d", total);
}// show gear page		
	       			
void character::show_power_page()
{	       			
  int total= 0;			
				
  gl_clearscreen(0);
  text_norm();	  
  gl_printf((lft+2)*8, (top+2)*8, "power                  Pts");
       				
  ability * look = power_head->next;
  int i = cur_y_default[POWER_PAGE];
  while (look != NULL)
  {    
    if (look != current)
    {  
      look->show(lft+2, i);
      total += look->get_pts();
    }  
    else
    {  
      text_highlight( );   
      look->show(lft+2, i);
      text_norm ( );
      total += look->get_pts();
    }		 
    look = look->next;
    i++;	 
  }		 
		 
  gl_printf((lft+2)*8, (i+1)*8, "Total powers: %d", total);
}// show power page		
		 		
		 		
void character::show_disadvantage_page()
{				
  int disadvantage_total = 0;
				
  gl_clearscreen(0);		
  text_norm();	   		
  gl_printf((lft+2)*8, (top+2)*8, "Value  Disadvantage");
		    	     	       
  ability * look = disadvantage_head->next;
  int i = cur_y_default[DISADVANTAGE_PAGE];
  while (look != NULL)		       
  {		    		       
    if (look != current) // other lines  	  
    {	 	    		       	   
      look->show(lft+2, i);	       	   
      disadvantage_total += look->get_pts();	  
    }	   	    		       
    else   	       	// currnet line       	       
    {		    		  
      text_highlight();	  
      look->show(lft+2, i);	  
      text_norm( );  
      disadvantage_total += look->get_pts();
    }		    
    look = look->next;
    i++;	    
  }		    
		   
  if (disadvantage_total <= ch_max_disadvantages)
  {
    gl_printf((lft+2)*8, (i+1)*8, 
	      "Total points from disadvantages: %d (Max: %d)", 
	      disadvantage_total, ch_max_disadvantages);
  }	      								     
  else	      								     
  {	      								     
    gl_printf((lft+2)*8, (i+1)*8, 
	      "Total points from disadvantage: %d (Total disadvantages: %d)", 
	      ch_max_disadvantages, disadvantage_total);      	   
  }									     
}// show disadvantage page					  	     
  								  
void character::show()						  
  								  
{ 				
  				
// show the box with the characters name as the title
//  draw();			
				
  gl_clearscreen(0);
  switch (page)			
  {				
    case(TITLE_PAGE):		
    {				
      show_title_page();	
      break;			
    }				
    case(STAT_PAGE):		
    // stats page		
    {				
      show_stat_page();		
      break;			
    }// stat page		
				
    case( COMBAT_PAGE ):	
    // weapon ranks		
    {				
      show_combat_page();	
      break;			
    }				
				
    case ( SKILL_PAGE ):	
    {				
      show_skill_page();	
      break;			
				
    }//skill page		
				
    case ( PERK_PAGE ):		
    {				
      show_perk_page();		
      break;			
    }				
				
    case ( GEAR_PAGE ):		
    {				
      show_gear_page();		
      break;			
    }				
				
    case ( POWER_PAGE ):	
    {				
      show_power_page();	
      break;			
    }				
				
    case ( DISADVANTAGE_PAGE ):	
    {				
      show_disadvantage_page();	
      break;			
    }				
				
    default:			
    {				
      gl_printf(1, 1, "%d", page);   	
      break;	      		
    }		      		
  }		      		
} // show	      		
		      		
		      		
///////////////////////////////////////////////////////////////////////////
/////////////    USER HELP    /////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
		      
		      
		      
		      
///////////////////////////////////////////////////////////////////////////
/////////       FILE MANIPULATIONS         ////////////////////////////////
///////////////////////////////////////////////////////////////////////////
		      
		      
int character::save() 
{	       	      
  FILE * out;  	      
  char * temp; 	      
  ability * look;     
  vga_prompt *p;
  	       
  if (file_name != NULL) delete file_name;
  file_name = NULL;
  	       	   			  
// open a file for saving the character;
  text_norm(); 
  gl_write(8,8, "Enter the file to save to");
  p = new vga_prompt(8, PROMPT_LINE);	  
  file_name = new_str(p->s);		  
  delete p;    	      			  
	       	      			  
  debuf(&file_name);  // remove leading and trailing spaces
	       	      			  
  gl_printf(8, 8, " Saving %s to %s        ", ch_name, file_name);
	       	      			  
  if ((out = fopen(file_name, "wt"))	  
      == NULL) 	      			  
  {	       	      			  
     fprintf(stderr, "Cannot open output file.\n");
     return 1; 	      			  
  }//fi	       	      			  
  
//  temp = new_str(20);                 // repair the input field
//  sprintf(temp, "%-20s", file_name);	  
//  delete file_name;			  
//  file_name = temp;			  
  
  // save character bio			  
 // debuf(&ch_name);			  
  fprintf(out, "%s\n", ch_name);	  
 // buff_str(&ch_name);			  
  fprintf(out, "%d\n", ch_age);		  
  fprintf(out, "%s\n", ch_race);	  
  fprintf(out, "%s\n", ch_sex);		  
  fprintf(out, "%s\n", ch_living);	  
  fprintf(out, "%d\n", ch_base);	  
  fprintf(out, "%d\n", ch_max_disadvantages);
  	       				  
  for (int i = 2; i <= PAGES; i++) // pages 2 to PAGES inclusive
  {	       				  
    fprintf(out, "*\n");  // tk comment so we know what goes in each section by reading the save file
    look = field_default(i);		  
    while (look != NULL)		  
    {					  
      look->save(out); // save the stats  
      look = look->next;		  
    }//while				  
  }//rof				  
  					  
  fclose(out);				  
  return 0; // save failure;		  
}//save					  
  				       	  
int character::load()		       	  
{ 	       			       	  
  FILE * in;   			       	  
  char * temp; 			       	  
  ability *look;		       	  
  int new_num, i;		       	  
  skill * new_skill;		       	  
  vga_prompt *p;		       	  
  	       			       	  
// open the character file	       	  
  gl_write(8,8,"Enter the file to load.               ");
  p = new vga_prompt(8, PROMPT_LINE);	  
  if (file_name != NULL) delete file_name;
  file_name = new_str(p->s);	       	  
  delete p;    
  	       
  if (is_empty(file_name)) return 1;// test for no input == user cancel
  	       			       	  
  debuf(&file_name);  // remove leading and trailing spaces
  	       			       	  
  gl_printf(8,8, " Loading %s", file_name);
  fprintf(stderr, "Loading %s\n", file_name);
  	       	    		       	  
  if ((in = fopen(file_name, "rt"))    	  
      == NULL) 	    		       	  
  {	       	    		       	  
     fprintf(stderr, "Cannot open input file %s.\n", file_name);
     return 1; 	    		       	  
  }//fi	       	    		    
  	       
  // Delete old character and setup for the new	
  assert(combat != NULL); 
  assert(skill_head != NULL); 
  assert(perk_head != NULL);
  assert(gear_head != NULL);
  assert(power_head != NULL);
  assert(disadvantage_head != NULL);
  assert(c_tail != NULL); 
  assert(skill_tail != NULL); 
  assert(perk_tail != NULL);
  assert(gear_tail != NULL);
  assert(power_tail != NULL);
  assert(disadvantage_tail != NULL);
  
  // NB: for stats we just change the values, no need to delete them	       
  del_weapon_ranks();  	    
  del_skills();	       	    
  del_perks(); 	       	    
  del_gear();  	       	    
  del_powers();	       	    
  del_disadvantages(); 	    
  	       	       	    
  if (ch_name != NULL) delete ch_name; 
  if (ch_living != NULL) delete ch_living;
  if (ch_race != NULL) delete ch_race; 
  if (ch_sex != NULL) delete ch_sex;   
  if (file_name != NULL) delete file_name;
  ch_name = NULL;
  ch_living = NULL;
  ch_race = NULL;
  ch_sex = NULL;
  file_name = NULL;
  	      
  assert(combat == NULL);   
  assert(skill_head == NULL); 
  assert(perk_head == NULL);
  assert(gear_head == NULL);
  assert(power_head == NULL);
  assert(disadvantage_head == NULL);
  assert(c_tail == NULL);   
  assert(skill_tail == NULL); 
  assert(perk_tail == NULL);
  assert(gear_tail == NULL);
  assert(power_tail == NULL);
  assert(disadvantage_tail == NULL);
  
// restrore dummy nodes			  
  
  // weapon ranks dummy head node	  
  combat = new weapon_rank(NULL, NULL, 0);
  c_tail = new weapon_rank(combat, NULL, 0);
  	       				  
  // skill dummy nodes			  
  skill_head = new skill(NULL, NULL, 0, NULL);
  skill_tail = new skill(skill_head, NULL, 0, NULL);
  	       				  
  // perk dummy nodes			  
  perk_head = new perk(NULL, NULL, 0);  
  perk_tail = new perk(perk_head, NULL, 0);
  	       				  
  // gear dummy nodes			  
  gear_head = new gear(NULL, NULL, 0);  
  gear_tail = new gear(gear_head, NULL, 0);
  	       				  
  // power dummy nodes			  
  power_head = new power(NULL, NULL, 0);
  power_tail = new power(power_head, NULL, 0);
  	       				  
  // disadvantage dummy nodes		  
  disadvantage_head = new disadvantage(NULL, NULL, 0);
  disadvantage_tail = new disadvantage(disadvantage_head, NULL, 0);
  	       
  assert(combat != NULL); 
  assert(skill_head != NULL); 
  assert(perk_head != NULL);
  assert(gear_head != NULL);
  assert(power_head != NULL);
  assert(disadvantage_head != NULL);
  assert(c_tail != NULL); 
  assert(skill_tail != NULL); 
  assert(perk_tail != NULL);
  assert(gear_tail != NULL);
  assert(power_tail != NULL);
  assert(disadvantage_tail != NULL);
  	       		  
// load character bio information      	  
  temp = new_str(MAX_LINE);	       	  
  // temp is used to load each line of input in
  // the input is then processed by sscanf() or copied with debuf()
  
  // get ch_name		       	  
  fgets(temp, MAX_LINE, in);	       	  
  temp[(strlen(temp)-1)] = '\0'; // remove new-line character
  ch_name = debuf(temp);   // copy the read name and trim it
  fprintf(stderr, "Name loaded: %s\n", ch_name);
  
  // get age			       	  
  fgets(temp, MAX_LINE, in);	       	  
  temp[(strlen(temp)-1)] = '\0'; // remove new-line character
  sscanf(temp, "%d", &ch_age);	    
  fprintf(stderr, "age loaded: %d\n", ch_age);
  				    	  
// get race			    	  
  fgets(temp, MAX_LINE, in);	    	  
  temp[(strlen(temp)-1)] = '\0'; // remove new-line character
  ch_race = debuf(temp);	   
  fprintf(stderr, "race loaded: %s\n", ch_race);
  				    	  
// get sex			    	  
  fgets(temp, MAX_LINE, in);	    	  
  temp[(strlen(temp)-1)] = '\0'; // remove new-line character
  ch_sex = debuf(temp);
  fprintf(stderr, "sex loaded: %s\n", ch_sex);
  				    	      
// get living			    	      
  fgets(temp, MAX_LINE, in);  fprintf(stderr, "read ");
  temp[(strlen(temp)-1)] = '\0'; // remove new-line character 
  fprintf(stderr, "modify ");
  ch_living = debuf(temp);   
  fprintf(stderr, "record ");
  fprintf(stderr, "living loaded: %s\n", ch_living);
  					      	    
// base points				      	    
  fgets(temp, MAX_LINE, in);		      	    
  temp[(strlen(temp)-1)] = '\0'; // remove new-line character
  sscanf(temp, "%d", &ch_base);		  	    
  fprintf(stderr, "base points loaded: %d\n", ch_base);
  					  	       
// max disadvantages			  	       
  fgets(temp, MAX_LINE, in);		  	       
  temp[(strlen(temp)-1)] = '\0'; // remove new-line character
  sscanf(temp, "%d", &ch_max_disadvantages);	       
  fprintf(stderr, "max disadvantages loaded: %d\n", ch_max_disadvantages);
  					  	    			  
  fprintf(stderr, "Character bio loaded.\n");	    			  
  // checked ok exit(EXIT_SUCCESS);	  				  
//  goto end_load;
  
// stats
  fprintf(stderr, "Loading Stats.\n");
  fgets(temp, MAX_LINE, in); // get separating star			  
  fprintf(stderr, "  star skipped\n");
  assert (ch_stats != NULL);   	      	  
  look = ch_stats->next;   
  fprintf(stderr, "  loop initialised\n");
  while (look != NULL) 	     		  
  {		       	     		  
    fgets(temp, MAX_LINE, in);
    fprintf(stderr, " read line; " );
    temp[(strlen(temp)-1)] = '\0'; // remove new-line character
    fprintf(stderr, " new line replace with null; ");
    sscanf(temp, "%d", &new_num); 	
    fprintf(stderr, " points read: %d; ", new_num);
    look->change_pts(new_num-(look->get_pts())); 
    fprintf(stderr, " stat updated\n");
    look = look->next; 	       	       	  
  }// while	       		       	  
  fprintf(stderr, "Stats loaded.\n");  	  
  	      	       		       	  
  fgets(temp, MAX_LINE, in); // get separating star
   	       	       		       	  
  // load abilities    		       	  
  // read in each line, striping terminating nulls.
  // Send each line to the appropriate ability loading rutine.
  // Move on to the next ability type when an asterix is found on the begining of the line
  for (i = 3; i <= PAGES; i++)	       	  
  {	       	       			  
    look = field_default(i);		  
  	       	       			  
    while ((fgets(temp, MAX_LINE, in) != NULL) && (temp[0] != '*'))
    // '*' marks between ability groups	  
    {  		       
      fprintf(stderr, "remove \\r ");
      temp[(strlen(temp)-1)] = '\0'; // remove new-line character
      fprintf(stderr, "process line\n");  
      look->load(debuf(temp)); 	       	  
      fprintf(stderr, "end of loop ");	  
    }//while   	       		    	  
    fprintf(stderr, "star or end of file\n");
  }//rof       	       		    	  
  fprintf(stderr, "\nLoad abilities.\n");	  
  	       	       		   	  
// set skill bases     		   	  
  new_skill = (skill*) skill_head->next;		  
  assert(new_skill != NULL);	   	  
  	       			   	  
  while (new_skill->next != NULL)    
  // do to all in the body of the list but not dummy end nodes!
  {	       			     
    new_skill->set_base(get_base(new_skill->find_base));
    new_skill = (skill*) new_skill->next; 
  }//while			   
  fprintf(stderr, "set skill bases\n");
  //  exit(EXIT_SUCCESS);	    
  				    
  end_load:	       		   	    
// clean up    			    
  if (temp != NULL) delete temp;   	       	       	    
  fclose(in);  			    
  return 0;    			    
}//load	       			    
  	       			    
  	       
void character::print_character()
{ 
  ability * look;
  int stat_total, combat_total, skill_total;	   
  int gear_total, perk_total, power_total;	   
  int disadvantage_total, disadvantage_contribution;
  int total_spent, balance;			   
  						   
#ifndef stdprn // if the stdprn is not available - kludge, don't know why this is happening
  FILE * stdprn;				   
  if ((stdprn = fopen("print.txt", "wt"))	   
      == NULL)					   
  {						   
     fprintf(stderr, "Cannot open output file.\n");
     return;					   
  }//fi						   
#endif						   
						   
  fprintf(stdprn, "\r"); // return carage to left edge
						   
// Bio Page					   
						   
  total_spent = 0;					   
  total_spent += stat_total = sum_stats();		   
  total_spent += combat_total = sum_combat();		   
  total_spent += skill_total = sum_skills();		   
  total_spent += perk_total = sum_perks();		   
  total_spent += gear_total = sum_gear();		   
  total_spent += power_total = sum_powers();

  disadvantage_total = sum_disadvantages();
  disadvantage_contribution = (disadvantage_total > ch_max_disadvantages) ? 
                              ch_max_disadvantages : disadvantage_total;
  balance = total_spent - ch_base - disadvantage_contribution - ch_experience;
  
 // text colour contents clear tk 
  fprintf(stdprn, "Name: %s\n\r", ch_name);
  if (ch_age < 0)
    fprintf(stdprn, "Age: Unknown / Ageless\n\r");
  else
    fprintf(stdprn, "Age: %d\n\r", ch_age);

  fprintf(stdprn, "Race: %s\r\n", ch_race);
  fprintf(stdprn, "Sex: %s\r\n", ch_sex);	    
  fprintf(stdprn, "Living: %s\r\n", ch_living);	    
						    
  fprintf(stdprn, "Stat Cost:    %5d\r\n", (stat_total));
  fprintf(stdprn, "Combat Cost:  %5d\r\n", (combat_total));
  fprintf(stdprn, "Skill Cost:   %5d\r\n", (skill_total));
  fprintf(stdprn, "Perk Cost:    %5d\r\n", (perk_total));
  fprintf(stdprn, "Gear Cost:    %5d\r\n", (gear_total));
  fprintf(stdprn, "Power Cost:   %5d\r\n", (power_total));
  fprintf(stdprn, "-------------------\r\n" );	    
  fprintf(stdprn, "Total:        %5d\r\n", (total_spent));
						    
  if (disadvantage_total > ch_max_disadvantages)    
    fprintf(stdprn, 
            "Disadvatages: %5d (Total: %d)\r\n", 
	    disadvantage_contribution, disadvantage_total);
  else
    fprintf(stdprn, 
            "Disadvatages: %5d (Max: %d)\r\n", 
	    disadvantage_total, ch_max_disadvantages); 
  fprintf(stdprn, "Base:         %5d\r\n", (ch_base));
  fprintf(stdprn, "-------------------\r\n" ); 	

  if (balance > 0)
    fprintf(stdprn, "Overspent:    %5d\r\n", (balance));
  else
    fprintf(stdprn, "Unspent:      %5d\r\n", (-balance));

// Stat Page
  // heading for stats columns
    fprintf(stdprn, "\nVal Characteristic  Base  Cost  Max  Pts");

  // show the stats

    look = ch_stats->next; // first ability in list;
    while (look != NULL)
    {
      look->print(stdprn);
      look = look->next;
    }

    fprintf(stdprn, "\r\n");
    print_subtotal_stats(stdprn);  // print the total
    fprintf(stdprn, "\r\n");

// Weapons Page
  fprintf(stdprn, "\r\nRanks  Field             @cost  pts  Notes");

  look = combat->next;
  while (look != NULL)
  {
    look->print(stdprn);
    look = look->next;
  }//while

  fprintf(stdprn, "\r\n\r\nTotal Combat Cost: %d\r\n", combat_total);

// Skill Page
  fprintf(stdprn, "\r\nVal  Skill             @cost  pts  Notes");

  look = skill_head->next;
  while (look != NULL)
  {
    look->print(stdprn);
    look = look->next;
  }//while

  fprintf(stdprn, "\r\n\r\nTotal Skill Cost: %d\r\n", (combat_total));

// Perks
  fprintf(stdprn, "\r\nPerk                  Pts");

  look = perk_head->next;
  while (look != NULL)
  {
    if (look != current)
    {
      look->print(stdprn);
    }
    look = look->next;
  }

  fprintf(stdprn, "\r\n\r\nTotal Perks: %d\r\n", (perk_total));

// Gear
  fprintf(stdprn, "\r\nGear                  Pts");

  look = gear_head->next;
  while (look != NULL)
  {
    if (look != current)
    {
      look->print(stdprn);
    }
    look = look->next;
  }

  fprintf(stdprn, "\r\n\r\nTotal Gear: %d\r\n", (gear_total));

// powers
  fprintf(stdprn, "\r\nPower                  Pts");

  look = power_head->next;
  while (look != NULL)
  {
    if (look != current)
    {
      look->print(stdprn);
    }
    look = look->next;
  }

  fprintf(stdprn, "\r\n\r\nTotal powers: %d\r\n", (power_total));


// Disadvantages
  fprintf(stdprn, "\r\nVal  Disadvantage");

  look = disadvantage_head->next;
  while (look != NULL)
  {
    if (look != current)
    {
      look->print(stdprn);
    }
    look = look->next;
  }
  if (disadvantage_total > ch_max_disadvantages)
    fprintf(stdprn, 
    "\r\n\r\nTotal points contributed (Max): %5d (Total disadvantages: %d)", 
    (disadvantage_contribution), (disadvantage_total));
  else
    fprintf(stdprn, 				   
    "\r\n\r\nTotal Points From Disadvantage: %d (Max: %d)\r\n",
    disadvantage_total, disadvantage_contribution);
}//print character


///////////////////////////////////////////////////////////////////////////
/////////         EDITING        //////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

char * character::dialog_title(char * field)
// returns a constructed title with Edit <ch_name>'s <field>
// calling function must delete returned string
{
  char * new_title;

  new_title = new_str(MAX_LINE);
  if (toupper(ch_name[(strlen(ch_name)-1)]) == 'S')
    sprintf(new_title, "Change %s' %s", ch_name, field);
  else
    sprintf(new_title, "Change %s's %s", ch_name, field);
  return new_title;	  
}// dialog title 	  
  		 	  
void character::edit_name()
{ 		 	  
  char *new_title;	  
  char *temp_name;	  
  vga_prompt *p;  	  
  	      	  	  
  // do the dialog	  
  blank_line(STATUS_LINE);
  gl_write(8, STATUS_LINE*8, "Enter the character's new name");
  p = new vga_prompt(8, PROMPT_LINE);
  temp_name = new_str(p->s); 
  delete p;   	      	     
	      	      	     
  // copy the new name to ch_name if it is not a cancel
  if (!is_empty(temp_name))  
  {	      	      	     
    delete ch_name;   	     
    debuf(&temp_name);	     
    ch_name = temp_name;     
	      	      	     
    delete file_name; 	      
    file_name = new_str(strlen(ch_name)+4); // 4 is the length of the extension
    strncpy(file_name, ch_name, strlen(ch_name));
    strncpy((file_name+strlen(ch_name)), ".chr", 4);
	      	      	     
    show();   	      	     
  }	      	      	     
  else	      	      	     
  {	      	      	     
    if (temp_name != NULL) delete temp_name; 
  }//fi	      	      	     
}// edit name 	      	     
	      	      	     
void character::edit_age()   
{	      	      	     
  char * field;	      	     		      
  vga_prompt *p;      	     
	      	      	     		      
  // make a working copy of the characters age
  field = set_field(ch_age, 30);
	      	      	     
  blank_line(STATUS_LINE);   
  gl_write(8, STATUS_LINE*8,"Enter new age");
  p = new vga_prompt(8, PROMPT_LINE);
  field = new_str(p->s);     
  delete p;	      	     
	   	      	     
  if (!is_empty(field))	     
  {	   	      	     
    sscanf(field, "%d", &ch_age);
    show();	      	   
  }	   	      
  delete field;	      
}//edit age	      
	   	      
void character::edit_race()
{	   	      
  char * temp_race;   
  vga_prompt *p;      
	   	      
  // make a working copy of the characters race
  temp_race = set_field(ch_race, 30);
	   	      
  // do the dialog    
  blank_line(STATUS_LINE);
  gl_write(8,STATUS_LINE*8,"Enter new race");
  p = new vga_prompt(8, PROMPT_LINE);
  temp_race = debuf(p->s);
  delete p;	      	  
  	   	      	  
  // copy the new race to ch_race if it is not a cancel
  if (temp_race != NULL)
  {		       	  
    delete ch_race;    	  
    ch_race = temp_race;  
    show();	       	  
  }		       	  
  else		       	  
  {		       	  
    delete temp_race;  	  
  }//fi		       	  
}// edit race	       	  
  		       	  
void character::edit_sex()
{ 		       	  
  char * temp_sex;     	  
  vga_prompt *p;       	  
  		       	 
  blank_line(STATUS_LINE);
  gl_write(8,STATUS_LINE*8,"Enter new sex.");
  p = new vga_prompt(8, PROMPT_LINE);
  temp_sex = debuf(p->s);	 // make a copy of the sex     
  delete p;	  
  if (temp_sex != NULL)
  {  		       
    delete ch_sex;     
    ch_sex = temp_sex; 
    show();
  }
  
}// edit sex	      	   
  		      	   
void character::edit_living()
{ 		      	   
  char * temp_living;	   
  vga_prompt *p; 	 
		 	   
  // make a working copy of the characters living
  temp_living = set_field(ch_living, 30);
		  	   
  // do the dialog	 
  blank_line(STATUS_LINE);
  gl_write(8,STATUS_LINE*8,"Enter new living");
  p = new vga_prompt(8, PROMPT_LINE);
  temp_living = new_str(p->s);
  delete p;	  	 
	   	  	 
  // copy the new living to ch_living if it is not a cancel
  if (!is_empty(temp_living))
  {	   	  	 
    delete ch_living;	 
    debuf(&temp_living); 
    ch_living = temp_living;
    show();		 
  }	   		 
  else	   		 
  {	   		 
    delete temp_living;	 	
  }//fi	   			
}// edit living			
				
void character::edit_base()	
{				
  char * field;	      	     		      
  vga_prompt *p;      	     	
	      	      	     		      
  // make a working copy of the characters age
  field = set_field(ch_base, 30);
	      	      	     	
  blank_line(STATUS_LINE);   	
  gl_write(8, STATUS_LINE*8,"Enter new base");
  p = new vga_prompt(8, PROMPT_LINE);	   
  field = debuf(p->s);			   
	   	      	     		   
  if (!is_empty(field))	     		   
  {	   	      	     		   
    sscanf(field, "%d", &ch_base);      	   
    show();	      	   
  }	   	      
  delete field;
  delete p;
}		
		
void character::edit_disadv()
{		   
  char * field;	      	     		      
  vga_prompt *p;      	     	
	      	      	     		      
  // make a working copy of the characters age
  field = set_field(ch_base, 30);
	      	      	     	
  blank_line(STATUS_LINE);   	
  gl_write(8, STATUS_LINE*8,"Enter new disadvantage maximum.");
  p = new vga_prompt(8, PROMPT_LINE);	   
  field = debuf(p->s);	     		   
	   	      	     		   
  if (!is_empty(field))	     		   
  {	   	      	     		   
    sscanf(field, "%d", &ch_max_disadvantages);      	   
    show();    	      	   
  }	       	      
  delete field;	   
  delete p;    	   
}// edit disadv max
	       
void character::edit_xp()
{	       
  char * field;	      	     		      
  vga_prompt *p;      	     	
	      	      	     		      
  // make a working copy of the characters age
  field = set_field(ch_base, 30);
	      	      	     	
  blank_line(STATUS_LINE);   	
  gl_write(8, STATUS_LINE*8,"Enter new experience total.");
  p = new vga_prompt(8, PROMPT_LINE);	   
  field = debuf(p->s);			   
	   	      	     		   
  if (!is_empty(field))	     		   
  {	   	      	     		   
    sscanf(field, "%d", &ch_experience);
    show();	      	      
  }	   	      	      
  delete field;		      
  delete p;		      
}// edit xp
	       		      
void character::bio_edit()
// edit character bio - not an object list of abilities
{	   
  switch (cury)
  {	   
    case (NAME_LINE):
    {	   
      edit_name();
      break;
    }	   
    case (AGE_LINE):
    {	   
      edit_age();
      break;
    }	   
    case (SEX_LINE):
    {	   
      edit_sex();	     
      break;		     
    }	   		     
    case (RACE_LINE):	     
    {	   		     
      edit_race();	     
      break;		     
    }	    		     
    case (LIVING_LINE):	     
    {	    		     
      edit_living();	     
      break;		     
    }	  
    case (DISADV_LINE):			    
    {
      edit_disadv();
      break;	    			 	   
    }	    	    		      	 	   
    case (BASE_LINE):
    {	    	    
      edit_base();
      break;				    
    }	    			      	    
    case (EXPERIENCE_LINE):		 	   
    {
      edit_xp();
      break;
    }	    			      
    default:		     	      
    {	    		     	      
      blank_line(STATUS_LINE);	      		     
      gl_printf(8,STATUS_LINE*8,"Bio Edit - cannot edit information on line %d.", cury);
      break;	   				      
    }	    					  
  }//switch 					  
}//bio edit 					  
	    
void character::edit(int cmd)
{	    
//  text_(contents_clr);
  switch (cmd)
  {	    
/* replaced with help in autochar.cpp
    case KEY_F1:
    {	    
      gl_write(8,8, "comment" );// coments on the characters construction. tk
      break;	   
    }	    	   
*/	    	   
    case KEY_F2:	   
    {	     	   
      if (save()) gl_write(8,8," Error Saving Chacter " );
      break; 
    }	     
	     
    case KEY_F3: 
    {	     
      if ( load() == 0 ) // success
      {	     
        cury = cur_y_default[page];
        current = field_default(page);
        show();
      } else {		
        gl_write(8,8," Error Loading Character ");
      }	     	     	
      break; 		
    }//load  		
	     		
    case KEY_CTRL_R:	
    case KEY_UP_ARROW:	
    {	     		
      if (page == TITLE_PAGE)
      {	     		
        if (cury > cur_y_default[TITLE_PAGE])
	{
       	  cury--;   		
          // check for blank lines to skip in cursor movement on the
          // title page. <TITLE_SKIP> is a const initalised at the top of
          // this file, so it can be altered any time.
          // strchr() is usually used to search for char in string but we use
          // it to find numbers in an array here.
          while (strchr(TITLE_SKIP, cury) != NULL)
            cury--;	
	}	 
	else // alread at top of page so wrap to bottom
	  cury = END_TITLE_PAGE;		       
      	     				       	       
        show(); // Would be optimal to redraw just the line we were on 
	        // and the one we go to but this is much easier 
      	break;				       
      }// title page			       
      if (current == NULL) break; // end of ability list
      if (current->prev != NULL && current->prev->prev != NULL) 
      // if not at the top line		       
      {	     	    			       
        // clear old highlight		       
        text_norm();			       
        current->show(lft + 2, cury);	       
	// move cursor 			       
        cury--;        			       
        current = current->prev;	       
        // draw new highlight  		       
        text_highlight();      		       
        current->show(lft + 2, cury);	       
      }	     	    			       
      break; 	    			       
    }// currsor up  			       
	     	    			       
    case KEY_DN_ARROW:			       
    {	     	    			       
      if (page == TITLE_PAGE)		       
      {	     	 			       
        if (cury == END_TITLE_PAGE)	       
	// end of page: wrap		       
	{  	 			       
       	  cury = cur_y_default[TITLE_PAGE];   	       
	}      	    			       
	else	    			       
	{	    			       
          cury++;   	 		       
          // check for blank lines to skip in cursor movement on the
          // title page. <TITLE_SKIP> is a const initalised at the top of
          // this file, so it can be altered any time.
          // strchr() is usually used to search for char in string but we use
          // it to find numbers in an array here.
          while (strchr(TITLE_SKIP, cury) != NULL)
            cury++; 
       	}	       
        show();	       
        break;	       
      }// title page   
	     	       
      if (current == NULL) break;
      if (current->next != NULL)
      {	     	       
        // clear highlighted line
        text_norm();   
        current->show(lft+2, cury);
        cury++;	       
        text_highlight();
        current = current->next;
        current->show(lft+2, cury);
      }	     	       
      break; 	       
    }// currsor down   	    
    
    ///////////// CHANGE POINTS / VALUE /////////////////// 
    
    case KEY_MINUS:
    // Subtract 1 from current value.
    {	     	       	      	     
      if (current != NULL) 
      {    
        current->change_value(-1);     
        show();
      }
      break; 	       	      	     	 
    } 	     	       	      	     	 
    case KEY_PLUS:     	      	     	 
    // Add 1 to current value.	     	 
    { 	     	       	       	       	 
      if (current != NULL) 
      {    	 
        current->change_value(1);
        show();
      }
      break;  	       		     	 
    } 	      	       		     	 
    case KEY_CTRL_MINUS:	     	 
    // Subtract 100 xp from current points.
    { 	      	       		     	  
      if (current != NULL) 
      {    	  
        current->change_pts(-100);
	show();
      }
      break;  	       		  	  
    } 	      	       		  	  
      	      	       		  	  
    case KEY_CTRL_PLUS:		  	  
    // Add 100xp to current points.	 
    { 	      	       		   	 
      if (current != NULL)
      {
	current->change_pts(100);
	show();
      }
      break;  	    		   	 
    } 	      	    		   	 
    case KEY_SHIFT_MINUS:
    // Subtract 10 xp from current points.
    { 
      if (current != NULL)
      {
      	current->change_pts(-10);
      	show();
      }
      break;
    } 
    case KEY_SHIFT_PLUS:
    // Add 10 xp to current points.
    { 		   
      if (current != NULL)
      {		   
      	current->change_pts(10);
      	show();
      }
      break;
    } 
      
    case KEY_ALT_MINUS:		   	 
    // Subtract 1 xp from current points.
    { 	      	    		   	
      if (current != NULL)
      {
	current->change_pts(-1);
	show();
      }
      break;	    		   	
    } 	    	    		   	
    case KEY_ALT_PLUS:		 	
    // Add 1xp to current points. 
    { 	     	    			
      if (current != NULL)
      {
	current->change_pts(1);
	show();
      }
      break; 	    
    } 	     	    
      	     	    
    case KEY_PGUP:  
    { 	     	    
      if (page == 1)
        page = PAGES;
      else	    	 
        page--;	    	 
      cury = cur_y_default[page];
      current = field_default(page);
      show();	    	 
      break; 	    	 
    } 	     	    	 
    case KEY_PGDN:  	 
    { 	     	    	 
      if (page == PAGES)
        page = 1;
      else     
        page++;	    	 
      cury = cur_y_default[page];
      current = field_default(page);
      show();  		 
      break;   		 
    } 	       		 
    case KEY_HOME:	 
    { 	       		 
      if (page == TITLE_PAGE) break;
      page = TITLE_PAGE;
      cury = cur_y_default[page];
      current = field_default(page);
      show();  
      break;   
    }// to title page
	       
    case (KEY_INS):			      
    {	       				      
      if (current == NULL) break; // stat or bio page, can't add to this 
      int success = 0;         	  // new ability added successfully 
      if (page == SKILL_PAGE)	  	      
      // skills - require entry of a base,    
      //          must treated differntly to other abilityies
      {	       	  			      
        char * new_base;  // to hold the input used to select a skill base 
	vga_prompt *p; 			      
	       	       			   
	text_norm();
	blank_line(STATUS_LINE);	 
       	gl_write(8, STATUS_LINE*8, "Enter Stat Base");
       	p = new vga_prompt(1, PROMPT_LINE);   
        new_base = debuf(p->s); 		      
	delete p;      	      		      
        if (is_empty(new_base)) break;// test for no input == user cancel
	       	       	     		      
	// make temp into a three letter stat code 
        str_upp(new_base); 	      
        if (strlen(new_base) > 3) 
          new_base[3] = '\0';     
	       	       	      
        // call with apropriat stat
        if (strcmp(new_base, "STR") == 0)
          success = (current->make_new(ch_str) == 0);
        else if (strcmp(new_base, "DEX") == 0)
          success = (current->make_new(ch_dex) == 0);
        else if (strcmp(new_base, "CON") == 0)
          success = (current->make_new(ch_con) == 0);
        else if (strcmp(new_base, "BOD") == 0)
          success = (current->make_new(ch_bod) == 0);
        else if (strcmp(new_base, "INT") == 0)
          success = (current->make_new(ch_int) == 0);
        else if (strcmp(new_base, "EGO") == 0) // old name for disipline
          success = (current->make_new(ch_dis) == 0);
        else if (strcmp(new_base, "DIS") == 0)
          success = (current->make_new(ch_dis) == 0);
        else if (strcmp(new_base, "PRE") == 0)
          success = (current->make_new(ch_pre) == 0);
        else if (strcmp(new_base, "COM") == 0)
          success = (current->make_new(ch_com) == 0);
        else   	       	      		 
        // default to general skill	 
          success = (current->make_new(NULL) == 0);
       	       	       	      		 
        delete new_base;   		 
      }// fi: skill 	 
      else // for any other ability    	 	      
        success = (current->make_new() == 0);
	       		      	   	 
      if (success)	      	   	 		
      // Whether to move to the newly created item, 
      // the old selected item or the
      // end of the list ??? staying at the end of the list is desirable if
      // entering a large number of abilityies and no sort function is 
      // available. May need to change this behaviour later 
      {	       		      	   	 		
        cury++;	       	       	   	 		
        show();	      	   	 
      }// fi: ability successfuly created	       	 	      	   
      break;   	 	      	   
    }// INS new ability    	 	      	   
	       	 	      
    case (KEY_DEL):	      
    {	       	 	      
      // abort if undeleteable
      if (current == NULL) break;
      if ((page == STAT_PAGE) || (current->prev == NULL))
        break;		      
	  		      
      if ((current != NULL) && (current->next != NULL))
      {	  		      
        current = current->next;
        delete current->prev; 
        show(); // must redraw the list to move others up
      }	  		      
	  		      
      break;		      
	  		      
    }//DEL		      
	  		      
    case (KEY_ENTER):	      
    {	  		      
    // abort if un-editable   
      if (page == TITLE_PAGE) 
      {	  		      
        bio_edit();	      
        break;		      
      }	  		      
      if ((current == NULL)       ||
          (current->prev == NULL) ||
          (current->next == NULL))
        break;		      
      current->edit();	      
      break;		      
    }// edit		      
	  		      
    case (KEY_CTRL_P):	      
    {	  		      
      print_character();      
      break;		      
    }	  		      
    case (' '):		      
    {	  		      
      show();		      	     
      break;		      	     
    }	  		      	     
       	  		      	     
    default:   	       	       	     
    {
      if ( (cmd >= 33) && (cmd < LAST_KEY) )
      // plain typing - TK activate edit / insert depending on location
      {   		       
	text_norm(); 
	blank_line(STATUS_LINE);
        gl_write(8,STATUS_LINE*8, "Insert and edit " );
      }		      
      break; 		      	     
    } 			      	     
  }//switch		      	     
}//edit			      
			      
			      
// end CHAR.CPP		      
			      

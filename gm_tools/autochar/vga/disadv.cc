// disadv.cpp -- disadvantages for char(acter)

#include <stdlib.h>
#include <stdio.h>

#include "disadv.h"
#include "sat.h"
#include "string.h"
#include "vga_prompt.h"

disadvantage::disadvantage(disadvantage * is_after, char * new_name, int new_pts) :
  ability(is_after, new_name, new_pts)
{
}

disadvantage::~disadvantage()
{
}

disadvantage::load(char * data)
{
  disadvantage * new_disadvantage;
  char * new_name;
  int new_pts;
  int new_cost; // place holder only

  sscanf(data, "%d%d", &new_pts, &new_cost);
  strtok(data, "\"");
  new_name = new_str(strtok(NULL, "\""));
  delete data;


  if (this->prev == NULL)
  // this is the dummy head node
    new_disadvantage = new disadvantage(this, new_name, new_pts);
  else
    new_disadvantage = new disadvantage((disadvantage*)prev, new_name, new_pts);

  delete new_name; // only a copy is taken by disadv constructor
  fprintf(stderr, "disadvantage loaded: %d %s %d\n",
	  new_disadvantage->pts_to_value(), new_disadvantage->name,
	  new_disadvantage->pts);
  return 0;					     
						     
}//load						     
						     
// virtual redefinitions			     
int disadvantage::pts_to_value()		     
{						     
  return pts;					     
}						     

void disadvantage::show(int x, int y)
{
 
  if (name == NULL)
    gl_write(8*x, 8*y, "----End of Disadvantages---");
  else		       
    gl_printf(8*x, 8*y, "%5d  %s", pts_to_value(), name);
}//show

void disadvantage::print(FILE *stdprn)
{
  if (name == NULL)
    fprintf(stdprn, "\r\n---End of Disadvantages---");
  else
    fprintf(stdprn, "\r\n%5d  %s", pts_to_value(), name);
}// print

int disadvantage::make_new(ability* based_on)
// returns non-zero on failure, eg user cancel
{
  char * new_name;
  int new_value; 
  disadvantage * new_disadvantage;
  vga_prompt *p;
	   
// get a name
  gl_write(8,8,"Enter a description of the disadvantage");
  p = new vga_prompt(8, PROMPT_LINE);
  new_name = new_str(p->s);
  delete p;   	       	 
  if (is_empty(new_name)) return 1;
	       	       
// get value   	       
  gl_write(8,8,"Enter the value of the disadvantage");
  p = new vga_prompt(8, PROMPT_LINE);
  if (is_empty(p->s)) return 1;	   
  sscanf(p->s, "%d", &new_value);  
  delete p;			   
  	   		   	   
// record new disadvantage 	   
  if (this->prev == NULL)  	   
  // first diasadvantage in list   
    new_disadvantage = new disadvantage(this, new_name, new_value);
  else	   		   	   
    new_disadvantage = new disadvantage((disadvantage*)this->prev, new_name, new_value);
	   		   	   
  return 0;		   	   
}//make new		   	   
			   	   
// end disadv.cpp	   	   
				   

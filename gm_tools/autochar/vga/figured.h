// figured.h - derived figured stats for char(acter)
#ifndef FIGURED_H
#define FIGURED_H

#include "ability.h"
#include "stat.h"
	       
class normal_def : public statistic
{      	       	
private:       	
  ability *base_stat;
protected:   
       	     
public:	     
  normal_def(ability * is_after, char* new_name, int new_pts = 0, 
	     ability * new_base_stat = NULL);
  ~normal_def();
  int get_base();
};	     
  	     
class spd : public statistic
{ 	     
private:     
  ability *dex;
protected:   
  	     
public:	     
  spd(ability * is_after, char* new_name, int new_pts, ability * new_dex);
  ~spd();    
  int get_base();
};
  
class rec : public statistic
// also used for mrc
{ 
private:
  ability * str;
  ability * con;
protected:
  
public:
  rec(ability * is_after, char* new_name, int new_pts, ability * new_str, ability * new_con);
  ~rec();
  int get_base();
};
  
class end : public statistic
// also used for man(a)
{ 
private:
  ability * con;
protected:
  
public:
  end(ability * is_after, char* new_name, int new_pts, ability * new_con);
  ~end();
  int get_base();
};
  
class stn : public statistic
{ 
private:
  ability * bod;
  ability * str;
  ability * con;
protected:
  
public:
  stn(ability * is_after, char* new_name, int new_pts, ability * new_bod, ability * new_str, ability * new_con);
  ~stn();
  int get_base();
};
  
class run : public statistic
{ 
private:
protected:
  
public:
  run(ability * is_after, char* new_name, int new_pts);
  ~run();
  int get_base();
}; 
   
class swm : public statistic
{  
private:
protected:
   
public:
  swm(ability * is_after, char* new_name, int new_pts);
  ~swm();
  int get_base();
};
  
#endif
// end - figured.h

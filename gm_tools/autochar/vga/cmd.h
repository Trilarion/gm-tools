// TECMD.H -- command processor header in Tiny Editor
// Copyright 1995, Martin L. Rinehart

#ifndef TRUE
#define TRUE  1
#define FALSE 0
#endif

int user_cmd();
int edit_cmd();
int plain_cmd();
int win_cmd();
int yn_cmd();

// end of TECMD.H

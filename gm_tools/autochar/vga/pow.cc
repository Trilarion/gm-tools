// perk.cpp -- perks for char(acter)
// Daniel Vale Jan 2001

#include <stdlib.h>
#include <stdio.h>

#include "disadv.h"
#include "sat.h"
#include "string.h"
#include "pow.h"
#include "vga_prompt.h"

power::power(power* is_after, char * new_name, int new_pts) :
  disadvantage(is_after, new_name, new_pts)
{
  debuf(&name);
}

power::load(char * data)
{
  power * new_power;
  char * new_name;
  int new_pts;
  int new_cost; // place holder only

  sscanf(data, "%d%d", &new_pts, &new_cost);
  (void) strtok(data, "\"");
  new_name = new_str(strtok(NULL, "\""));
  delete data;

  if (this->prev == NULL)
  // this is the dummy head node
    new_power = new power(this, new_name, new_pts);
  else
    new_power = new power((power*)prev, new_name, new_pts);

  delete new_name; // is reallocated for in constructor
  fprintf(stderr, "power loaded: %d %s %d %d\n",
	  new_power->pts_to_value(), new_power->name,
	  new_power->cost, new_power->pts);
  return 0;

}//load

power::~power()
{
}

// virtual redefinitions
void power::show(int x, int y)
{ 
  if (name == NULL)
    gl_printf(8*x, 8*y, "---End of powers---");
  else			
    gl_printf(8*x, 8*y, "%-20s  %d", name, pts);
}//show
       
void power::print(FILE *stdprn)
{      
  if (name == NULL)
    fprintf(stdprn, "\r\n--End of powers---");
  else 
    fprintf(stdprn, "\r\n%-20s  %d", name, pts);
}// print
       
int power::make_new(ability* based_on)
// returns non-zero on failure, eg user cancel
{      	   
  char * new_name;
  int new_value; 
  power* new_power;
  vga_prompt *p;
       	   	
// get a name
  text_norm();
  gl_write(8,8,"Enter a description of the power");
  p = new vga_prompt(8, PROMPT_LINE);
  if (is_empty(p->s)) return 1;
  new_name = new_str(p->s);
  delete p;	       
       	   	       
// get value		 
  
  blank_line(STATUS_LINE);
  gl_write(8,STATUS_LINE*8, "Enter the cost of the power");
  p = new vga_prompt(8, PROMPT_LINE);
  if (is_empty(p->s)) return 1;	   
  sscanf(p->s, "%d", &new_value);  
  delete p;			   
  	   			   
// record new power		   
  if (this->prev == NULL)	   
  // first power in list	   
    new_power= new power(this, new_name, new_value);
  else				   
    new_power= new power((power*)this->prev, new_name, new_value);
  				   
  return 0;			   
}//make new			   
  				   
// end - power.cpp		   
  				   
				   

// skill.h -- header for skills for char(acter)
#ifndef SKILL_H
#define SKILL_H

#include <stdio.h>

#include "ability.h"
#include "cost.h"

class skill : public ability
{
private:
protected:
  ability * base_stat;  // pointer to a stat to be used as the skill base

public:
  char * find_base; // used for character to assign an appropriate base stat

  skill(ability *isafter, char *new_name, int new_cost, ability * based_on, int new_pts = 0, char* new_find = NULL);
  ~skill();
  void show(int x, int y);
  void print(FILE *stdprn);
  int make_new();
  int make_new(ability * based_on);
  void set_base(ability * based_on);  // changes/sets the base 
  void save(FILE*out);
  int load(char * data);			 
#ifdef HERO  
  void change_value(int amt); 
  // overide virtual base function to go from stat based to 8-
#endif  
  int pts_to_value();
};
  
#endif
// end - skill.h

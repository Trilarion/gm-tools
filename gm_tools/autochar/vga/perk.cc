// perk.cpp -- perks for char(acter)
// Daniel Vale Jan 2001

#include <stdlib.h>
#include <stdio.h>

#include "disadv.h"
#include "sat.h"
#include "string.h"
#include "perk.h"
#include "vga_prompt.h"

perk::perk(perk* is_after, char * new_name, int new_pts) :
  disadvantage(is_after, new_name, new_pts)
{
  debuf(&name);
}

perk::load(char * data)
{
  perk * new_perk;
  char * new_name;
  int new_pts;
  int new_cost; // place holder only

  sscanf(data, "%d%d", &new_pts, &new_cost);
  strtok(data, "\"");                      // move to begining of name
  new_name = new_str(strtok(NULL, "\""));  // read in name

  delete data;	       
		       
  if (this->prev == NULL)
  // this is the dummy head node
    new_perk = new perk(this, new_name, new_pts);
  else		       
    new_perk = new perk((perk*)prev, new_name, new_pts);
		       
  delete new_name; // is reallocated for in constructor
  fprintf(stderr, "perk loaded: %d %s %d %d \n",     
   	  new_perk->pts_to_value(), new_perk->name, 
   	  new_perk->cost, new_perk->pts);
  return 0;
}//load

perk::~perk()
{
}

// virtual redefinitions
void perk::show(int x, int y)
{
  if (name == NULL)	
    gl_printf(8*x, 8*y, "---End of Perks---");
  else 			
    gl_printf(8*x, 8*y, "%-20s  %d", name, pts);
}//show

void perk::print(FILE *stdprn)
{
  if (name == NULL)    
    fprintf(stdprn, "\r\n--End of Perks---");
  else		       
    fprintf(stdprn, "\r\n%-20s  %d", name, pts);
}// print	       
		       
int perk::make_new(ability* based_on)
// returns non-zero on failure, eg user cancel
{		       
  char *new_name;      
  int new_value;       
  vga_prompt *sd;      
  perk *new_perk;      
      		       
// get a name	       
  new_name = new_str(30);
  text_norm();		  
  blank_line(STATUS_LINE);
  gl_printf(8,STATUS_LINE*8,"Enter a description of the perk: ");
  sd = new vga_prompt(1, PROMPT_LINE);
  new_name = new_str(sd->s);
  delete sd;	       	    
  if (is_empty(new_name)) return 1;
		       	   
// get value	       	   
  blank_line(STATUS_LINE);
  gl_printf(8,8,"Enter the cost of the perk");
  sd = new vga_prompt(1, PROMPT_LINE);
  if (is_empty(sd->s)) return 1;
  sscanf(sd->s, "%d", &new_value);
  delete sd;		  
		       	  
// record new perk     	  
  if (this->prev == NULL) 
  // first perk in list
    new_perk= new perk(this, new_name, new_value);
  else		       
    new_perk= new perk((perk*)this->prev, new_name, new_value);
	
  return 0;
}//make new
	
// end - perk.cpp
	

/* gear.cc -- equipment for character
 *
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "disadv.h"
#include "gear.h"
#include "common.h"
#include "prompt.h"
#include "sat.h"
#include "err_log.h"

gear::gear(gear* is_after, char * new_name, int new_pts, int new_price,
           int new_num/* = 0*/, int new_ready/* = 0*/) :
disadvantage(is_after, new_name, new_pts)
{
  price = new_price;
  num = new_num;
  ready = new_ready;
  debuf(&name);
}

void gear::save(FILE *out)
// all items are saved in the format:
// ready num price pts item_description
{
  if (name == NULL) return;
  log_event("gear::load - %s ready: %d num: %d price: %d pts: %d\n",
            name, ready, num, price, pts);
  fprintf(out, "%d %d %d %d \"%s\"\n", ready, num, price, pts, name);
}// save


int gear::load(char * data)
// all items are saved in the format:
// ready num price pts item_description
{
  gear * new_gear;
  char * new_name;
  int new_ready, new_num, new_price, new_pts;

  sscanf(data, "%d %d %d %d", &new_ready, &new_num, &new_price, &new_pts);
  (void) strtok(data, "\"");
  new_name = new_str(strtok(NULL, "\""));
  delete data;

  if (this->prev == NULL)
  // this is the dummy head node
    new_gear = new gear(this, new_name, new_pts, new_price, new_num, new_ready);
  else
    new_gear = new gear((gear*)prev, new_name, new_pts, new_price, new_num, new_ready);

  delete new_name; // is reallocated for in constructor
  log_event("gear::load - %s ready: %d num: %d price: %d pts: %d\n",
            new_gear->name, new_gear->ready, new_gear->num,
          new_gear->price, new_gear->pts);

  return 0;
}//load

gear::~gear()
{
  if (prev != NULL) prev->next = next;
  if (next != NULL) next->prev = prev;
  if (name != NULL) {
    delete name;
    name = NULL;
  }
}

void gear::make_ready()
{
  ready = TRUE;
}

void gear::make_unready()
{
  ready = FALSE;
}

int gear::is_ready()
{
  return ready;
}//is ready



// virtual redefinitions

void gear::show(int x, int y, dwindow *win, int options/* = 0*/)
{
  if (name == NULL)
  {
    dcur_move(x, y, win);
    dwrite(win, "---End of gear---");
  } else {
    dcur_move(x, y, win);
    if (options & SHOW_READY)
    {
      if (ready)
        dwrite(win, "* ");
      else
        dwrite(win, "  ");
    }
    dwrite(win, "%-20s  ", name);
    if (options & SHOW_PTS) dwrite(win, "%d ", pts);
    if (options & SHOW_PRICE) dwrite(win, "%d", price);

  }
}//show

void gear::print(FILE *stdprn)
{
  if (name == NULL)
    fprintf(stdprn, "\r\n--End of gear---");
  else
    fprintf(stdprn, "%-20s  %d", name, pts);
}// print

int gear::make_new(ability* based_on)
// returns non-zero on failure, eg user cancel
{
  char * new_name;
  int new_value;
  gear* new_gear;
  prompt *p;

// get a name
  say_prompt("Enter a description of the gear: ");
  p = new prompt();
  new_name = new_str(p->s);
  delete p;
  if (is_empty(new_name)) return 1;

// get value
  say_prompt("Enter the cost of the gear in xp: ");
  p = new prompt();
  if (is_empty(p->s)) return 1;
  sscanf(p->s, "%d", &new_value);
  delete p;

// record new gear
  if (this->prev == NULL)
  // first gear in list
    new_gear = new gear(this, new_name, new_value);
  else
    new_gear = new gear((gear*)this->prev, new_name, new_value);

  return 0;
}//make new

// end - gear.cpp




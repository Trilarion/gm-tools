/* w_rank.cc - weapon ranks for character
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */


#include <stdlib.h>
#include <stdio.h>
#include <string.h> 
#include <assert.h>

#include "w_rank.h"
#include "common.h"
#include "sat.h"  
#include "prompt.h"
#include "err_log.h"
		   
weapon_rank::weapon_rank(weapon_rank * is_after, const char * new_name, 
			 int new_cost, int new_pts) :
  ability(is_after, new_name, new_pts, new_cost)
{			    
// NOTE: order of params is different between w_rank and ability constructors  
} // weapon_rank constructor

weapon_rank::~weapon_rank()
{
  if (prev != NULL) prev->next = next;
  if (next != NULL) next->prev = prev;
  if (name != NULL) {
    delete name;
    name = NULL;
  }
  
}// weapon rank destructor

void weapon_rank::show(int x, int y, dwindow *win, int options)
{						 
  dcur_move(x, y, win);
  if (name == NULL)
    // dummy end node for inserting new last line
    dwrite(win, "---End of Combat Ranks---");
  else	       	       
    dwrite(win, "%5d  %-20s%3d%5d", (pts_to_value()), name, cost, pts);
}// show    
	    
void weapon_rank::print(FILE *stdprn)
{	    
  if (name == NULL) // dummy end node for inserting new last line
    fprintf(stdprn, "\r\n---End of Weapons---");
  else	    
    fprintf(stdprn, "\r\n%5d  %-20s%3d%5d", (pts_to_value()), name, cost, pts);
}// print   
	    
int weapon_rank::make_new(ability* based_on)
// creates a new weapon skill and iserts before current.
{	    	      	
  weapon_rank * new_weapon;
  char * temp;	      	
  char * new_name;;   				   
  int new_cost;	      	     			   
  prompt *p;	       	     			   
      	      	      	     			   
// get a name for the weapon skill
  say_prompt("Enter the weapon group: ");
  p = new prompt();   				    
  new_name = new_str(p->s);	   		    
  delete p;  	      		   		    
	    	      		   		    
  if (is_empty(new_name)) return 1;// test for no input == user cancel
  debuf(&new_name);   		   		    
	    	      		   		    
// get cost 	      		   		    
  say_prompt("Enter the weapon cost: ");
  p = new prompt();   				    
  if (is_empty(p->s))  	    			    
  {	  	      	    			    
    delete p;          	       	       	       	    
    return 1;	      	
  }	   	      	
	   	      	
  sscanf(p->s, "%d", &new_cost);
  delete p;	      	
		      	     	    
// create the new weapon skill	    
  if (prev == NULL)   	     	    
  // this is the first to be created
  {		      	     	    
    new_weapon = new weapon_rank(this, new_name, new_cost);
  }		      	     	    
  else		      	     	    
    new_weapon = new weapon_rank((weapon_rank*)prev, new_name, new_cost);
		      	     	    
  delete new_name;    
  return 0;	      
}//make new	      
		      
void weapon_rank::save(FILE*out)
		 
{		 
  ability::save(out);
}		      
		      
int weapon_rank::load(char * data)
{ 		      
  weapon_rank *new_weapon_rank;
  char *new_name, *tempstr;
  int new_pts;	      
  int new_cost;	      
  		      
  log_event("loading weapon\n");
  sscanf(data, "%d%d", &new_pts, &new_cost); // get points spent and cost
  tempstr = new_str(data);                   // a tempory is used as strtok must
                                             // be able to change the value of it
  strtok(tempstr, "\"");                     // move to the first quote
  new_name = new_str(strtok(NULL, "\"")); // copy to the second quote
  delete tempstr;                            // done with the tempory
  		      	    
  if (this->prev == NULL)   
  // called from the dummy head node
    new_weapon_rank = new weapon_rank(this, new_name, new_cost, new_pts);
  else		      	    
    new_weapon_rank = new weapon_rank((weapon_rank*)prev, new_name, new_cost, new_pts);
  		      	    
  delete new_name; // is reallocated for in constructor
  log_event(tempstr, "weapon rank loaded: %d %s %d %d \n",     
  	  new_weapon_rank->pts_to_value(), new_weapon_rank->name, 
  	  new_weapon_rank->cost,  new_weapon_rank->pts);
  // ensure data is deallocated elsewhere tk
  return 0;	 	      
} // load  	 	      
	   	 	      
// end - w_rank.cc	      
		 	      

#includeable makefile for character.o
#Daniel Vale MAY 2002

ifndef CHAR_MK
CHAR_MK = 1

CHAR_COMMON := $(INCLUDE_DIR)/dandefs.h $(DANIO_H) \
               $(STATUS_DIR)/status_line.h \
               $(DANIO_DIR)/keys.h

$(CHAR_DIR)/ability.o : $(CHAR_DIR)/ability.h \
      $(CHAR_DIR)/ability.cc $(CHAR_DIR)/cost.h $(CHAR_COMMON) \
            $(INCLUDE_DIR)/sat.h 
	g++ -c -o $(CHAR_DIR)/ability.o $(DIR_LIST) $(CHAR_DIR)/ability.cc

$(CHAR_DIR)/disadv.o : $(CHAR_DIR)/ability.h $(CHAR_DIR)/disadv.h \
      $(CHAR_DIR)/disadv.cc $(CHAR_COMMON) \
           $(INCLUDE_DIR)/sat.h \
           $(PROMPT_DIR)/prompt.h
	g++ -c -o $(CHAR_DIR)/disadv.o $(DIR_LIST) $(CHAR_DIR)/disadv.cc

$(CHAR_DIR)/figured.o : $(CHAR_DIR)/ability.h $(CHAR_DIR)/figured.h \
      $(CHAR_DIR)/figured.cc $(CHAR_DIR)/cost.h $(CHAR_DIR)/stat.h
	g++ -c -o $(CHAR_DIR)/figured.o $(DIR_LIST) $(CHAR_DIR)/figured.cc

$(CHAR_DIR)/gear.o : $(CHAR_COMMON) $(CHAR_DIR)/gear.h $(CHAR_DIR)/gear.cc \
      $(CHAR_DIR)/ability.h $(CHAR_DIR)/disadv.h \
         $(INCLUDE_DIR)/sat.h \
         $(PROMPT_DIR)/prompt.h 
	g++ -c -o $(CHAR_DIR)/gear.o $(DIR_LIST) $(CHAR_DIR)/gear.cc

$(CHAR_DIR)/help.o: $(CHAR_COMMON) $(CHAR_DIR)/help.h $(CHAR_DIR)/help.cc 
	g++ -c -o $(CHAR_DIR)/help.o $(DIR_LIST) $(CHAR_DIR)/help.cc

$(CHAR_DIR)/perk.o : $(CHAR_COMMON) $(CHAR_DIR)/perk.h $(CHAR_DIR)/perk.cc \
      $(CHAR_DIR)/ability.h $(CHAR_DIR)/disadv.h \
         $(INCLUDE_DIR)/sat.h $(PROMPT_DIR)/prompt.h \
         $(DANIO_DIR)/keys.h 
	g++ -c -o $(CHAR_DIR)/perk.o $(DIR_LIST) $(CHAR_DIR)/perk.cc

$(CHAR_DIR)/pow.o : $(CHAR_COMMON) $(CHAR_DIR)/pow.h $(CHAR_DIR)/pow.cc \
       $(CHAR_DIR)/disadv.h $(CHAR_DIR)/ability.h \
        $(INCLUDE_DIR)/sat.h $(PROMPT_DIR)/prompt.h \
        $(DANIO_DIR)/keys.h
	g++ -c -o $(CHAR_DIR)/pow.o $(DIR_LIST) $(CHAR_DIR)/pow.cc

$(CHAR_DIR)/skill.o : $(CHAR_COMMON) $(CHAR_DIR)/skill.h $(CHAR_DIR)/skill.cc \
      $(CHAR_DIR)/ability.h $(CHAR_DIR)/cost.h $(CHAR_DIR)/ability.h \
          $(PROMPT_DIR)/prompt.h $(DANIO_DIR)/keys.h
	g++ -c -o $(CHAR_DIR)/skill.o $(DIR_LIST) $(CHAR_DIR)/skill.cc

$(CHAR_DIR)/stat.o : $(CHAR_DIR)/stat.cc $(CHAR_DIR)/stat.h \
      $(CHAR_DIR)/ability.h $(INCLUDE_DIR)/sat.h
	g++ -c -o $(CHAR_DIR)/stat.o $(DIR_LIST) $(CHAR_DIR)/stat.cc

$(CHAR_DIR)/w_rank.o : $(CHAR_COMMON) $(CHAR_DIR)/w_rank.cc \
      $(CHAR_DIR)/w_rank.h $(CHAR_DIR)/ability.h \
      $(INCLUDE_DIR)/sat.h $(PROMPT_DIR)/prompt.h \
      $(DANIO_DIR)/keys.h
	g++ -c -o $(CHAR_DIR)/w_rank.o $(DIR_LIST) $(CHAR_DIR)/w_rank.cc

$(CHAR_DIR)/character.o : $(CHAR_COMMON) $(CHAR_DIR)/character.h \
      $(CHAR_DIR)/character.cc $(CHAR_DIR)/ability.h $(CHAR_DIR)/cost.h \
              $(CHAR_DIR)/figured.h $(CHAR_DIR)/stat.h \
              $(CHAR_DIR)/gear.h \
              $(PROMPT_DIR)/prompt.h 
	g++ -c -o $(CHAR_DIR)/character.o $(DIR_LIST) \
                  $(CHAR_DIR)/character.cc

$(CHAR_DIR)/bio.o : $(CHAR_DIR)/bio.h $(CHAR_DIR)/bio.cc
	g++ -c -o $(CHAR_DIR)/bio.o $(DIR_LIST) $(CHAR_DIR)/bio.cc

include $(STATUS_DIR)/status_line.mk
include $(DANIO_DIR)/../danio.mk


endif

/* pow.cc -- powers for character
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

   		   
#include <stdlib.h>
#include <stdio.h> 
#include <string.h>
   		   
#include "disadv.h"
#include "pow.h"
#include "common.h"
#include "prompt.h"
#include "sat.h"
#include "err_log.h"
		   
power::power(power* is_after, char * new_name, int new_pts) :
  disadvantage(is_after, new_name, new_pts)
{		   
  debuf(&name);	   
}		   
		   
int power::load(char * data)
{		   
  power * new_power;
  char * new_name; 
  int new_pts;	   
  int new_cost; // place holder only
		   
  sscanf(data, "%d%d", &new_pts, &new_cost);
  (void) strtok(data, "\"");
  new_name = new_str(strtok(NULL, "\""));
  delete data;	   
		   
  if (this->prev == NULL)
  // this is the dummy head node
    new_power = new power(this, new_name, new_pts);
  else		   
    new_power = new power((power*)prev, new_name, new_pts);
		   
  delete new_name; // is reallocated for in constructor
  log_event("power loaded: %d %s %d %d\n",
	  new_power->pts_to_value(), new_power->name,
	  new_power->cost, new_power->pts);
  return 0;	   
		   
}//load		   
		   
power::~power()	   
{      		   
  if (prev != NULL) prev->next = next;
  if (next != NULL) next->prev = prev;
  if (name != NULL) {
    delete name;
    name = NULL;
  }
  
}      		   
		   
// virtual redefinitions
void power::show(int x, int y, dwindow *win, int options)
{ 	    	   
  if (name == NULL)
  {	    	  
    dcur_move(x, y, win);
    dwrite(win, "---End of Powers---");
  } else {	  	    
    dcur_move(x, y, win);	    
    dwrite(win, "%-20s  %d", name, pts);
  }	    		    
}//show	    	   	    
       	    	   	    
void power::print(FILE *stdprn)
{      	    	   
  if (name == NULL)
    fprintf(stdprn, "\r\n--End of powers---");
  else 	    	   
    fprintf(stdprn, "\r\n%-20s  %d", name, pts);
}// print	   
       		   
int power::make_new(ability* based_on)
// returns non-zero on failure, eg user cancel
{      	   
  char * new_name;
  int new_value; 
  power* new_power;
  prompt *p;
       	   	
// get a name
  say_prompt("Enter a description of the power: ");
  p = new prompt();	 			
  if (is_empty(p->s)) return 1;			
  new_name = new_str(p->s);			
  delete p;	       	 			
       	   	       	 			
// get value		 			
  say_prompt("Enter the cost of the power: ");   	
  p = new prompt();
  if (is_empty(p->s)) return 1;	   
  sscanf(p->s, "%d", &new_value);  
  delete p;	 		   
  	   	 		   
// record new power		   
  if (this->prev == NULL)	   
  // first power in list	   
    new_power = new power(this, new_name, new_value);
  else	       	 		   
    new_power = new power((power*)this->prev, new_name, new_value);
  		 		   
  return 0;	 		   
}//make new	 		   
  		 		   
// end - power.cc
  		 		   
		 		   
		 

/* skill.cc -- skills for character
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

   		       
#include <stdlib.h>    
#include <assert.h>    
#include <stdio.h>     
#include <string.h>    
   		       
#include "skill.h"     
#include "ability.h"
#include "cost.h"
#include "common.h"
#include "prompt.h"
#include "err_log.h"
   	   		    
skill::skill(ability *isafter, char *new_name, int new_cost, 
   	     ability * based_on, int new_pts, char* new_find) :
  ability(isafter, new_name, new_pts, new_cost)
{			    	      
  if (based_on != NULL)
    log_event("skill::constructor - based on: %s, find_base: %s\n",
       	      based_on->name, new_find);
  else	      
    log_event("skill::constructor - based on: general, find_base: %s\n",
       	      new_find);
  base_stat = based_on;	    
  find_base = new_str(new_find);
}// skill constructor 	   		    
	   	  	    
skill::~skill()	  
{      		  
  log_event("begin skill::destructor\n");
  log_event("skill::destructor name %s\n", name);
  log_event("skill::destructor prev: %d, next: %d\n", prev, next);
  if (prev != NULL) prev->next = next;
  if (next != NULL) next->prev = prev;
  log_event("skill::destructor prev: %d, next: %d\n", prev, next);
  if (name != NULL) {
    delete name;
    name = NULL;
  }
  
  log_event("end skill::destructor\n");
} // ~skill	   
	   	  
void skill::set_base(ability * based_on)
// changes/sets the base	 
{	     
//  assert(based_on != NULL);
  base_stat = based_on;
  if (base_stat)
    log_event("skill base set: %d %s %s %d %d\n",	
              pts_to_value(), name,	 
              base_stat->name, cost, pts);
  else	     
    log_event("skill base set: %d %s General %d %d\n",	
              pts_to_value(), name,	 
              cost, pts);
} // set base  		   		 
	       	 		 
	       	 	
int skill::make_new(ability * based_on)
// make a new skill based on a stat
{	       	 	
  skill * new_skill = NULL;	
  char* new_name = NULL;		     
  int new_cost;	 		     
  prompt *p;   		     
	      	   
  if (based_on == NULL)
    log_event("skill::make_new - based on general\n");
  else	     				      
    log_event("skill::make_new - based on %s\n", based_on->name);
      	  
  say_prompt("Enter the name of the new skill: ");
  p = new prompt();					     
  if (is_empty(p->s))    	     			     
  {   		   
    delete p;	   
    return errUSER_CANCEL;// test for no input == user cancel	     
  }   	     	   	     	     			     
  new_name = new_str(p->s);  	     			     
  delete p;  		 
  log_event("skill::make_new - new_name %s\n", new_name);
					    
  say_prompt("Is this a background skill? ");  
  p = new prompt();	 	       	    		     
  if (is_empty(p->s)) 	 	     	    	       
  {    	     		 	     	    	       
    delete p;				    
    return errUSER_CANCEL;		    	     		       
  }   	     			     	    	       
  if (toupper(p->s[0]) == 'Y')		    
  {					    
    log_event("skill::make_new - background skill\n");
    new_cost = cBSK;  		     	    	       
  } else {		      
    log_event("skill::make_new - non-background skill\n");
    new_cost = cSKL;		     		       	  
  }
  delete p;   	    		     		       
      	     			     		       
  if (this->prev != NULL)	     		       
    // isert before current skill (after previous skill)
    new_skill = new skill(this->prev, new_name, new_cost, based_on/*, (char*) based_on->name*/); 	   
  else	     	    		     		       	    	    		     
    // insert first skill in list (this is the header node) 	    		     
    new_skill = new skill(this, new_name, new_cost, based_on/*, (char*) based_on->name*/);
	     					       
  delete new_name;   // clean up		       
  return 0;  					       
}// make new 					       
	     					       
int skill::load(char * data)			       
{ 	   					       
  skill *new_skill;				       
  char * new_name;				       
  char * base_data;				       
  int new_pts;					       
  int new_cost;					       
  	   					       
  // get pts					       
  sscanf(data, "%d%d", &new_pts, &new_cost);	       
  (void) strtok(data, "\"");  // find the begining of new_name
  new_name = new_str(strtok(NULL, "\"")); // get new_name
  base_data = new_str(strtok(NULL, "\"")); // get base_data
  log_event("skill::load base_data: %s\n", base_data);
  delete data;			   		       
	    			   		       
  // prepare base_data for stat search		       
  debuf(&base_data);    // remove whitespace	       
  str_upp(base_data);    // standardis in upper case   
  base_data[3] = '\0';  // shorten to three letter code
	    			   		       
  // NB: base_stat is set from character after the load is compleated
	    			   		       
  if (this->prev == NULL)	   		       
  // this is the dummy head node   		       
    new_skill = new skill(this, new_name, new_cost, NULL, new_pts, base_data);
  else	    			   		       
    new_skill = new skill((skill*)prev, new_name, new_cost, NULL, new_pts, base_data);
	   			   		       
  delete base_data;		   		       
  delete new_name; // is reallocated for in constructor
  log_event("skill loaded: %d %s %d %d \n",     
	  new_skill->pts_to_value(), new_skill->name, 
	  new_skill->cost,  new_skill->pts);
  return SUCCESS; 					       
}//load	    					       
	    					       
int skill::pts_to_value()			       
{	    					       
#ifdef HERO 					       
  int temp = 0;	  	 		       	       
  int stat_base;				       
  int base; 					       
  	    					       
  if (pts < 10) return 0;			       		   
  if (pts < 30) return 8;			       		   
	    	 				       
  if (base_stat != NULL)			       
  {	    	 				       
    stat_base = 9 + (int) ((float) base_stat->pts_to_value() / 5.0 + 0.5);
    base = ((stat_base > 11) ? stat_base : 11);	       
  }	    	 				       
  else base = 11;				       
  	      	  	 			       		   
  temp = base + ((pts - 30) / cost);		       
  return temp;	 				       
  	    	 				       
#else 	    	 				       
  int temp; 	 				       
  	    	 				       
  if (pts < 10) return 0;			       
  temp = 30;	 				       
  temp += base_stat->pts_to_value(); 		       
  temp += (pts - 10) / cost;	     		       
#endif      	 		     		       
}// pts_to_value - hero's version    		       
      				     		       
void skill::show(int x, int y, dwindow *win, int options)
{     	    				   
  dcur_move(x, y, win);
  if (name != NULL)  
  {   	     	     
    if (base_stat != NULL)
      dwrite(win, "%3d  %14s%12s%4d%5d", 
      	     pts_to_value(), name, base_stat->name, cost, pts);
    else       	       	       	  
      dwrite(win, "%3d  %14s     General%4d%5d", 
      	     pts_to_value(), name, cost, pts);
  } else	     			  
    dwrite(win, "---End of Skills---");
} // skill show	  
      	    	  
void skill::print(FILE *stdprn)
{     	    	  
  char *show_pts, *show_cost;
      		 
  if (name != NULL)
    fprintf(stdprn, "\r\n%3d  %s%d  %d", pts_to_value(), name, cost, pts);
  else
    fprintf(stdprn, "\r\n---End of Skills---");
}//print	 
      		 
void skill::save(FILE * out)
      	    	 
{     	    	 
  if (name != NULL)
  {   	    	 
    ability::save(out);
    fseek(out, -1, SEEK_CUR); // go back 1 byte, before \n
    if (base_stat != NULL)
      fprintf(out, " %s\n", base_stat->name);
    else    	     
      fprintf(out, " GENERAL\n");
  }	    
	    
}// save    
  	    
#ifdef HERO 
void skill::change_value(int amt)
// overide virtual base function to go from stat based to 8-
{      	    
  int value, new_val;
  	   
  assert (amt); // amt is non-zero
  if (name == NULL) return; // skip dummy node;
    	   	    
  value = pts_to_value(); // don't want to keep recalculating it here
    	   
  if ((value == 0) && (amt < 0)) return; // dont go < 0 value
  
  new_val = value + amt; // change the value by the amount req 
				    
  if ((amt < 0) && (pts >= 30) && (pts < (30 + cost)))
  // this is the point of overiding the base class, we don't want skills to 
  // stop at stat based, we want to go down to fam, even though this is 
  // likely less than indicated by amt 
  {  	     	    			  	   
    new_val = 8;    			  
    change_pts(-20); // we need to dump at least this much   
  }  	       				      
      	       				      
  if (amt > 0) 				      
  // go up 
  { 	     
    while (pts_to_value() < new_val)
      change_pts(1);
  } 	     
  else     
  // go down 
  { 	     
    while (pts_to_value() > (new_val-1))
      change_pts(-1);
    change_pts(1);  // the overshoot is so we get _all_ the points back
  } 	     
}//change value
#endif	     
     	     
     	     
// end - skill.cpp
	   
	   

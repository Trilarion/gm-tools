/* figured.cc - figured stats for character
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

#include <stdlib.h>
		 
#include "figured.h"
#include "cost.h"
      		 
int normal_def::get_base()
{ 
#ifdef HERO				  
  // round(base / 5)			  
  int temp;				  
  temp = (int) ((float) base_stat->pts_to_value() / 5.0 + 0.5);
  return temp; 				  
#else 					  
  return 0;
#endif  
}     		 
      		 
normal_def::normal_def(ability * is_after, char* new_name, int new_pts, 
      		       ability * new_base_stat)
  : statistic(is_after, new_name, new_pts, cPD, mPD)
{     		       	   		      
  base_stat = new_base_stat;       	      
}     	   	       			      
      	   	       			      
int spd::get_base()    			      
{
#ifdef HERO
  return 1 + dex->pts_to_value() / 10;
#else 				  
  return dex->pts_to_value() + 30;
#endif  
}		       			      
		       			      
spd::spd(ability * is_after, char* new_name, int new_pts, ability * new_dex)
  : statistic(is_after, new_name, new_pts, cSPD, mSPD)
{  		       			      
  dex = new_dex;       			      
}  	   

#ifdef HERO   		       			      
int rec::get_base()    			      
{  		       
//round(str/5) + round(con/5)
  int temp;	       
  temp = (int) ((float) str->pts_to_value() / 5.0 + 0.5);
  temp += (int) ((float) con->pts_to_value() / 5.0 + 0.5);
  return temp;	 
}  
   
rec::rec(ability * is_after, char* new_name, int new_pts, ability * new_str, ability * new_con)
  : statistic(is_after, new_name, new_pts, cREC, mREC)
{  
  str = new_str;
  con = new_con;
}  

rec::~rec() {}

#endif

int end::get_base()
{  
#ifdef HERO
  return con->pts_to_value() * 2;
#else
  return con->pts_to_value() * 4;
#endif  
}     
      
end::end(ability * is_after, char* new_name, int new_pts, ability * new_con)
  : statistic(is_after, new_name, new_pts, cEND, mEND)
{     
  con = new_con;
}     
      
int stn::get_base()
{     
#ifdef HERO  
  //bod + round(str/2) + round(con/2)
  int temp;  		     
  temp = bod->pts_to_value();
  temp += (int) ((float) str->pts_to_value() / 2.0 + 0.5);
  temp += (int) ((float) con->pts_to_value() / 2.0 + 0.5);
  return temp; 		     
#else			     
  // bod * 2 + str + con     
  int temp;  		     
  temp = bod->pts_to_value() *2;
  temp += str->pts_to_value();
  temp += con->pts_to_value();
  return temp; 
#endif       
}     
      
stn::stn(ability * is_after, char* new_name, int new_pts, ability * new_bod, ability * new_str, ability * new_con)
  : statistic(is_after, new_name, new_pts, cSTN, mSTN)
{     
  bod = new_bod;
  str = new_str;
  con = new_con;
}     

int run::get_base()
{ 
#ifdef HERO
  return 6;
#else   
  return 20;
#endif  
}     
      
run::run(ability * is_after, char* new_name, int new_pts)
  : statistic(is_after, new_name, new_pts, cRUN, mRUN)
{     
}     
      
int swm::get_base()
{     
#ifdef HERO
  return 2;
#else  
  return 5;
#endif  
}     
      
swm::swm(ability * is_after, char* new_name, int new_pts)
  : statistic(is_after, new_name, new_pts, cSWM, mSWM)
{     
}     
      
swm::~swm() {}
run::~run() {}
stn::~stn() {}
end::~end() {}
spd::~spd() {}
normal_def::~normal_def () {}

// end figured.cpp

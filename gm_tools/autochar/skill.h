/* skill.h -- header for skills for character
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

#ifndef SKILL_H
#define SKILL_H

#include <stdio.h>

#include "ability.h"
#include "cost.h"

class skill : public ability
{
private:
protected:
  ability * base_stat;  // pointer to a stat to be used as the skill base
      
public:
  char * find_base; 
   // used during the loading of a character to store the data from the chr
   // file during the ability::load sequence, the character then calls
   // its get_base() rutine with the skill::find_base data to get the base and
   // pass it down to skill::set_base() rutine. This is all neccessary because
   // the skill cannot refer to the character to get 
      
  skill(ability *isafter, char *new_name, int new_cost, 
      	ability * based_on, int new_pts = 0, char* new_find = NULL);
  ~skill();
  void show(int x, int y, dwindow *win = NULL, int options = 0);
  void print(FILE *stdprn);
  int make_new(ability * based_on);
  void set_base(ability * based_on);  // changes/sets the base 
  void save(FILE*out);
  int load(char * data);			 
#ifdef HERO  
  void change_value(int amt); 
  // overide virtual base function to go from stat based to 8-
#endif  
  int pts_to_value();
};    
      
#endif
// end - skill.h
      

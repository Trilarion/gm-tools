/* figured.h - derived figured stats for character
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

#ifndef FIGURED_H
#define FIGURED_H

#include "ability.h"
#include "stat.h"
#include "cost.h"

class normal_def : public statistic
{      	       	
private:       	
  ability *base_stat;
protected:   
       	     
public:	     
  normal_def(ability * is_after, char* new_name, int new_pts = 0, 
	     ability * new_base_stat = NULL);
  ~normal_def();
  int get_base();
};	     
  	     
class spd : public statistic
{ 	     
private:     
  ability *dex;
protected:   
  	     
public:	     
  spd(ability * is_after, char* new_name, int new_pts, ability * new_dex);
  ~spd();    
  int get_base();
};

#ifdef HERO  
class rec : public statistic
// also used for mrc
{ 
private:
  ability * str;
  ability * con;
protected:
  
public:
  rec(ability * is_after, char* new_name, int new_pts, ability * new_str, ability * new_con);
  ~rec();
  int get_base();
};
#endif

class end : public statistic
// also used for man(a)
{ 
private:
  ability * con;
protected:
  
public:
  end(ability * is_after, char* new_name, int new_pts, ability * new_con);
  ~end();
  int get_base();
};
  
class stn : public statistic
{ 
private:
  ability * bod;
  ability * str;
  ability * con;
protected:
  
public:
  stn(ability * is_after, char* new_name, int new_pts, ability * new_bod, ability * new_str, ability * new_con);
  ~stn();
  int get_base();
};
  
class run : public statistic
{ 
private:
protected:
  
public:
  run(ability * is_after, char* new_name, int new_pts);
  ~run();
  int get_base();
}; 
   
class swm : public statistic
{  
private:
protected:
   
public:
  swm(ability * is_after, char* new_name, int new_pts);
  ~swm();
  int get_base();
};
  
#endif
// end - figured.h

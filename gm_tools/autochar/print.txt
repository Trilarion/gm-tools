Name: Lyza
Age: 19
Race: Human
Sex: Female
Living: Ranger
Stat Cost:     1070
Combat Cost:    210
Skill Cost:     210
Perk Cost:        0
Gear Cost:        0
Power Cost:       0
-------------------
Total:         1490
Disadvatages:   300 (Max: 1000)
Base:          1500
-------------------
Unspent:        310

Val Characteristic  Base  Cost  Max  Pts
 14 Strength          10    10   20   40
 23 Dexterity         10    30   20  480
 13 Constitution      10    20   20   60
 14 Body              10    20   20   80
 13 Intelegence       10    10   20   30
 13 Ego               10    20   20   60
 13 Presence          10    10   20   30
 10 Comliness         10     5   20    0
  8 PD                 3    10    8   50
  4 ED                 3    10    8   10
  4 Speed              3   100    4  100
 30 Endurance         26     5   50   20
 30 Stun              28    10   50   20
  7 Recovery           6    20   10   20
 26 Mana              26     5   50    0
  6 Mana Recovery      6    20   10    0
  8 Running            6    20   10   40
  5 Swimming           2    10    5   30
          Total Statistics Costs:   1070     


Ranks  Field             @cost  pts  Notes
    1  fam com melee        20   20
    1  fam com missile      20   20
    1  fam staff            10   10
    4  Rng ofset Bows       20   80
    4  OCV with bows        20   80
---End of Weapons---

Total Combat Cost: 210

Val  Skill             @cost  pts  Notes
 14  Stealth             20  30
 12  Tracking            20  30
 12  Paramedic           20  30
  8  Riding              20  10
  8  Conversation        20  10
  8  Traps               20  10
 12  Survival            20  30
 12  AK Local Area       10  30
  8  Animal Handler      20  10
  8  PS Ranger           10  10
  8  fam Game Animals    10  10
---End of Skills---

Total Skill Cost: 210

Perk                  Pts
--End of Perks---

Total Perks: 0

Gear                  PtsBow                   0Sling                 0Leather Armour        0
--End of gear---

Total Gear: 0

Power                  Pts
--End of powers---

Total powers: 0

Val  Disadvantage
  100  Hunted by Poachers  
   50  Wanderlust          
  100  Greed               
   50  Reputation 8-       

Total Points From Disadvantage: 300 (Max: 300)

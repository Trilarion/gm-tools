/* cost.h - costs of abilities for character
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */
      
#ifndef COST_H
#define COST_H

#define HERO // comment this out for Frame rules 
// NB: costs are in xp (= 1/10 HERO cp)

#ifdef HERO    
/////////////////////////// Hero costs ////////////////////// 
#define cSTR 10
#define cDEX 30
#define cCON 20
#define cBOD 20
#define cBODY 20
#define cINT 10
#define cDIS 20
#define cPRE 10
#define cCOM  5
      	      
#define cPD  10		   
#define cED  10 
#define cSPD 100 
#define cEND  5	       	   
#define cSTN 10	
#define cSTUN 10    // for when i'm feeling verbose	
#define cREC 20
#define cMAN  5
#define cMRC  20
#define cRUN 20 	   	   
#define cSWM 10 	   	   
      	       	   	   
#define cPER 30 // Perception
#define cSKL 20 // cost to increase most skills
#define cBSK 10 // cost to increase background skills
      	       	    	    
// maximums    	    	    
#define mPD  8	    	    
#define mED  8	    
#define mSPD 4 
#define mEND 50    	    
#define mSTN 50
#define mREC 10
#define mMAN 50
#define mMRC 10
#define mRUN 10	    	    
#define mSWM  5    	    


#else	       	    
///////////////////// Frame costs ////////////////////////////
	      	    
#define cSTR 10	    
#define cDEX 30	    
#define cCON 20	    
#define cBOD 20
#define cBODY 20
#define cINT 10
#define cTECH 10 
#define cDIS 20
#define cPRE 10
#define cCOM  5
      	      
#define cPD  5		   
#define cED  5
#define cMD  5
#define cSPD 5 // Rediness 
#define cEND  5	       	   
#define cSTN 10	   	   
#define cSTUN 10	   	   
#define cMAN  5	   	   
#define cRUN 5	   	   
#define cSWM 2	   	   
      		   	   
#define cPER 2 // Perception
#define cSKL 2 // cost to increase most skills
#define cBSK 1 // cost to increase background skills
      		   	    
// maximums	   	    
#define mPD  10	   	    
#define mED  10	   
#define mMD  10	
#define mSPD 100 // reaction
#define mEND 100   	    
#define mSTN 100   	    
#define mMAN 100   	    
#define mRUN 30	   	    
#define mSWM  15   	    
      		   
#endif // Hero / Frame
#endif // already included	       	      	    
// end - cost.h	   	    
      		   

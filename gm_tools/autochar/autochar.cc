/* autochar.cc - automated character sheet
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

   
#include <string.h>	 	
#include <stdio.h>	 	
#include <stdlib.h>    	 	
#include <time.h>
   
#include "character.h"
#include "common.h"
//#include "cmd.h"   
#include "sat.h"	    
#include "prompt.h"
#include "err_log.h"

#define PROMPT_LEN 20 
#define PROMPT_COLOUR 7

// prompt states
#define pMAIN 1

void show_time(time_t *real_time)
{      				
  // show time			
  time(real_time); 	 	
  setpcolour(STATUS_COLOUR);	
  dcur_move(TIME_X, STATUS_LINE); 	
  dwrite(ctime(real_time));	
//  drefresh(); 	   		
} // show time			
	  
void handle_user_selection(prompt *main_prompt, character *dummy, 
	  		   int prompt_state)
// process user inputs					   
{ 	  
  int err = SUCCESS;
  	  	   
  switch (prompt_state)
  {	  	   
    case pMAIN:	   
    {	     	   
      switch(main_prompt->key)
      {	  	   
  	case danioKEY_F1:
  	{ 	     
  	  dummy->help();
       	  break;   
	} 	     
        case danioKEY_F2:      	    
        { 	     	       	    
          if (dummy->save())
          { 	     
            say_status("Error saving chacter");
       	    log_event("Save error\n");
          }	     
          break;     	   	    
        } 	     	   	    
          	     		    
        case danioKEY_F3:       	  
        { 	     		  
          if ( dummy->load() == SUCCESS )
          {	      		  
            dummy->show();   	
          } else {   		
            say_status("Error loading character");
            log_event("Error loading character\n");
          }	     	     	
          break;     	     	
        }//load      	     	
    	     	     	
        case danioKEY_UP_ARROW:	
        { 	   
	  dummy->cursor_up();
	  dummy->show();
       	  break;       
        }// currsor up  			       
      	     	       			       
        case danioKEY_DOWN_ARROW:			       
        { 	       
	  dummy->cursor_down();
	  dummy->show();
	  break;       
        }// currsor down       	    
        case danioKEY_PGUP:  
        {	       	   
          dummy->change_page(-1);
          dummy->show();      	    	 
          break;       	    	 
        } 	       	    	 
        case danioKEY_PGDN:  	 
        {	       	    
          dummy->change_page(+1);
          dummy->show();      	   	 
          break;       	   	 
        } 	       	   	 
        case danioKEY_HOME: 	 
        {  	       	   
          err = dummy->change_page_to(TITLE_PAGE);
          dummy->show();      	
          break;       	
        }// to title page
           	       	
        case (danioKEY_INS):			      
        {  	       
          dummy->cursor_make_new();
          break;       
        }// INS new ability    	 	      	   
           	       	 	      
        case (danioKEY_DEL):	      
        {  	       
          err = dummy->delete_current();
          break; 		      
        }//DEL 	 	      
           	   		      
        case (danioKEY_ENTER):	      
        {  	 
          err = dummy->cursor_edit();
          break; 		      
        }// edit 		      
                                                                                                                                                                                                                                                                                                                                    	
        ///////////// CHANGE POINTS / VALUE /////////////////// 
          	   	 
        case danioKEY_MINUS:
        // Subtract 1 from current value.
        {      	     	       	      	     
          dummy->change_current_value(-1);     
          dummy->show();    	  
          break;   	       	      	     	 
        }  	     	       	      	     	 
        case danioKEY_PLUS:     	      	     	 
        // Add 1 to current value.	     	 
        {  	     	       	       	       	 
          dummy->change_current_value(1);
          dummy->show();	
          break;   	       		     	 
        }     	 
	      	 
        case (danioKEY_CTRL_P):	      
        { 	   		      
          dummy->print_character();      
          break; 		      
        }  	   		      
        case (' '):		      
        {  	   		      
          dummy->show();		      	     
          break;		      	     
        }  	   		      	     
	  
	default:
          dwrite("Key pressed: %d\n", main_prompt->key);
          //err = dummy->edit(main_prompt->key);
  	      	
      }	// switch: main_prompt->key
    	      	 	  
    }// case: pMAIN  	  
    	      		  
  }// switch: prompt state
       	  	
  if (err == errUSER_CANCEL)
    say_status("Edit canceled.");
} // handle user selection
	  
	  
	  
void init()
{
  int err;

  start_log(); // initilize err log
  err = init_danio();		
  if (err)			
  {    				
    fprintf(stderr, "init() - init_danio() err: %d\n", err);
    log_event("init() - init_danio() err: %d\n", err);
  } else 	      	    	
    log_event("init() - init_danio() ok\n");
       		     	      	   
  dcur(CURSOR_INVIS); // cursor invisible    		    
  log_event("init() - cursor off\n");
}// init

void setup(character **dummy, prompt **main_prompt)
{		      
  // create the character 	      	 	    
  *dummy = new character();	      	 	    
  log_event("main - New character created.\n");	    
  (*dummy)->show();       	   	      	  	    
	  	     
  // setup the prompt
  *main_prompt = new prompt(PROMPT_LEN, NULL, PROMPT_COLOUR, NO_AUTHORITY);
  dgetch_wait(0);  // prevent dgetch() from stopping flow
}// setup
   
void intro()
{  
  say_status("[ESC] to quit.   [F1] for help.");    
} // intro		 
   			 
   			 
       			 	
int main()	      	 	      		    
{      		      	 	 
  class character *dummy; 	      		    
  prompt *main_prompt;
  int prompt_state = pMAIN;
  time_t *real_time = new time_t;
       	  	      	 	 
  init();		 
  setup(&dummy, &main_prompt);
  intro();		 
       			 	
  // input loop	       	       			    
  main_prompt->get_ed_cmd();	
  while (main_prompt->key != danioKEY_ESC)
  {    	       	      	 	    
    if (main_prompt->key != -1)	    
    {				    
      log_event("main - command: %d\n", main_prompt->key);
      handle_user_selection(main_prompt, dummy, prompt_state);
    }				    
     				    
    main_prompt->get_ed_cmd();	    
    // show_time(real_time); 	    
  } // while: main_prompt->key != danioKEY_ESC 	       	 
      	     	     	 	
  // clean up
  log_event("Call character deletion.\n");
  delete dummy;	    	 	
  if (main_prompt != NULL) delete main_prompt;
  close_danio();    	   	
  end_log();   	    	 	

  return 0;
}// main     	       		
     	     	       		
     	     	    
     		    
		    

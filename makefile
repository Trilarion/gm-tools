# makefile - make everything related to gm-tools

# define the root of the source tree
SOURCE_DIR := .

#include this file, which tells make where to find everything else
include $(SOURCE_DIR)/include/map.mk

# make everything
ifeq ($(OS),Windows_NT)

all : $(ADV_DIR)/adv_game 
# saving these for latter while debugging
# $(CHAR_DIR)/autochar $(COMBAT_DIR)/combat $(RAN_ADV_DIR)/ran_adv
	cp $(ADV_DIR)/adv_game.exe ./
#	cp $(CHAR_DIR)/autochar.exe ./
#	cp $(COMBAT_DIR)/combat.exe ./
#	cp $(RAN_ADV_DIR)/ran_adv.exe ./

else

all : $(ADV_DIR)/adv_game $(CHAR_DIR)/autochar $(COMBAT_DIR)/combat $(RAN_ADV_DIR)/ran_adv $(DANIO_DIR)/../colours
	cp $(ADV_DIR)/adv_game ./
	cp $(CHAR_DIR)/autochar ./
	cp $(COMBAT_DIR)/combat ./
	cp $(RAN_ADV_DIR)/ran_adv ./
	cp $(DANIO_DIR)/../colours ./

endif

$(ADV_DIR)/adv_game :
	cd $(ADV_DIR) && $(MAKE)

$(CHAR_DIR)/autochar :
	cd $(CHAR_DIR) && $(MAKE)

$(COMBAT_DIR)/combat :
	cd $(COMBAT_DIR) && $(MAKE)

$(RAN_ADV_DIR)/ran_adv :
	cd $(RAN_ADV_DIR) && $(MAKE)

$(DANIO_DIR)/../colours :
	cd $(DANIO_DIR)/../ && $(MAKE) colours

ifeq ($(OS),Windows_NT)
else

# this isn't really clean FIXME
clean :
	rm $(ADV_DIR)/adv_game 
	rm $(CHAR_DIR)/autochar
	rm $(COMBAT_DIR)/combat
	rm $(RAN_ADV_DIR)/ran_adv
	rm $(DANIO_DIR)/../colours
	rm adv_game 
	rm autochar
	rm combat
	rm ran_adv
	rm colours


endif

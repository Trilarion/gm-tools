# map.mk - a map: a list of commonly used definitions with no targets
#  which can be included in make files, specifically used to specify
#  locations of commonly used source
# Daniel Vale JAN 2003

ifndef MAP_MK
MAP_MK = 1

INCLUDE_DIR := $(SOURCE_DIR)/include
STATUS_DIR := $(SOURCE_DIR)/status_line

ifndef DANIO_DIR
DANIO_DIR := $(SOURCE_DIR)/danio/cursio
endif

DANIO_BASE := $(SOURCE_DIR)/danio
PROMPT_DIR := $(SOURCE_DIR)/prompt
CHAR_DIR := $(SOURCE_DIR)/gm_tools/autochar
MENU_DIR := $(SOURCE_DIR)/menu
ADV_DIR := $(SOURCE_DIR)/gm_tools/adv_game
COMBAT_DIR := $(SOURCE_DIR)/gm_tools/combat
RAN_ADV_DIR := $(SOURCE_DIR)/gm_tools/ran_adv
LOG_DIR := $(SOURCE_DIR)/log


#default flags for g++
# -g turns on debugger
# -Wall means all g++ warnings
CC_FLAGS = -g -Wall 

# Use this as an option to gcc so that it can find header files. This 
# will also cause gcc to use the keys.h from the appropriate DANIO_DIR
# If changing interface, "touch keys.h" to remake all .o's that use it
DIR_LIST := -I$(MENU_DIR) -I$(CHAR_DIR) -I$(PROMPT_DIR) -I$(DANIO_DIR) \
            -I$(STATUS_DIR) -I$(INCLUDE_DIR) -I$(DANIO_BASE) -I$(ADV_DIR) \
	    -I$(LOG_DIR) \
            $(CC_FLAGS)
# fix me: CC_FLAGS has been included with DIR_LIST as this is allready in 		
# most of my makefiles. It should proabably be the other way around, i.e.
# DIR_LIST should be included in CC_FLAGS. Unfortunately CC_FLAGS was added 
# more recently - dan JAN 2004

# define DANIO_LINK
# this is the library linking argument for g++, for programs that use danio
# it is included here to appear before the primary target, which must use it
include $(DANIO_DIR)/link.mk

# I have mispelt / abriviated this so many times that I have decided to support 
# the abreviated version in the interests of productivity
DANIO_LNK := DANIO_LINK

# DANIO_ALL should be a dependancy for any target that uses a danio interface
DANIO_H := $(DANIO_DIR)/../danio.h $(DANIO_DIR)/../danscr.h \
           $(DANIO_DIR)/../dankbd.h $(DANIO_DIR)/../dwindow.h \
	   $(DANIO_DIR)/../dansys.h
DANIO_SRC := $(DANIO_DIR)/danio.cc $(DANIO_DIR)/danscr.cc \
             $(DANIO_DIR)/dankbd.cc $(DANIO_DIR)/dwindow.cc \
	     $(DANIO_DIR)/dansys.cc
DANIO_ALL := $(DANIO_H) $(DANIO_SRC)

DANIO_OBJS := $(DANIO_DIR)/../danio.o $(DANIO_DIR)/../danscr.o \
           $(DANIO_DIR)/../dankbd.o $(DANIO_DIR)/../dwindow.o \
	   $(DANIO_DIR)/../dansys.o


endif


/*  sat.h -- Strings and Things for formatting  	
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

#ifndef SAT_H
#define SAT_H

#include <ctype.h> 
		    
#define MAX_CHARS 1024
#define BLANK20  "                    "

char * set_field(char* to_eddit, int field_width);
char * set_field(int to_eddit, int field_width);
// creates a string of <field_width> characters containing <to_eddit>
// if to_eddit is longer than <field_width> then it will be truncated
// calling function MUST delete returned string


char* new_prn_pts(int pts);
// convers pts (in xp) to a printable (char*) character points
// dynamicly allocates char*

int is_empty(char*s);
// returns non-zero if string contains only blanks.

void ins_chars( char* s, int nchars=1 );
// inserts [nchars] characters at [*s]

void del_chars( char*, int nchars = 1 );
// deletes [nchars] characters at [*s]

char* new_str( int nchars = MAX_CHARS);
char* new_str( const char* str );
// generates a new string with runtime memory allocation
// if [param == int] {string is initalized to blanks} else {string is a copy}
// WARNING: ALLOCATES MEMORY: _DO_ *delete* IT YOURSELF

char* right_word( char* here );
// finds the left end of the word on the right
// a word contains alfa-numerics or ')'

char* left_word( char* left, char* here );
// finds the left end of the current word (if any); else
//       the left end of the word on the left
// a word contains alfa-numerics or ')'


char* debuf( char* str );
// useage: s = debuf(str)
// returns a copy of <str> without leading and trailing spaces
// WARNING str is _NOT_ *delete*d: return value is a copy.
void debuf( char** str );
// useage: debuf(&str)
// modifies <str> to remove leading spaces and trailing spaces

char* buff_str( char* str, int size = 1, char buf_char = ' ' );
// usage: newstr = buff_str(str, size, buf_char)
// WARNING str is _NOT_ *delete*d:
// returns a copy of <str> with <size> leading <buf_char>'s
//      and <size> trailing <buf_char>'s

// if you want to modify <str>: buff_str(&str)
void buff_str( char** str, int size = 1, char buf_char = ' ' );
// useage: buff_str(&str)
// <str> is modified

void str_upp( char *str );
// usage: str_upp( &str )
// converts all letters to upper case using toupper(char)

#endif
// end of SAT.H

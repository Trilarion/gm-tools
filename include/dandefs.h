/*  dandefs.h - commonly used bits for dan
 *
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires see http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

#ifndef DANDEFS_H
#define DANDEFS_H

#include <stdlib.h> // for random
#include <stdio.h>
#include <stdarg.h> // for va_list

#define YES 1
#define NO 0
#define TRUE 1 // depreciated usage use "true" and "false"
#define FALSE 0

#define ON 1
#define OFF 0
#ifndef MAX_INPUT
#define MAX_INPUT 255  // also defined in linux/limits.h included indirectly by iostream.h (same value, for now...)
#endif
#define MAX_LINE 80
#define STDIO_CARRAGE_RETURN 10

// error codes
#define SUCCESS 0 // depreciated usage
#define errNO_ERROR 0 // prefered
#define errUSER_CANCEL 1
#define errUNINITIALISED 2
#define errNO_MATCH_FOUND 3 // Required value not found
#define errINIT_FAILED 4 // Failed to achieve neccessary initialisation
#define errREDUNDANT 5 // The result sought is already satisfied
#define errINVALID 6 // Data not of correct format
#define errOUT_OF_BOUNDS 7 // Boundry condition failed / value out of range
#define errEOF 8 // end of file / file abnormally truncated / unexpected eof
#define errUSEAGE 9 // failed to interpret command line

#define BLANK_LINE "                                                                                "
//                  12345678901234567890123456789012345678901234567890123456789012345678901234567890
//                  00000000011111111112222222222333333333344444444445555555555666666666677777777778

#endif
// end - dandefs.h



/* err_log.cc - easy error logging
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

#include <stdio.h>  // this should use danio FIX ME
#include <stdarg.h>

#include "err_log.h"
#include "dandefs.h"
#include "danio.h"

FILE *err_log;   // this stuff should all be moved to danio. FIX_ME. 
                  //     WARNING: if you are trying to fix this:
                  // This has implications for backward compatablity 
                  // that need to be thought about first!
                  // This is beyond scope of the "GM Tools" project. 

int error_log_active = FALSE;
int LOG_LEVEL = logDEFAULT;

                               
int start_log()             
{ 
  error_log_active = TRUE;  
  err_log = fopen("./err.log", "wt");
  if (err_log == NULL)
    return errINIT_FAILED;
  log_event("start_log() - Log initiated\n");
  return errNO_ERROR;
}                           
                
void end_log()           
{               
  log_event("end_log() - Log closed\n");   
  error_log_active = FALSE;
  fclose(err_log);      
}                 
            
void suspend_log()      
// suspend writing of log entries, ignoring log_event calls
{                 
  log_event("log suspended\n");  
  error_log_active = FALSE;
}                 
                  
void resume_log()       
// resume writing of log entries, after a suspend_log
{                    
  error_log_active = TRUE;
  log_event("log resumed\n"); 
}// resume log       
            
void log_event(const int i)
{          
  if (error_log_active)
  {
    fprintf(err_log, "%d", i); 
    fflush(err_log);
  }// fi
}// log event
              
void log_event(const char *fmt, ...)
{       
  va_list ap;


  //  printf("log_event()\n");  // debug
  if (error_log_active)
  {
    va_start(ap, fmt);
    //printf("log_event() - va_start() ok\n"); // debug
    vfprintf(err_log, fmt, ap);
    //vprintf(fmt, ap); // debug
    //printf("log_event() - vfprintf() ok\n"); // debug
    va_end(ap);
    //printf("log_event() - va_end() ok\n"); // debug 
    fflush(err_log);
    //printf("log_event() - fflush() ok\n"); // debug
  }// fi
  //printf("log_event() - end\n"); // debug
} // log event
    
void vlog_event(const char *fmt, va_list ap)
{            
  if (error_log_active)
  {
    vfprintf(err_log, fmt, ap);
    fflush(err_log);
  }// fi
  
} // log event

int log_event(const int log_level, const char *fmt, ...)
  // make a log entry - allows formatted output like printf
  // log_level is the priority of the loged event, see below
{
  va_list ap;

  if (error_log_active && log_level <= LOG_LEVEL)
  {
    va_start(ap, fmt);
    vfprintf(err_log, fmt, ap);
    va_end(ap);
    fflush(err_log);
    return errNO_ERROR;
  }// fi
  return errREDUNDANT;
}// log event - with priority control

int vlog_event(const int log_level, const char *fmt, va_list ap)
  // make a log entry - allows passing on of variable argument lists
  // log_level is the priority of the loged event, see below
{
  if (error_log_active && log_level <= LOG_LEVEL)
  {
    vfprintf(err_log, fmt, ap);
    fflush(err_log);
    return errNO_ERROR;
  }// fi
  return errREDUNDANT;
} // vlog event - with priority control

int start_log(int log_level)
  // open the log file, set the log level
{
  LOG_LEVEL = log_level;
  return start_log();
}// start log, setting log level
  
void set_log_level(int log_level)
  // change the log level  
{
  LOG_LEVEL = log_level;
}  
  
int get_log_level()
  // returns the current log level
{
  return LOG_LEVEL;  
}// get log level.  


void log_fatal_event(int err, char *s, ...)
  // log a fatal error and call exit. This function will not return!
{
  va_list ap;                           
                                
  va_start(ap, s); 
  vlog_event(s, ap);
  va_end(ap);                   

  end_log();
  close_danscr();

  exit(err);
}// log fatal error

void vlog_fatal_event(int err, char *s, va_list ap)
  // log a fatal error and call exit. This function will not return!
{
  vlog_event(s, ap);
  end_log();
  close_danscr();
  exit(err);
}// vlog fatal error



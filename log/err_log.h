/* err_log.h - easy error loging
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */
 
 #ifndef ERR_LOG_H
 #define ERR_LOG_H

#include <stdio.h> // should use danio FIXME
#include <stdarg.h>

extern FILE *err_log;
extern int error_log_active;

const int logNONE = 0;
const int logERROR = 1;
const int logWARNING = 2;
const int logCONTROL = 3;
const int logDEFAULT = 4;
const int logALL = 5;

int start_log();
  // open the log file - depreciated, specify a log level.

int start_log(int log_level);
  // open the log file, set the log level
  
void set_log_level(int log_level);
  // change the log level  
  
int get_log_level();
  // returns the current log level
  
  
void end_log();   
  // close the log file

void log_event(const int i);    // depreciated, use of formatted output is prefferable
  // make a log entry

void log_event(const char *fmt, ...);
  // make a log entry - allows formatted output like printf

void vlog_event(const char *fmt, va_list ap);
  // make a log entry - allows passing on of variable argument lists

int log_event(const int log_level, const char *fmt, ...);
  // make a log entry - allows formatted output like printf
  // log_level is the priority of the loged event, see below
  // returns errNO_ERROR (0) if event was logged or errREDUNDANT if log_level <= current loging level

int vlog_event(const int log_level, const char *fmt, va_list ap);
  // make a log entry - allows passing on of variable argument lists
  // log_level is the priority of the loged event, see below
  // returns errNO_ERROR (0) if event was logged or errREDUNDANT if log_level <= current loging level

void suspend_log();
  // suspend writing of log entries, ignoring log_event calls

void resume_log();
  // resume writing of log entries, after a suspend_log

void log_fatal_event(int err, char* format, ...);
  // log a fatal error and call exit. This function will not return!

void vlog_fatal_event(int err, char* format, va_list ap);
  // log a fatal error and call exit. This function will not return!

/* NOTE: Log levels
 * 0: logging off: It is an bug for a log event to have a priority of 0 or less.
 * 1: Fatal errors only: Only fatal errors are to be reported at this level.
 * 2: Warnings / non-fatal errors: runtime detected error conditions that are recoverable are reported at this level.
 * 3: High level flow control: Changes to flags or events that direct overal program execution. Initialisation options.
 * 4: Program Flow: Report entry to significant program functions or significant decisions. Report user input. Normal level for debugging.
 * 5: All. Log all events that can be logged.
 */
 
 
 #endif
 
